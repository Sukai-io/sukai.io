import React from 'react'
import { Image, ImageSourcePropType } from 'react-native'
import { Card, Text } from 'native-base'
import styles from './FeatureItem.styles'
import FlatTouchables from '../../../components/buttons/FlatTouchables'

type Props = {
  name: String,
  icon: ImageSourcePropType,
  onPress: Function,
}

const FeatureItem = ({ name, icon, onPress }: Props) => (
  <FlatTouchables onPress={onPress}>
    <Card style={styles.container}>
      <Image style={styles.icon} source={icon} />
      <Text style={styles.title}>{name}</Text>
    </Card>
  </FlatTouchables>
)

export default FeatureItem
