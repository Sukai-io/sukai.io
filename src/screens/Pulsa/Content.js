/* eslint-disable max-len */
import React, {Component} from 'react';
import {View, Switch, ActivityIndicator} from 'react-native';
import {debounce} from 'lodash';
import {connect} from 'react-redux';
import numeral from 'numeral';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import {Container, Icon, Item, Label, Input, Text} from 'native-base';
import {Button} from 'react-native-elements';
import FastImage from 'react-native-fast-image';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {FormikProps} from 'formik';
import styles from './styles';
import HeaderBack from '../../components/header/HeaderBack';
import Horizontal from '../../components/wrapper/Horizontal';

import {ApplicationStyles} from '../../themes';
import PulsaItem from './components/PulsaItem';
import VerticalList from '../../components/list/VerticalList';
import PaymentModal from '../Checkout/components/PaymentModal';
import TotalItem from '../Electricity/components/TotalItem';
// import NoteItem from './components/NoteItem'
import {FormValues} from './types';

type Props = FormikProps<FormValues> & {
  loading: Boolean,
  loadingCheckout: Boolean,
  remember: Boolean,
  providerPriceList: Array<Object>,
  doPriceList: Function,
  doClear: Function,
  doSaveRemember: Function,
};

type State = {
  showPayment: Boolean,
  providerType: String,
};

class Content extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      showPayment: false,
      providerType: '',
    };
    this.handleInputChanged = debounce(this.onChangeText, 300);
  }

  providerName = {
    TELKOMSEL: 'Telkomsel',
    INDOSAT: 'Indosat',
    XL: 'XL',
    THREE: 'Three',
    SMART: 'Smart',
    AXIS: 'AXIS',
    CERIA: 'Ceria',
  };

  componentDidMount() {
    const {doClear, values} = this.props;
    doClear();
    if (values.ppob_number !== '') {
      this.onChangeText(values.ppob_number);
    }
  }

  handlePaymentVisibility = value => {
    this.setState({showPayment: value});
  };

  renderProviderIcon = () => {
    const {providerType} = this.state;
    const {
      TELKOMSEL,
      AXIS,
      CERIA,
      INDOSAT,
      SMART,
      THREE,
      XL,
    } = this.providerName;
    switch (providerType) {
      case TELKOMSEL:
        return 'https://gadgetren.com/wp-content/uploads/2017/05/Telkomsel-Logo-Header.jpg';
      case INDOSAT:
        return 'https://cdns.klimg.com/merdeka.com/i/w/news/2018/03/28/956735/670x335/pelanggan-indosat-bisa-lepas-dari-jeratan-tarif-im3-ooredoo-prime.png';
      case THREE:
        return 'https://www.kanalkalimantan.com/wp-content/uploads/2018/12/Tri-3.jpg';
      case AXIS:
        return 'https://cdns.klimg.com/newshub.id/news/2016/05/03/58424/664xauto-pelanggan-axis-bakalan-segera-cicipi-indahnya-layanan-4g-160503w.jpg';
      case XL:
        return 'https://upload.wikimedia.org/wikipedia/id/thumb/5/55/XL_logo_2016.svg/200px-XL_logo_2016.svg.png';
      case SMART:
        return 'https://cdns.klimg.com/merdeka.com/i/w/news/2017/01/26/805463/670x335/di-luar-jawa-100-persen-pengguna-smartfren-pakai-layanan-4g.png';
      case CERIA:
        return 'https://upload.wikimedia.org/wikipedia/id/thumb/1/1c/Ceria.svg/1200px-Ceria.svg.png';
      default:
        return '';
    }
  };

  onChangeText = (value: String) => {
    const {setFieldValue} = this.props;
    const {
      TELKOMSEL,
      AXIS,
      CERIA,
      INDOSAT,
      SMART,
      THREE,
      XL,
    } = this.providerName;
    setFieldValue('ppob_number', value);
    const prefix = parseInt(value.substring(0, 3), 0);
    if (
      (prefix >= 811 && prefix <= 813) ||
      (prefix >= 821 && prefix <= 823) ||
      (prefix >= 851 && prefix <= 853)
    ) {
      this.handleProviderChange(TELKOMSEL);
    } else if (
      (prefix >= 814 && prefix <= 816) ||
      (prefix >= 855 && prefix <= 858)
    ) {
      this.handleProviderChange(INDOSAT);
    } else if (
      (prefix >= 817 && prefix <= 819) ||
      prefix === 859 ||
      prefix === 877 ||
      prefix === 878
    ) {
      this.handleProviderChange(XL);
    } else if (prefix >= 895 && prefix <= 899) {
      this.handleProviderChange(THREE);
    } else if (prefix >= 881 && prefix <= 889) {
      this.handleProviderChange(SMART);
    } else if ((prefix >= 831 && prefix <= 833) || prefix === 838) {
      this.handleProviderChange(AXIS);
    } else if (prefix == 828) {
      this.handleProviderChange(CERIA);
    } else {
      this.handleProviderChange('');
    }
  };

  handleProviderChange = providerType => {
    this.setState({providerType}, () => {
      const {doPriceList, setFieldValue} = this.props;
      if (providerType !== '') {
        setFieldValue('ppob_provider', providerType);
        doPriceList({provider: providerType});
      }
    });
  };

  render() {
    const {showPayment, providerType} = this.state;
    const {
      loadingCheckout,
      loading,
      remember,
      handleSubmit,
      errors,
      touched,
      values,
      setFieldTouched,
      setFieldValue,
      providerPriceList,
      doSaveRemember,
    } = this.props;
    const totalPrice =
      providerPriceList.length > 0
        ? providerPriceList[values.price_index].price
        : null;
    return (
      <Container style={styles.mainContainer}>
        <PaymentModal
          isVisible={showPayment}
          onBackButtonPress={() => this.handlePaymentVisibility(false)}
          onBackdropPress={() => this.handlePaymentVisibility(false)}
        />
        <HeaderBack withoutRight title="Pulsa" />
        <KeyboardAwareScrollView
          style={styles.container}
          keyboardShouldPersistTaps="always">
          <View style={styles.firstSubContainer}>
            <Text style={styles.titleText}>Nomor Handphone</Text>
            <Horizontal style={ApplicationStyles.flexCenter}>
              {providerType !== '' ? (
                <FastImage
                  source={{uri: this.renderProviderIcon()}}
                  style={styles.iconProvider}
                  resizeMode="contain"
                />
              ) : (
                <Icon
                  name="wifi"
                  type="MaterialCommunityIcons"
                  style={styles.icon}
                />
              )}
              <Item inlineLabel style={styles.inputContainer}>
                <Label>+62</Label>
                <Input
                  onChangeText={this.handleInputChanged}
                  keyboardType="number-pad"
                  onBlur={() => setFieldTouched('ppob_number')}
                  defaultValue={values.ppob_number}
                />
              </Item>
            </Horizontal>
            {errors.ppob_number && touched.ppob_number && (
              <Text style={styles.error}>{errors.ppob_number}</Text>
            )}
          </View>
          <View style={styles.secondSubContainer}>
            <Horizontal style={styles.thirdSubContainer}>
              <Text style={styles.description}>
                Simpan nomor ini untuk transaksi berikutnya
              </Text>
              <Switch
                value={remember}
                onValueChange={() => doSaveRemember('pulsa')}
              />
            </Horizontal>
            {loading ? (
              <ActivityIndicator />
            ) : (
              <VerticalList
                data={providerPriceList}
                scrollEnabled={false}
                renderItem={({item, index}) => {
                  return (
                    <PulsaItem
                      onPress={() => setFieldValue('price_index', index)}
                      price={item.name}
                      salePrice={item.price}
                      selected={values.price_index === index}
                    />
                  );
                }}
              />
            )}
          </View>
          {/* <NoteItem /> */}
          {totalPrice && (
            <TotalItem total={numeral(totalPrice).format('0,0.00')} />
          )}
          <Button
            loading={loadingCheckout}
            disabled={loadingCheckout}
            onPress={handleSubmit}
            raised
            containerStyle={styles.buttonContainer}
            buttonStyle={styles.button}
            title="BELI"
          />
        </KeyboardAwareScrollView>
      </Container>
    );
  }
}

const mapStateToProps = ({loading, ppob}) => {
  return {
    loading:
      loading.effects.ppob.getProvider || loading.effects.ppob.getProviderPrice,
    loadingCheckout: loading.effects.ppob.doCheckout,
    remember: ppob.remember_pulsa,
    providerPriceList: ppob.provider_price_list,
  };
};

const mapDispatchToProps = ({ppob}) => {
  return {
    doPriceList: data => ppob.getProviderPrice(data),
    doClear: () => ppob.clear(),
    doSaveRemember: type => ppob.saveRemember(type),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Content);
