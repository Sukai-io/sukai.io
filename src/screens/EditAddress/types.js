export type FormValues = {
  address_id: String,
  address_type: String,
  address_location: String,
  address_district: String,
  address_city: String,
  address_city_name: String,
  address_province: String,
  address_province_name: String,
  address_zipcode: String,
};
