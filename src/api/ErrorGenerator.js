import {Alert} from 'react-native';
import {showToast} from '../utils/ToastUtils';

const errorGenerator = (e, message = null) => {
  if (message) {
    Alert.alert('An error occured', message);
  } else if (e.problem === 'CLIENT_ERROR') {
    Alert.alert('CLIENT_ERROR', 'Please check your form again');
  } else if (e.problem === 'SERVER_ERROR') {
    Alert.alert('SERVER_ERROR', 'There is a problem with the server, please try again later.');
  } else if (e.problem === 'TIMEOUT_ERROR') {
    showToast('Timeout, please check your connections.');
  } else if (e.problem === 'CONNECTION_ERROR') {
    Alert.alert('CONNECTION_ERROR', 'Please check your connections.');
  } else if (e.problem === 'NETWORK_ERROR') {
    Alert.alert('NETWORK_ERROR', 'Please check your connections.');
  } else if (e.problem === 'CANCEL_ERROR') {
    Alert.alert('CANCEL_ERROR', 'Your request has been canceled');
  } else {
    Alert.alert('An error occured', 'Please try again later..');
  }
};

export default errorGenerator;
