import R from 'ramda';
import api from '../ApiClient';
import {transformToFormData} from '../ApiHelper';
import {showToast} from '../../utils/ToastUtils';
import errorGenerator from '../ErrorGenerator';
import {goBack} from '../../utils/NavigationUtils';

const getSuccess = data => R.pathOr(false, ['success'], data);

const adHolder = {
  image_thumbnail: 'https://via.placeholder.com/200x200.png?text=Smart+Adv+Banner',
  title: 'Smart adv title',
};

export default {
  state: {
    homeAds: [...Array(5).keys()].map(item => {
      return adHolder;
    }),
    allAds: [...Array(6).keys()].map(item => {
      return adHolder;
    }),
    focusedAdId: '0',
    detailAds: {},
  },
  reducers: {
    setHomeAds(state, data) {
      let ads = data?.data?.advert_list;
      return {...state, homeAds: ads};
    },
    _emptyAllAds(state) {
      return {...state, allAds: []};
    },
    setAllAds(state, data) {
      let ads = data?.data?.advert_list;
      return {...state, allAds: ads};
    },
    setAdInsights(state, data) {
      return {...state, adInsights: data?.data?.advert_report};
    },
    setMyAds(state, data) {
      return {...state, myAds: data?.data?.advert_list};
    },
    setMyAd(state, data) {
      return {...state, myAd: data?.data?.advert_list?.[0]};
    },
    setFocusedAdId(state, data) {
      return {...state, focusedAdId: data};
    },
    setDetailAds(state, data) {
      return {...state, detailAds: data};
    },
  },
  selectors: {
    getFocusedAdId() {
      return rootState => rootState.smartadv.focusedAdId;
    },
  },
  effects: dispatch => ({
    async getHomeAds() {
      await dispatch.smartadv.setMyAd({advert_list: [null]});
      const response = await api.listAds({
        type: 'multi',
        limit: 5,
        offset: 0,
      });
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.smartadv.setHomeAds(response.data);
        return Promise.resolve(response.data);
      }
    },
    async getAllAds(offset) {
      const response = await api.listAds({
        type: 'multi',
        limit: 100,
        offset,
      });
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.smartadv.setAllAds(response.data);
        return Promise.resolve(response.data);
      }
    },
    async getMyAds() {
      await dispatch.smartadv.setMyAd({advert_list: [null]});
      const response = await api.listAds({
        type: 'mine',
        limit: 10,
        offset: 0,
      });
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.smartadv.setMyAds(response.data);
        return Promise.resolve(response.data);
      } else {
        await dispatch.smartadv.setMyAds({data: {advert_list: []}});
      }
    },
    async getDetailAds(params) {
      const response = await api.detailAds(params);
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.smartadv.setDetailAds(response.data);
        return Promise.resolve(response.data);
      }
    },
    async getAdInsights(data) {
      const response = await api.adInsight(transformToFormData(data));
      console.log({response});
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.smartadv.setAdInsights(response.data);
        return Promise.resolve(response.data);
      }
    },
    async createAd(data) {
      try {
        await dispatch.smartadv.setMyAd({advert_list: [null]});
        const response = await api.createAd(data);
        const success = getSuccess(response.data);
        if (response.ok && success) {
          await dispatch.smartadv.getMyAds();
          await dispatch.smartadv.getHomeAds();
          showToast('Ads created successfully');
          goBack();
          return Promise.resolve(response.data);
        }
        errorGenerator(response, !success ? response.data?.data?.error : null);
        return Promise.reject(response);
      } catch (err) {
        return Promise.reject();
      }
    },
    async claimAdBonus(advert_id) {
      try {
        const response = await api.claimAdBonus({advert_id});
        const success = getSuccess(response.data);
        if (response.ok && success) {
          showToast('Kamu mendapat bonus sebesar +500');
          await dispatch.smartadv.getHomeAds();
          await dispatch.balance.getBalance();
          return Promise.resolve(response.data);
        }
        errorGenerator(response, !success ? response.data?.data?.error : null);
        return Promise.reject(response);
      } catch (err) {}
    },
    async editAd(data) {
      try {
        await dispatch.smartadv.setMyAd({advert_list: [null]});
        const response = await api.editAd(transformToFormData(data));
        const success = getSuccess(response.data);
        if (response.ok && success) {
          await dispatch.smartadv.getMyAds();
          await dispatch.smartadv.getHomeAds();
          showToast('Perubahan iklan berhasil disimpan');
          goBack();
          return Promise.resolve(response.data);
        }
        errorGenerator(response, !success ? response.data?.data?.error : null);
        return Promise.reject(response);
      } catch (err) {}
    },
    async adTopup(data) {
      try {
        const response = await api.adTopup(transformToFormData(data));
        const success = getSuccess(response.data);
        if (response.ok && success) {
          showToast('Berhasil menambah budget iklan');
          goBack();
          await dispatch.smartadv.getMyAds();
          await dispatch.smartadv.getHomeAds();
          await dispatch.balance.getBalance();
          return Promise.resolve(response.data);
        }
        errorGenerator(response, !success ? response.data?.data?.error : null);
        return Promise.reject(response);
      } catch (err) {}
    },
    async getAd(params) {
      try {
        const response = await api.listAds(params);
        const success = getSuccess(response.data);
        if (response.ok && success) {
          await dispatch.smartadv.setMyAd(response.data);
          return Promise.resolve(response.data);
        }
        errorGenerator(response, !success ? response.data?.data?.error : null);
        return Promise.reject(response);
      } catch (err) {}
    },
    async emptyAd() {
      await dispatch.smartadv.setMyAd({advert_list: [null]});
    },
    async emptyAllAds() {
      await dispatch.smartadv._emptyAllAds();
    },
    async updateFocusedAdId(id) {
      await dispatch.smartadv.setFocusedAdId(id);
    },
    async updateAdStatus(data) {
      try {
        const response = await api.updateAdStatus(transformToFormData(data));
        const success = getSuccess(response.data);
        if (response.ok && success) {
          showToast('Berhasil mengubah status iklan : ' + (data.advert_status ? 'Active' : 'Inactive'));
          await dispatch.smartadv.getMyAds();
          await dispatch.smartadv.getHomeAds();
          await dispatch.smartadv.getAd({
            id,
            type: 'mine',
            limit: 1,
            offset: 0,
          });
          return Promise.resolve(response.data);
        }
        errorGenerator(response, !success ? response.data?.data?.error : null);
        return Promise.reject(response);
      } catch (err) {}
    },
  }),
};
