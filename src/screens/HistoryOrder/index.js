import React from 'react';
import {Container, Tabs, Tab} from 'native-base';
import styles from './styles';
import HeaderBack from '../../components/header/HeaderBack';
import Buyer from './Buyer';
import Seller from './Seller';

const HistoryOrder = props => (
  <Container style={styles.mainContainer}>
    <HeaderBack hasTabs title="History Order" withoutRight />
    <Tabs>
      <Tab textStyle={styles.tabText} activeTextStyle={styles.activeTabText} heading="As Buyer">
        <Buyer {...props} />
      </Tab>
      <Tab textStyle={styles.tabText} activeTextStyle={styles.activeTabText} heading="As Seller">
        <Seller {...props} />
      </Tab>
    </Tabs>
  </Container>
);

export default HistoryOrder;
