import React from 'react'
import { View, KeyboardAvoidingView, ScrollView } from 'react-native'
import Modal, { ModalProps } from 'react-native-modal'
import { Item, Input, Text } from 'native-base'
import { Button } from 'react-native-elements'
import styles from './PaymentModal.styles'
import { Colors } from '../../../themes'

type Props = ModalProps

const PaymentModal = (props: Props) => (
  <Modal style={styles.modal} hideModalContentWhileAnimating useNativeDriver {...props}>
    <View style={styles.container}>
      <ScrollView>
        <KeyboardAvoidingView behavior="position">
          <View style={styles.subContainer}>
            <Text style={styles.title}>Konfirmasi</Text>
            <Text style={styles.subtitle}>Saldo Sukai-Wallet Anda akan otomatis berkurang</Text>
            <Text style={styles.marginTop}>
              <Text style={styles.subtitle}>Silahkan masukkan </Text>
              <Text style={styles.subtitleBold}>Password Anda </Text>
              <Text style={styles.subtitle}>dibawah ini untuk konfirmasi pembayaran</Text>
            </Text>
            <Item>
              <Input
                placeholderTextColor={Colors.gray500}
                placeholder="Masukkan Password"
                secureTextEntry
                style={styles.input}
              />
            </Item>
            <Button
              raised
              containerStyle={styles.buttonContainer}
              buttonStyle={styles.button}
              title="LANJUTKAN"
            />
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  </Modal>
)

export default PaymentModal
