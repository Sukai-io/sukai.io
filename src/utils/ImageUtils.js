import * as ImageResizer from 'react-native-image-resizer';

export const imageResizer = (uriItem: string) => {
  return ImageResizer.default
    .createResizedImage(uriItem, 1024, 768, 'JPEG', 100)
    .then(({uri, size}) => {
      console.log(size);
      return uri;
    })
    .catch(err => {
      console.log(err);
      alert('Unable to resize the photo');
    });
};

export const normalizedSource = source =>
  source && typeof source.uri === 'string' && !source.uri.split('http')[1]
    ? null
    : source;
