import * as React from 'react'
import { View, TouchableWithoutFeedback } from 'react-native'
import { Input, InputProps } from 'react-native-elements'
import { Text, Icon } from 'native-base'
import styles from './styles/AuthInput.styles'
import { Colors } from '../../themes'

type Props = InputProps & {
  name: string,
  isMarginTop: boolean,
  hasEye: boolean,
  onEyePress?: Function,
  error: String,
}

const AuthInput = (props: Props) => {
  const {
    name, isMarginTop, error, hasEye, secureTextEntry, onEyePress,
  } = props
  return (
    <View>
      <Input
        {...props}
        leftIconContainerStyle={styles.leftIcon}
        placeholderTextColor={Colors.primary}
        containerStyle={[styles.item, isMarginTop && styles.margin]}
        inputStyle={styles.input}
        errorStyle={styles.error}
        inputContainerStyle={{ borderBottomWidth: 0 }}
        leftIcon={<Icon name={name} style={styles.icon} type="MaterialCommunityIcons" />}
        rightIcon={
          hasEye ? (
            <TouchableWithoutFeedback onPress={onEyePress}>
              <Icon
                name={secureTextEntry ? 'eye' : 'eye-off'}
                style={styles.icon}
                type="MaterialCommunityIcons"
              />
            </TouchableWithoutFeedback>
          ) : (
            undefined
          )
        }
      />
      {error && <Text style={styles.error}>{error}</Text>}
    </View>
  )
}

AuthInput.defaultProps = {
  onEyePress: null,
}

export default AuthInput
