import { StyleSheet } from 'react-native';
import { Fonts, Colors, Metrics } from '../../themes';

export default StyleSheet.create({
  container: {
    height: 65
  },
  text: {
    fontSize: Fonts.size.h6,
    fontFamily: Fonts.type.bold,
    color: Colors.snow,
  },
  subtitle: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.bold,
    color: Colors.gray900,
  },
  custom: {
    fontSize: 24,
    color: 'white',
  },
  cart: {
    fontSize: Metrics.icons.medium,
    color: 'white',
  },
  badge: {
    backgroundColor: Colors.bloodOrange,
  },
  imageContainer: {
    height: 40,
    width: 40,
    borderRadius: 40/2,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 2,
    borderColor: Colors.snow,
    backgroundColor: Colors.gray200,
  }
});
