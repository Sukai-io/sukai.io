import React, {useState, useCallback, useMemo, useContext} from 'react';
import {View} from 'react-native';
import {connect} from 'react-redux';

// Styles
import {Container, Content, Text, Body, CardItem} from 'native-base';
import {Dropdown} from 'react-native-material-dropdown';
import {Button, Card} from 'react-native-elements';
import styles from './styles';
import HeaderBack from '../../components/header/HeaderBack';
import {ApplicationStyles, Colors, Fonts} from '../../themes';
import Horizontal from '../../components/wrapper/Horizontal';
import CartItem from './components/CartItem';
import AddAddressModal from './components/AddAddressModal';
import {goTo} from '../../utils/NavigationUtils';
import {useFormik} from 'formik';
import VerticalList from '../../components/list/VerticalList';
import validationSchema from './validationSchema';
import {path} from 'ramda';
import store from '../../config/CreateStore';
import ApiClient from '../../api/ApiClient';
import {transformToFormData} from '../../api/ApiHelper';
import {numeralFormatter} from '../../utils/TextUtils';
import { LocalizationContext } from '../../localization';

const shipping = [
  {
    label: 'JNE',
    value: 'jne',
  },
  {
    label: 'TIKI',
    value: 'tiki',
  },
];

const CheckoutContent = ({onSubmit, selectedAddress, cartsCheckout}) => {
  const {translations} = useContext(LocalizationContext);
  const [showAddress, setAddressVisibility] = useState(false);
  const {values, errors, touched, setFieldValue, handleSubmit} = useFormik({
    initialValues: {
      product: cartsCheckout,
    },
    enableReinitialize: true,
    validationSchema,
    onSubmit,
  });
  const isAllShippingFeeFilled = useMemo(() => values.product.filter(item => item.service === '')?.length === 0, [
    values,
  ]);
  const totalCartQty = useMemo(
    () => values.product?.reduce((acc, item) => acc + item.products.reduce((acc, item) => acc + item.cart_qty, 0), 0),
    [values],
  );
  const totalShippingFee = useMemo(
    () => values.product?.reduce((acc, item) => acc + (item.service !== '' ? parseInt(item.service, 10) : 0), 0),
    [values],
  );
  const totalProductPrice = useMemo(
    () =>
      values.product?.reduce(
        (acc, item) => acc + item.products.reduce((acc, item) => acc + parseInt(item.cart_price, 10), 0),
        0,
      ),
    [values],
  );
  const totalProductAndShippingPrice = useMemo(() => totalShippingFee + totalProductPrice, [
    totalShippingFee,
    totalProductPrice,
  ]);

  const handleShippingChange = useCallback(
    async (value, idMember) => {
      await handleCheckShippingFee(value, idMember);
    },
    [values, setFieldValue],
  );

  const handleServiceChange = useCallback(
    async (value, idMember) => {
      await setFieldValue(
        'product',
        values.product?.map(itemProduct => ({
          ...itemProduct,
          service: itemProduct?.id_member === idMember ? value : itemProduct.service,
        })),
      );
    },
    [values, setFieldValue],
  );

  const handleCheckShippingFee = useCallback(
    async (value, idMember) => {
      const product = values.product?.find(item => item.id_member === idMember);
      const totalWeight = product?.products?.reduce(
        (acc, item) => acc + item.cart_qty * parseInt(item.product_weight, 10),
        0,
      );
      const response = await ApiClient.checkShippingFee(
        transformToFormData({
          origin_id: parseInt(selectedAddress?.address_city, 10),
          destination_id: parseInt(product?.seller_city, 10),
          weight_gram: totalWeight,
          courier: value,
        }),
      );
      const success = response.data?.success;
      if (response.ok && success) {
        setFieldValue(
          'product',
          values.product?.map(itemProduct => ({
            ...itemProduct,
            service_options:
              itemProduct?.id_member === idMember
                ? response.data?.data?.costs?.map(item => ({
                    label: item.service,
                    value: path(['cost', 0, 'value'], item),
                  })) || []
                : itemProduct.service_options,
            courier: itemProduct?.id_member === idMember ? value : itemProduct.courier,
            service: itemProduct?.id_member === idMember ? '' : itemProduct.service,
          })),
        );
      }
    },
    [values, selectedAddress, setFieldValue],
  );

  const handleNoteChange = useCallback(
    async (value, idMember, idProduct) => {
      await setFieldValue(
        'product',
        values.product?.map(item => ({
          ...item,
          products:
            item?.id_member === idMember
              ? item.products.map(item => ({...item, note: item.id === idProduct ? value : item.note}))
              : item.products,
        })),
      );
    },
    [values, setFieldValue],
  );

  return (
    <Container style={styles.mainContainer}>
      <HeaderBack title={translations.ACTION_CHECKOUT} withoutRight />
      <AddAddressModal
        isVisible={showAddress}
        onBackButtonPress={() => setAddressVisibility(false)}
        onBackdropPress={() => setAddressVisibility(false)}
      />
      <Content style={styles.container}>
        <Card containerStyle={styles.cardContainer}>
          <CardItem>
            <Body>
              <Text style={styles.title}>{translations.LABEL_ADDRESS_SHIPPING_INFO}</Text>
            </Body>
          </CardItem>
          {selectedAddress ? (
            <CardItem>
              <Body>
                <Text>{selectedAddress?.address_type}</Text>
                <Text note>{selectedAddress?.address_location}</Text>
                <Text
                  note>{`${selectedAddress?.address_district}, ${selectedAddress?.address_city_name}, ${selectedAddress?.address_province_name}`}</Text>
                <Text note>{selectedAddress?.address_zipcode}</Text>
                <Button
                  onPress={() => goTo('AddressListScreen', {isEdit: true})}
                  raised
                  containerStyle={styles.buttonImageContainer}
                  buttonStyle={styles.buttonEdit}
                  titleStyle={styles.buttonTitle}
                  icon={{
                    name: 'ios-create',
                    type: 'ionicon',
                    color: 'white',
                    size: 20,
                  }}
                  title={translations.ACTION_EDIT}
                />
              </Body>
            </CardItem>
          ) : (
            <CardItem>
              <Button
                onPress={() => goTo('AddressListScreen', {isEdit: true})}
                raised
                containerStyle={styles.buttonImageContainer}
                buttonStyle={styles.buttonImage}
                titleStyle={styles.buttonTitle}
                icon={{
                  name: 'ios-pin',
                  type: 'ionicon',
                  color: 'white',
                  size: 20,
                }}
                title={translations.ACTION_SET_ADDRESS}
              />
            </CardItem>
          )}
        </Card>
        {selectedAddress && (
          <>
            <VerticalList
              data={values.product}
              scrollEnabled={false}
              renderItem={({item, index}) => (
                <Card containerStyle={styles.cardContainer}>
                  <CardItem>
                    <Body>
                      <Text style={styles.title}>{item.seller}</Text>
                      <Text style={styles.subtitle}>{item.seller_city_name}</Text>
                    </Body>
                  </CardItem>
                  <CardItem>
                    <Body>
                      {item.products?.map((product, index) => (
                        <CartItem
                          key={index.toString()}
                          dataSource={product}
                          isLast
                          onNoteChange={value => handleNoteChange(value, item.id_member, product.id)}
                        />
                      ))}
                    </Body>
                  </CardItem>
                  <CardItem>
                    <View style={ApplicationStyles.flex}>
                      <Dropdown
                        onChangeText={value => handleShippingChange(value, item.id_member)}
                        containerStyle={ApplicationStyles.flex}
                        label={translations.LABEL_CHOOSE_DELIVERY_SERVICE}
                        data={shipping}
                        dropdownOffset={styles.pickerOffset}
                        itemColor={Colors.gray600}
                        fontSize={Fonts.size.medium}
                        textColor={Colors.primary}
                        baseColor={Colors.gray600}
                        itemTextStyle={{
                          fontFamily: Fonts.type.base,
                          fontSize: Fonts.size.medium,
                        }}
                        labelTextStyle={{
                          fontFamily: Fonts.type.base,
                          fontSize: Fonts.size.medium,
                        }}
                        style={{
                          fontFamily: Fonts.type.base,
                          fontSize: Fonts.size.medium,
                        }}
                        value={item.courier}
                      />
                      {path(['product', index, 'courier'], touched) && path(['product', index, 'courier'], errors) && (
                        <Text style={styles.error}>{path(['product', index, 'courier'], errors)}</Text>
                      )}
                    </View>
                  </CardItem>
                  <CardItem>
                    <View style={ApplicationStyles.flex}>
                      <Dropdown
                        onChangeText={value => handleServiceChange(value, item.id_member)}
                        containerStyle={ApplicationStyles.flex}
                        label={translations.LABEL_CHOOSE_SERVICE}
                        data={item.service_options}
                        dropdownOffset={styles.pickerOffset}
                        itemColor={Colors.gray600}
                        fontSize={Fonts.size.medium}
                        textColor={Colors.primary}
                        baseColor={Colors.gray600}
                        itemTextStyle={{
                          fontFamily: Fonts.type.base,
                          fontSize: Fonts.size.medium,
                        }}
                        labelTextStyle={{
                          fontFamily: Fonts.type.base,
                          fontSize: Fonts.size.medium,
                        }}
                        style={{
                          fontFamily: Fonts.type.base,
                          fontSize: Fonts.size.medium,
                        }}
                        value={item.service}
                      />
                      {path(['product', index, 'service'], touched) && path(['product', index, 'service'], errors) && (
                        <Text style={styles.error}>{path(['product', index, 'service'], errors)}</Text>
                      )}
                    </View>
                  </CardItem>
                  {item.courier !== '' && item.service !== '' && (
                    <CardItem>
                      <Body>
                        <Horizontal style={styles.flex}>
                          <Text style={styles.infoCourier}>{translations.LABEL_COURIER_INFO}</Text>

                          <View style={styles.flex}>
                            <Text style={styles.shipping}>{`${shipping.find(o => o.value === item.courier)?.label} - ${
                              item.service_options.find(o => o.value === item.service)?.label
                            }`}</Text>
                            <Text style={styles.subTotalShipping}>{numeralFormatter(item.service)}</Text>
                          </View>
                        </Horizontal>
                      </Body>
                    </CardItem>
                  )}
                </Card>
              )}
            />
            {isAllShippingFeeFilled && (
              <>
                <Card containerStyle={styles.cardContainer}>
                  <CardItem>
                    <Body>
                      <Horizontal style={ApplicationStyles.flexCenter}>
                        <Text style={styles.totalLeft}>{translations.LABEL_DELIVERY_COST}</Text>
                        <Text style={styles.totalRight}>{numeralFormatter(totalShippingFee)}</Text>
                      </Horizontal>
                      <Horizontal style={styles.grandContainer}>
                        <Text style={styles.totalLeft}>{translations.LABEL_TOTAL_PRICE_ITEM_WITH_BALANCE.replace('$qty', totalCartQty)}</Text>
                        <Text style={styles.totalRight}>{numeralFormatter(totalProductPrice)}</Text>
                      </Horizontal>
                      <Horizontal style={styles.grandContainer}>
                        <Text style={[styles.grantTotal, {textAlign: 'left'}]}>{translations.LABEL_TOTAL_PAYMENT}</Text>
                        <Text style={styles.grantTotal}>{numeralFormatter(totalProductAndShippingPrice)}</Text>
                      </Horizontal>
                    </Body>
                  </CardItem>
                </Card>
              </>
            )}
          </>
        )}
      </Content>
      {selectedAddress && (
        <Button
          onPress={handleSubmit}
          raised
          containerStyle={styles.buttonContainer}
          buttonStyle={styles.button}
          icon={{name: 'ios-send', type: 'ionicon', color: 'white'}}
          title={translations.ACTION_CHECKOUT.toUpperCase()}
        />
      )}
    </Container>
  );
};

const mapStateToProps = state => {
  const {auth} = state;
  return {
    userData: auth.userData,
    selectedAddress: auth.selectedAddress,
    cartsCheckout: store.select.cart.getCartCheckout(state),
  };
};

// eslint-disable-next-line no-unused-vars
const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutContent);
