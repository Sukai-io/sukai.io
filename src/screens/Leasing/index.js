import React, {Component} from 'react';
import {connect} from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

import {Formik} from 'formik';
import {Container} from 'native-base';
import Content from './Content';
import {FormValues} from './types';
import validation from './validation';
import ApiClient from '../../api/ApiClient';
import errorGenerator from '../../api/ErrorGenerator';
import Loader from '../../components/indicator/Loader';

class LeasingScreen extends Component {
  state = {
    loading: false,
  };

  handleLoading = value => {
    this.setState({loading: value});
  };

  handleSubmit = async (values: FormValues) => {
    const {navigation} = this.props;
    this.handleLoading(true);
    const response = await ApiClient.ppobCheckInquiry({
      ...values,
      ppob_action: 'inquiry',
    });
    this.handleLoading(false);
    const {success} = response.data;
    const error = response.data?.data?.result?.data?.message;
    if (response.ok && success) {
      const {inquiry} = response.data?.data?.result;
      navigation.navigate('LeasingCheckoutScreen', {
        data: {...inquiry, ppob_provider: values.ppob_provider},
      });
    } else {
      setTimeout(() => errorGenerator(response, !success ? error : null), 300);
    }
  };

  render() {
    return (
      <Container>
        <Loader loading={this.state.loading} />
        <Formik
          initialValues={{
            ppob_provider: '',
            ppob_code: '',
            ppob_number: '',
          }}
          onSubmit={(values: FormValues) => this.handleSubmit(values)}
          validationSchema={validation}
          render={props => <Content {...props} />}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ppob}) => {
  return {
    providerPriceList: ppob.provider_price_list,
  };
};

export default connect(mapStateToProps)(LeasingScreen);
