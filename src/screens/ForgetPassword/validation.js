import { object as yupObject, string as yupString } from 'yup'

export default yupObject().shape({
  username: yupString().required('A username is required.'),
  email: yupString()
    .email()
    .required('An email is required.'),
})
