/* eslint-disable no-nested-ternary */
/* eslint-disable max-len */
import React from 'react';
import {withNavigation} from 'react-navigation';
import {connect} from 'react-redux';

import {Icon, Header, Left, Button, Body, Right, Text, NativeBase} from 'native-base';
import {Badge} from 'react-native-elements';
import styles from './styles';
import store from '../../config/CreateStore';

type Props = NativeBase.Header & {
  title: string,
  navigation: any,
  onBackPress: Function,
  topupPending: Number,
};

const HeaderTopup = (props: Props) => {
  const {title, navigation, onBackPress, topupPending} = props;
  return (
    <Header {...props}>
      <Left>
        <Button transparent onPress={() => (onBackPress ? onBackPress() : navigation.goBack())}>
          <Icon name="arrow-back" />
        </Button>
      </Left>
      <Body>
        <Text numberOfLines={1} style={styles.text}>
          {title}
        </Text>
      </Body>
      <Right>
        <Button transparent onPress={() => navigation.navigate('HistoryTopupScreen')}>
          <Icon name="history" style={styles.cart} type="MaterialCommunityIcons" />
        </Button>
        <Button transparent onPress={() => navigation.navigate('TopUpConfirmScreen')}>
          <Icon name="ios-notifications" style={styles.cart} />
          {topupPending && <Badge status="error" containerStyle={{position: 'absolute', top: 10, right: 14}} />}
        </Button>
      </Right>
    </Header>
  );
};

const mapStateToProps = ({balance}) => ({
  topupPending: store.select.balance.pendingTopup(balance),
});

export default connect(mapStateToProps)(withNavigation(HeaderTopup));
