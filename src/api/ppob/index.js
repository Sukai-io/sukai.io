/* eslint-disable no-nested-ternary */
import R from 'ramda';
import api from '../ApiClient';
import errorGenerator from '../ErrorGenerator';
import {goBack} from '../../utils/NavigationUtils';
import {showToast} from '../../utils/ToastUtils';

const getSuccess = data => R.pathOr(false, ['success'], data);

export default {
  state: {
    provider_list: [],
    provider_price_list: [],
    remember_pulsa: true,
    remember_electricity: true,
    remember_emoney: true,
    remember_leasing: true,
    number_pulsa: '',
    number_electricity: '',
    number_emoney: '',
    number_leasing: '',
  },
  reducers: {
    clear(state) {
      return {
        ...state,
        provider_list: [],
        provider_price_list: [],
      };
    },
    setProvider(state, data) {
      return {
        ...state,
        provider_list: data.data.provider_list.map(o => ({
          label: o.provider_name,
          value: o.provider_name,
        })),
      };
    },
    setDefaultProvider(state, data) {
      return {
        ...state,
        provider_list: data.data.provider_list.map(o => ({
          label: o.provider_name,
          value: o.provider_name,
        })),
      };
    },
    setProviderPrice(state, data) {
      return {
        ...state,
        provider_price_list: data.data.provider_price_list,
      };
    },
    saveRemember(state, type) {
      return {
        ...state,
        ...(type === 'pulsa'
          ? {
              remember_pulsa: !state.remember_pulsa,
            }
          : type === 'electricity'
          ? {
              remember_electricity: !state.remember_electricity,
            }
          : type === 'emoney'
          ? {
              remember_emoney: !state.remember_emoney,
            }
          : {
              remember_leasing: !state.remember_leasing,
            }),
      };
    },
    saveNumber(state, data) {
      return {
        ...state,
        ...(data.type === 'pulsa'
          ? {
              number_pulsa: state.remember_pulsa ? data.number : null,
            }
          : data.type === 'electricity'
          ? {
              number_electricity: state.remember_electricity ? data.number : null,
            }
          : data.type === 'emoney'
          ? {
              number_emoney: state.remember_emoney ? data.number : null,
            }
          : {
              number_leasing: state.remember_leasing ? data.number : null,
            }),
      };
    },
  },
  effects: dispatch => ({
    async getProvider(data) {
      const response = await api.provider(data);
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.ppob.setProvider(response.data);
        return Promise.resolve(response.data);
      }
      errorGenerator(response);
      return Promise.reject(response);
    },
    async getProviderPrice(data) {
      const response = await api.providerPrice(data);
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.ppob.setProviderPrice(response.data);
        return Promise.resolve(response.data);
      }
      errorGenerator(response);
      return Promise.reject(response);
    },
    async doCheckout(data) {
      const response = await api.ppobCheckout(data);
      const success = getSuccess(response.data);
      const error = R.path(['data', 'error'], response.data);
      if (response.ok && success) {
        await dispatch.balance.getBalance();
        goBack();
        showToast(`Transaksi berhasil`, 'OK', 5000);
        return Promise.resolve(response.data);
      }
      setTimeout(() => errorGenerator(response, !success ? error : null), 400);
      return Promise.reject(response);
    },
  }),
};
