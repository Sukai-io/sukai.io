import { StyleSheet } from 'react-native'
import {
  ApplicationStyles, Metrics, Fonts, Colors,
} from '../../themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  categoryContainer: {
    ...ApplicationStyles.shadow,
    borderRadius: Metrics.doubleBaseMargin,
    paddingHorizontal: Metrics.smallMargin,
    paddingBottom: Metrics.doubleBaseMargin,
    marginBottom: Metrics.doubleBaseMargin,
  },
  categoryText: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.bold,
    color: Colors.gray700,
    padding: Metrics.doubleBaseMargin,
  },
})
