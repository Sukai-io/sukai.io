import { StyleSheet } from 'react-native'
import {
  ApplicationStyles, Metrics, Fonts, Colors,
} from '../../../themes'

export default StyleSheet.create({
  container: {
    ...ApplicationStyles.flexCenter,
    ...ApplicationStyles.shadow,
    borderRadius: Metrics.doubleBaseMargin,
    paddingVertical: Metrics.doubleBaseMargin,
  },
  icon: {
    width: Metrics.icons.medium,
    height: Metrics.icons.medium,
  },
  title: {
    marginTop: Metrics.baseMargin,
    fontSize: Fonts.size.mini,
    color: Colors.gray900,
  },
})
