import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Container, Row, Col, Text, Content} from 'native-base';
import styles from './styles';
import HeaderBack from '../../components/header/HeaderBack';
import VerticalList from '../../components/list/VerticalList';
import Loading from '../../components/indicator/Loading';
import {formattedDateTime} from '../../utils/TextUtils';

class HistoryBonus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      offset: 0,
      initialPage: true,
    };
    this.handleLoadMore = this.handleLoadMore.bind(this);
  }

  componentDidMount() {
    const {getBonusHistory} = this.props;
    const {offset} = this.state;
    getBonusHistory({limit: 10, offset});
  }

  handleLoadMore() {
    this.setState(
      prevState => ({
        offset: prevState.offset + 10,
        initialPage: false,
      }),
      () => {
        const {getBonusHistory} = this.props;
        const {offset} = this.state;
        getBonusHistory({limit: 10, offset});
      },
    );
  }

  handleRefresh = () => {
    this.setState({offset: 0},
      () => {
        const {getBonusHistory} = this.props;
        getBonusHistory({limit: 10, offset: 0});
      },
    )
  }

  render() {
    const {bonuses, loading} = this.props;
    const {initialPage, offset} = this.state;
    return (
      <Container style={styles.mainContainer}>
        <HeaderBack title="History Bonus" withoutRight />
        {loading && initialPage ? (
          <Loading isTopIndicator />
        ) : (
          <Content  style={{flex: 1}} contentContainerStyle={{flex: 1}}>
            <VerticalList
              data={bonuses}
              windowSize={13}
              maxToRenderPerBatch={6}
              updateCellsBatchingPeriod={30}
              withRefreshControl
              refreshing={loading && offset === 0}
              onRefresh={this.handleRefresh}
              renderItem={({item}) => (
                <Col style={styles.list}>
                  <Row style={{justifyContent: 'space-between'}}>
                    <Text style={styles.title}>{item.bonus_uniquecode}</Text>
                    <Text style={styles.amount}>{`+${item.amount}`}</Text>
                  </Row>
                  <Text style={styles.desc}>{item.desc}</Text>
                  <Text style={styles.date}>{formattedDateTime(item.datecreated)}</Text>
                </Col>
              )}
              onEndReached={this.handleLoadMore}
            />
          </Content>
        )}
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    bonuses: state.balance.bonus_histories,
    loading: state.loading.effects.balance.getBonusHistory,
  };
};

const mapDispatchToProps = ({balance}) => {
  return {
    getBonusHistory: params => balance.getBonusHistory(params),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HistoryBonus);
