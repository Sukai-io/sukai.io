import React from 'react';
import {View, Modal, ActivityIndicator} from 'react-native';
import styles from './Loader.styles';
import {Colors} from '../../themes';

type Props = {
  loading?: boolean,
};

const Loader = (props: Props) => {
  const {loading} = props;

  return (
    <Modal
      transparent
      animationType="none"
      visible={loading}
      onRequestClose={() => {}}>
      <View style={styles.modalBackground}>
        <View style={styles.activityIndicatorWrapper}>
          <ActivityIndicator
            animating={loading}
            size="large"
            color={Colors.primary}
          />
        </View>
      </View>
    </Modal>
  );
};

Loader.defaultProps = {
  loading: false,
};

export default Loader;
