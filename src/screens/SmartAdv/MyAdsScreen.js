import React, {useEffect, useState, useRef, useContext} from 'React';
import {View, TouchableOpacity, Image, Dimensions} from 'react-native';
import {Text, Container, Content, Button, Icon} from 'native-base';
import {Fonts, Colors} from '../../themes';
import {connect} from 'react-redux';
import HeaderBack from '../../components/header/HeaderBack';
import Loading from '../../components/Loading';
import {FlatGrid} from 'react-native-super-grid';
import ActionSheet from 'react-native-actionsheet';
import {LocalizationContext} from '../../localization';

const MyAdsScreen = ({myAds, key, updateAdStatus, emptyAd, getMyAds, navigation}) => {
  const {translations} = useContext(LocalizationContext);
  let [ad, setAd] = useState({});
  let actionSheet = useRef(null);

  useEffect(() => {
    getMyAds();
  }, [key]);

  const {width, height} = Dimensions.get('window');

  const adOption = index => {
    index == 0 && navigation.navigate('AdForm', ad);
    // index == 1 && navigation.navigate('AdBalanceForm', ad);
    index == 1 && updateAdStatus({advert_id: ad.id, advert_status: ad.status === 'ACTIVE' ? 0 : 1});
  };

  const renderAd = ({item: {image_thumbnail, id, title, status, total_budget, remaining_budget}}) => {
    return (
      <TouchableOpacity
        onPress={() => {
          emptyAd();
          setAd({title, id, status});
          actionSheet.current.show();
        }}>
        <View style={{padding: 4}}>
          <Image style={{width: '100%', height: width / 2 - 20, borderRadius: 15}} source={{uri: image_thumbnail}} />
          <Image
            resizeMode={'contain'}
            style={{width: 100, height: 20, position: 'absolute', top: 10, right: 0}}
            source={
              status === 'ACTIVE'
                ? require('../../images/Icons/active.png')
                : require('../../images/Icons/inactive.png')
            }
          />
          <Text numberOfLines={1} style={{fontSize: Fonts.size.small, marginTop: 3}}>
            {title}
          </Text>
          <View style={{marginTop: 3}}>
            <Text numberOfLines={1} style={{fontSize: Fonts.size.mini, color: Colors.gray600}}>
              Total of budget : {total_budget}
            </Text>
            <Text numberOfLines={1} style={{fontSize: Fonts.size.mini, color: Colors.gray600}}>
              Remaining of budget : {remaining_budget}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <Container>
      <HeaderBack
        customRight="add"
        onRightPress={() => {
          emptyAd();
          navigation.navigate('AdForm', {id: null});
        }}
        title={translations.TITLE_MANAGE_AD}
      />
      <Content style={{paddingTop: 5, paddingHorizontal: 3}}>
        {typeof myAds === 'object' && myAds.length == 0 && (
          <Text style={{textAlign: 'center', fontSize: Fonts.size.small, paddingVertical: 20, color: Colors.gray700}}>
            {translations.INFO_AD_EMPTY_LIST}
          </Text>
        )}
        {typeof myAds === 'object' && <FlatGrid itemDimension={130} items={myAds} renderItem={renderAd} />}
      </Content>
      <View style={{backgroundColor: Colors.gray100, elevation: 5, paddingHorizontal: 20, paddingVertical: 10}}>
        <Button onPress={() => navigation.navigate('AdInsight')} iconLeft block>
          <Icon name={'md-information-circle-outline'} />
          <Text>{translations.ACTION_AD_INSIGHT}</Text>
        </Button>
      </View>
      <Loading visible={typeof myAds != 'object'} />
      <ActionSheet
        ref={actionSheet}
        title={ad.title}
        options={[
          'Edit Ads',
          //  'Add Budget',
          ad.status === 'ACTIVE' ? 'Nonactive' : 'Activate',
          'Cancel',
        ]}
        destructiveButtonIndex={2}
        onPress={adOption}
      />
    </Container>
  );
};

const mapStateToProps = ({smartadv}) => {
  return {
    myAds: smartadv.myAds,
  };
};

const mapDispatchToProps = ({smartadv}) => {
  return {
    getMyAds: smartadv.getMyAds,
    updateAdStatus: data => smartadv.updateAdStatus(data),
    emptyAd: smartadv.emptyAd,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MyAdsScreen);
