import React, { useContext } from 'react';
import { TouchableOpacity } from 'react-native';
import { withNavigation } from 'react-navigation';

import { Text, View } from 'native-base';
import styles from './Wallet.styles';
import { LocalizationContext } from '../../../localization';

const Wallet = ({navigation, balance}) => {
  const { translations } = useContext(LocalizationContext);
  return (
    <View style={styles.container}>
      <Text style={styles.textLeft}>{translations.TITLE_YOUR_WALLET} : {balance}</Text>
      <View style={styles.rightView}>
        <TouchableOpacity onPress={() => navigation.navigate('Scan')}>
          <Text style={styles.textRight}>{translations.TITLE_TRANSFER.toUpperCase()}</Text>
        </TouchableOpacity>
        <View style={styles.divider} />
        <TouchableOpacity onPress={() => navigation.navigate('WithdrawScreen')}>
          <Text style={styles.textRight}>{translations.TITLE_WITHDRAW.toUpperCase()}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default withNavigation(Wallet);
