import * as React from 'react'
import { View, StyleSheet, ViewStyle } from 'react-native'

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
})

type Props = {
  children: React.Node,
  style?: ViewStyle,
}

const Horizontal = (props: Props) => {
  const { children, style } = props
  return <View style={[styles.container, style]}>{children}</View>
}

Horizontal.defaultProps = {
  style: null,
}

export default Horizontal
