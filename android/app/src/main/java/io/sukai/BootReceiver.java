package io.sukai;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;

import io.sukai.lockscreen.utils.LockScreen;
import io.sukai.lockscreen.utils.LockscreenService;

public class BootReceiver extends BroadcastReceiver {

    public void onReceive(Context context, Intent arg1)
    {
        Intent intent = new Intent(context, LockscreenService.class);
        LockScreen lc = new LockScreen();
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        boolean active = pref.getBoolean(context.getString(R.string.lockscreen_status), true);
        if(active){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(intent);
            } else {
                context.startService(intent);
            }
            lc.active();
        }
    }

}
