/* eslint-disable react/state-in-constructor */
import React, {useCallback, useState, useRef, useContext} from 'react';
import {connect} from 'react-redux';

import CheckoutContent from './Content';
import AlertPro from 'react-native-alert-pro';
import {numeralFormatter} from '../../utils/TextUtils';
import {Fonts, Colors} from '../../themes';
import Loader from '../../components/indicator/Loader';
import {transformToFormData} from '../../api/ApiHelper';
import {LocalizationContext} from '../../localization';

const CheckoutScreen = ({balance, loading, selectedAddress, doCheckout}) => {
  const alertRef = useRef(null);
  const [values, setValues] = useState(null);
  const {translations} = useContext(LocalizationContext);
  const onSubmit = useCallback(values => {
    alertRef.current.open();
    setValues(values);
  }, []);
  const handleConfirm = useCallback(() => {
    doCheckout(
      transformToFormData({
        product: JSON.stringify(
          values.product.map(item => ({
            id_seller: item.id_member,
            shipping_fee: item.service,
            courier: item.courier,
            courier_service: item.service_options.find(o => o.value === item.service)?.label,
            products: item.products.map(item => ({product_id: item.id, qty: item.cart_qty, notes: item.note || ''})),
          })),
        ),
        address_id: selectedAddress?.id,
      }),
    );
    alertRef.current.close();
  }, [values, selectedAddress]);
  return (
    <>
      <Loader loading={loading} />
      <AlertPro
        ref={alertRef}
        onConfirm={handleConfirm}
        onCancel={() => alertRef.current.close()}
        title="Konfirmasi Pembelian"
        message={translations.INFO_BALANCE_WILL_DEDUCTED.replace('$balance', numeralFormatter(balance))}
        textCancel={translations.ACTION_CANCEL}
        textConfirm={translations.ACTION_CHECKOUT}
        customStyles={{
          container: {
            width: 300,
            shadowColor: '#000000',
            shadowOpacity: 0.1,
            shadowRadius: 10,
          },
          title: {
            fontSize: Fonts.size.h6,
          },
          buttonCancel: {
            backgroundColor: Colors.gray400,
          },
          buttonConfirm: {
            backgroundColor: Colors.bloodOrange,
          },
        }}
      />
      <CheckoutContent onSubmit={onSubmit} />
    </>
  );
};

const mapStateToProps = ({loading, balance, auth}) => {
  return {
    selectedAddress: auth.selectedAddress,
    balance: balance.balance,
    loading: loading.effects.order.doCheckout,
  };
};

const mapDispatchToProps = ({order}) => {
  return {
    doCheckout: data => order.doCheckout(data),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutScreen);
