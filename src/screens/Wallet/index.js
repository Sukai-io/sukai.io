import React, {Component} from 'react';
import {View, ActivityIndicator, Alert} from 'react-native';
import AlertPro from 'react-native-alert-pro';
import {connect} from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import {Container, Content, Card, Text, Item, Label, Input} from 'native-base';
import {Formik, FormikProps} from 'formik';
import styles from './styles';
import HeaderBack from '../../components/header/HeaderBack';
import {Fonts, Colors} from '../../themes';
import InputWithButton from './components/InputWithButton';
import withdrawValidation from './withdrawValidation';
import transferValidation from './transferValidation';
import store from '../../config/CreateStore';
import ApiClient from '../../api/ApiClient';
import {transformToFormData} from '../../api/ApiHelper';
import {numeralFormatter} from '../../utils/TextUtils';
import Loader from '../../components/indicator/Loader';

type WithdrawFormValues = {
  withdraw: Number,
};

type TransferFormValues = {
  username: String,
  amount: Number,
};

class WalletScreen extends Component {
  state = {
    loadingGet: false,
  };

  componentDidMount() {
    const {getBalance} = this.props;
    getBalance();
  }

  handleLoading = value => {
    this.setState({loadingGet: value});
  };

  renderWithdrawForm = ({
    handleSubmit,
    setFieldValue,
    setFieldTouched,
    touched,
    errors,
  }: FormikProps<WithdrawFormValues>) => {
    const {loading, loadingWithdrawal, balance} = this.props;
    return (
      <Card style={styles.cardContainer}>
        <View style={styles.amountContainer}>
          <Text style={styles.titleBold}>Sukai-Wallet Saya</Text>
          {loading ? <ActivityIndicator /> : <Text style={styles.amount}>{balance}</Text>}
        </View>
        <Text style={styles.marginTop}>
          <Text style={styles.titleBold}>Ketentuan Withdraw</Text>
        </Text>
        <Text style={styles.title}>1. Minimal withdraw Rp. 100.000 : </Text>
        <Text style={styles.title}>2. Biaya admin Rp. 10.000 per withdraw</Text>
        <InputWithButton
          loadingButton={loadingWithdrawal}
          disabledButton={loadingWithdrawal}
          onPress={handleSubmit}
          titleButton="WITHDRAW"
          placeholder="Jumlah Withdraw"
          keyboardType="number-pad"
          onBlur={() => setFieldTouched('withdraw')}
          onChangeText={value => setFieldValue('withdraw', value)}
        />
        {errors.withdraw && touched.withdraw && <Text style={styles.error}>{errors.withdraw}</Text>}
      </Card>
    );
  };

  renderTransferForm = ({
    handleSubmit,
    setFieldValue,
    setFieldTouched,
    touched,
    errors,
  }: FormikProps<TransferFormValues>) => {
    const {loadingTransfer} = this.props;
    return (
      <Card style={styles.cardContainer}>
        <Text style={styles.amount}>Transfer</Text>
        <Item stackedLabel>
          <Label>Username *</Label>
          <Input
            autoCapitalize="none"
            onBlur={() => setFieldTouched('username')}
            onChangeText={value => setFieldValue('username', value)}
            style={{fontFamily: Fonts.type.base}}
          />
        </Item>
        {errors.username && touched.username && <Text style={styles.error}>{errors.username}</Text>}
        <InputWithButton
          loadingButton={loadingTransfer}
          disabledButton={loadingTransfer}
          onPress={handleSubmit}
          titleButton="TRANSFER"
          placeholder="Jumlah Transfer"
          keyboardType="number-pad"
          onBlur={() => setFieldTouched('amount')}
          onChangeText={value => setFieldValue('amount', value)}
        />
        {errors.amount && touched.amount && <Text style={styles.error}>{errors.amount}</Text>}
      </Card>
    );
  };

  handleWithdrawSubmit = values => {
    const {doWithdrawal} = this.props;
    doWithdrawal({withdraw_amount: values.withdraw});
  };

  handleTransferSubmit = (values: TransferFormValues) => {
    this.handleLoading(true);
    ApiClient.searchMember(transformToFormData({username: values.username})).then(({data: {data, success}, ok}) => {
      this.handleLoading(false);
      setTimeout(() => {
        if (ok && success) {
          const name = data?.data?.data?.member_name || '-';
          Alert.alert(
            'Konfirmasi',
            `Anda akan melakukan transfer ke ${name} sebesar ${numeralFormatter(
              values.amount,
            )}. Yakin ingin melanjutkan?`,
            [
              {
                text: 'Batal',
                onPress: () => {},
                style: 'cancel',
              },
              {
                text: 'OK',
                onPress: () => {
                  const {doTransfer} = this.props;
                  doTransfer({transfer_username_receiver: values.username, transfer_amount: values.amount});
                },
              },
            ],
          );
        } else {
          Alert.alert('Terjadi Kesalahan', data?.error);
        }
      }, 500);
    });
  };

  render() {
    const {navigation} = this.props;
    const {loadingGet} = this.state;
    return (
      <Container style={styles.mainContainer}>
        <Loader loading={loadingGet} />
        <AlertPro
          ref={ref => {
            this.AlertPro = ref;
          }}
          onConfirm={() => this.AlertPro.close()}
          onCancel={() => this.AlertPro.close()}
          title="Konfirmasi Top Up"
          message="Anda yakin telah melengkapi pembayaran?"
          textCancel="Batal"
          textConfirm="Iya"
          customStyles={{
            container: {
              width: 300,
              shadowColor: '#000000',
              shadowOpacity: 0.1,
              shadowRadius: 10,
            },
            title: {
              fontSize: Fonts.size.h6,
            },
            buttonCancel: {
              backgroundColor: Colors.gray400,
            },
            buttonConfirm: {
              backgroundColor: Colors.bloodOrange,
            },
          }}
        />
        <HeaderBack onBackPress={() => navigation.navigate('HomeTab')} withoutRight title="Wallet Saya" />
        <Content style={styles.container}>
          <Formik
            initialValues={{withdraw: ''}}
            onSubmit={(values: WithdrawFormValues) => this.handleWithdrawSubmit(values)}
            validationSchema={withdrawValidation}
            render={(actions: FormikProps<WithdrawFormValues>) => this.renderWithdrawForm(actions)}
          />
          <Formik
            initialValues={{username: '', amount: ''}}
            onSubmit={(values: TransferFormValues) => this.handleTransferSubmit(values)}
            validationSchema={transferValidation}
            render={(actions: FormikProps<TransferFormValues>) => this.renderTransferForm(actions)}
          />
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = ({loading, balance}) => {
  return {
    loading: loading.effects.balance.getBalance,
    loadingTransfer: loading.effects.balance.doTransfer,
    loadingWithdrawal: loading.effects.balance.doWithdrawal,
    balance: store.select.balance.getFormattedBalance(balance),
  };
};

const mapDispatchToProps = ({balance}) => {
  return {
    getBalance: () => balance.getBalance(),
    doTransfer: data => balance.doTransfer(data),
    doWithdrawal: data => balance.doWithdrawal(data),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(WalletScreen);
