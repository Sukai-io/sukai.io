import React from 'react'
import { ActivityIndicator, ViewStyle } from 'react-native'
import { Button, Text, NativeBase } from 'native-base'
import styles from './styles/AuthButton.styles'
import { Colors } from '../../themes'

type Props = NativeBase.Button & {
  title: String,
  loading: Boolean,
  customStyle: ViewStyle,
}

const AuthButton = (props: Props) => {
  const { title, loading, customStyle } = props
  return (
    <Button full style={[styles.container, customStyle]} {...props}>
      {loading ? (
        <ActivityIndicator color={Colors.primary} />
      ) : (
        <Text style={styles.text}>{title}</Text>
      )}
    </Button>
  )
}

export default AuthButton
