import { StyleSheet } from 'react-native'
import {
  ApplicationStyles, Metrics, Colors, Fonts,
} from '../../themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    ...ApplicationStyles.flex,
    backgroundColor: Colors.gray100,
    padding: Metrics.baseMargin,
  },
  buttonContainer: {
    marginTop: Metrics.doubleBaseMargin,
  },
  button: {
    backgroundColor: Colors.bloodOrange,
  },
  cardContainer: {
    ...ApplicationStyles.shadow,
    borderRadius: Metrics.doubleBaseMargin,
    padding: Metrics.section,
  },
  title: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    color: Colors.gray700,
  },
  titleBold: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.bold,
    color: Colors.gray700,
  },
})
