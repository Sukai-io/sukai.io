import R, {pathOr} from 'ramda';
import api from '../ApiClient';
import {transformToFormData} from '../ApiHelper';
import errorGenerator from '../ErrorGenerator';
import {goBack} from '../../utils/NavigationUtils';
import SimpleToast from 'react-native-simple-toast';

const getSuccess = data => R.pathOr(false, ['success'], data);

export default {
  state: {
    countries: [],
    provinces: [],
    provinces_rajaongkir: [],
    cities: [],
    cities_rajaongkir: [],
    banks: [],
    partnerships: [],
    digital_competitions: [],
    shipping_addresses: [],

    budgets: [],
    currency_points: [],
    bank_accounts: [],
    payment_options: {},
  },
  reducers: {
    setCountries(state, data) {
      return {
        ...state,
        countries: data?.data?.country_list?.map(item => ({
          id: item.country_id,
          name: item.country_name,
        })),
        provinces: [],
        cities: [],
      };
    },
    setProvinces(state, data) {
      return {
        ...state,
        provinces: data?.data?.province_list?.map(item => ({
          id: item.province_id,
          name: item.province_name,
        })),
        cities: [],
      };
    },
    setProvincesRajaOngkir(state, data) {
      return {
        ...state,
        provinces_rajaongkir: data?.data?.list_province_rajaongkir?.map(item => ({
          id: item.province_id,
          name: item.province_name,
        })),
        cities_rajaongkir: [],
      };
    },
    setCities(state, data) {
      return {
        ...state,
        cities: data?.data?.city_list?.map(item => ({
          id: item.regional_id,
          name: item.regional_name,
        })),
      };
    },
    setCitiesRajaOngkir(state, data) {
      return {
        ...state,
        cities_rajaongkir: data?.data?.list_city_rajaongkir?.map(item => ({
          id: item.city_id,
          name: item.city_name,
        })),
      };
    },
    setBanks(state, data) {
      return {
        ...state,
        banks: data?.data?.bank_list?.map(item => ({
          id: item.id,
          name: item.bank_name,
        })),
      };
    },
    setPartnerships(state, data) {
      return {
        ...state,
        partnerships: data?.data?.partner_data,
      };
    },
    setInitialDigitalCompetitions(state, data) {
      return {
        ...state,
        digital_competitions: data?.data?.partner_data,
      };
    },
    setDigitalCompetitions(state, data) {
      return {
        ...state,
        digital_competitions: state.digital_competitions.concat(data?.data?.partner_data),
      };
    },
    setShippingAddresses(state, data) {
      return {
        ...state,
        shipping_addresses: data.data?.list_shipping_address,
      };
    },
    setDefaultShippingAddresses(state) {
      return {
        ...state,
        shipping_addresses: [],
      };
    },
    setBudgets(state, data) {
      return {
        ...state,
        budgets: data.data.data,
      };
    },
    setCurrencyPoints(state, data) {
      return {
        ...state,
        currency_points: data.data.data,
      };
    },
    setPaymentOptions(state, data) {
      return {
        ...state,
        payment_options: data.data.data,
      };
    },
    setBankAccounts(state, data) {
      return {
        ...state,
        bank_accounts: data.data.data,
      };
    },
  },
  effects: dispatch => ({
    async getCountries() {
      const response = await api.listCountry();
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.support.setCountries(response.data);
        return Promise.resolve(response.data);
      }
    },
    async getProvinces() {
      const response = await api.listProvince();
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.support.setProvinces(response.data);
        return Promise.resolve(response.data);
      }
    },
    async getProvincesRajaOngkir() {
      const response = await api.listProvinceRajaOngkir();
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.support.setProvincesRajaOngkir(response.data);
        return Promise.resolve(response.data);
      }
    },
    async getCities(params) {
      const response = await api.listCity(params);
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.support.setCities(response.data);
        return Promise.resolve(response.data);
      }
    },
    async getCitiesRajaOngkir(params) {
      const response = await api.listCityRajaOngkir(params);
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.support.setCitiesRajaOngkir(response.data);
        return Promise.resolve(response.data);
      }
    },
    async getBanks() {
      const response = await api.listBank();
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.support.setBanks(response.data);
        return Promise.resolve(response.data);
      }
    },
    async getPartnerships() {
      const response = await api.listPartnership();
      const success = getSuccess(response.data);
      if (response.ok && success) {
        dispatch.support.setPartnerships(response.data);
        return Promise.resolve(response.data);
      }
    },
    async getDigitalCompetitions(data) {
      const response = await api.listDigitalCompetition(transformToFormData(data));
      const success = getSuccess(response.data);
      if (response.ok && success) {
        if (data.offset == 0) {
          await dispatch.support.setInitialDigitalCompetitions(response.data);
        } else {
          await dispatch.support.setDigitalCompetitions(response.data);
        }
        return Promise.resolve(response.data);
      }
    },
    async getShippingAddresses(data, rootState) {
      const response = await api.listShippingAddress(transformToFormData({member_id: rootState?.auth?.userData?.id}));
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.support.setShippingAddresses(response.data);
        const shippingAddress = pathOr([], ['data', 'list_shipping_address'], response.data);
        if (shippingAddress.length !== 0) {
          await dispatch.auth.setSelectedAddress(shippingAddress[0]);
        }
        return Promise.resolve(response.data);
      }
      if (!success) {
        await dispatch.support.setDefaultShippingAddresses();
      }
    },
    async getBudgets() {
      const response = await api.listBudget();
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.support.setBudgets(response.data);
        return Promise.resolve(response.data);
      }
    },
    async getCurrencyPoints() {
      const response = await api.listCurrencyPoin();
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.support.setCurrencyPoints(response.data);
        return Promise.resolve(response.data);
      }
    },
    async getPaymentOptions(params) {
      const response = await api.listPaymentOptions(params);
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.support.setPaymentOptions(response.data);
        return Promise.resolve(response.data);
      }
    },
    async getBankAccounts() {
      const response = await api.listBankAccount();
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.support.setBankAccounts(response.data);
        return Promise.resolve(response.data);
      }
    },
    async addDigitalCompetition(data) {
      const response = await api.addDigitalCompetition(transformToFormData(data));
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.support.getDigitalCompetitions({limit: 10, offset: 0});
        goBack();
        SimpleToast.show(response.data?.data);
        return Promise.resolve(response.data);
      }
      setTimeout(() => errorGenerator(response, (!success && response?.data?.data?.error) || null), 500);
    },
    async addShippingAddress(data) {
      const response = await api.addShippingAddress(transformToFormData(data));
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.support.getShippingAddresses();
        goBack();
        SimpleToast.show(response.data?.data);
        return Promise.resolve(response.data);
      }
      setTimeout(() => errorGenerator(response, (!success && response?.data?.data?.error) || null), 500);
    },
    async editShippingAddress(data) {
      const response = await api.editShippingAddress(transformToFormData(data));
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.support.getShippingAddresses();
        goBack();
        SimpleToast.show(response.data?.data);
        return Promise.resolve(response.data);
      }
      setTimeout(() => errorGenerator(response, (!success && response?.data?.data?.error) || null), 500);
    },
    async deleteShippingAddress(data) {
      const response = await api.deleteShippingAddress(transformToFormData(data));
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.support.getShippingAddresses();
        goBack();
        SimpleToast.show(response.data?.data);
        return Promise.resolve(response.data);
      }
      errorGenerator(response, (!success && response?.data?.data?.error) || null);
    },
  }),
};
