import Metrics from './Metrics';
import Colors from './Colors';
import Fonts from './Fonts';

export default {
  screen: {
    mainContainer: {
      flex: 1,
      backgroundColor: Colors.transparent,
    },
    authContainer: {
      flex: 1,
      backgroundColor: Colors.snow,
      padding: 30,
    },
    backgroundImage: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
    },
    container: {
      flex: 1,
      paddingTop: Metrics.baseMargin,
      backgroundColor: Colors.transparent,
    },
    section: {
      margin: Metrics.section,
      padding: Metrics.baseMargin,
    },
    sectionText: {
      ...Fonts.style.normal,
      paddingVertical: Metrics.doubleBaseMargin,
      color: Colors.primary,
      marginVertical: Metrics.smallMargin,
      textAlign: 'center',
    },
    subtitle: {
      color: Colors.primary,
      padding: Metrics.smallMargin,
      marginBottom: Metrics.smallMargin,
      marginHorizontal: Metrics.smallMargin,
    },
    titleText: {
      ...Fonts.style.h2,
      fontSize: 14,
      color: Colors.gray800,
    },
    description: {
      ...Fonts.style.description,
    },
    normalText: {
      ...Fonts.style.normal,
    },
    flex: { flex: 1 },
    flexCenter: { flex: 1, alignItems: 'center' },
    flexCenterJustity: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    flexBetweenCenter: {
      flex: 1,
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    error: {
      ...Fonts.style.error,
    },
  },
  flex: { flex: 1 },
  rowFlex: { flex: 1, flexDirection: 'row' },
  rowCenterBetween: { flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' },
  flexEnd: { flex: 1, alignItems: 'flex-end' },
  flexCenter: { flex: 1, alignItems: 'center' },
  flexCenterAround: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  flexBetween: { flex: 1, justifyContent: 'space-between' },
  flexBetweenCenter: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  iconPad: { padding: 12 },
  cardPad: {
    paddingTop: Metrics.marginVertical,
    paddingHorizontal: Metrics.marginHorizontal,
    paddingBottom: 14,
  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 1,
    elevation: 1,
  },
};
