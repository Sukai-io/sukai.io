import React, {useCallback} from 'react';
import {useDispatch} from 'react-redux';

import {Formik, FormikProps} from 'formik';
import Content from './Content';
import {FormValues} from './types';
import validation from './validation';

const NewDigitalCompetition = () => {
  const {
    support: {addDigitalCompetition},
  } = useDispatch();
  const handleSubmit = useCallback((values, {resetForm}: FormikProps<FormValues>) => {
    addDigitalCompetition(values).then(() => {
      resetForm({});
    });
  }, []);
  return (
    <Formik
      initialValues={{
        title: '',
        description: '',
        url: '',
        digcom_image: null,
      }}
      onSubmit={(values, actions) => handleSubmit(values, actions)}
      validationSchema={validation}
      render={props => <Content {...props} />}
    />
  );
};

export default NewDigitalCompetition;
