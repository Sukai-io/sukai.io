import { StyleSheet } from 'react-native'
import {
  ApplicationStyles, Metrics, Colors, Fonts,
} from '../../themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
    paddingTop: Metrics.baseMargin,
    backgroundColor: Colors.gray100,
  },
  cardContainer: {
    ...ApplicationStyles.shadow,
    ...ApplicationStyles.flex,
    padding: Metrics.smallMargin,
    marginRight: 0,
    marginLeft: 0,
    marginTop: 0,
    marginBottom: Metrics.doubleBaseMargin,
  },
  label: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    color: Colors.gray700,
  },
  title: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.bold,
    color: Colors.gray700,
    marginBottom: 0,
  },
  buttonImageContainer: {
    marginTop: Metrics.baseMargin,
  },
  buttonImage: {
    backgroundColor: Colors.primary,
    height: 40,
  },
  buttonEdit: {
    backgroundColor: Colors.green500,
    height: 40,
  },
  buttonTitle: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    marginLeft: Metrics.smallMargin,
  },
  pickerOffset: {
    top: Metrics.doubleBaseMargin,
  },
  input: {
    fontSize: Fonts.size.regular,
    paddingLeft: 0,
  },
  buttonWrapper: {
    ...ApplicationStyles.flex,
    padding: Metrics.smallMargin,
  },
  buttonContainer: {
    ...ApplicationStyles.flex,
  },
  buttonReset: {
    backgroundColor: Colors.gray400,
    height: 40,
    flex: 1,
  },
  buttonConfirm: {
    backgroundColor: Colors.bloodOrange,
    height: 40,
    flex: 1,
  },
  buttonContainerTextStyle: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.base,
  },
  separator: {
    width: Metrics.baseMargin,
  },
  image: {
    width: '100%',
    height: 200,
    borderRadius: Metrics.baseMargin,
    marginTop: Metrics.baseMargin,
  },
})
