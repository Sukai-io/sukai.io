import { StyleSheet } from 'react-native'
import { Metrics, Colors, Fonts } from '../../../themes'

export default StyleSheet.create({
  withdrawContainer: {
    marginTop: Metrics.doubleBaseMargin,
    alignItems: 'flex-end',
  },
  input: {
    fontSize: Fonts.size.regular,
    color: Colors.gray700,
    fontFamily: Fonts.type.base,
    paddingLeft: 0,
  },
  buttonContainer: {
    marginTop: Metrics.doubleBaseMargin,
  },
  button: {
    backgroundColor: Colors.bloodOrange,
    height: 40,
  },
  buttonTitle: {
    fontSize: Fonts.size.medium,
  },
})
