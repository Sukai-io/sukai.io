/* eslint-disable no-unused-vars */
import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {Container, Content, Text, Grid, Row, Col} from 'native-base';
import styles from './styles';
import Loading from '../../components/indicator/Loading';
import VerticalList from '../../components/list/VerticalList';
import {formattedDate, formattedTime, numeralFormatter} from '../../utils/TextUtils';
import {Colors} from '../../themes';

class Withdraw extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      offset: 0,
      initialPage: true,
    };
    this.handleLoadMore = this.handleLoadMore.bind(this);
  }

  componentDidMount() {
    const {getWithdrawalHistory} = this.props;
    const {offset} = this.state;
    getWithdrawalHistory({limit: 10, offset});
  }

  handleLoadMore() {
    this.setState(
      prevState => ({
        offset: prevState.offset + 10,
        initialPage: false,
      }),
      () => {
        const {getWithdrawalHistory} = this.props;
        const {offset} = this.state;
        getWithdrawalHistory({limit: 10, offset});
      },
    );
  }

  render() {
    const {withdrawals, loading} = this.props;
    const {initialPage} = this.state;
    if (loading && initialPage) {
      return <Loading isTopIndicator />;
    }
    return (
      <VerticalList
        data={withdrawals}
        windowSize={13}
        maxToRenderPerBatch={6}
        updateCellsBatchingPeriod={30}
        renderItem={({item}) => (
          <Row style={styles.list}>
            <Col size={1}>
              <Text style={styles.date}>{`${formattedDate(item.datecreated)}\n${formattedTime(
                item.datecreated,
              )}`}</Text>
            </Col>
            <Col size={1}>
              <Text style={styles.amount}>{numeralFormatter(parseInt(item.nominal_receipt, 10))}</Text>
            </Col>
            <Col size={1}>
              <Text style={[styles.activity, item.status !== 'PENDING' && {color: Colors.green500}]}>
                {item.status}
              </Text>
            </Col>
          </Row>
        )}
        onEndReached={this.handleLoadMore}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    withdrawals: state.balance.withdrawal_histories,
    loading: state.loading.effects.balance.getWithdrawalHistory,
  };
};

const mapDispatchToProps = ({balance}) => {
  return {
    getWithdrawalHistory: params => balance.getWithdrawalHistory(params),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Withdraw);
