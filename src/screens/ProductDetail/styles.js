import { StyleSheet } from 'react-native'
import {
  ApplicationStyles, Metrics, Colors, Fonts,
} from '../../themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  flex: {
    ...ApplicationStyles.flex,
  },
  buttonContainer: {
    padding: Metrics.baseMargin,
  },
  button: {
    backgroundColor: Colors.bloodOrange,
  },
  price: {
    fontSize: Fonts.size.h6,
    fontFamily: Fonts.type.bold,
    color: Colors.primary,
  },
  title: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.bold,
    color: Colors.gray700,
    marginBottom: Metrics.baseMargin,
  },
  subtitleContainer: {
    ...ApplicationStyles.flexCenter,
    marginTop: Metrics.smallMargin,
  },
  subtitle: {
    ...ApplicationStyles.flex,
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    color: Colors.gray600,
  },
  cardContainer: {
    ...ApplicationStyles.shadow,
    ...ApplicationStyles.flex,
    padding: Metrics.smallMargin,
    marginRight: 0,
    marginLeft: 0,
    marginTop: 0,
    marginBottom: Metrics.doubleBaseMargin,
  },
  icon: {
    fontSize: Metrics.icons.tiny,
    color: Colors.gray400,
  },
  description: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    color: Colors.gray600,
    marginLeft: Metrics.baseMargin,
  },
  avatar: {
    width: Metrics.images.large,
    height: Metrics.images.large,
    borderRadius: Metrics.images.large / 2,
  },
  logistic: {
    ...ApplicationStyles.flex,
    height: Metrics.images.large,
  },
})
