import React from 'react'
import { View, KeyboardAvoidingView, ScrollView } from 'react-native'
import Modal, { ModalProps } from 'react-native-modal'
import { Dropdown } from 'react-native-material-dropdown'
import { Item, Input } from 'native-base'
import { Button } from 'react-native-elements'
import styles from './AddAddressModal.styles'
import { Colors } from '../../../themes'

type Props = ModalProps

const data = [
  {
    value: 'Jawa',
  },
  {
    value: 'Bali',
  },
  {
    value: 'Kalimantan',
  },
]

const AddAddressModal = (props: Props) => (
  <Modal style={styles.modal} hideModalContentWhileAnimating useNativeDriver {...props}>
    <View style={styles.container}>
      <ScrollView>
        <KeyboardAvoidingView behavior="position">
          <View style={styles.subContainer}>
            <Dropdown
              containerStyle={styles.dropdown}
              label="Pilih Provinsi"
              data={data}
              dropdownOffset={styles.pickerOffset}
            />
            <Item>
              <Input
                placeholderTextColor={Colors.gray500}
                placeholder="Masukkan Alamat"
                style={styles.input}
              />
            </Item>
            <Item>
              <Input
                placeholderTextColor={Colors.gray500}
                placeholder="Masukkan Kodepos"
                style={styles.input}
              />
            </Item>
            <Button
              raised
              containerStyle={styles.buttonContainer}
              buttonStyle={styles.button}
              title="SIMPAN ALAMAT"
            />
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  </Modal>
)

export default AddAddressModal
