import {object as yupObject, string as yupString, number as yupNumber} from 'yup';

export default yupObject().shape({
  member_name: yupString().required(),
  member_email: yupString()
    .email()
    .required(),
  member_address: yupString().required(),
  member_province: yupString()
    .nullable()
    .required(),
  member_city: yupString().when('member_province', {
    is: null,
    then: yupString().nullable(),
    otherwise: yupString()
      .nullable()
      .required(),
  }),
  member_district: yupString().when('member_city', {
    is: null,
    then: yupString(),
    otherwise: yupString().required(),
  }),
  member_gender: yupString()
    .nullable()
    .required(),
  member_phone: yupNumber()
    .typeError('Hanya boleh berisi angka')
    .required(),
  member_idcard: yupNumber()
    .typeError('Hanya boleh berisi angka')
    .required(),
  member_bank: yupString().required(),
  member_bill: yupNumber()
    .typeError('Hanya boleh berisi angka')
    .required(),
  member_bill_name: yupString().required(),
  member_branch: yupString().required(),
});
