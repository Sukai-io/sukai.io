import { StyleSheet } from 'react-native'
import {
  ApplicationStyles, Fonts, Metrics, Colors,
} from '../../../themes'

export default StyleSheet.create({
  grandContainer: {
    marginTop: Metrics.smallMargin,
    marginHorizontal: Metrics.section,
    ...ApplicationStyles.flexCenter,
  },
  totalLeft: {
    ...ApplicationStyles.flex,
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.base,
    color: Colors.gray600,
  },
  grantTotal: {
    ...ApplicationStyles.flex,
    textAlign: 'right',
    fontSize: Fonts.size.h6,
    fontFamily: Fonts.type.bold,
    color: Colors.primary,
  },
})
