import React, {useEffect, useCallback} from 'react';
import {connect} from 'react-redux';
import CartItem from './components/CartItem';
import VerticalList from '../../components/list/VerticalList';

const Seller = ({navigation, loading, sellerHistory, getSellerHistory}) => {
  const handleRefresh = useCallback(() => {
    getSellerHistory();
  }, [])
  useEffect(() => {
    getSellerHistory();
  }, []);
  return (
    <VerticalList
      data={sellerHistory}
      withRefreshControl
      refreshing={loading}
      onRefresh={handleRefresh}
      renderItem={({item}) => (
        <CartItem
          dataSource={item}
          onPress={() => navigation.navigate('SellerDetailScreen', {detail: item, type: 'seller'})}
        />
      )}
    />
  );
};

const mapStateToProps = ({loading, order}) => {
  return {
    loading: loading.effects.order.getSellerHistory,
    sellerHistory: order.seller_history,
  };
};

const mapDispatchToProps = ({order}) => {
  return {
    getSellerHistory: () => order.getSellerHistory(),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Seller);
