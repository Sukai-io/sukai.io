import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Formik, FormikProps} from 'formik';
import {Container} from 'native-base';
import AlertPro from 'react-native-alert-pro';
import {FormValues} from './types';
import Content from './Content';
import validation from './validation';
import {Fonts, Colors} from '../../themes';
import Loader from '../../components/indicator/Loader';
import {numeralFormatter} from '../../utils/TextUtils';

type Props = FormikProps<FormValues> & {
  doCheckout: Function,
  doSaveNumber: Function,
  number: String,
  providerPriceList: Array<Object>,
};

type State = {
  values: Object,
};

class PulsaScreen extends Component<Props, State> {
  state = {
    values: null,
  };

  handleSubmit = (values: FormValues) => {
    this.setState({values}, () => this.AlertPro.open());
  };

  handleConfirm = () => {
    const {doCheckout, doSaveNumber, providerPriceList} = this.props;
    const {values} = this.state;
    doSaveNumber({type: 'pulsa', number: values.ppob_number});
    doCheckout({
      ppob_action: 'topup',
      ppob_provider: values.ppob_provider,
      ppob_number: `0${values.ppob_number}`,
      ppob_code: providerPriceList[values.price_index].code,
    });
    this.AlertPro.close();
  };

  render() {
    const {number, balance, loadingCheckout} = this.props;
    return (
      <Container>
        <Loader loading={loadingCheckout} />
        <AlertPro
          ref={ref => {
            this.AlertPro = ref;
          }}
          onConfirm={this.handleConfirm}
          onCancel={() => this.AlertPro.close()}
          title="Konfirmasi Pembelian"
          message={`Saldo saat ini : ${numeralFormatter(
            balance,
          )}.\nSaldo Anda akan berkurang setelah pembelian Pulsa`}
          textCancel="Batal"
          textConfirm="Beli"
          customStyles={{
            container: {
              width: 300,
              shadowColor: '#000000',
              shadowOpacity: 0.1,
              shadowRadius: 10,
            },
            title: {
              fontSize: Fonts.size.h6,
            },
            buttonCancel: {
              backgroundColor: Colors.gray400,
            },
            buttonConfirm: {
              backgroundColor: Colors.bloodOrange,
            },
          }}
        />
        <Formik
          initialValues={{
            price_index: 0,
            ppob_provider: '',
            ppob_number: number || '',
          }}
          enableReinitialize
          onSubmit={(values: FormValues) => this.handleSubmit(values)}
          validationSchema={validation}
          render={(props: Props) => <Content {...props} />}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ppob, loading, balance}) => {
  return {
    balance: balance.balance,
    number: ppob.number_pulsa,
    providerPriceList: ppob.provider_price_list,
    loadingCheckout: loading.effects.ppob.doCheckout,
  };
};

const mapDispatchToProps = ({ppob}) => {
  return {
    doCheckout: data => ppob.doCheckout(data),
    doSaveNumber: data => ppob.saveNumber(data),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PulsaScreen);
