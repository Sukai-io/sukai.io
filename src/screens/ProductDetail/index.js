import React, {Component} from 'react';
import {connect} from 'react-redux';

import DropdownAlert from 'react-native-dropdownalert';
import {Container, Content, CardItem, Body, Text, Left, Icon} from 'native-base';
import {Button, Card} from 'react-native-elements';
import HeaderBack from '../../components/header/HeaderBack';
import styles from './styles';
import Slider from './components/Slider';
import Horizontal from '../../components/wrapper/Horizontal';
import {ApplicationStyles} from '../../themes';
import Loading from '../../components/indicator/Loading';
import {numeralFormatter} from '../../utils/TextUtils';

class ProductDetailScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const {navigation, getProductDetail} = this.props;
    const id = navigation.getParam('id', null);
    getProductDetail({type: 'id', value: id});
  }

  handleAddToCart = () => {
    const {addToCart, productDetail} = this.props;
    addToCart({
      ...productDetail,
      cart_qty: 1,
      cart_price: parseInt(productDetail?.product_price, 10),
    });
    this.dropDownAlertRef.alertWithType('success', 'Added', 'Produk telah ditambahkan ke keranjang', null, 500);
  };

  render() {
    const {loading, productDetail} = this.props;
    const productCategory =
      productDetail?.category?.reduce(
        (acc, item, index) => (index === 0 ? `${item.name}` : `${acc}, ${item.name}`),
        '',
      ) || '';
    return (
      <Container style={styles.mainContainer}>
        <HeaderBack title="" />
        <DropdownAlert
          ref={ref => {
            this.dropDownAlertRef = ref;
          }}
          inactiveStatusBarStyle="light-content"
        />
        {loading ? (
          <Loading isTopIndicator />
        ) : (
          <Content style={styles.container}>
            <Card containerStyle={styles.cardContainer}>
              {/* <CardItem cardBody> */}
              <Slider images={[productDetail?.product_image_url || '']} />
              {/* </CardItem> */}
              <CardItem>
                <Body>
                  <Text note>{productDetail?.product_name}</Text>
                  <Text style={styles.price}>{`Rp. ${numeralFormatter(productDetail?.product_price)}`}</Text>
                </Body>
              </CardItem>
            </Card>
            <Card containerStyle={styles.cardContainer}>
              <CardItem>
                <Body>
                  <Text style={styles.title}>Informasi Produk</Text>
                  <Horizontal style={ApplicationStyles.flexCenter}>
                    <Text style={styles.subtitle}>Berat</Text>
                    <Text style={[styles.subtitle, {textAlign: 'right'}]}>{`${productDetail?.product_weight} gr`}</Text>
                  </Horizontal>
                  <Horizontal style={styles.subtitleContainer}>
                    <Text style={styles.subtitle}>Kategori</Text>
                    <Text style={[styles.subtitle, {textAlign: 'right'}]}>{productCategory}</Text>
                  </Horizontal>
                  <Horizontal style={styles.subtitleContainer}>
                    <Text style={styles.subtitle}>Stok</Text>
                    <Text style={[styles.subtitle, {textAlign: 'right'}]}>{productDetail?.product_stock}</Text>
                  </Horizontal>
                </Body>
              </CardItem>
            </Card>
            {/* <Card containerStyle={styles.cardContainer}>
              <CardItem>
                <Body>
                  <Text style={styles.title}>Pengiriman</Text>
                  <Horizontal>
                    <FastImage
                      style={styles.logistic}
                      resizeMode="contain"
                      source={{
                        uri:
                          'https://i2.wp.com/w3cargo.com/wp-content/uploads/2019/06/jne.jpg?zoom=2&resize=720%2C380&ssl=1',
                      }}
                    />
                    <FastImage
                      style={styles.logistic}
                      resizeMode="contain"
                      source={{
                        uri:
                          'https://3.bp.blogspot.com/-oQqXvd6_uWE/WgZ-5e5BJQI/AAAAAAAAAI4/IkT3shaHUJ4Cp-JSJz3yIBr3uvasluPSwCLcBGAs/s320/Untitled.png',
                      }}
                    />
                    <FastImage
                      style={styles.logistic}
                      resizeMode="contain"
                      source={{
                        uri:
                          'https://mmc.tirto.id/image/otf/700x0/2019/02/04/logo-pos-indonesia-website-_ratio-16x9.jpg',
                      }}
                    />
                  </Horizontal>
                </Body>
              </CardItem>
            </Card> */}
            <Card containerStyle={styles.cardContainer}>
              <CardItem>
                <Body>
                  <Text style={styles.title}>Deksripsi Produk</Text>
                  {/* <Text style={styles.subtitle}>S M L XL</Text> */}
                  <Text style={styles.subtitle}>-</Text>
                </Body>
              </CardItem>
            </Card>
            <Card containerStyle={styles.cardContainer}>
              <CardItem>
                <Body>
                  <Text style={[styles.title, {marginBottom: 0}]}>Informasi Penjual</Text>
                </Body>
              </CardItem>
              <CardItem>
                <Left>
                  {/* <FastImage
                    source={{
                      uri:
                        'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
                    }}
                    style={styles.avatar}
                  /> */}
                  <Body>
                    <Horizontal style={{alignItems: 'center'}}>
                      <Icon name="person" style={styles.icon} />
                      <Text style={styles.description}>{productDetail?.seller}</Text>
                    </Horizontal>
                    <Horizontal style={{alignItems: 'center', marginTop: 8}}>
                      <Icon name="pin" style={styles.icon} />
                      <Text style={styles.description}>{productDetail?.seller_city_name || '-'}</Text>
                    </Horizontal>
                  </Body>
                </Left>
              </CardItem>
            </Card>
          </Content>
        )}
        {productDetail && (
          <Button
            disabled={productDetail?.product_stock == 0}
            disabledTitleStyle={{color: 'white'}}
            onPress={this.handleAddToCart}
            raised
            containerStyle={styles.buttonContainer}
            buttonStyle={styles.button}
            icon={{name: 'ios-cart', type: 'ionicon', color: 'white'}}
            title={productDetail?.product_stock == 0 ? 'STOK KOSONG' : 'ADD TO CART'}
          />
        )}
      </Container>
    );
  }
}

const mapStateToProps = ({product, loading}, {navigation}) => {
  const id = navigation.getParam('id', null);
  return {
    loading: loading.effects.product.getProductDetail,
    productDetail: product.product_detail[id] || null,
  };
};

const mapDispatchToProps = ({cart, product}) => {
  return {
    addToCart: item => cart.addToCart(item),
    getProductDetail: params => product.getProductDetail(params),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetailScreen);
