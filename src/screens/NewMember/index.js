import React, { Component } from 'react';
import { connect } from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
import { Formik } from 'formik';
import { FormValues } from './types';
import Content from './Content';
import validation from './validation';
import Loader from '../../components/indicator/Loader';
import { goBack } from '../../utils/NavigationUtils';

class NewMemberScreen extends Component {
  componentDidMount() {
    const { getBanks, getCountries, getProvinces } = this.props;
    getCountries();
    getProvinces();
    getBanks();

    // !allowDoRegister &&
    //   SimpleToast.show(
    //     'Saldo Sukai Wallet Anda tidak mencukupi untuk melakukan pendaftaran anggota baru. Silahkan Top Up Sukai Wallet terlebih dahulu!',
    //   );
  }

  handleSubmit = (values: FormValues, { resetForm }) => {
    const { postNewMember, userData } = this.props;
    const data = Object.entries(values).reduce(
      (acc, [key, value]) => ({
        ...acc,
        ...(key === 'is_member_username_valid' ? {} : { [key]: value }),
      }),
      {},
    );
    postNewMember({
      ...data,
      // sponsored: values.reg_member_sponsor_id === userData?.id ? 'as_sponsor' : 'other_sponsor',
    }).then(() => {
      resetForm({});
      goBack();
    });
  };

  render() {
    return (
      <>
        <Loader loading={this.props.loading} />
        <Formik
          initialValues={{
            asClient: false,
            sponsored: '1',
            reg_member_sponsor_id: '',
            reg_member_sponsor: '',
            reg_member_username: '',
            is_member_username_valid: false,
            reg_member_password: '',
            reg_member_name: '',
            reg_member_email: '',
            reg_member_address: '',
            reg_member_province: null,
            reg_member_country: null,
            reg_member_city: null,
            reg_member_district: '',
            reg_member_gender: null,
            reg_member_phone: '',
            reg_member_idcard: '',
            reg_member_bank: '',
            reg_member_bill: '',
            reg_member_bill_name: '',
            reg_member_branch: '',
          }}
          enableReinitialize
          onSubmit={(values, actions) => this.handleSubmit(values, actions)}
          validationSchema={validation}
          render={props => <Content {...props} />}
        />
      </>
    );
  }
}

const mapStateToProps = ({ loading, balance, auth }) => {
  return {
    loading: loading.effects.member.postNewMember,
    userData: auth.userData,
    allowDoRegister: balance.allowDoRegister,
  };
};

const mapDispatchToProps = ({ member, support }) => {
  return {
    postNewMember: data => member.postNewMember(data),
    getCountries: data => support.getCountries(data),
    getProvinces: data => support.getProvinces(data),
    getBanks: () => support.getBanks(),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NewMemberScreen);
