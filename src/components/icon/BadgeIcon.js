import React from 'react';
import {View} from 'react-native';
import {Icon, IconProps} from 'react-native-elements';
import styles from './BadgeIcon.styles';
import {useSelector} from 'react-redux';
import store from '../../config/CreateStore';

const BadgeIcon = (props: IconProps) => {
  const isNewsUnread = useSelector(store.select.auth.isNewsUnread);
  const isInboxUnread = useSelector(store.select.auth.isInboxUnread);
  const {iconName} = props;
  return (
    <View>
      <Icon {...props} name={iconName} type="ionicon" />
      {iconName === 'ios-mail' && isInboxUnread && <View style={styles.notifBadge} />}
      {iconName === 'md-list' && isNewsUnread && <View style={styles.notifBadge} />}
    </View>
  );
};

export default BadgeIcon;
