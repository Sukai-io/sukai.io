import React, {useState, useEffect, useCallback} from 'React';
import {Modal, View, TouchableOpacity, Image, Dimensions, ScrollView, Switch} from 'react-native';
import {Text, Icon, Container, Content, Button, Right} from 'native-base';
import {Fonts, Colors} from '../../themes';
import {connect} from 'react-redux';
import HeaderBack from '../../components/header/HeaderBack';
import {FlatGrid} from 'react-native-super-grid';
import InputText from '../NewMember/components/InputText';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import ActionSheet from 'react-native-actionsheet';
import ImagePicker from 'react-native-image-crop-picker';
import Loading from '../../components/Loading';

const AdInsight = ({
  getAdInsights,
  adInsights,
  navigation: {
    navigate,
    state: {key},
  },
}) => {
  let [loading, setLoading] = useState(true);
  let [filter, setFilter] = useState(false);
  let [singleInsight, setSingleInsight] = useState(null);
  let [insights, setInsights] = useState([]);
  let [params, setParams] = useState({
    limit: '',
    offset: '',
    search_title: '',
    search_status: '',
    search_total_budget_min: '',
    search_total_budget_max: '',
    search_total_used_budget_min: '',
    search_total_used_budget_max: '',
    search_remaining_min: '',
    search_remaining_max: '',
    search_total_viewer_min: '',
    search_total_viewer_max: '',
  });

  useEffect(() => {
    setLoading(true);
    getAdInsights(params);
  }, [key]);

  useEffect(() => {
    adInsights == null && setLoading(true);
    typeof adInsights === 'object' && setLoading(false);
    typeof adInsights === 'object' &&
      setInsights(
        adInsights.map(item => {
          return {...item, expanded: false};
        }),
      );
  }, [adInsights]);

  const toggleInsight = useCallback(
    index => {
      let newInsights = JSON.parse(JSON.stringify(insights));
      newInsights[index].expanded = !newInsights[index].expanded;
      setInsights(newInsights);
    },
    [insights],
  );

  const handleChange = value => {
    let newParams = {...params, ...value};
    setParams(newParams);
  };

  const onFilter = () => {
    setFilter(false);
    setLoading(true);
    getAdInsights(params);
  };

  renderInsight = (ad, index) => {
    let {title, status, total_budget, expanded, total_used_budget, remaining_budget, total_viewer, detail} = ad;
    return (
      <TouchableOpacity
        onPress={() => toggleInsight(index)}
        activeOpacity={0.85}
        style={{padding: 10}}
        key={Math.random()}>
        <View style={{backgroundColor: Colors.white, elevation: 3, borderRadius: 12}}>
          <View style={{padding: 15}}>
            <Text style={{marginBottom: 5}}>{title}</Text>
            <View style={{flexDirection: 'row', marginTop: 5}}>
              <Text style={{fontSize: Fonts.size.small, color: Colors.gray700, flex: 1}}>
                Dilihat : {total_viewer} kali
              </Text>
              <Text style={{fontSize: Fonts.size.small, color: Colors.darkgreen}}>
                Sisa budget : {remaining_budget}
              </Text>
            </View>
            {expanded && (
              <View>
                <View style={{flexDirection: 'row', marginTop: 3}}>
                  <Text style={{fontSize: Fonts.size.small, color: Colors.gray700, flex: 1}}>
                    Budget terpakai : {total_used_budget}
                  </Text>
                  <Text style={{fontSize: Fonts.size.small, color: Colors.gray700}}>Total budget : {total_budget}</Text>
                </View>
                <Text style={{marginTop: 12, fontSize: Fonts.size.medium, marginBottom: 5}}>Detail Audiens</Text>
                <Text style={{fontSize: Fonts.size.small, color: Colors.gray700}}>
                  {(detail || 'Belum ada audiens')
                    .split(';')
                    .map(item => item.trim())
                    .join('\n')}
                </Text>
              </View>
            )}
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              paddingBottom: 7,
              alignItems: 'flex-end',
              paddingTop: 5,
              backgroundColor: Colors.gray100,
              borderBottomLeftRadius: 12,
              borderBottomRightRadius: 12,
            }}>
            <Text style={{fontSize: Fonts.size.mini, color: Colors.gray700, fontWeight: '700'}}>
              {expanded ? 'Tutup' : 'Selengkapnya'}
            </Text>
            {expanded && (
              <Image
                style={{width: 6, height: 6, marginLeft: 8, marginBottom: 3}}
                source={require('../../images/Icons/chevron_up.png')}
              />
            )}
            {!expanded && (
              <Image
                style={{width: 6, height: 6, marginLeft: 8, marginBottom: 3}}
                source={require('../../images/Icons/chevron_down.png')}
              />
            )}
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <Container>
      <HeaderBack title={'Ads Insight'} onRightPress={() => setFilter(true)} customRight={'ios-search'} />
      <KeyboardAwareScrollView keyboardShouldPersistTaps="always">
        <Content style={{paddingVertical: 5, paddingHorizontal: 3}}>
          {insights.length == 0 && typeof adInsights === 'object' && (
            <Text style={{textAlign: 'center', fontSize: Fonts.size.small, paddingVertical: 20, color: Colors.gray700}}>
              {insights.length == 0 &&
                (params.search_title ? `Tidak ada iklan "${params.search_title}"` : 'Anda belum memiliki iklan')}
            </Text>
          )}
          {(insights || []).map(renderInsight)}
        </Content>
      </KeyboardAwareScrollView>
      <Modal animationType={'slide'} onRequestClose={() => setFilter(false)} visible={filter}>
        <Container>
          <HeaderBack onBackPress={() => setFilter(false)} title={'Cari Iklan'} withoutRight />
          <Content style={{paddingVertical: 5, paddingHorizontal: 3}}>
            <InputText
              autoFocus
              onChangeText={value => handleChange({search_title: value})}
              value={params.search_title}
              placeholder={'Ketik judul iklan'}
              label={'Judul iklan'}
            />
          </Content>
          <View style={{paddingHorizontal: 20, paddingVertical: 10, backgroundColor: Colors.gray100, elevation: 3}}>
            <Button onPress={onFilter} block>
              <Text>Cari Iklan</Text>
            </Button>
          </View>
        </Container>
      </Modal>
      <Loading visible={loading} />
    </Container>
  );
};

const mapStateToProps = ({smartadv}) => {
  return {
    adInsights: smartadv.adInsights,
  };
};

const mapDispatchToProps = ({smartadv}) => {
  return {
    getAdInsights: data => smartadv.getAdInsights(data),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AdInsight);
