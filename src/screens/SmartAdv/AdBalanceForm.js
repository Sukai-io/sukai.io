import React, { useState, useEffect } from 'React';
import {View, TouchableOpacity, Image, Dimensions, ScrollView, Switch} from 'react-native';
import {Text, Container, Button, Right} from 'native-base';
import {Fonts, Colors} from '../../themes';
import {connect} from 'react-redux';
import HeaderBack from '../../components/header/HeaderBack';
import { FlatGrid } from 'react-native-super-grid';
import InputText from '../NewMember/components/InputText';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import ActionSheet from 'react-native-actionsheet';
import ImagePicker from 'react-native-image-crop-picker';
import Loading from '../../components/Loading';

const AdBalanceForm = ({ myAd, test, getAd, emptyAd, updateAdStatus, createAd, adTopup, navigation: { navigate, state: { params: {id}, key } } }) => {

	let [ad, setAd] = useState({})
	let [active, setActive] = useState(false)
	const [amount, setAmount] = useState("")

	const toggleActive = () => {
		updateAdStatus({
			advert_id: id,
			advert_status: active ? 0 : 1
		})
		setActive(!active)
	}

	let [submitable, setSubmitable] = useState(false)
	let [loading, setLoading] = useState(false)

	let [selectedImage, setSelectedImage] = useState(null)

	useEffect(() => {
		id && setLoading(true)
		id && getAd({
			id,
			type: 'mine',
			limit: 1,
			offset: 0
		})
	}, [id, key]);

	useEffect(() => {
		id && myAd && setLoading(false)
		id && myAd && setAd(myAd)
		id && myAd && setActive(myAd.status === 'ACTIVE')
		emptyAd()
	}, [myAd]);

	const { width, height } = Dimensions.get('window')

	const onSubmit = () => {
		setLoading(true)
		adTopup({
			advert_id: id,
			advert_budget: amount
		})
	}

	return (
		<Container>
			<HeaderBack 
				title={'Tambah Budget'}
				onRightPress={() => navigation.navigate('TopUpScreen')}
				withoutRight />
			<KeyboardAwareScrollView keyboardShouldPersistTaps="always">
				<ScrollView style={{ paddingVertical: 5, paddingHorizontal: 3 }}>
					<InputText
			            label="Judul Iklan"
			            disabled
			            value={ad.title}
			          />
			        <InputText
			        	disabled
			            label="Total Budget"
			            value={ad.total_budget}
			          />
			        <InputText
			        	disabled
			            label="Budget Terpakai"
			            value={ad.used_budget}
			          />
			        <InputText
			        	autoFocus
			        	placeholder={'Nominal tambahan saldo'}
			        	keyboardType={'number-pad'}
			        	onChangeText={amount => setAmount(amount.toString().replace(/\D/g,''))}
			            label="Topup Budget"
			            value={amount.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
			          />
			    </ScrollView>
			</KeyboardAwareScrollView>
		    <View style={{ paddingHorizontal: 20, paddingVertical: 10, backgroundColor: Colors.gray100, elevation: 3 }}>
		    	<Button disabled={parseInt(amount || 0) < 1000} onPress={onSubmit} block>
		    		<Text>Tambah Saldo</Text>
		    	</Button>
		    </View>
	        <Loading visible={loading} />
		</Container>
	)

}

const mapStateToProps = ({smartadv}) => {
  return {
    myAd: smartadv.myAd
  }
};

const mapDispatchToProps = ({smartadv}) => {
  return {
    emptyAd: smartadv.emptyAd,
    getAd: params => smartadv.getAd(params),
    adTopup: data => smartadv.adTopup(data),
    updateAdStatus: data => smartadv.updateAdStatus(data),
    createAd: data => smartadv.createAd(data)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdBalanceForm);