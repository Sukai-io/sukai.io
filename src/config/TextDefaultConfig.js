import * as React from 'react';
import {Text} from 'react-native';
import {Input} from 'native-base';
import {Fonts} from '../themes';

const oldRender = Text.render;
Text.render = function textRender(...args) {
  const origin = oldRender.call(this, ...args);
  return React.cloneElement(origin, {
    style: [{fontFamily: Fonts.type.reg}, origin.props.style],
  });
};

Input.render = function textRender(...args) {
  const origin = oldRender.call(this, ...args);
  return React.cloneElement(origin, {
    style: [{fontFamily: Fonts.type.reg}, origin.props.style],
  });
};
