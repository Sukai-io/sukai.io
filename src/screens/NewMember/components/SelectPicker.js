import React, {memo, useContext} from 'react';
import {Text} from 'react-native';
import styles from '../styles';
import MultiSelect from 'react-native-multiple-select';
import {Fonts, Colors} from '../../../themes';
import {LocalizationContext} from '../../../localization';

const SelectPicker = ({error, label, single = true, selectedItems, ...otherProps}) => {
  const {translations} = useContext(LocalizationContext);

  return (
    <>
      <Text style={styles.pickerLabel}>{label}</Text>
      <MultiSelect
        hideTags
        single={single}
        uniqueKey="id"
        selectText={single ? translations.ACTION_PICK_AN_ITEM : ''}
        searchInputPlaceholderText={translations.ACTION_PICK_AN_ITEM}
        onChangeInput={text => console.log(text)}
        altFontFamily="Quicksand-Regular"
        fontFamily="Quicksand-Regular"
        itemFontFamily="Quicksand-Regular"
        selectedItemFontFamily="Quicksand-Regular"
        tagRemoveIconColor="#CCC"
        tagBorderColor="#CCC"
        tagTextColor="#CCC"
        selectedItemTextColor="#CCC"
        selectedItemIconColor="#CCC"
        itemTextColor="#000"
        displayKey="name"
        searchInputStyle={{color: '#CCC', fontFamily: Fonts.type.base, fontSize: Fonts.size.medium}}
        submitButtonColor={Colors.primary}
        submitButtonText="Submit"
        styleMainWrapper={{
          marginTop: 8,
          marginBottom: 0,
          marginLeft: 17,
          marginRight: 16,
        }}
        styleDropdownMenu={{fontFamily: Fonts.type.base, fontSize: Fonts.size.medium}}
        styleTextDropdown={{
          fontFamily: Fonts.type.base,
          fontSize: Fonts.size.medium,
          color: Colors.gray600,
          paddingLeft: 3,
        }}
        styleTextDropdownSelected={{
          fontSize: Fonts.size.medium,
          color: 'black',
          fontFamily: Fonts.type.base,
          paddingLeft: 2,
        }}
        styleDropdownMenuSubsection={{paddingRight: 0, fontFamily: Fonts.type.base, fontSize: Fonts.size.medium}}
        styleInputGroup={{paddingLeft: 0}}
        selectedItems={selectedItems}
        textInputProps={{autoFocus: false}}
        {...otherProps}
      />
      {error && <Text style={styles.error}>{error}</Text>}
    </>
  );
};

export default memo(SelectPicker);
