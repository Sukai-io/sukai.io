import api from '../ApiClient';
import {pathOr, path} from 'ramda';
import {goBack} from '../../utils/NavigationUtils';
import {showToast} from '../../utils/ToastUtils';
import errorGenerator from '../ErrorGenerator';
import {transformToFormData} from '../ApiHelper';

const getSuccess = data => pathOr(false, ['success'], data);

export default {
  state: {
    seller_history: [],
    buyer_history: [],
  },
  reducers: {
    setSellerHistory(state, data) {
      return {
        ...state,
        seller_history: data?.data?.checkout,
      };
    },
    setBuyerHistory(state, data) {
      return {
        ...state,
        buyer_history: data?.data?.checkout,
      };
    },
  },
  effects: dispatch => ({
    async doCheckout(data) {
      const response = await api.orderCheckout(data);
      const success = getSuccess(response.data);
      const error = path(['data', 'error'], response.data);
      if (response.ok && success) {
        await dispatch.balance.getBalance();
        await dispatch.cart.clearCart();
        goBack();
        showToast(`Transaksi berhasil`, 'OK', 5000);
        return Promise.resolve(response.data);
      }
      setTimeout(() => errorGenerator(response, !success ? error : null), 400);
      return Promise.reject(response);
    },
    async getSellerHistory() {
      const response = await api.listOrder(transformToFormData({type: 'seller'}));
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.order.setSellerHistory(response.data);
        return Promise.resolve(response.data);
      }
    },
    async getBuyerHistory() {
      const response = await api.listOrder(transformToFormData({type: 'buyer'}));
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.order.setBuyerHistory(response.data);
        return Promise.resolve(response.data);
      }
    },
    async doResiInput(data) {
      const response = await api.orderResiInput(transformToFormData(data));
      const success = getSuccess(response.data);
      const error = path(['data', 'error'], response.data);
      if (response.ok && success) {
        await dispatch.order.getSellerHistory();
        goBack();
        showToast(response.data?.data, 'OK', 5000);
        return Promise.resolve(response.data);
      }
      setTimeout(() => errorGenerator(response, !success ? error : null), 400);
      return Promise.reject(response);
    },
    async doConfirm(data) {
      const response = await api.orderConfirm(transformToFormData(data));
      const success = getSuccess(response.data);
      const error = path(['data', 'error'], response.data);
      if (response.ok && success) {
        await dispatch.order.getBuyerHistory();
        goBack();
        showToast(response.data?.data, 'OK', 5000);
        return Promise.resolve(response.data);
      }
      setTimeout(() => errorGenerator(response, !success ? error : null), 400);
      return Promise.reject(response);
    },
  }),
};
