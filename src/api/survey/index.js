import api from '../ApiClient';
import {pathOr, path} from 'ramda';
import {goTo, resetAndGoTo} from '../../utils/NavigationUtils';
import {showToast} from '../../utils/ToastUtils';
import errorGenerator from '../ErrorGenerator';

const getSuccess = data => pathOr(false, ['success'], data);

export default {
  state: {
    questions: [],
  },
  reducers: {
    setQuestions(state, payload) {
      return {
        ...state,
        questions: payload?.data?.question_list.map(item => ({
          ...item,
          answered: '',
        })),
      };
    },
    setSelectedQuestions(state, payload) {
      return {
        ...state,
        questions: state.questions.map(item => (item.id === payload.id ? payload : item)),
      };
    },
  },
  effects: dispatch => ({
    async getSurveyStatus() {
      const response = await api.checkSurveyStatus();
      const success = getSuccess(response.data);
      if (response.ok && success) {
        goTo('Main');
      } else if (response.ok === false && success === false) {
        goTo('Main');
      } else {
        goTo('IntroSurveyScreen');
      }
    },
    async getQuestions() {
      const response = await api.surveyQuestions();
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.survey.setQuestions(response.data);
        return Promise.resolve(response.data);
      }
    },
    async doSaveSurvey(data) {
      const response = await api.saveSurvey(data);
      const success = getSuccess(response.data);
      const error = path(['data', 'error'], response.data);
      if (response.ok && success) {
        resetAndGoTo('HomeDrawer');
        showToast(response.data?.data?.message || '', 'OK', 5000);
        return Promise.resolve(response.data);
      }
      setTimeout(() => errorGenerator(response, !success ? error : null), 400);
      return Promise.reject(response);
    },
  }),
};
