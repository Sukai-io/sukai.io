const colors = {
  white: '#ffffff',
  background: '#1F0808',
  clear: 'rgba(0,0,0,0)',
  facebook: '#003fa3',
  transparent: 'rgba(0,0,0,0)',
  silver: '#F7F7F7',
  steel: '#CCCCCC',
  error: 'rgba(200, 0, 0, 0.8)',
  ricePaper: 'rgba(255,255,255, 0.75)',
  frost: '#D8D8D8',
  cloud: 'rgba(200,200,200, 0.35)',
  windowTint: 'rgba(0, 0, 0, 0.4)',
  panther: '#161616',
  charcoal: '#595959',
  coal: '#2d2d2d',
  bloodOrange: '#fb5f26',
  orange: '#ff970f',
  snow: 'white',
  ember: 'rgba(164, 0, 48, 0.5)',
  fire: '#e73536',
  drawer: 'rgba(30, 30, 29, 0.95)',
  eggplant: '#251a34',
  border: '#483F53',
  banner: '#5F3E63',
  text: '#E0D7E5',

  primary: '#80cbc4',
  darkPrimary: '#4f9a94',

  orange50: '#fff3e0',

  blue200: '#90caf9',
  blueTransparent: 'rgba(144, 202, 249, 0.4)',
  blackTransparent: 'rgba(52, 52, 52, 0.7)',
  grayTransparent: 'rgba(230, 230, 230, 0.4)',

  gray50: '#fafafa',
  gray100: '#f5f5f5',
  gray200: '#eeeeee',
  gray300: '#e0e0e0',
  gray400: '#bdbdbd',
  gray500: '#9e9e9e',
  gray600: '#757575',
  gray700: '#616161',
  gray800: '#424242',
  gray900: '#212121',

  green500: '#4CAF50',
  darkgreen: '#30ad24',
  lightgreen: '#8cff8e'
}

export default colors
