import { StyleSheet } from 'react-native'
import {
  ApplicationStyles, Metrics, Fonts, Colors,
} from '../../../themes'

export default StyleSheet.create({
  container: {
    ...ApplicationStyles.shadow,
    borderRadius: Metrics.doubleBaseMargin,
    padding: Metrics.marginHorizontal,
  },
  subContainer: {
    ...ApplicationStyles.flexBetweenCenter,
    padding: Metrics.baseMargin,
    borderBottomColor: Colors.gray300,
    borderBottomWidth: 0.5,
  },
  amountText: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.bold,
    color: Colors.gray700,
  },
  sub2Container: {
    ...ApplicationStyles.flex,
    paddingVertical: Metrics.section,
  },
  sub3Container: {
    ...ApplicationStyles.flexCenter,
  },
  icon: {
    width: Metrics.icons.medium,
    height: Metrics.icons.medium,
  },
  title: {
    marginTop: Metrics.baseMargin,
    fontSize: Fonts.size.medium,
    color: Colors.gray900,
  },
})
