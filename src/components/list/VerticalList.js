import * as React from 'react';
import {FlatList, FlatListProps, ViewStyle, RefreshControl} from 'react-native';
import {ApplicationStyles} from '../../themes';
import colors from '../../themes/Colors';

type Props = FlatListProps & {
  customStyle: ViewStyle,
  refreshing: Boolean,
  onRefresh: Function,
  withRefreshControl: Boolean,
};

const VerticalList = (props: Props) => {
  const {customStyle, refreshing, onRefresh, withRefreshControl} = props;
  return (
    <FlatList
      style={[ApplicationStyles.flex, customStyle]}
      keyExtractor={(item, index) => index.toString()}
      onEndReachedThreshold={0.5}
      initialNumToRender={6}
      refreshControl={
        withRefreshControl ? (
          <RefreshControl colors={[colors.primary]} refreshing={refreshing} onRefresh={onRefresh} />
        ) : null
      }
      {...props}
    />
  );
};

export default VerticalList;
