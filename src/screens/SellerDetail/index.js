import React, {useMemo, useCallback, useState} from 'react';
import {connect} from 'react-redux';

import {View, Alert, TouchableOpacity} from 'react-native';
import {Container, CardItem, Body, Text, Item, Input} from 'native-base';
import {Card, Button} from 'react-native-elements';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import HeaderBack from '../../components/header/HeaderBack';
import {ApplicationStyles} from '../../themes';
import Horizontal from '../../components/wrapper/Horizontal';
import styles from './styles';
import {formattedDateTime, numeralFormatter} from '../../utils/TextUtils';
import {pathOr} from 'ramda';
import CartItem from '../BuyerDetail/components/CartItem';
import colors from '../../themes/Colors';
import Loader from '../../components/indicator/Loader';
import {Clipboard} from 'react-native';
import {showToast} from '../../utils/ToastUtils';

const SellerDetail = ({navigation, loading, doResiInput}) => {
  const [resi, setResi] = useState(null);
  const detail = useMemo(() => navigation.getParam('detail', null));
  const totalProductPrice = useMemo(
    () =>
      pathOr([], ['products'], detail).reduce(
        (acc, item) => acc + parseInt(item.product_price, 10) * parseInt(item.product_qty, 10),
        0,
      ),
    [detail],
  );
  const totalProductAndShippingPrice = useMemo(() => parseInt(detail?.shipping_fee, 10) + totalProductPrice, [
    totalProductPrice,
    detail,
  ]);
  const handleCopyResi = useCallback(() => {
    if (detail?.no_resi) {
      Clipboard.setString(detail?.no_resi);
      showToast('Copy to Clipboard');
    }
  }, [detail]);

  const handleSubmit = useCallback(() => {
    if (resi) {
      doResiInput({checkout_group_id: detail?.id, confirmation: 1, no_resi: resi});
    } else {
      Alert.alert('Perhatian', 'Mohon input resi terlebih dahulu');
    }
  }, [resi, detail]);
  return (
    <Container>
      <HeaderBack title="Detail Order" withoutRight />
      <Loader loading={loading} />
      <KeyboardAwareScrollView style={styles.container}>
        <View style={styles.status}>
          <Text style={styles.statusText}>{detail?.checkout_status}</Text>
        </View>
        <Card containerStyle={styles.cardContainer}>
          <CardItem>
            <Body>
              <Text style={styles.title}>Informasi Order</Text>
            </Body>
          </CardItem>
          <CardItem>
            <Body>
              <Text selectable>{detail?.checkout_code}</Text>
              <Text selectable note>
                {`Penjual: ${detail?.seller_name}`}
              </Text>
              <Text selectable note>
                {`Waktu Order: ${formattedDateTime(detail?.datecreated)}`}
              </Text>
              <Text selectable note>
                {`Kurir: ${detail?.courier?.toUpperCase()} - ${detail?.courier_service}`}
              </Text>
            </Body>
          </CardItem>
        </Card>
        <Card containerStyle={styles.cardContainer}>
          <CardItem>
            <Body>
              <Text style={styles.title}>Informasi Produk</Text>
            </Body>
          </CardItem>
          <CardItem>
            <Body>
              {pathOr([], ['products'], detail).map((item, index) => (
                <CartItem
                  key={index.toString()}
                  onPress={() =>
                    navigation.navigate('ProductDetailScreen', {
                      id: item.product_id,
                    })
                  }
                  dataSource={item}
                />
              ))}
            </Body>
          </CardItem>
        </Card>
        <Card containerStyle={styles.cardContainer}>
          <CardItem>
            <Body>
              <Text style={styles.title}>Tujuan Pengiriman</Text>
            </Body>
          </CardItem>
          <CardItem>
            <Body>
              <Text selectable>{detail?.member_name}</Text>
              <Text selectable note>
                {detail?.member_shipping_info?.address_location}
              </Text>
              <Text selectable note>
                {`${detail?.member_shipping_info?.address_district}, ${detail?.member_shipping_info?.address_city_name}, ${detail?.member_shipping_info?.address_province_name}`}
              </Text>
              <Text selectable note>
                {detail?.member_shipping_info?.address_zipcode}
              </Text>
            </Body>
          </CardItem>
        </Card>
        <Card containerStyle={styles.cardContainer}>
          <CardItem>
            <Body>
              <Horizontal style={ApplicationStyles.flexCenter}>
                <Text style={styles.totalLeft}>Biaya Pengiriman</Text>
                <Text style={styles.totalRight}>{numeralFormatter(detail?.shipping_fee)}</Text>
              </Horizontal>
              <Horizontal style={styles.grandContainer}>
                <Text style={styles.totalLeft}>Total Pembayaran</Text>
                <Text style={styles.grantTotal}>{numeralFormatter(totalProductAndShippingPrice)}</Text>
              </Horizontal>
            </Body>
          </CardItem>
        </Card>
        <Card containerStyle={styles.cardContainer}>
          <CardItem>
            <Body>
              <Horizontal style={ApplicationStyles.flexCenter}>
                <Text style={styles.totalLeft}>Nomor Resi</Text>
                {detail?.no_resi ? (
                  <TouchableOpacity onPress={handleCopyResi} style={{flex: 1}}>
                    <Text selectable style={styles.resi}>
                      {detail?.no_resi}
                    </Text>
                  </TouchableOpacity>
                ) : (
                  <Item inlineLabel style={{flex: 1}}>
                    <Input
                      onChangeText={setResi}
                      placeholder="Input no resi..."
                      style={[styles.totalLeft, {color: colors.gray800}]}
                      placeholderTextColor={colors.gray400}
                    />
                  </Item>
                )}
              </Horizontal>
            </Body>
          </CardItem>
        </Card>
        {detail?.checkout_status?.toLowerCase() === 'menunggu pengiriman' && (
          <Button
            onPress={handleSubmit}
            raised
            containerStyle={styles.buttonContainer}
            buttonStyle={styles.button}
            title="SUBMIT RESI"
          />
        )}
      </KeyboardAwareScrollView>
    </Container>
  );
};

const mapStateToProps = ({loading}) => {
  return {
    loading: loading.effects.order.doResiInput,
  };
};

const mapDispatchToProps = ({order}) => {
  return {
    doResiInput: data => order.doResiInput(data),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SellerDetail);
