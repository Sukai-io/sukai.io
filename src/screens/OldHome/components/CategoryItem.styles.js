import {StyleSheet} from 'react-native';
import {ApplicationStyles, Metrics, Fonts, Colors} from '../../../themes';

export default StyleSheet.create({
  container: {
    ...ApplicationStyles.flexCenter,
    paddingHorizontal: Metrics.doubleBaseMargin,
    marginBottom: Metrics.doubleBaseMargin,
  },
  icon: {
    width: Metrics.icons.medium,
    height: Metrics.icons.medium,
  },
  title: {
    marginTop: Metrics.baseMargin,
    fontSize: Fonts.size.small,
    color: Colors.gray900,
  },
});
