import React from 'react';
import {ActivityIndicator, View, StyleSheet} from 'react-native';
import colors from '../../themes/Colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF40',
  },
  topContainer: {flex: 1, paddingTop: 16},
  padding: {
    padding: 18,
  },
});

const Loading = ({isTopIndicator}) => (
  <View style={isTopIndicator ? styles.topContainer : styles.container}>
    <ActivityIndicator
      size="large"
      color={colors.primary}
      style={styles.padding}
    />
  </View>
);

export default Loading;
