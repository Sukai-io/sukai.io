import { StyleSheet } from 'react-native'
import {
  Metrics, Colors, Fonts, ApplicationStyles,
} from '../../../themes'

export default StyleSheet.create({
  modal: { margin: 0, justifyContent: 'center' },
  container: {
    backgroundColor: 'white',
  },
  subContainer: {
    padding: Metrics.doubleBaseMargin,
  },
  title: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.bold,
    color: Colors.gray700,
  },
  subtitle: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    color: Colors.gray700,
    marginTop: Metrics.doubleBaseMargin,
  },
  marginTop: {
    marginTop: Metrics.smallMargin,
  },
  subtitleBold: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.bold,
    color: Colors.gray700,
  },
  dropdown: {
    ...ApplicationStyles.flex,
    marginHorizontal: 6,
    marginTop: 0,
  },
  input: {
    fontSize: Fonts.size.medium,
    color: Colors.gray800,
    marginTop: Metrics.doubleBaseMargin,
  },
  buttonContainer: {
    marginTop: Metrics.doubleBaseMargin,
  },
  button: {
    backgroundColor: Colors.bloodOrange,
  },
})
