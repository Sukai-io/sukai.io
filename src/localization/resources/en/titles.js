export default {
  TITLE_ACCESSORIES: 'Accessories',
  TITLE_ADD_NEW_PRODUCT: 'Add New Product',

  TITLE_CLICK_AD: 'Click this Ads & Get your points',
  TITLE_CREATE_AD: 'Create Ads',

  TITLE_DATE: 'Date',
  TITLE_DISPLAY_PROMO: 'Display Promo',
  TITLE_DRAFT: 'Draft',
  TITLE_DONATION: 'DONATION',

  TITLE_EDIT_AD: 'Edit Ads',
  TITLE_EDIT_ADDRESS: 'Edit Address',
  TITLE_ELECTRONICS: 'Electronics',

  TITLE_FAQ: 'FAQ',
  TITLE_FEATURE: 'Our Feature',

  TITLE_HELP: 'Help',
  TITLE_HI_USER: 'Hey $name',
  TITLE_HISTORY: 'History',
  TITLE_HISTORY_BONUS: 'Bonus History',
  TITLE_HISTORY_ORDER: 'Order History',
  TITLE_HISTORY_TOPUP: 'Topup History',
  TITLE_HISTORY_WALLET: 'Wallet History',
  TITLE_HOME: 'Home',
  TITLE_HOME_INTERIOR: 'Home & Interior',

  TITLE_INFORMATION_BANK: 'Bank Information',
  TITLE_INFORMATION_PERSONAL: 'Personal Information',
  TITLE_INFORMATION_USER_TYPE: 'User Type Information',

  TITLE_LANGUAGE: 'Language',

  TITLE_MANAGE_AD: 'Manage Ads',
  TITLE_MARKETPLACE: 'Marketplace',
  TITLE_MOBILE_PHONE: 'Mobilephone',

  TITLE_NFT: 'NFT',

  TITLE_OFFICIAL_STORE: 'OFFICIAL STORE',

  TITLE_PRODUCT_LIST: 'Product List',
  TITLE_PROFILE: 'Profile',

  TITLE_RECEIVED: 'Received',
  TITLE_REGISTRATION: 'Registration',

  TITLE_SMART_ADV: 'Smart Adv',
  TITLE_SHOES: 'Shoes',
  TITLE_SHOW_ALL: 'Show All',
  TITLE_SUKAI_WALLET: 'Sukai Wallet',

  TITLE_TRANSFER: 'Transfer',

  TITLE_UPLOAD_AD_BANNER: 'Upload Ads Banner',

  TITLE_WITHDRAW: 'Withdraw',

  TITLE_WALLET: 'My Point',

  TITLE_WELCOME: 'Welcome to SUKAI',
  
  TITLE_YOUR_WALLET: 'Your Wallet Point',
};
