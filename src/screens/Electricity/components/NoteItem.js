import React from 'react'
import { Text, View } from 'react-native'
import styles from './NoteItem.styles'

const NoteItem = () => (
  <View style={styles.container}>
    <Text style={styles.title}>Keterangan</Text>
    <Text>
      <Text style={styles.desc}>1. Informasi kode yang Anda bayar akan dikirimkan </Text>
      <Text style={styles.descBold}>maksimal 2x24 jam</Text>
    </Text>
    <Text>
      <Text style={styles.desc}>2. Pembelian token listrik </Text>
      <Text style={styles.descBold}>tidak dapat dilakukan pada jam 23:00-00:59 WIB</Text>
    </Text>
  </View>
)

export default NoteItem
