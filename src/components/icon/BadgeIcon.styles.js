import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  notifBadge: {
    width: 10,
    height: 10,
    borderRadius: 5,
    backgroundColor: 'red',
    top: 3,
    right: -3,
    position: 'absolute',
  },
});
