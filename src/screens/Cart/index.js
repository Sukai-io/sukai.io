/* eslint-disable react/prefer-stateless-function */
import React, {Component} from 'react';
import {connect} from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
import {Container, Text} from 'native-base';

// Styles
import styles from './styles';
import HeaderBack from '../../components/header/HeaderBack';
import CartItem from './components/CartItem';
import VerticalList from '../../components/list/VerticalList';
import TotalItem from './components/TotalItem';
import {numeralFormatter} from '../../utils/TextUtils';
import { LocalizationContext } from '../../localization';

type Props = {
  navigation: any,
  carts: Array<Object>,
  increaseQtyCart: Function,
  decreaseQtyCart: Function,
  deleteFromCart: Function,
};

class CartScreen extends Component<Props> {
  static contextType = LocalizationContext;

  render() {
    const {
      navigation,
      carts,
      increaseQtyCart,
      decreaseQtyCart,
      deleteFromCart,
    } = this.props;
    const {translations} = this.context
    const totalPrice = carts.reduce((acc, item) => acc + item.cart_price, 0);
    const totalQty = carts.reduce((acc, item) => acc + item.cart_qty, 0);
    return (
      <Container style={styles.mainContainer}>
        <HeaderBack withoutRight title="Shopping Cart" />
        {carts.length === 0 ? (
          <Text style={styles.empty}>{translations.LABEL_CART_EMPTY}</Text>
        ) : (
          <Container>
            <VerticalList
              data={carts}
              renderItem={({item}) => {
                return (
                  <CartItem
                    name={item.product_name}
                    price={`Rp. ${numeralFormatter(item.cart_price)}`}
                    qty={item.cart_qty}
                    uri={item.product_thumbnail_url}
                    seller={item.seller}
                    onPress={() => navigation.navigate('ProductDetailScreen')}
                    onPlus={() => increaseQtyCart(item.id)}
                    onMinus={() => decreaseQtyCart(item.id)}
                    onDelete={() => deleteFromCart(item)}
                  />
                );
              }}
            />
            <TotalItem
              onCheckout={() => navigation.navigate('CheckoutScreen')}
              totalPrice={`Rp. ${numeralFormatter(totalPrice)}`}
              totalQty={totalQty}
            />
          </Container>
        )}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  carts: state.cart.carts,
});

const mapDispatchToProps = ({cart}) => {
  return {
    increaseQtyCart: id => cart.increaseQtyCart(id),
    decreaseQtyCart: id => cart.decreaseQtyCart(id),
    deleteFromCart: item => cart.deleteFromCart(item),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CartScreen);
