import {StyleSheet} from 'react-native';
import color from "color"
import {ApplicationStyles, Metrics, Fonts, Colors} from '../../themes';

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  list: {
    marginLeft: Metrics.baseMargin,
    marginRight: Metrics.baseMargin,
    paddingVertical: Metrics.doubleBaseMargin,
    borderBottomColor: Colors.gray300,
    borderBottomWidth: 0.5,
  },
  date: {
    ...ApplicationStyles.flex,
    fontSize: Fonts.size.medium,
    color: Colors.gray600,
    marginTop: 8,
  },
  amount: {
    fontSize: Fonts.size.medium,
    color: Colors.primary,
    textAlign: 'center',
    marginLeft: 16,
  },
  activity: {
    ...ApplicationStyles.flex,
    fontSize: Fonts.size.medium,
    color: Colors.gray700,
    textAlign: 'right',
  },
  success: {
    fontSize: Fonts.size.small,
    color: Colors.green500,
    textAlign: 'right',
  },
  activeTabText: {
    fontSize: Fonts.size.regular,
    color: Colors.snow,
    fontFamily: Fonts.type.bold,
  },
  tabText: {
    fontSize: Fonts.size.regular,
    color: color(Colors.primary).lighten(0.3).hex(),
    fontFamily: Fonts.type.bold,
  },
  desc: {
    fontSize: 14,
    color: Colors.gray800,
  },
});
