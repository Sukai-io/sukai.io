import React from 'react';
import {Platform} from 'react-native';
import {Radio, Text} from 'native-base';
import styles from './UserTypeItem.styles';
import Horizontal from '../../../components/wrapper/Horizontal';
import FlatTouchables from '../../../components/buttons/FlatTouchables';
import {Icon} from 'react-native-elements';
import { Colors } from '../../../themes';

type Props = {
  onPress: Function,
  onInfoPress: Function,
  selected: Boolean,
  desc: String,
};

const platform = Platform.OS;

const UserTypeItem = ({selected, onPress, desc, onInfoPress}: Props) => (
  <FlatTouchables onPress={onPress}>
    <Horizontal style={styles.container}>
      <Radio selected={selected} onPress={onPress} />
      <Text style={[styles.description, styles.subContainer, {paddingLeft: platform === 'ios' && !selected ? 10 : 0}]}>
        {desc}
      </Text>
      <Icon raised name="help" type="ionicon" color={Colors.darkPrimary} size={10} onPress={onInfoPress} />
    </Horizontal>
  </FlatTouchables>
);

export default UserTypeItem;
