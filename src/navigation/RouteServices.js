/* eslint-disable no-underscore-dangle */

import { NavigationActions } from 'react-navigation';

let _navigator;

const setTopLevelNavigator = navigatorRef => {
  _navigator = navigatorRef;
};

const navigate = (routeName, params) => {
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    }),
  );
};

const back = () => {
  _navigator.dispatch(NavigationActions.back());
};

const dispatch = action => {
  _navigator.dispatch(action);
};

// add other navigation functions that you need and export them

export default {
  navigate,
  dispatch,
  setTopLevelNavigator,
  back,
};
