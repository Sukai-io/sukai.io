import { StyleSheet } from 'react-native';
import { Metrics, Fonts, Colors } from '../../../themes';

export default StyleSheet.create({
  container: {
    elevation: 5,
    backgroundColor: Colors.snow,
    borderRadius: Metrics.smallMargin,
    borderColor: Colors.blue200,
    borderWidth: 0.5,
    padding: Metrics.smallMargin,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 35,
    marginHorizontal: Metrics.icons.tiny,
    marginTop: Metrics.baseMargin,
  },
  rightView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  divider: {
    height: 15,
    borderWidth: 0.5,
  },
  textLeft: {
    fontFamily: Fonts.type.bold,
    fontSize: Fonts.size.medium,
    marginLeft: Metrics.baseMargin, 
  },
  textRight: {
    fontSize: Fonts.size.mini,
    color: Colors.bloodOrange,
    marginHorizontal: Metrics.baseMargin,
  },
});
