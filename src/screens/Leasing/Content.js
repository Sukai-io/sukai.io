import React, {Component} from 'react';
import {View} from 'react-native';
import {connect} from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import {Container, Icon, Item, Input, Text} from 'native-base';
import {Button} from 'react-native-elements';
import {Dropdown} from 'react-native-material-dropdown';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {FormikProps} from 'formik';
import styles from './styles';
import HeaderBack from '../../components/header/HeaderBack';
import Horizontal from '../../components/wrapper/Horizontal';
import {Colors, Fonts, Metrics} from '../../themes';
import PaymentModal from '../Checkout/components/PaymentModal';
import {FormValues} from './types';
import Loading from '../../components/indicator/Loading';

type Props = FormikProps<FormValues> & {
  loading: Boolean,
  loadingCheckout: Boolean,
  providerList: Array<Object>,
  providerPriceList: Array<Object>,
  getProvider: Function,
  getPriceList: Function,
  doClear: Function,
};

class Content extends Component<Props> {
  state = {
    showPayment: false,
  };

  componentDidMount() {
    const {doClear, getProvider} = this.props;
    doClear();
    getProvider({type: 'postpaid'});
  }

  handlePaymentVisibility = value => {
    this.setState({showPayment: value});
  };

  handleProviderValue = value => {
    const {setFieldValue, getPriceList} = this.props;
    setFieldValue('ppob_provider', value);
    getPriceList({provider: value});
  };

  render() {
    const {showPayment} = this.state;
    const {
      handleSubmit,
      setFieldTouched,
      setFieldValue,
      errors,
      touched,
      providerList,
      providerPriceList,
      loadingCheckout,
      loading,
    } = this.props;
    return (
      <Container style={styles.mainContainer}>
        <PaymentModal
          isVisible={showPayment}
          onBackButtonPress={() => this.handlePaymentVisibility(false)}
          onBackdropPress={() => this.handlePaymentVisibility(false)}
        />
        <HeaderBack withoutRight title="Leasing" />
        <KeyboardAwareScrollView contentContainerStyle={styles.container}>
          <View style={styles.firstSubContainer}>
            <Dropdown
              containerStyle={styles.dropdownContainer}
              label="Kode Produk Multifinance"
              data={providerList}
              dropdownOffset={styles.pickerOffset}
              itemTextStyle={styles.dropdownItem}
              itemColor={Colors.gray600}
              fontSize={Fonts.size.medium}
              textColor={Colors.primary}
              baseColor={Colors.gray600}
              onChangeText={this.handleProviderValue}
            />
            {errors.ppob_provider && touched.ppob_provider && (
              <Text style={styles.error}>{errors.ppob_provider}</Text>
            )}
            {loading && <Loading isTopIndicator />}
            {!loading && providerPriceList.length > 0 && (
              <>
                <Dropdown
                  containerStyle={styles.dropdownContainer}
                  label="Kode Provider"
                  data={providerPriceList
                    .filter(item => item.type === 'postpaid')
                    .map(o => ({
                      label: o.name,
                      value: o.code,
                    }))}
                  dropdownOffset={styles.pickerOffset}
                  itemTextStyle={styles.dropdownItem}
                  itemColor={Colors.gray600}
                  fontSize={Fonts.size.medium}
                  textColor={Colors.primary}
                  baseColor={Colors.gray600}
                  onChangeText={value => setFieldValue('ppob_code', value)}
                />
                {errors.ppob_code && touched.ppob_code && (
                  <Text style={styles.error}>{errors.ppob_code}</Text>
                )}
                <Text
                  style={[styles.titleText, {marginTop: Metrics.baseMargin}]}>
                  No. Pelanggan
                </Text>
                <Horizontal style={{alignItems: 'center'}}>
                  <Icon
                    name="waves"
                    type="MaterialCommunityIcons"
                    style={styles.icon}
                  />
                  <Item style={styles.inputContainer}>
                    <Input
                      keyboardType="number-pad"
                      onBlur={() => setFieldTouched('ppob_number')}
                      onChangeText={value =>
                        setFieldValue('ppob_number', value)
                      }
                    />
                  </Item>
                </Horizontal>
                {errors.ppob_number && touched.ppob_number && (
                  <Text style={styles.error}>{errors.ppob_number}</Text>
                )}
              </>
            )}
          </View>
          <Button
            loading={loadingCheckout}
            disabled={loadingCheckout}
            onPress={handleSubmit}
            raised
            containerStyle={styles.buttonContainer}
            buttonStyle={styles.button}
            title="CEK NOMOR LEASING"
          />
        </KeyboardAwareScrollView>
      </Container>
    );
  }
}

const mapStateToProps = ({loading, ppob}) => {
  return {
    loading: loading.effects.ppob.getProviderPrice,
    loadingCheckout: loading.effects.ppob.doCheckout,
    providerList: ppob.provider_list,
    providerPriceList: ppob.provider_price_list,
  };
};

const mapDispatchToProps = ({ppob}) => {
  return {
    getProvider: data => ppob.getProvider(data),
    getPriceList: data => ppob.getProviderPrice(data),
    doClear: () => ppob.clear(),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Content);
