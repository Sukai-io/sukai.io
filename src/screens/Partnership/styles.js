import { StyleSheet } from 'react-native'
import {
  ApplicationStyles, Metrics, Fonts, Colors,
} from '../../themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    ...ApplicationStyles.flex,
    backgroundColor: Colors.gray100,
    padding: Metrics.baseMargin,
  },
  cardContainer: {
    ...ApplicationStyles.shadow,
    borderRadius: Metrics.doubleBaseMargin,
    padding: Metrics.section,
    alignItems: 'center',
  },
  subTitle: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.bold,
    color: Colors.gray700,
  },
  title: {
    fontSize: Fonts.size.h5,
    fontFamily: Fonts.type.bold,
    color: Colors.gray700,
    marginTop: Metrics.smallMargin,
  },
  buttonContainer: {
    marginTop: Metrics.doubleBaseMargin,
  },
  button: {
    backgroundColor: Colors.bloodOrange,
  },
})
