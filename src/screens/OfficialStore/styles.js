import {StyleSheet} from 'react-native';
import {ApplicationStyles, Metrics, Colors, Fonts} from '../../themes';

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
    paddingTop: Metrics.baseMargin,
    backgroundColor: Colors.gray100,
  },
  title: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.bold,
    color: Colors.gray700,
    marginBottom: Metrics.doubleBaseMargin,
  },
  cardContainer: {
    ...ApplicationStyles.flex,
    padding: Metrics.doubleBaseMargin,
    marginRight: 0,
    marginLeft: 0,
    marginTop: 0,
    marginBottom: Metrics.doubleBaseMargin,
  },
});
