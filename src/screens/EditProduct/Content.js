import React, {useCallback, useContext} from 'react';
import FastImage from 'react-native-fast-image';
import {useSelector, shallowEqual} from 'react-redux';
import ImagePicker from 'react-native-image-picker';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import {Container, Text, Body, CardItem, Label} from 'native-base';
import {Button, Card} from 'react-native-elements';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Props} from './Props';
import {imageResizer} from '../../utils/ImageUtils';
import InputText from '../NewMember/components/InputText';
import styles from '../NewProduct/styles';
import SelectPicker from '../NewProduct/components/SelectPicker';
import Loading from '../../components/indicator/Loading';
import HeaderProfile from '../EditProfile/components/HeaderProfile';
import Loader from '../../components/indicator/Loader';
import { LocalizationContext } from '../../localization';

const Content = ({
  values,
  touched,
  errors,
  setFieldValue,
  handleChange,
  handleBlur,
  handleSubmit,
}: Props) => {
  const {translations} = useContext(LocalizationContext)
  const productCategories = useSelector(
    ({product: {product_category_list}}) => product_category_list,
    shallowEqual,
  );
  const loadingGet = useSelector(
    ({loading}) => loading.effects.product.getProductDetail,
    shallowEqual,
  );
  const loadingPost = useSelector(
    ({loading}) => loading.effects.product.postNewProduct,
    shallowEqual,
  );
  const handleImagePicker = useCallback(() => {
    ImagePicker.showImagePicker(null, async response => {
      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        console.log(response.fileSize);
        const uri = await imageResizer(response.uri);
        const imgSource = {
          data: response.data,
          name: response.fileName,
          type: 'image/jpeg',
          uri,
          path: response.path,
          width: response.width,
          height: response.height,
          fileSize: response.fileSize,
          isVertical: response.isVertical,
          originalRotation: response.originalRotation,
        };
        setFieldValue('product_image', imgSource);
      }
    });
  }, []);
  return (
    <Container style={styles.mainContainer}>
      <HeaderProfile title={translations.ACTION_EDIT} onSave={handleSubmit} />
      <Loader loading={loadingPost} />
      {loadingGet ? (
        <Loading />
      ) : (
        <KeyboardAwareScrollView
          style={styles.container}
          keyboardShouldPersistTaps="always">
          <Card containerStyle={styles.cardContainer}>
            <CardItem>
              <Body>
                <Text style={styles.title}>{translations.LABEL_STOCK_INFO}</Text>
              </Body>
            </CardItem>
            <InputText
              label={`${translations.LABEL_STOCK_ITEM} *`}
              value={values.product_stock}
              error={touched.product_stock && errors.product_stock}
              onBlur={handleBlur('product_stock')}
              onChangeText={handleChange('product_stock')}
            />
          </Card>

          <Card containerStyle={styles.cardContainer}>
            <CardItem>
              <Body>
                <Text style={styles.title}>{translations.LABEL_PRODUCT_INFO}</Text>
              </Body>
            </CardItem>
            <InputText
              label={`${translations.LABEL_PRODUCT_NAME} *`}
              value={values.product_name}
              error={touched.product_name && errors.product_name}
              onBlur={handleBlur('product_name')}
              onChangeText={handleChange('product_name')}
            />
            <InputText
              label={`${translations.LABEL_PRODUCT_DETAIL} *`}
              value={values.product_detail}
              error={touched.product_detail && errors.product_detail}
              onBlur={handleBlur('product_detail')}
              onChangeText={handleChange('product_detail')}
            />
            <InputText
              label={`${translations.LABEL_WEIGHT} * `}
              value={values.product_weight}
              error={touched.product_weight && errors.product_weight}
              keyboardType="number-pad"
              onBlur={handleBlur('product_weight')}
              onChangeText={handleChange('product_weight')}
            />
            <InputText
              label={`${translations.LABEL_PRODUCT_PRICE} *`}
              value={values.product_price}
              error={touched.product_price && errors.product_price}
              keyboardType="number-pad"
              onBlur={handleBlur('product_price')}
              onChangeText={handleChange('product_price')}
            />
            <InputText
              label={`${translations.LABEL_PRODUCT_COMMISION} *`}
              value={values.product_commission}
              error={touched.product_commission && errors.product_commission}
              keyboardType="number-pad"
              onBlur={handleBlur('product_commission')}
              onChangeText={handleChange('product_commission')}
            />
            <CardItem>
              <Body>
                <Label style={styles.label}>{translations.LABEL_IMAGE} *</Label>
                {values.product_image?.uri && (
                  <FastImage
                    resizeMode="cover"
                    source={{uri: values.product_image?.uri}}
                    style={styles.image}
                  />
                )}
                <Button
                  onPress={handleImagePicker}
                  raised
                  containerStyle={styles.buttonImageContainer}
                  buttonStyle={styles.buttonImage}
                  titleStyle={styles.buttonTitle}
                  icon={{
                    name: 'ios-image',
                    type: 'ionicon',
                    color: 'white',
                    size: 20,
                  }}
                  title={translations.ACTION_CHOOSE_IMAGE.toUpperCase()}
                />
              </Body>
            </CardItem>
            {errors.product_image && (
              <Text style={styles.error}>{errors.product_image}</Text>
            )}
          </Card>
          <Card containerStyle={styles.cardContainer}>
            <CardItem>
              <Body>
                <Text style={styles.title}>{translations.LABEL_CATEGORY_INFO}</Text>
              </Body>
            </CardItem>
            <SelectPicker
              selectText={translations.LABEL_PRODUCT_CATEGORY}
              items={productCategories.map(item => ({
                id: item.id,
                name: item.category_name,
              }))}
              selectedItems={values.product_category}
              onSelectedItemsChange={handleChange('product_category')}
              error={touched.product_category && errors.product_category}
            />
          </Card>
        </KeyboardAwareScrollView>
      )}
    </Container>
  );
};

export default Content;
