import { StyleSheet } from "react-native";
import { ApplicationStyles, Metrics, Colors, Fonts } from "../../../themes";

export default StyleSheet.create({
  headerContainer: {
    ...ApplicationStyles.rowCenterBetween,
    paddingHorizontal: Metrics.baseMargin,
    marginBottom: Metrics.smallMargin,
    alignItems: 'center',
  },
  textFeature: {
    fontFamily: Fonts.type.bold,
    color: Colors.facebook
  },
  textShowAll: {
    fontFamily: Fonts.type.base,
    fontSize: Fonts.size.small,
    color: Colors.gray700,
  },
  bodyLeftView: {
    backgroundColor: Colors.blue200,
    borderRightWidth: 0.5,
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    borderRightColor: Colors.snow,
    borderTopColor: Colors.snow,
    borderBottomColor: Colors.snow,
    paddingVertical: 7,
    paddingHorizontal: Metrics.baseMargin,
    elevation: 5,
    borderTopRightRadius: Metrics.doubleBaseMargin,
    borderBottomRightRadius: Metrics.doubleBaseMargin,
    marginRight: Metrics.baseMargin,
  },
  bodyRightView: {
    backgroundColor: Colors.blue200,
    borderLeftWidth: 0.5,
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    borderLeftColor: Colors.snow,
    borderTopColor: Colors.snow,
    borderBottomColor: Colors.snow,
    paddingVertical: 7,
    paddingHorizontal: Metrics.baseMargin,
    elevation: 5,
    borderTopLeftRadius: Metrics.doubleBaseMargin,
    borderBottomLeftRadius: Metrics.doubleBaseMargin,
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
  },
  iconStar: {
    fontSize: Fonts.size.h5,
  },
  iconArrow: {
    fontSize: Fonts.size.medium,
    color: Colors.grayTransparent,
  },
  viewFeatureText: {
    paddingHorizontal: Metrics.smallMargin,
    flexDirection: 'row',
    alignItems: 'center'
  },
  featureText: {
    fontFamily: Fonts.type.bold,
    fontSize: Fonts.size.small,
    color: Colors.gray800
  },
  iconFeature: {
    width: Fonts.size.regular,
    height: Fonts.size.regular,
    fontSize: Fonts.size.regular,
    marginRight: Metrics.smallMargin,
  },
  divider: {
    height: 15,
    borderColor: Colors.snow,
    borderWidth: 0.5
  }
});