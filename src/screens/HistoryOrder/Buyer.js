import React, {useEffect, useState, useCallback} from 'react';
import {connect} from 'react-redux';
import CartItem from './components/CartItem';
import VerticalList from '../../components/list/VerticalList';

const Buyer = ({navigation, loading, buyerHistory, getBuyerHistory}) => {
  const handleRefresh = useCallback(() => {
    getBuyerHistory();
  }, [])
  useEffect(() => {
    getBuyerHistory();
  }, []);
  return (
    <VerticalList
      data={buyerHistory}
      withRefreshControl
      refreshing={loading}
      onRefresh={handleRefresh}
      renderItem={({item}) => (
        <CartItem
          dataSource={item}
          onPress={() => navigation.navigate('BuyerDetailScreen', {detail: item, type: 'buyer'})}
        />
      )}
    />
  );
};

const mapStateToProps = ({loading, order}) => {
  return {
    loading: loading.effects.order.getBuyerHistory,
    buyerHistory: order.buyer_history,
  };
};

const mapDispatchToProps = ({order}) => {
  return {
    getBuyerHistory: () => order.getBuyerHistory(),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Buyer);
