export type FormValues = {
  title: String,
  description: String,
  url: String,
  digcom_image: Object,
};
