import React, {useState, useEffect, useContext} from 'React';
import _ from 'lodash';
import {Card} from 'react-native-elements';
import {View, TouchableOpacity, Image, Dimensions, ScrollView, Switch} from 'react-native';
import {Text, Container, Button, Right, CardItem, Body} from 'native-base';
import {Fonts, Colors, Metrics, ApplicationStyles} from '../../themes';
import {connect} from 'react-redux';
import HeaderBack from '../../components/header/HeaderBack';
import InputText from '../NewMember/components/InputText';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import ActionSheet from 'react-native-actionsheet';
import ImagePicker from 'react-native-image-crop-picker';
import Loading from '../../components/Loading';
import {LocalizationContext} from '../../localization';
import SelectPicker from '../NewMember/components/SelectPicker';
import {StyleSheet} from 'react-native';
import Horizontal from '../../components/wrapper/Horizontal';
import {useMemo} from 'React';
import {numeralFormatter} from '../../utils/TextUtils';
import {transformToFormData} from '../../api/ApiHelper';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Metrics.baseMargin,
    backgroundColor: Colors.gray100,
  },
  cardContainer: {
    ...ApplicationStyles.shadow,
    ...ApplicationStyles.flex,
    padding: Metrics.smallMargin,
    marginRight: 0,
    marginLeft: 0,
    marginTop: 0,
    marginBottom: Metrics.doubleBaseMargin,
    paddingBottom: Metrics.doubleBaseMargin,
  },
  cardSubContainer: {
    ...ApplicationStyles.flexBetweenCenter,
    paddingHorizontal: 17,
  },
  title: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.bold,
    color: Colors.gray700,
    marginBottom: 0,
  },
});

const AdForm = ({
  budgets,
  currencyPoints,
  surveyQuestions,
  paymentOptions,
  myAd,
  getAd,
  emptyAd,
  updateAdStatus,
  createAd,
  editAd,
  getBudgets,
  getCurrencyPoints,
  getPaymentOptions,
  getBankAccounts,
  getQuestions,
  navigation: {
    state: {
      params: {id},
      key,
    },
  },
}) => {
  const {translations} = useContext(LocalizationContext);
  let [values, setValues] = useState({
    // advert_id: id,
    advert_title: null,
    advert_desc: null,
    advert_budget: null,
    advert_img: null,
    advert_url: null,
    advert_poin: null,
    advert_duration: null,
    advert_target: null,
    advert_answer: null,
    range_from: null,
    range_to: null,
    advert_amount_trf: null,
    advert_currency: null,
    advert_payment: null,
  });

  let [ad, setAd] = useState({});
  let [active, setActive] = useState(false);

  const toggleActive = () => {
    updateAdStatus({
      advert_id: id,
      advert_status: active ? 0 : 1,
    });
    setActive(!active);
  };

  let [submitable, setSubmitable] = useState(false);
  let [loading, setLoading] = useState(false);

  let [selectedImage, setSelectedImage] = useState(null);

  const totalCost = useMemo(() => {
    let result = 0;
    if (values.advert_poin || values.advert_budget) {
      result = parseFloat(values.advert_budget) * parseFloat(values.advert_poin);
    }
    return result;
  }, [values.advert_poin, values.advert_budget]);

  const paymentAddress = useMemo(() => {
    let result = '';
    if (values.advert_payment) {
      const paymentObject = _.get(paymentOptions, [values.advert_payment, 'data'], null);
      if (paymentObject) {
        if (paymentObject.name) {
          result = result.concat(`${paymentObject.name}`);
        }
        if (paymentObject.account) {
          if (paymentObject.name) {
            result = result.concat('\n');
          }
          result = result.concat(`${paymentObject.account}`);
        }
        if (paymentObject.account_name) {
          if (paymentObject.account) {
            result = result.concat('\n');
          }
          result = result.concat(`${paymentObject.account_name}`);
        }
      }
    }
    return result;
  }, [values.advert_payment, paymentOptions]);

  const evalValues = values => {
    let newSubmitable =
      values.advert_title &&
      values.advert_desc &&
      values.advert_budget &&
      values.advert_poin &&
      values.advert_duration &&
      values.advert_target &&
      values.advert_amount_trf &&
      values.advert_currency &&
      values.advert_payment &&
      selectedImage != null;

    if (values.advert_target) {
      const targetObject = surveyQuestions.find(item => item.id === values.advert_target);
      if (targetObject) {
        if (targetObject.filter_type === 'range') {
          newSubmitable = newSubmitable && values.range_from && values.range_to;
        } else {
          newSubmitable = newSubmitable && values.advert_answer;
        }
      }
    }
    setSubmitable(newSubmitable);
    console.log({newSubmitable});
  };

  const handleChange = value => {
    let newValues = {...values, ...value};
    setValues(newValues);
    // if (newValues.advert_budget) newValues.advert_budget = newValues.advert_budget.toString().replace(/\D/g, '');
    // setValues(newValues);
    evalValues(newValues);
  };

  const {width, height} = Dimensions.get('window');

  const uploadOption = index => {
    index == 0 &&
      ImagePicker.openCamera({
        width: 480,
        height: 480,
        cropping: true,
      }).then(image => {
        setSelectedImage(image);
      });
    index == 1 &&
      ImagePicker.openPicker({
        width: 480,
        height: 480,
        cropping: true,
      }).then(image => {
        setSelectedImage(image);
      });
  };

  const onSave = () => {
    let {advert_answer, ...restData} = values;
    if (selectedImage) {
      restData.advert_img = {
        uri: selectedImage.path,
        name: `${Math.random()}.${selectedImage.mime.split('/')[1]}`,
        type: selectedImage.mime,
      };
    }
    console.log('restData', restData);
    setLoading(true);
    const formData = transformToFormData(restData);
    if (Array.isArray(advert_answer)) {
      advert_answer.forEach(item => {
        formData.append('advert_answer[]', item);
      });
    } else {
      formData.append('advert_answer', advert_answer);
    }
    !id && createAd(formData).then().catch(() => setLoading(false));
    if (id) {
      let editData = {
        advert_title_edit: restData.advert_title,
        advert_desc_edit: restData.advert_desc,
        advert_id_edit: id,
      };
      if (selectedImage) {
        editData.advert_img_edit = restData.advert_img;
      }
      editAd({...editData});
    }
  };

  const renderQuestionForm = () => {
    const questionObject = surveyQuestions.find(item => item.id === values.advert_target);
    if (questionObject) {
      switch (questionObject.filter_type) {
        case 'options':
          return (
            <SelectPicker
              label={`Choose ${questionObject.question} *`}
              items={questionObject.answer.map(item => ({id: item, name: item}))}
              selectedItems={values.advert_answer ? [values.advert_answer] : []}
              onSelectedItemsChange={value => handleChange({advert_answer: value[0]})}
            />
          );
        case 'options_multiple':
          return (
            <SelectPicker
              single={false}
              label={`Choose ${questionObject.question} *`}
              items={questionObject.answer.map(item => ({id: item, name: item}))}
              selectedItems={values.advert_answer ? [...values.advert_answer] : []}
              onSelectedItemsChange={value => handleChange({advert_answer: value})}
            />
          );
        case 'range':
          return (
            <Horizontal>
              <View style={{flex: 1}}>
                <InputText
                  label={`Range From *`}
                  onChangeText={value => handleChange({range_from: value})}
                  value={values.range_from}
                  keyboardType="number-pad"
                />
              </View>
              <View style={{flex: 1}}>
                <InputText
                  label={`Range To *`}
                  onChangeText={value => handleChange({range_to: value})}
                  value={values.range_to}
                  keyboardType="number-pad"
                />
              </View>
            </Horizontal>
          );
        default:
          return null;
      }
    }
    return null;
  };

  useEffect(() => {
    getBudgets();
    getCurrencyPoints();
    getQuestions();
  }, []);

  useEffect(() => {
    if (totalCost > 0) {
      handleChange({advert_amount_trf: String(totalCost)});
    }
  }, [totalCost]);

  useEffect(() => {
    if (values.advert_currency) {
      getPaymentOptions({currency: values.advert_currency});
    }
  }, [values.advert_currency]);

  useEffect(() => {
    id && setLoading(true);
    id &&
      getAd({
        id,
        type: 'mine',
        limit: 1,
        offset: 0,
      });
  }, [id, key]);

  useEffect(() => {
    id &&
      myAd &&
      setValues({
        advert_id: myAd.id,
        advert_title: myAd.title,
        advert_desc: myAd.description,
        advert_budget: myAd.total_budget,
        advert_thumbnail: myAd.image_thumbnail,
      });
    id && myAd && setLoading(false);
    id && myAd && setAd(myAd);
    id && myAd && setActive(myAd.status === 'ACTIVE');
    id && myAd && setSubmitable(true);
    emptyAd();
  }, [myAd]);

  useEffect(() => {
    evalValues(values);
  }, [key, selectedImage]);

  console.log('values', values);

  return (
    <Container>
      <HeaderBack
        title={id ? translations.TITLE_EDIT_AD : translations.TITLE_CREATE_AD}
        onRightPress={() => navigation.navigate('TopUpScreen')}
        withoutRight={!ad.id}
        customRightAny={
          !ad.id
            ? null
            : () => (
                <Right style={{alignItems: 'center'}}>
                  <View style={{marginBottom: -2}}>
                    <Switch
                      onValueChange={toggleActive}
                      thumbColor={active ? Colors.lightgreen : Colors.gray200}
                      trackColor={Colors.lightgreen}
                      value={active}
                    />
                  </View>
                  <TouchableOpacity onPress={toggleActive}>
                    <Text
                      style={{
                        fontWeight: '600',
                        color: active ? Colors.lightgreen : Colors.gray200,
                        marginLeft: 5,
                        paddingBottom: 2,
                        width: 70,
                      }}>
                      {active ? 'Active' : 'Inactive'}
                    </Text>
                  </TouchableOpacity>
                </Right>
              )
        }
      />
      <KeyboardAwareScrollView keyboardShouldPersistTaps="always" style={styles.container}>
        <Card containerStyle={styles.cardContainer}>
          <CardItem>
            <Body>
              <Text style={styles.title}>Ads General Info</Text>
            </Body>
          </CardItem>
          <InputText
            label={translations.LABEL_AD_TITLE + '*'}
            onChangeText={value => handleChange({advert_title: value})}
            value={values.advert_title}
          />
          <InputText
            label={translations.LABEL_AD_DESC + '*'}
            multiline
            numberOfLines={4}
            style={{textAlignVertical: 'top'}}
            onChangeText={value => handleChange({advert_desc: value})}
            value={values.advert_desc}
          />
          <InputText
            label="Ad URL"
            onChangeText={value => handleChange({advert_url: value})}
            placeholder="https://"
            value={values.advert_url}
          />
          {/* <InputText
            disabled={id}
            label={translations.LABEL_AD_BUDGET}
            keyboardType={'number-pad'}
            onChangeText={value => handleChange({advert_budget: value})}
            value={(values.advert_budget || '')
              .toString()
              .replace(/\D/g, '')
              .replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
          /> */}
          <SelectPicker
            label="Budget *"
            items={budgets.map(item => ({id: item.budget, name: item.budget}))}
            selectedItems={values.advert_budget ? [values.advert_budget] : []}
            onSelectedItemsChange={value => {
              handleChange({
                advert_budget: value[0],
                advert_duration: budgets.find(item => item.budget === value[0]).duration,
              });
            }}
          />
          {values.advert_duration && (
            <InputText
              label="Duration (days)"
              disabled
              value={String(values.advert_duration ? `${values.advert_duration} days` : '')}
            />
          )}
          <SelectPicker
            label="Currency Point *"
            items={currencyPoints.map(item => ({id: item.name, name: `${item.name} (${item.poin})`}))}
            selectedItems={values.advert_currency ? [values.advert_currency] : []}
            onSelectedItemsChange={value => {
              const result = currencyPoints.find(item => item.name === value[0]);
              handleChange({
                advert_currency: value[0],
                advert_poin: result.poin,
                advert_payment: result.payment,
              });
            }}
          />
          <View style={{paddingTop: 10}}>
            <Text style={{fontSize: Fonts.size.medium, margin: 18, color: Colors.gray700}}>
              {translations.LABEL_AD_BANNER + '*'}
            </Text>
            <TouchableOpacity
              onPress={() => uploadOption(1)}
              activeOpacity={0.9}
              style={{paddingHorizontal: 18, height: width / 2, flexDirection: 'row'}}>
              <View
                style={{
                  width: '50%',
                  borderRadius: 15,
                  backgroundColor: 'white',
                  elevation: 3,
                  height: width / 2 - 10,
                }}>
                <Image
                  resizeMode={'cover'}
                  style={{width: '100%', height: '100%', borderRadius: 15}}
                  source={
                    selectedImage || values.advert_thumbnail
                      ? {uri: selectedImage ? selectedImage.path : values.advert_thumbnail}
                      : require('../../images/Icons/camera.png')
                  }
                />
              </View>
            </TouchableOpacity>
          </View>
        </Card>
        <Card containerStyle={styles.cardContainer}>
          <CardItem>
            <Body>
              <Text style={styles.title}>Ads Targeting Info</Text>
            </Body>
          </CardItem>
          <SelectPicker
            label="Targeting *"
            items={surveyQuestions.map(item => ({id: item.id, name: item.question}))}
            selectedItems={values.advert_target ? [values.advert_target] : []}
            onSelectedItemsChange={value => {
              handleChange({
                range_from: null,
                range_to: null,
                advert_answer: null,
                advert_target: value[0],
              });
            }}
          />
          {renderQuestionForm()}
        </Card>
        <Card containerStyle={styles.cardContainer}>
          <CardItem>
            <Body>
              <Text style={styles.title}>Summary</Text>
            </Body>
          </CardItem>
          {paymentAddress !== '' ? (
            <Horizontal style={[styles.cardSubContainer, {alignItems: 'flex-start'}]}>
              <Text style={[Fonts.style.description, {color: Colors.gray600}]}>Payment Address</Text>
              <Text style={[Fonts.style.description, {color: Colors.gray600, textAlign: 'right'}]}>
                {paymentAddress}
              </Text>
            </Horizontal>
          ) : null}
          <Horizontal style={[styles.cardSubContainer, {marginTop: Metrics.baseMargin}]}>
            <Text style={[Fonts.style.description, {color: Colors.gray600}]}>Total Cost</Text>
            <Text style={[Fonts.style.description, {color: Colors.gray600}]}>{numeralFormatter(totalCost)}</Text>
          </Horizontal>
        </Card>
      </KeyboardAwareScrollView>
      <View style={{paddingHorizontal: 20, paddingVertical: 10, backgroundColor: Colors.gray100, elevation: 3}}>
        <Button disabled={!submitable} onPress={onSave} block>
          <Text>{id ? translations.ACTION_AD_SAVE : translations.ACTION_AD_CREATE}</Text>
        </Button>
      </View>
      <ActionSheet
        ref={o => (this.ActionSheet = o)}
        title={translations.TITLE_UPLOAD_AD_BANNER}
        options={[translations.ACTION_USE_CAMERA, translations.ACTION_CHOOSE_FROM_GALLERY, translations.ACTION_CANCEL]}
        cancelButtonIndex={2}
        onPress={uploadOption}
      />
      <Loading visible={loading} />
    </Container>
  );
};

const mapStateToProps = ({smartadv, support, survey}) => {
  return {
    myAd: smartadv.myAd,
    budgets: support.budgets,
    currencyPoints: support.currency_points,
    paymentOptions: support.payment_options,
    surveyQuestions: survey.questions,
  };
};

const mapDispatchToProps = ({smartadv, support, survey}) => {
  return {
    emptyAd: smartadv.emptyAd,
    getAd: params => smartadv.getAd(params),
    editAd: data => smartadv.editAd(data),
    updateAdStatus: data => smartadv.updateAdStatus(data),
    createAd: data => smartadv.createAd(data),
    getBudgets: support.getBudgets,
    getCurrencyPoints: support.getCurrencyPoints,
    getPaymentOptions: support.getPaymentOptions,
    getQuestions: survey.getQuestions,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AdForm);
