import { StyleSheet } from 'react-native'
import { Colors, Fonts, Metrics } from '../../../themes'

export default StyleSheet.create({
  container: {
    height: 44,
    backgroundColor: Colors.darkPrimary,
    borderRadius: Metrics.smallMargin,
    marginTop: Metrics.section,
    marginHorizontal: 34,
  },
  text: { fontSize: Fonts.size.regular, color: Colors.snow },
})
