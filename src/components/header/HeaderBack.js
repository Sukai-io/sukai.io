/* eslint-disable no-nested-ternary */
/* eslint-disable max-len */
import React from 'react'
import { withNavigation } from 'react-navigation'
import { connect } from 'react-redux'

import {
  Icon, Header, Left, Button, Body, Right, Text, NativeBase,
} from 'native-base'
import { Badge } from 'react-native-elements'
import styles from './styles'
import store from '../../config/CreateStore'

type Props = NativeBase.Header & {
  title: string,
  navigation: any,
  onBackPress: Function,
  onRightPress: Function,
  withoutRight: Boolean,
  customRight: String,
  customRightAny: any,
  carts: Number,
}

const HeaderBack = (props: Props) => {
  const {
    title, navigation, onBackPress, onRightPress, withoutRight, customRight, customRightAny, carts,
  } = props
  return (
    <Header {...props}>
      <Left>
        <Button transparent onPress={() => (onBackPress ? onBackPress() : navigation.goBack())}>
          <Icon name="arrow-back" />
        </Button>
      </Left>
      <Body>
        <Text numberOfLines={1} style={styles.text}>
          {title}
        </Text>
      </Body>
      {customRightAny ? customRightAny() : null}
      {!customRightAny && (withoutRight ? (
        <Right />
      ) : customRight ? (
        <Right>
          <Button transparent onPress={onRightPress}>
            <Icon name={customRight} style={styles.custom} />
          </Button>
        </Right>
      ) : (
        <Right>
          <Button transparent onPress={() => navigation.navigate('CartScreen')}>
            <Icon name="cart" style={styles.cart} />
            <Badge
              value={carts}
              status="success"
              badgeStyle={styles.badge}
              containerStyle={{ position: 'absolute', top: 2, right: 4 }}
            />
          </Button>
        </Right>
      ))}
    </Header>
  )
}

const mapStateToProps = (state) => ({
  carts: store.select.cart.getCartQty(state),
})

export default connect(mapStateToProps)(withNavigation(HeaderBack))
