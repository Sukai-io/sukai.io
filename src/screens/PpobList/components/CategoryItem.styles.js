import { StyleSheet } from 'react-native'
import {
  ApplicationStyles, Metrics, Fonts, Colors,
} from '../../../themes'

export default StyleSheet.create({
  container: {
    ...ApplicationStyles.flexCenter,
  },
  icon: {
    fontSize: Metrics.icons.medium,
    color: Colors.bloodOrange,
  },
  secondIcon: {
    fontSize: Metrics.icons.medium,
    color: Colors.facebook,
  },
  title: {
    marginTop: Metrics.baseMargin,
    fontSize: Fonts.size.small,
    color: Colors.gray900,
  },
})
