import { StyleSheet, Dimensions } from 'react-native'
import {
  ApplicationStyles, Metrics, Fonts, Colors,
} from '../../themes'

const deviceHeight = Dimensions.get('window').height

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  image: {
    width: 200,
    height: 150,
    alignSelf: 'center',
    marginTop: Metrics.baseMargin,
  },
  sectionContainer: {
    marginTop: deviceHeight - deviceHeight / 2 - deviceHeight / 4 - deviceHeight / 8,
  },
  forget: {
    marginTop: Metrics.marginVertical,
    paddingTop: 0,
    paddingBottom: 0,
    height: 20,
    marginBottom: 70,
  },
  forgetText: {
    ...Fonts.style.description,
    color: 'white',
  },
  signup: {
    height: 50,
    backgroundColor: Colors.darkPrimary,
  },
  text: { fontSize: Fonts.size.regular, color: Colors.snow, fontFamily: Fonts.type.base },
})
