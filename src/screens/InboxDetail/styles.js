import { StyleSheet } from 'react-native'
import {
  ApplicationStyles, Metrics, Fonts, Colors,
} from '../../themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
    padding: Metrics.section,
  },
  date: {
    ...ApplicationStyles.flex,
    fontSize: Fonts.size.small,
    color: Colors.gray500,
  },
  title: {
    ...ApplicationStyles.flex,
    fontSize: Fonts.size.input,
    color: Colors.gray900,
    marginTop: Metrics.smallMargin,
  },
  desc: {
    ...ApplicationStyles.flex,
    fontSize: Fonts.size.medium,
    color: Colors.gray600,
    marginTop: Metrics.doubleBaseMargin,
  },
})
