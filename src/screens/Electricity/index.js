import React, {Component} from 'react';
import {connect} from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

import {Formik, FormikProps} from 'formik';
import {Container} from 'native-base';
import AlertPro from 'react-native-alert-pro';
import {Fonts, Colors} from '../../themes';
import Content from './Content';
import {FormValues} from './types';
import validation from './validation';
import {numeralFormatter} from '../../utils/TextUtils';

type Props = FormikProps<FormValues> & {
  doCheckout: Function,
  doSaveNumber: Function,
  number: String,
  providerPriceList: Array<Object>,
};

type State = {
  values: Object,
};

class ElectricityScreen extends Component<Props, State> {
  state = {
    values: null,
  };

  handleSubmit = (values: FormValues) => {
    this.setState({values}, () => this.AlertPro.open());
  };

  handleConfirm = () => {
    const {doSaveNumber, doCheckout, providerPriceList} = this.props;
    const {values} = this.state;
    doSaveNumber({type: 'electricity', number: values.ppob_number});
    doCheckout({
      ppob_action: 'topup',
      ppob_provider: values.ppob_provider,
      ppob_number: values.ppob_number,
      ppob_code: providerPriceList[values.price_index].code,
    });
    this.AlertPro.close();
  };

  render() {
    const {number, balance} = this.props;
    return (
      <Container>
        <AlertPro
          ref={ref => {
            this.AlertPro = ref;
          }}
          onConfirm={this.handleConfirm}
          onCancel={() => this.AlertPro.close()}
          title="Konfirmasi Pembelian"
          message={`Saldo saat ini : ${numeralFormatter(
            balance,
          )}.\nSaldo Anda akan berkurang setelah pembelian Voucher Listrik`}
          textCancel="Batal"
          textConfirm="Beli"
          customStyles={{
            container: {
              width: 300,
              shadowColor: '#000000',
              shadowOpacity: 0.1,
              shadowRadius: 10,
            },
            title: {
              fontSize: Fonts.size.h6,
            },
            buttonCancel: {
              backgroundColor: Colors.gray400,
            },
            buttonConfirm: {
              backgroundColor: Colors.bloodOrange,
            },
          }}
        />
        <Formik
          initialValues={{
            price_index: 0,
            ppob_provider: 'PLN',
            ppob_number: number || '',
          }}
          enableReinitialize
          onSubmit={(values: FormValues) => this.handleSubmit(values)}
          validationSchema={validation}
          render={(props: Props) => <Content {...props} />}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ppob, balance}) => {
  return {
    balance: balance.balance,
    number: ppob.number_electricity,
    providerPriceList: ppob.provider_price_list,
  };
};

const mapDispatchToProps = ({ppob}) => {
  return {
    doCheckout: data => ppob.doCheckout(data),
    doSaveNumber: data => ppob.saveNumber(data),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ElectricityScreen);
