import { StyleSheet } from 'react-native'
import {
  ApplicationStyles, Colors, Fonts, Metrics,
} from '../../themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    ...ApplicationStyles.flex,
    backgroundColor: Colors.gray100,
  },
  empty: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    color: Colors.gray700,
    alignSelf: 'center',
    padding: Metrics.doubleSection,
  },
})
