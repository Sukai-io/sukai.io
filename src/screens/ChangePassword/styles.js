import { StyleSheet } from 'react-native'
import {
  ApplicationStyles, Metrics, Colors, Fonts,
} from '../../themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    ...ApplicationStyles.flex,
    backgroundColor: Colors.gray100,
    padding: Metrics.baseMargin,
  },
  buttonContainer: {
    marginTop: Metrics.doubleBaseMargin,
  },
  button: {
    backgroundColor: Colors.bloodOrange,
  },
  error: {
    ...Fonts.style.error,
    marginHorizontal: 16,
  },
})
