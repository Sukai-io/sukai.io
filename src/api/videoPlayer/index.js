import {PLAYER_STATES} from 'react-native-media-controls';

const initialState = {
  currentTime: 0,
  duration: 0,
  isFullScreen: false,
  isLoading: true,
  paused: false,
  playerState: PLAYER_STATES.PLAYING,
};

export default {
  state: initialState,
  reducers: {
    setInitialState(){
      return initialState
    },
    setPaused(state) {
      return {
        ...state,
        paused: !state.paused,
      };
    },
    setPlayerState(state, payload) {
      return {
        ...state,
        playerState: payload,
      };
    },
    setCurrentTime(state, payload) {
      return {
        ...state,
        currentTime: payload,
      };
    },
    setDuration(state, payload) {
      return {
        ...state,
        duration: payload,
      };
    },
    setIsLoading(state, payload) {
      return {
        ...state,
        isLoading: payload,
      };
    },
    setIsFullScreen(state, payload) {
      return {
        ...state,
        isFullScreen: payload,
      };
    },
  },
};
