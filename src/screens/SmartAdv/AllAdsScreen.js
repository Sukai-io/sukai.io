import React, { useEffect, useState, useRef } from 'React';
import {View, TouchableOpacity, Image, Dimensions} from 'react-native';
import {Text, Container, Content, Button, Icon} from 'native-base';
import {Fonts, Colors} from '../../themes';
import {connect} from 'react-redux';
import HeaderBack from '../../components/header/HeaderBack';
import Loading from '../../components/Loading';
import { FlatGrid } from 'react-native-super-grid';
import ActionSheet from 'react-native-actionsheet';

const AllAdsScreen = ({ allAds, key, updateAdStatus, getAllAds, navigation, userData }) => {

	let [ad, setAd] = useState({})
	let [offset, setOffset] = useState(0)
	let actionSheet = useRef(null)

	useEffect(() => {
		getAllAds(offset);
	}, [key]);

	const { width, height } = Dimensions.get('window')

	const adOption = index => {
		index == 0 && navigation.navigate('AdForm', ad)
		index == 1 && navigation.navigate('AdBalanceForm', ad)
		index == 2 && updateAdStatus({advert_id: ad.id, advert_status: ad.status === 'ACTIVE' ? 0 : 1})
	}

	const renderAd = ({item: { member_id, image_thumbnail, id, title, status, total_budget, remaining_budget, bonus_status }}) => {
		return <TouchableOpacity onPress={() => {
			navigation.navigate('AdDetail', { id });
		}}>
			<View style={{ padding: 4 }}>
				<Image style={{ width: '100%', height: width / 2 - 20, borderRadius: 15 }} source={{ uri: image_thumbnail }} />
				<Text numberOfLines={1} style={{ fontSize: Fonts.size.small, marginTop: 3 }}>{title}</Text>
				{userData && bonus_status == 0 && member_id != userData.id && (<Image resizeMode={'contain'} style={{ position: 'absolute', width: 60, height: 20, top: 12, right: 10 }} source={require('../../images/Icons/bonus500.png')} />)}
          		{userData && bonus_status == 1 && member_id != userData.id && (<Image resizeMode={'contain'} style={{ position: 'absolute', width: 60, height: 20, top: 12, right: 10 }} source={require('../../images/Icons/bonus500claimed.png')} />)}
			</View>
		</TouchableOpacity>
	}

	return (
		<Container>
			<HeaderBack 
				withoutRight
				title="List Iklan" />
			<Content style={{ paddingTop: 5, paddingHorizontal: 3 }}>
				{typeof allAds === 'object' && (<FlatGrid
				  itemDimension={130}
				  items={allAds}
				  renderItem={renderAd} />)}
			</Content>
			<Loading visible={typeof allAds != 'object'} />
			<ActionSheet
	          	ref={actionSheet}
	          	title={ad.title}
	          	options={['Edit Iklan', 'Tambah Budget', ad.status === 'ACTIVE' ? 'Nonaktifkan' : 'Aktifkan', 'Batal']}
	          	destructiveButtonIndex={3}
	          	onPress={adOption}
	        />
		</Container>
	)

}

const mapStateToProps = ({smartadv, auth}) => {
  return {
    allAds: smartadv.allAds,
    userData: auth.userData,
  }
};

const mapDispatchToProps = ({smartadv}) => {
  return {
    getAllAds: offset => smartadv.getAllAds(offset),
    updateAdStatus: data => smartadv.updateAdStatus(data)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AllAdsScreen);