import React from 'react';
import { Modal, ActivityIndicator, View } from 'react-native';
import { Colors } from '../themes';

const Loading = ({visible}) => {

	return <Modal transparent animationType={'fade'} visible={visible}>
		<View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0)', alignItems: 'center', justifyContent: 'center' }}>
			<ActivityIndicator color={Colors.fire} size={'large'} />
		</View>
	</Modal>

}

export default Loading