import { object as yupObject, string as yupString, ref as yupRef } from 'yup'

export default yupObject().shape({
  old_password: yupString().required(),
  password: yupString().required(),
  confirm_password: yupString()
    .required()
    .oneOf([yupRef('password'), null], 'Passwords must match'),
})
