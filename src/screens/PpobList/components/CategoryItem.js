import React from 'react'
import { View } from 'react-native'
import { Text, Icon } from 'native-base'
import styles from './CategoryItem.styles'
import FlatTouchables from '../../../components/buttons/FlatTouchables'

type Props = {
  name: String,
  icon: String,
  onPress: Function,
  isFirst: Boolean,
}

const CategoryItem = ({
  name, icon, onPress, isFirst,
}: Props) => (
  <FlatTouchables onPress={onPress}>
    <View style={styles.container}>
      <Icon
        style={isFirst ? styles.icon : styles.secondIcon}
        name={icon}
        type="MaterialCommunityIcons"
      />
      <Text style={styles.title}>{name}</Text>
    </View>
  </FlatTouchables>
)

export default CategoryItem
