import React from 'react';
import {Container, Tabs, Tab} from 'native-base';
import styles from './styles';
import HeaderBack from '../../components/header/HeaderBack';
import InHistory from './In';
import OutHistory from './Out';
// import Activity from './Activity'

const HistoryWallet = props => (
  <Container style={styles.mainContainer}>
    <HeaderBack hasTabs title="History Wallet" withoutRight />
    <Tabs>
      <Tab textStyle={styles.tabText} activeTextStyle={styles.activeTabText} heading="IN">
        <InHistory {...props} />
      </Tab>
      <Tab textStyle={styles.tabText} activeTextStyle={styles.activeTabText} heading="OUT">
        <OutHistory {...props} />
      </Tab>
      {/* <Tab textStyle={styles.tabText} activeTextStyle={styles.activeTabText} heading="Activity">
        <Activity {...props} />
      </Tab> */}
    </Tabs>
  </Container>
);

export default HistoryWallet;
