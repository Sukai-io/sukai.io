import {object as yupObject, string as yupString} from 'yup';

export default yupObject().shape({
  title: yupString().required(),
  description: yupString().required(),
  url: yupString().required(),
  digcom_image: yupObject()
    .nullable()
    .required(),
});
