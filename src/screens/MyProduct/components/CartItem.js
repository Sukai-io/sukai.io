import React from 'react';
import {View} from 'react-native';
import {Text, Thumbnail, Icon} from 'native-base';
import Horizontal from '../../../components/wrapper/Horizontal';
import styles from './CartItem.styles';
import FlatTouchables from '../../../components/buttons/FlatTouchables';
import {numeralFormatter} from '../../../utils/TextUtils';
import {ApplicationStyles} from '../../../themes';

type Props = {
  onPress: Function,
  onDelete: Function,
  dataSource: Object,
  isLast: Boolean,
};

const CartItem = ({isLast, onPress, onDelete, dataSource}: Props) => (
  <FlatTouchables onPress={onPress}>
    <Horizontal style={[ApplicationStyles.flex, isLast && styles.isLast]}>
      <Horizontal style={[styles.container]}>
        <Thumbnail
          square
          source={{
            uri: dataSource?.product_thumbnail_url,
          }}
          resizeMode="contain"
        />
        <View style={styles.infoContainer}>
          <Text style={styles.title}>{dataSource?.product_name}</Text>
          <Text style={styles.qty}>{`Stok: ${dataSource?.product_stock}`}</Text>
          <Text
            style={
              styles.qty
            }>{`Berat: ${dataSource?.product_weight} gr`}</Text>
          <Text style={styles.price}>{`Rp ${numeralFormatter(
            dataSource?.product_price,
          )}`}</Text>
        </View>
      </Horizontal>
      <FlatTouchables onPress={onDelete} style={styles.deleteContainer}>
        <Icon name="ios-trash" style={styles.deleteIcon} />
      </FlatTouchables>
    </Horizontal>
  </FlatTouchables>
);

export default CartItem;
