import React from 'react'
import { View } from 'react-native'
import { Text, Icon, Button } from 'native-base'
import FastImage from 'react-native-fast-image'
import Horizontal from '../../../components/wrapper/Horizontal'
import styles from './CartItem.styles'
import FlatTouchables from '../../../components/buttons/FlatTouchables'

type Props = {
  onPress: Function,
  onDelete: Function,
  onMinus: Function,
  onPlus: Function,
  uri: String,
  name: String,
  seller: String,
  qty: String,
  price: String,
}

const CartItem = ({
  onPress, onDelete, onMinus, onPlus, name, price, qty, seller, uri,
}: Props) => (
  <FlatTouchables onPress={onPress} style={styles.touch}>
    <Horizontal style={styles.container}>
      <Horizontal style={styles.subContainer}>
        <FastImage
          style={styles.avatar}
          resizeMode="contain"
          source={{
            uri,
          }}
        />
        <View style={styles.infoContainer}>
          <Text style={styles.title}>{name}</Text>
          <Horizontal style={styles.personContainer}>
            <Icon name="ios-person" style={styles.personIcon} />
            <Text style={styles.marginLeft}>
              <Text style={styles.by}>Seller: </Text>
              <Text style={styles.by}>{seller}</Text>
            </Text>
          </Horizontal>
          <Horizontal style={{ alignItems: 'center' }}>
            <Text style={styles.qty}>Qty: </Text>
            <Button
              onPress={qty === 1 ? undefined : onMinus}
              icon
              transparent
              style={styles.buttonOperator}
            >
              <Icon
                name="minus-circle-outline"
                type="MaterialCommunityIcons"
                style={[styles.operatorIcon, qty === 1 && styles.disabledIcon]}
              />
            </Button>
            <View style={styles.amount}>
              <Text style={styles.qty}>{qty}</Text>
            </View>
            <Button onPress={onPlus} icon transparent style={styles.buttonOperator}>
              <Icon
                name="plus-circle-outline"
                type="MaterialCommunityIcons"
                style={styles.operatorIcon}
              />
            </Button>
          </Horizontal>
          <Text style={styles.price}>{price}</Text>
        </View>
      </Horizontal>
      <FlatTouchables onPress={onDelete} style={styles.deleteContainer}>
        <Icon name="ios-trash" style={styles.deleteIcon} />
      </FlatTouchables>
    </Horizontal>
  </FlatTouchables>
)

export default CartItem
