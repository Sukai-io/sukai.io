import { object as yupObject, number as yupNumber } from 'yup'

export default yupObject().shape({
  withdraw: yupNumber()
    .required()
    .min(100000),
})
