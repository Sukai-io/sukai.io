/* eslint-disable no-unused-vars */
import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import {
  Container, Content, Text, Grid, Row, Col,
} from 'native-base'
import styles from './styles'

class Activity extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <Container>
        <Content>
          <Grid>
            <Row style={styles.list}>
              <Col size={1}>
                <Text style={styles.date}>25 Apr 2018</Text>
              </Col>
              <Col size={1}>
                <Text style={styles.amount}>25.000</Text>
              </Col>
              <Col size={1}>
                <Text style={styles.activity}>PPOB Pulsa</Text>
              </Col>
            </Row>
            <Row style={styles.list}>
              <Col>
                <Text style={styles.date}>25 Apr 2018</Text>
              </Col>
              <Col>
                <Text style={styles.amount}>25.000</Text>
              </Col>
              <Col>
                <Text style={styles.activity}>PPOB Pulsa</Text>
              </Col>
            </Row>
            <Row style={styles.list}>
              <Col>
                <Text style={styles.date}>25 Apr 2018</Text>
              </Col>
              <Col>
                <Text style={styles.amount}>25.000</Text>
              </Col>
              <Col>
                <Text style={styles.activity}>PPOB Pulsa</Text>
              </Col>
            </Row>
          </Grid>
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    // fetching: AuthSelectors.fetching(state.auth),
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    // doLogin: data => dispatch(AuthActions.loginRequest(data)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Activity)
