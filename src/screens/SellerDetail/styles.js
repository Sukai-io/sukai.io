import {StyleSheet} from 'react-native';
import {ApplicationStyles, Metrics, Colors, Fonts} from '../../themes';

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
    paddingTop: Metrics.baseMargin,
    backgroundColor: Colors.gray100,
  },
  cardContainer: {
    ...ApplicationStyles.shadow,
    ...ApplicationStyles.flex,
    padding: Metrics.smallMargin,
    marginRight: 0,
    marginLeft: 0,
    marginTop: 0,
    marginBottom: Metrics.doubleBaseMargin,
  },
  title: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.bold,
    color: Colors.gray700,
    marginBottom: 0,
  },
  subtitle: {
    ...ApplicationStyles.flex,
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    color: Colors.gray600,
    marginTop: Metrics.smallMargin,
  },
  infoCourier: {
    ...ApplicationStyles.flex,
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    color: Colors.gray600,
  },
  shipping: {
    ...ApplicationStyles.flex,
    textAlign: 'right',
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    color: Colors.primary,
  },
  subTotalShipping: {
    ...ApplicationStyles.flex,
    textAlign: 'right',
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    color: Colors.gray900,
  },
  totalLeft: {
    ...ApplicationStyles.flex,
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.base,
    color: Colors.gray600,
  },
  totalRight: {
    ...ApplicationStyles.flex,
    textAlign: 'right',
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.base,
    color: Colors.gray900,
  },
  grandContainer: {
    marginTop: Metrics.smallMargin,
    ...ApplicationStyles.flexCenter,
  },
  grantTotal: {
    ...ApplicationStyles.flex,
    textAlign: 'right',
    fontSize: Fonts.size.h6,
    fontFamily: Fonts.type.bold,
    color: Colors.primary,
  },
  status: {
    ...ApplicationStyles.flex,
    padding: Metrics.smallMargin,
    backgroundColor: Colors.orange50,
    marginBottom: Metrics.baseMargin,
  },
  statusText: {
    fontSize: Fonts.size.medium,
    color: Colors.gray600,
    textAlign: 'center',
  },
  resi: {
    ...ApplicationStyles.flex,
    textAlign: 'right',
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.base,
    color: Colors.gray900,
    textDecorationLine: 'underline',
  },
  buttonContainer: {
    marginHorizontal: Metrics.baseMargin,
    marginBottom: 32,
  },
  buttonDisable: {
    backgroundColor: Colors.gray400,
  },
  button: {
    backgroundColor: Colors.bloodOrange,
  },
});
