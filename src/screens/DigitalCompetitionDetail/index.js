import React, {useCallback} from 'react';
import {Container, Text, CardItem, Body} from 'native-base';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import HeaderBack from '../../components/header/HeaderBack';
import styles from './styles';
import {formattedDate} from '../../utils/TextUtils';
import FastImage from 'react-native-fast-image';
import {normalizedSource} from '../../utils/ImageUtils';
import Horizontal from '../../components/wrapper/Horizontal';
import {ApplicationStyles} from '../../themes';
import {Card} from 'react-native-elements';
import {TouchableOpacity, Linking} from 'react-native';

const DigitalCompetitionDetail = ({navigation}) => {
  const item = navigation.getParam('item', null);
  const handleOpenUrl = useCallback(() => {
    Linking.openURL(item?.url);
  }, [item]);
  return (
    <Container>
      <HeaderBack withoutRight />
      <KeyboardAwareScrollView style={styles.container}>
        <Text style={styles.date}>{formattedDate(item?.datecreated)}</Text>
        <Text style={styles.title}>{item?.title}</Text>
        <Text style={styles.desc}>{item.description}</Text>
        <FastImage
          source={normalizedSource({uri: item?.digcom_image_url})}
          style={styles.image}
          resizeMode="contain"
        />
        <Card containerStyle={styles.cardContainer}>
          <CardItem>
            <Body>
              <Text style={styles.cardTitle}>Informasi</Text>
              <Horizontal style={ApplicationStyles.flexCenter}>
                <Text style={styles.cardSubtitle}>Pemilik</Text>
                <Text style={[styles.cardSubtitle, {textAlign: 'right'}]}>
                  {item?.username}
                </Text>
              </Horizontal>
              <Horizontal style={styles.subtitleContainer}>
                <Text style={styles.cardSubtitle}>Status</Text>
                <Text style={[styles.cardSubtitle, {textAlign: 'right'}]}>
                  {item?.status}
                </Text>
              </Horizontal>
              <Horizontal style={styles.subtitleContainer}>
                <Text style={styles.cardSubtitle}>URL</Text>
                <TouchableOpacity
                  style={ApplicationStyles.flex}
                  onPress={handleOpenUrl}>
                  <Text style={[styles.cardSubtitleUrl, {textAlign: 'right'}]}>
                    {item?.url}
                  </Text>
                </TouchableOpacity>
              </Horizontal>
            </Body>
          </CardItem>
        </Card>
      </KeyboardAwareScrollView>
    </Container>
  );
};

export default DigitalCompetitionDetail;
