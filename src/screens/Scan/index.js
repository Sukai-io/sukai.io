import React, {Component} from 'react';
import {View} from 'react-native';
import {connect} from 'react-redux';
import styles from './styles';
import {RNCamera} from 'react-native-camera';
import ViewFinder from '../../components/ViewFinder';
import QRCode from 'react-native-qrcode-svg';
import {Images} from '../../themes';
import RouteServices from '../../navigation/RouteServices';
import metrics from '../../themes/Metrics';
import HeaderBack from '../../components/header/HeaderBack';
export class index extends Component {
  constructor(props) {
    super(props);

    this.state = {
      text: '',
    };
  }

  componentDidMount() {
    let {user} = this.props;

    if (user) {
      this.setState({
        text: user.username,
      });
    }
  }

  _onFindBarcode = barcodes => {
    if (Array.isArray(barcodes) && barcodes.length > 0 && !Array.isArray(barcodes[0].data)) {
      // console.log(barcodes);
      let dataUsername = barcodes[0].data;
      try {
        dataUsername = JSON.parse(dataUsername);
      } catch (e) {
        dataUsername = barcodes[0].data;
      }

      if (dataUsername !== null && typeof dataUsername === 'string') {
        let optionsReset = {
          index: 1,
          routes: [{name: 'Home'}, {name: 'TransferScreen', params: {username: dataUsername}}],
        };
        RouteServices.navigate('TransferScreen', {username: dataUsername});
        // NavigationService.reset(optionsReset)
      }
    }

    // NavigationService.navigate('EwalletTransfer', { username: barcodes[0].data })
    // Alert.alert('Kode Ditemukan!', 'Username Barcode adalah : ' + barcodes[0].data)
  };

  takePicture = async () => {
    if (this.camera) {
      const options = {quality: 0.5, base64: true};
      const data = await this.camera.takePictureAsync(options);
      console.log(data.uri);
    }
  };
  render() {
    const logo = Images.logo_barcode;
    let {user} = this.props;
    return (
      <View style={styles.container}>
        <HeaderBack title="Sukai" withoutRight />
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.on}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          onGoogleVisionBarcodesDetected={({barcodes}) => {
            // console.log(barcodes);
            if (Array.isArray(barcodes) && barcodes.length > 0) {
              // console.log(barcodes);
              //
              this._onFindBarcode(barcodes);
            }
          }}>
          <ViewFinder />
        </RNCamera>
        <View style={styles.panelInfoCode}>
          <QRCode value={user.username} size={metrics.screenHeiht * (1 / 4)} logo={logo} logoMargin={10} />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.userData,
});

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(index);
