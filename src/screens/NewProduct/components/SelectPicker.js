import React, {memo} from 'react';
import {Text} from 'react-native';
import styles from '../styles';
import MultiSelect from 'react-native-multiple-select';
import {Fonts, Colors} from '../../../themes';

const SelectPicker = ({error, ...otherProps}) => {
  return (
    <>
      <MultiSelect
        hideTags
        uniqueKey="id"
        searchInputPlaceholderText="Search Items..."
        onChangeInput={text => console.log(text)}
        altFontFamily="Quicksand-Regular"
        fontFamily="Quicksand-Regular"
        itemFontFamily="Quicksand-Regular"
        selectedItemFontFamily="Quicksand-Regular"
        tagRemoveIconColor="#CCC"
        tagBorderColor="#CCC"
        tagTextColor="#CCC"
        selectedItemTextColor="#CCC"
        selectedItemIconColor="#CCC"
        itemTextColor="#000"
        displayKey="name"
        searchInputStyle={{color: '#CCC', fontSize: 17}}
        submitButtonColor={Colors.primary}
        submitButtonText="Submit"
        styleMainWrapper={{
          marginTop: 8,
          marginBottom: 0,
          marginLeft: 17,
          marginRight: 16,
        }}
        styleTextDropdown={{fontSize: 14, color: Colors.gray600}}
        styleTextDropdownSelected={{
          fontSize: 17,
          color: 'black',
          fontFamily: Fonts.type.base,
        }}
        styleDropdownMenuSubsection={{paddingRight: 0}}
        styleInputGroup={{paddingLeft: 0, marginTop: 16}}
        {...otherProps}
      />
      {error && <Text style={styles.error}>{error}</Text>}
    </>
  );
};

export default memo(SelectPicker);
