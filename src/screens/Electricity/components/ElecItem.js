import React from 'react'
import { View, Platform } from 'react-native'
import { Radio, Text } from 'native-base'
import styles from './ElecItem.styles'
import Horizontal from '../../../components/wrapper/Horizontal'
import FlatTouchables from '../../../components/buttons/FlatTouchables'

type Props = {
  onPress: Function,
  selected: Boolean,
  price: String,
  salePrice: String,
}

const platform = Platform.OS

const ElecItem = ({
  price, salePrice, selected, onPress,
}: Props) => (
  <FlatTouchables onPress={onPress}>
    <Horizontal style={styles.container}>
      <Radio selected={selected} />
      <Text
        style={[
          styles.description,
          styles.subContainer,
          { paddingLeft: platform === 'ios' && !selected ? 10 : 0 },
        ]}
      >
        {price}
      </Text>
      <View style={styles.amountContainer}>
        <Text style={styles.amount}>{salePrice}</Text>
      </View>
    </Horizontal>
  </FlatTouchables>
)

export default ElecItem
