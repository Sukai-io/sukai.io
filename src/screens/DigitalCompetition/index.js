import React, {useEffect, useState, useCallback} from 'react';
import {connect} from 'react-redux';
import {Container, ListItem, Text, Right, Icon, Body, Content} from 'native-base';
import styles from './styles';
import HeaderBack from '../../components/header/HeaderBack';
import VerticalList from '../../components/list/VerticalList';
import {formattedDate} from '../../utils/TextUtils';
import {goTo} from '../../utils/NavigationUtils';

const DigitalCompetition = ({getDigitalCompetitions, digitalCompetitions, navigation, loading}) => {
  const [offset, setOffset] = useState(0);
  const handleLoadMore = useCallback(() => {
    setOffset(value => value + 10);
  }, []);
  const handleRefresh = useCallback(() => {
    setOffset(0);
  }, []);
  useEffect(() => {
    getDigitalCompetitions({limit: 10, offset});
  }, [offset]);
  return (
    <Container style={styles.mainContainer}>
      <HeaderBack
        customRight="add"
        onRightPress={() => goTo('NewDigitalCompetitionScreen')}
        title="DigitalCompetition"
      />
      <Content contentContainerStyle={{flex: 1}}>
        <VerticalList
          contentContainerStyle={{padding: 0}}
          data={digitalCompetitions}
          windowSize={13}
          maxToRenderPerBatch={6}
          updateCellsBatchingPeriod={30}
          withRefreshControl
          refreshing={loading && offset === 0}
          onRefresh={handleRefresh}
          renderItem={({item}) => (
            <ListItem onPress={() => navigation.navigate('DigitalCompetitionDetailScreen', {item})} style={styles.list}>
              <Body>
                <Text style={styles.selected}>{item.title}</Text>
                <Text note style={styles.dateSelected}>
                  {formattedDate(item.datecreated)}
                </Text>
              </Body>
              <Right>
                <Icon style={styles.iconSelected} name="arrow-forward" />
              </Right>
            </ListItem>
          )}
          onEndReached={handleLoadMore}
        />
      </Content>
    </Container>
  );
};

const mapStateToProps = ({loading, support: {digital_competitions}}) => {
  return {
    loading: loading.effects.support.getPartnerships,
    digitalCompetitions: digital_competitions,
  };
};

const mapDispatchToProps = ({support: {getDigitalCompetitions}}) => {
  return {
    getDigitalCompetitions,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DigitalCompetition);
