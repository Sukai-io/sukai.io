import _ from 'lodash';

export function transformToFormData(item) {
  const formData = new FormData();
  _.forIn(item, (value, key) => {
    formData.append(key, value);
  });
  return formData;
}
