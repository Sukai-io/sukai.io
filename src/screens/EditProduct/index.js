import React, {useCallback, useEffect, useMemo} from 'react';
import {connect} from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

import {Formik, FormikProps} from 'formik';
import Content from './Content';
import {FormValues} from './types';
import validation from './validation';
import {transformToFormData} from '../../api/ApiHelper';

const EditProductScreen = ({
  productDetail,
  getProductDetail,
  postEditProduct,
  navigation,
}) => {
  const id = useMemo(() => navigation.getParam('id', null));
  const handleSubmit = useCallback(
    (
      {
        product_image,
        product_id,
        product_stock,
        product_category: productCategory,
        ...restProduct
      }: FormValues,
      {resetForm}: FormikProps<FormValues>,
    ) => {
      const formData = transformToFormData({
        ...restProduct,
        product_id,
        ...(product_image?.path ? {product_image} : {}),
      });
      const formStockData = transformToFormData({
        product_id,
        product_stock,
      });
      productCategory.forEach(item => {
        formData.append('product_category[]', item);
      });
      postEditProduct({data: formData, stock: formStockData}).then(() => {
        resetForm({});
      });
    },
    [],
  );
  useEffect(() => {
    getProductDetail({type: 'id', value: id});
  }, []);
  return (
    <Formik
      initialValues={{
        product_stock: productDetail?.product_stock || '',
        product_id: productDetail?.id || '',
        product_name: productDetail?.product_name || '',
        product_detail: productDetail?.product_detail || '',
        product_weight: productDetail?.product_weight || '',
        product_price: productDetail?.product_price || '',
        product_commission: productDetail?.product_commission || '',
        product_image: productDetail?.product_image_url
          ? {uri: productDetail?.product_image_url}
          : {uri: null},
        product_category: productDetail?.category?.map(item => item.id) || [],
      }}
      enableReinitialize
      onSubmit={(values, actions) => handleSubmit(values, actions)}
      validationSchema={validation}
      render={props => <Content {...props} />}
    />
  );
};

const mapStateToProps = ({product}, {navigation}) => {
  const id = navigation.getParam('id', null);
  return {
    productDetail: product.product_detail[id] || null,
  };
};

const mapDispatchToProps = ({product}) => {
  return {
    getProductDetail: params => product.getProductDetail(params),
    postEditProduct: params => product.postEditProduct(params),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditProductScreen);
