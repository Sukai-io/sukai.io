import React, {Component} from 'react';
import {View, ActivityIndicator} from 'react-native';
import AlertPro from 'react-native-alert-pro';
import {connect} from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import {Container, Content, Card, Text} from 'native-base';
import {Formik, FormikProps} from 'formik';
import styles from './styles';
import HeaderBack from '../../components/header/HeaderBack';
import {Fonts, Colors} from '../../themes';
import withdrawValidation from './withdrawValidation';
import store from '../../config/CreateStore';
import Loader from '../../components/indicator/Loader';
import InputWithButton from '../Wallet/components/InputWithButton';

type WithdrawFormValues = {
  withdraw: Number,
};

type TransferFormValues = {
  username: String,
  amount: Number,
};

class WithdrawScreen extends Component {
  state = {
    loadingGet: false,
  };

  componentDidMount() {
    const {getBalance} = this.props;
    getBalance();
  }

  handleLoading = value => {
    this.setState({loadingGet: value});
  };

  renderWithdrawForm = ({
    handleSubmit,
    setFieldValue,
    setFieldTouched,
    touched,
    errors,
  }: FormikProps<WithdrawFormValues>) => {
    const {loading, loadingWithdrawal, balance} = this.props;
    return (
      <Card style={styles.cardContainer}>
        <View style={styles.amountContainer}>
          <Text style={styles.titleBold}>My Sukai-Wallet</Text>
          {loading ? <ActivityIndicator /> : <Text style={styles.amount}>{balance}</Text>}
        </View>
        <Text style={styles.marginTop}>
          <Text style={styles.titleBold}>Withdraw Policy</Text>
        </Text>
        <Text style={styles.title}>1. Minimal withdraw 100 point : </Text>
        <Text style={styles.title}>2. Admin fee 5%</Text>
        <InputWithButton
          loadingButton={loadingWithdrawal}
          disabledButton={loadingWithdrawal}
          onPress={handleSubmit}
          titleButton="WITHDRAW"
          placeholder="Withdraw Amount"
          keyboardType="number-pad"
          onBlur={() => setFieldTouched('withdraw')}
          onChangeText={value => setFieldValue('withdraw', value)}
        />
        {errors.withdraw && touched.withdraw && <Text style={styles.error}>{errors.withdraw}</Text>}
      </Card>
    );
  };

  handleWithdrawSubmit = values => {
    const {doWithdrawal} = this.props;
    doWithdrawal({withdraw_amount: values.withdraw});
  };

  render() {
    const {loadingGet} = this.state;
    return (
      <Container style={styles.mainContainer}>
        <Loader loading={loadingGet} />
        <AlertPro
          ref={ref => {
            this.AlertPro = ref;
          }}
          onConfirm={() => this.AlertPro.close()}
          onCancel={() => this.AlertPro.close()}
          title="Konfirmasi Top Up"
          message="Anda yakin telah melengkapi pembayaran?"
          textCancel="Batal"
          textConfirm="Iya"
          customStyles={{
            container: {
              width: 300,
              shadowColor: '#000000',
              shadowOpacity: 0.1,
              shadowRadius: 10,
            },
            title: {
              fontSize: Fonts.size.h6,
            },
            buttonCancel: {
              backgroundColor: Colors.gray400,
            },
            buttonConfirm: {
              backgroundColor: Colors.bloodOrange,
            },
          }}
        />
        <HeaderBack withoutRight title="Withdraw" />
        <Content style={styles.container}>
          <Formik
            initialValues={{withdraw: ''}}
            onSubmit={(values: WithdrawFormValues) => this.handleWithdrawSubmit(values)}
            validationSchema={withdrawValidation}
            render={(actions: FormikProps<WithdrawFormValues>) => this.renderWithdrawForm(actions)}
          />
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = ({loading, balance}) => {
  return {
    loading: loading.effects.balance.getBalance,
    loadingWithdrawal: loading.effects.balance.doWithdrawal,
    balance: store.select.balance.getFormattedBalance(balance),
  };
};

const mapDispatchToProps = ({balance}) => {
  return {
    getBalance: () => balance.getBalance(),
    doWithdrawal: data => balance.doWithdrawal(data),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(WithdrawScreen);
