package io.sukai.nativemodules;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import io.sukai.R;
import io.sukai.lockscreen.utils.LockScreen;

public class LockscreenModule extends ReactContextBaseJavaModule {
  private static ReactApplicationContext reactContext;

  private static final String DURATION_SHORT_KEY = "SHORT";
  private static final String DURATION_LONG_KEY = "LONG";
  private SharedPreferences.Editor editor;
  private SharedPreferences pref;

  LockscreenModule(ReactApplicationContext context) {
    super(context);
    reactContext = context;
    pref = PreferenceManager.getDefaultSharedPreferences(reactContext);
    editor = pref.edit();
  }

  @Override
  public String getName() {
    return "LockscreenModule";
  }

  @ReactMethod
  public void activate(boolean active) {
    LockScreen lc = new LockScreen();
    lc.init(reactContext);
    if(active)
    {
      editor.putBoolean(reactContext.getString(R.string.lockscreen_status), true);
      editor.apply();
      lc.active();
    } else {
      editor.putBoolean(reactContext.getString(R.string.lockscreen_status), false);
      editor.apply();
      lc.deactivate();
    }
  }

  @ReactMethod
  public void setEndpoint(String endpoint){
    editor.putString(reactContext.getString(R.string.endpoint), endpoint);
    editor.apply();
  }

  @ReactMethod
  public void setAuthToken(String authToken){
    SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(reactContext);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString(reactContext.getString(R.string.authToken), authToken);
    editor.apply();
  }

  @ReactMethod
  public void getLockscrenStatus(Callback booleanCallback){
    boolean status = pref.getBoolean(reactContext.getString(R.string.lockscreen_status), true);
    activate(status);
    booleanCallback.invoke(status);
  }

  @ReactMethod
  public void getAdId(Callback intCallback){
    int currentAdId = pref.getInt(reactContext.getString(R.string.ad_id), 0);
    editor.putInt(reactContext.getString(R.string.ad_id), 0);
    editor.apply();
    intCallback.invoke(currentAdId);
  }

}