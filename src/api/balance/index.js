import R from 'ramda';
import api from '../ApiClient';
import errorGenerator from '../ErrorGenerator';
import store from '../../config/CreateStore';
import {numeralFormatter} from '../../utils/TextUtils';
import {goTo, resetAndGoTo, goBack} from '../../utils/NavigationUtils';
import {showToast} from '../../utils/ToastUtils';
import {transformToFormData} from '../ApiHelper';

const getSuccess = data => R.pathOr(false, ['success'], data);

export default {
  state: {
    allowDoRegister: false,
    balance: 0,
    topup_pending: false,
    topup_histories: [],
    bonus_histories: [],
    wallet_in_histories: [],
    wallet_out_histories: [],
    withdrawal_histories: [],
  },
  reducers: {
    setBalance(state, data) {
      return {...state, balance: data.data.ewallet_balance};
    },
    setTopupPending(state, data) {
      return {...state, topup_pending: data.data.ewallet_topup_pending};
    },
    setTopupHistory(state, data) {
      return {...state, topup_histories: data.data.ewallet_topup_history};
    },
    clear(state) {
      return {...state, topup_pending: {}};
    },
    setRegisterFlag(state, flag) {
      return {...state, allowDoRegister: flag};
    },
    setDefaultBonus(state) {
      return {
        ...state,
        bonus_histories: [],
      };
    },
    setInitialBonus(state, data) {
      return {...state, bonus_histories: data?.data?.bonus_history};
    },
    setBonus(state, data) {
      return {
        ...state,
        bonus_histories: state.bonus_histories.concat(data?.data?.bonus_history),
      };
    },
    setDefaultWithdrawal(state) {
      return {
        ...state,
        withdrawal_histories: [],
      };
    },
    setInitialWithdrawal(state, data) {
      return {...state, withdrawal_histories: data?.data};
    },
    setWithdrawal(state, data) {
      return {
        ...state,
        withdrawal_histories: state.withdrawal_histories.concat(data?.data),
      };
    },
    setDefaultInHistories(state) {
      return {
        ...state,
        wallet_in_histories: [],
      };
    },
    setInitialInHistories(state, data) {
      return {...state, wallet_in_histories: data?.data?.ewallethistorydata};
    },
    setInHistories(state, data) {
      return {
        ...state,
        wallet_in_histories: state.wallet_in_histories.concat(data?.data?.ewallethistorydata),
      };
    },
    setDefaultOutHistories(state) {
      return {
        ...state,
        wallet_out_histories: [],
      };
    },
    setInitialOutHistories(state, data) {
      return {...state, wallet_out_histories: data?.data?.ewallethistorydata};
    },
    setOutHistories(state, data) {
      return {
        ...state,
        wallet_out_histories: state.wallet_out_histories.concat(data?.data?.ewallethistorydata),
      };
    },
  },
  selectors: {
    getFormattedBalance() {
      return state => numeralFormatter(state.balance);
    },
    pendingTopup() {
      return state => R.path(['topup_pending'], state);
    },
  },
  effects: dispatch => ({
    async checkSaldoRegister() {
      const response = await api.checkSaldoForRegister();
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.balance.setRegisterFlag(true);
        return Promise.resolve(response.data);
      }
      await dispatch.member.setRegisterFlag(false);
    },
    async getBalance(data, rootState) {
      const username = store.select.auth.getUsername(rootState);
      const response = await api.walletBalance({username});
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.balance.setBalance(response.data);
        await dispatch.balance.checkSaldoRegister();
        return Promise.resolve(response.data);
      }
      return Promise.reject(response);
    },
    async getTopupPending(data, rootState) {
      const username = store.select.auth.getUsername(rootState);
      const response = await api.topupPending({username});
      const success = getSuccess(response.data);
      if (response.ok && success) {
        dispatch.balance.setTopupPending(response.data);
        return Promise.resolve(response.data);
      }
      errorGenerator(response, response.data?.data?.error);
      return Promise.reject(response);
    },
    async getTopupHistory(data, rootState) {
      const username = store.select.auth.getUsername(rootState);
      const response = await api.topupHistory({username});
      const success = getSuccess(response.data);
      if (response.ok && success) {
        dispatch.balance.setTopupHistory(response.data);
        return Promise.resolve(response.data);
      }
      errorGenerator(response);
      return Promise.reject(response);
    },
    async getBonusHistory(params, rootState) {
      const response = await api.bonusHistory({...params, username: rootState?.auth?.userData?.username});
      const success = getSuccess(response.data);
      if (response.ok && success) {
        if (params.offset == 0) {
          await dispatch.balance.setInitialBonus(response.data);
        } else {
          await dispatch.balance.setBonus(response.data);
        }
        return Promise.resolve(response.data);
      }
      if (!success && params.offset == 0) {
        await dispatch.balance.setDefaultBonus();
      }
    },
    async getWithdrawalHistory(data) {
      const response = await api.withdrawalHistory(transformToFormData(data));
      const success = getSuccess(response.data);
      if (response.ok && success) {
        if (data.offset == 0) {
          await dispatch.balance.setInitialWithdrawal(response.data);
        } else {
          await dispatch.balance.setWithdrawal(response.data);
        }
        return Promise.resolve(response.data);
      }
      if (!success && data.offset == 0) {
        await dispatch.balance.setDefaultWithdrawal();
      }
    },
    async doTopup(data, rootState) {
      const username = store.select.auth.getUsername(rootState);
      const response = await api.topup({username, ...data});
      const success = getSuccess(response.data);
      const error = R.path(['data', 'error'], response.data);
      if (response.ok && success) {
        goTo('TopUpConfirmScreen');
        return Promise.resolve(response.data);
      }
      errorGenerator(response, !success ? error : null);
      return Promise.reject(response);
    },
    async doTopupConfirm(data, rootState) {
      const username = store.select.auth.getUsername(rootState);
      const response = await api.topupConfirm({username, ...data});
      const success = getSuccess(response.data);
      const error = R.path(['data', 'error'], response.data);
      const message = R.path(['data', 'message'], response.data);
      if (response.ok && success) {
        dispatch.balance.clear();
        resetAndGoTo('HomeDrawer');
        showToast(message);
        return Promise.resolve(response.data);
      }
      errorGenerator(response, !success ? error : null);
      return Promise.reject(response);
    },
    async doTransfer(data, rootState) {
      const username = store.select.auth.getUsername(rootState);
      const response = await api.transfer({
        transfer_username_sender: username,
        ...data,
      });
      const success = getSuccess(response.data);
      const error = R.path(['data', 'error'], response.data);
      if (response.ok && success) {
        dispatch.balance.getBalance();
        goBack();
        showToast('Transfer berhasil');
        return Promise.resolve(response.data);
      }
      errorGenerator(response, !success ? error : null);
      return Promise.reject(response);
    },
    async doWithdrawal(data) {
      const response = await api.withdrawal(transformToFormData(data));
      const success = getSuccess(response.data);
      const error = R.path(['data', 'message'], response.data);
      if (response.ok && success) {
        dispatch.balance.getBalance();
        goBack();
        showToast('Proses berhasil, mohon menunggu verifikasi');
        return Promise.resolve(response.data);
      }
      errorGenerator(response, !success ? error : null);
      return Promise.reject(response);
    },
    async getWalletHistory(data) {
      const response = await api.walletHistory(transformToFormData(data));
      const success = getSuccess(response.data);
      if (response.ok && success) {
        if (data.search_type === 'IN') {
          if (data.offset == 0) {
            await dispatch.balance.setInitialInHistories(response.data);
          } else {
            await dispatch.balance.setInHistories(response.data);
          }
        } else {
          if (data.offset == 0) {
            await dispatch.balance.setInitialOutHistories(response.data);
          } else {
            await dispatch.balance.setOutHistories(response.data);
          }
        }
        return Promise.resolve(response.data);
      }
      if (!success && data.search_type === 'IN' && data.offset == 0) {
        await dispatch.balance.setDefaultInHistories();
      }
      if (!success && data.search_type === 'OUT' && data.offset == 0) {
        await dispatch.balance.setDefaultOutHistories();
      }
    },
  }),
};
