import { StyleSheet } from "react-native";
import { ApplicationStyles, Metrics, Colors, Fonts } from "../../../themes";

export default StyleSheet.create({
  headerContainer: {
    ...ApplicationStyles.rowFlex,
    paddingHorizontal: Metrics.baseMargin,
    marginVertical: Metrics.doubleBaseMargin,
    alignItems: 'center',
  },
  textFeature: {
    fontFamily: Fonts.type.bold,
    color: Colors.facebook,
    marginRight: Metrics.baseMargin,
  },
  textPromo: {
    fontFamily: Fonts.type.bold,
    fontSize: Fonts.size.small,
    color: Colors.gray700,
    marginRight: 8
  },
  image: {
    borderColor: Colors.snow,
    borderWidth: 1,
    height: 150,
    width: 200,
    marginRight: Metrics.smallMargin,
    backgroundColor: Colors.snow,
    elevation: 5,
  }
});