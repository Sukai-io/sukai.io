import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import {Text} from 'native-base';
import styles from './CartItem.styles';
import {numeralFormatter} from '../../../utils/TextUtils';
import colors from '../../../themes/Colors';
import FastImage from 'react-native-fast-image';

const CartItem = ({isLast, dataSource, onPress}) => {
  const totalPrice = parseInt(dataSource?.product_price, 10) * parseInt(dataSource?.product_qty, 10);
  return (
    <TouchableOpacity onPress={onPress} style={[styles.container, isLast && styles.isLast]}>
      <FastImage
        resizeMode="contain"
        source={{
          uri: dataSource?.product_image_url,
        }}
        style={{backgroundColor: colors.gray500, width: 60, height: 60}}
      />
      <View style={styles.infoContainer}>
        <Text style={styles.title}>{dataSource?.product_name}</Text>
        <Text style={styles.qty}>{`Qty: ${dataSource?.product_qty}`}</Text>
        <Text style={styles.qty}>{`Berat: ${dataSource?.product_weight}`}</Text>
        <Text style={styles.qty}>{`Note: ${dataSource?.product_notes || '-'}`}</Text>
        <Text style={styles.price}>{numeralFormatter(totalPrice)}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default CartItem;
