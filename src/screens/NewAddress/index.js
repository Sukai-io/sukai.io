import React, {Component} from 'react';
import {connect} from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
import {Formik} from 'formik';
import {FormValues} from './types';
import Content from './Content';
import validation from './validation';

class NewAddressScreen extends Component {
  componentDidMount() {
    const {getProvinces} = this.props;
    getProvinces();
  }

  handleSubmit = (values: FormValues, {resetForm}) => {
    const {provinces, cities, addShippingAddress} = this.props;
    const province = provinces?.find(item => item.id == values.address_province)
      ?.name;
    const city = cities?.find(item => item.id == values.address_city)?.name;

    addShippingAddress({
      ...values,
      address_province_name: province,
      address_city_name: city,
    }).then(() => {
      const {shippingAddresses, setSelectedAddress} = this.props;
      setSelectedAddress(
        shippingAddresses.find(
          item => item.address_type === values.address_type,
        ),
      );
      resetForm({});
    });
  };

  render() {
    return (
      <Formik
        initialValues={{
          address_type: '',
          address_location: '',
          address_district: '',
          address_city: null,
          address_province: null,
          address_zipcode: '',
        }}
        enableReinitialize
        onSubmit={(values, actions) => this.handleSubmit(values, actions)}
        validationSchema={validation}
        render={props => <Content {...props} />}
      />
    );
  }
}

const mapStateToProps = ({
  support: {
    provinces_rajaongkir,
    cities_rajaongkir,
    shipping_addresses: shippingAddresses,
  },
}) => ({
  provinces: provinces_rajaongkir,
  cities: cities_rajaongkir,
  shippingAddresses,
});

const mapDispatchToProps = ({support, auth}) => {
  return {
    setSelectedAddress: data => auth.setSelectedAddress(data),
    getProvinces: () => support.getProvincesRajaOngkir(),
    addShippingAddress: data => support.addShippingAddress(data),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NewAddressScreen);
