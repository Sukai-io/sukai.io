import React, {memo} from 'react';
import {Text, TextInputProps} from 'react-native';
import {CardItem, Item, Label, Input} from 'native-base';
import styles from '../styles';
import {Colors, ApplicationStyles} from '../../../themes';

type Props = TextInputProps & {
  label: String,
  error: String,
  style: Object,
};

const InputText = ({label, error, style, ...otherProps}: Props) => (
  <>
    <CardItem style={{paddingBottom: 0, marginTop: 8}}>
      <Item stackedLabel style={ApplicationStyles.flex}>
        <Label style={styles.label}>{label}</Label>
        <Input style={[styles.input, style]} placeholderTextColor={Colors.gray300} {...otherProps} />
      </Item>
    </CardItem>
    {error && <Text style={styles.error}>{error}</Text>}
  </>
);

export default memo(InputText);
