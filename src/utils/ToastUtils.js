import {Toast} from 'native-base';

const showToast = (message, title, duration) => {
  return Toast.show({
    text: message,
    duration: duration || 3000,
    buttonText: title || 'OK',
  });
};

const showToastDanger = message => {
  return Toast.show({
    text: message,
    buttonText: 'OK',
    duration: 3000,
    type: 'danger',
  });
};

export {showToast, showToastDanger};
