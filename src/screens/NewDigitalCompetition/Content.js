import React, {useCallback} from 'react';
import FastImage from 'react-native-fast-image';
import ImagePicker from 'react-native-image-picker';

// Styles
import {Container, Text, Body, CardItem, Label} from 'native-base';
import {Button, Card} from 'react-native-elements';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Props} from './Props';
import styles from './styles';
import {imageResizer} from '../../utils/ImageUtils';
import InputText from '../NewMember/components/InputText';
import HeaderProfile from '../EditProfile/components/HeaderProfile';
import {useSelector, shallowEqual} from 'react-redux';
import Loader from '../../components/indicator/Loader';

const Content = ({values, touched, errors, setFieldValue, handleChange, handleBlur, handleSubmit}: Props) => {
  const loading = useSelector(({loading}) => loading.effects.support.addDigitalCompetition, shallowEqual);
  const handleImagePicker = useCallback(() => {
    ImagePicker.showImagePicker(null, async response => {
      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        console.log(response.fileSize);
        const uri = await imageResizer(response.uri);
        const imgSource = {
          data: response.data,
          name: response.fileName,
          type: 'image/jpeg',
          uri,
          path: response.path,
          width: response.width,
          height: response.height,
          fileSize: response.fileSize,
          isVertical: response.isVertical,
          originalRotation: response.originalRotation,
        };
        setFieldValue('digcom_image', imgSource);
      }
    });
  }, []);
  return (
    <Container style={styles.mainContainer}>
      <HeaderProfile title="Tambah" onSave={handleSubmit} />
      <Loader loading={loading} />
      <KeyboardAwareScrollView style={styles.container} keyboardShouldPersistTaps="always">
        <Card containerStyle={styles.cardContainer}>
          <CardItem>
            <Body>
              <Text style={styles.title}>Informasi Digital Competition</Text>
            </Body>
          </CardItem>
          <InputText
            label="Judul *"
            value={values.title}
            error={touched.title && errors.title}
            onBlur={handleBlur('title')}
            onChangeText={handleChange('title')}
          />
          <InputText
            label="Deskripsi *"
            value={values.description}
            error={touched.description && errors.description}
            onBlur={handleBlur('description')}
            onChangeText={handleChange('description')}
          />
          <InputText
            label="Url *"
            value={values.url}
            error={touched.url && errors.url}
            onBlur={handleBlur('url')}
            onChangeText={handleChange('url')}
            autoCapitalize="none"
          />
          <CardItem>
            <Body>
              <Label style={styles.label}>Image *</Label>
              {values.digcom_image?.uri && (
                <FastImage resizeMode="cover" source={{uri: values.digcom_image?.uri}} style={styles.image} />
              )}
              <Button
                onPress={handleImagePicker}
                raised
                containerStyle={styles.buttonImageContainer}
                buttonStyle={styles.buttonImage}
                titleStyle={styles.buttonTitle}
                icon={{
                  name: 'ios-image',
                  type: 'ionicon',
                  color: 'white',
                  size: 20,
                }}
                title="CHOOSE IMAGE"
              />
            </Body>
          </CardItem>
          {errors.digcom_image && <Text style={styles.error}>{errors.digcom_image}</Text>}
        </Card>
      </KeyboardAwareScrollView>
    </Container>
  );
};

export default Content;
