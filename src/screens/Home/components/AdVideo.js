import React from 'react';
import {View, Text, ImageBackground, TouchableOpacity, Dimensions} from 'react-native';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import {Icon} from 'native-base';
import {Images, ApplicationStyles, Colors} from '../../../themes';
import styles from './AdVideo.style';

const Divider = () => <View style={styles.divider} />;

const VideoItem = ({
  item: {image_thumbnail, title},
  //   {
  //   brand, company, duration, point, view, type
  // }
}) => {
  let company,
    duration,
    point,
    view,
    type = '';
  return (
    <TouchableOpacity style={styles.container}>
      <ImageBackground style={styles.imageVideo} source={{uri: image_thumbnail}} resizeMode="stretch">
        <View style={ApplicationStyles.screen.flexCenterJustity}>
          {/* <Icon type="AntDesign" name="play" style={styles.iconPlay} /> */}
        </View>
        <View style={styles.blackTransparentView}>
          <View>
            <Text style={styles.textBrand}>{title || '-'}</Text>
            <Text style={styles.textCompany}>{company || '-'}</Text>
          </View>
          <Text style={styles.textDuration}>{duration}</Text>
        </View>
        <View style={styles.blueTransparentView}>
          <View style={ApplicationStyles.screen.flexCenterJustity}>
            <Text style={styles.textBold}>{`${point || '-'} Points`}</Text>
          </View>
          <Divider />
          <View style={ApplicationStyles.screen.flexCenterJustity}>
            <Text style={styles.textBold}>{`${view || '-'} Viewed`}</Text>
          </View>
          <Divider />
          <View style={ApplicationStyles.screen.flexCenterJustity}>
            <Text style={styles.textBold}>{type || '-'}</Text>
          </View>
        </View>
      </ImageBackground>
    </TouchableOpacity>
  );
};

const AdVideo = ({data = []}) => {
  const [snapIndex, setSnapindex] = React.useState(0);
  return (
    <>
      <Carousel
        data={data}
        onBeforeSnapToItem={snapIndex => setSnapindex(snapIndex)}
        onSnapToItem={snapIndex => setSnapindex(snapIndex)}
        activeSlideAlignment="start"
        inactiveSlideScale={1}
        firstItem={snapIndex}
        renderItem={VideoItem}
        sliderWidth={Dimensions.get('window').width}
        itemWidth={Dimensions.get('window').width}
      />
      <Pagination
        dotsLength={data.length}
        activeDotIndex={snapIndex}
        containerStyle={{justifyContent: 'center', paddingVertical: 10}}
        dotContainerStyle={{marginHorizontal: 3}}
        dotStyle={{
          width: 8,
          height: 8,
          borderRadius: 5,
          backgroundColor: Colors.gray900,
          elevation: 2,
        }}
        inactiveDotStyle={{
          backgroundColor: Colors.gray800,
        }}
        inactiveDotOpacity={0.2}
        inactiveDotScale={1}
      />
    </>
  );
};

export default AdVideo;
