import { StyleSheet, Dimensions } from 'react-native'
import { ApplicationStyles, Metrics } from '../../themes'

const deviceHeight = Dimensions.get('window').height

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  sectionContainer: {
    marginTop: deviceHeight - deviceHeight / 4 - deviceHeight / 3 - deviceHeight / 8,
  },
  marginTop: {
    marginTop: Metrics.smallMargin,
  },
})
