import {
  object as yupObject,
  string as yupString,
  number as yupNumber,
} from 'yup';

export default yupObject().shape({
  price_index: yupNumber(),
  ppob_provider: yupString().required(),
  ppob_number: yupNumber()
    .typeError('Hanya boleh berisi angka')
    .required(),
});
