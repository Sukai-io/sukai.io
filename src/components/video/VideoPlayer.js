import React, {useState, useRef} from 'react';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {StyleSheet, View} from 'react-native';
import MediaControls, {PLAYER_STATES} from 'react-native-media-controls';
import Video from 'react-native-video';
import {PixelRatio} from 'react-native';
import {Dimensions} from 'react-native';
import {Colors, Fonts} from '../../themes';
import VideoPlayerModal from './VideoPlayerModal';

const VideoPlayer = ({video, style, videoStyle}) => {
  const videoPlayer = useRef(null);
  const [currentTime, setCurrentTime] = useState(0);
  const [duration, setDuration] = useState(0);
  const [isFullScreen, setIsFullScreen] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [paused, setPaused] = useState(false);
  const [playerState, setPlayerState] = useState(PLAYER_STATES.PLAYING);

  const onSeek = seek => {
    videoPlayer?.current.seek(seek);
  };

  const onPaused = () => {
    setPaused(!paused);
    setPlayerState(paused ? PLAYER_STATES.PLAYING : PLAYER_STATES.PAUSED);
  };

  const onReplay = () => {
    setPlayerState(PLAYER_STATES.PLAYING);
    videoPlayer?.current.seek(0);
  };

  const onProgress = data => {
    // Video Player will continue progress even if the video already ended
    if (!isLoading) {
      setCurrentTime(data.currentTime);
    }
  };

  const onLoad = data => {
    setDuration(data.duration);
    setIsLoading(false);
  };

  const onLoadStart = () => setIsLoading(true);

  const onEnd = () => {
    // Uncomment this line if you choose repeat=false in the video player
    // setPlayerState(PLAYER_STATES.ENDED);
  };

  const onSeeking = currentTime => setCurrentTime(currentTime);

  const onFullScreen = () => {
    if(!isFullScreen){
      onPaused()
    }
    setIsFullScreen(!isFullScreen)
  };

  return (
    <View style={style}>
      <Video
        onEnd={onEnd}
        onLoad={onLoad}
        onLoadStart={onLoadStart}
        onProgress={onProgress}
        paused={paused}
        ref={ref => (videoPlayer.current = ref)}
        resizeMode="cover"
        source={video}
        repeat
        style={[styles.backgroundVideo, videoStyle]}
        volume={0.0}
      />
      <MediaControls
        isFullScreen={isFullScreen}
        duration={duration}
        isLoading={isLoading}
        mainColor={Colors.gray400}
        onFullScreen={onFullScreen}
        onPaused={onPaused}
        onReplay={onReplay}
        onSeek={onSeek}
        onSeeking={onSeeking}
        playerState={playerState}
        showOnStart={false}
        progress={currentTime}
        
      />
      <VideoPlayerModal isVisible={isFullScreen} toggleModal={onFullScreen} videoDetail={video} />
    </View>
  );
};

const styles = StyleSheet.create({
  backgroundVideo: {
    height: 200,
    width: '100%',
  },
  // mediaControls: {
  //   height: '100%',
  //   flex: 1,
  //   alignSelf: 'center',
  // },
  container: {
    flex: 1,
  },
  toolbar: {
    marginTop: 30,
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 5,
  },
  mediaPlayer: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'black',
  },
  modal: {margin: 0},
  modalContent: {
    backgroundColor: 'black',
    margin: 0,
  },
  videoPlayer: {
    height: PixelRatio.roundToNearestPixel(Dimensions.get('window').width / (16 / 9)),
    alignSelf: 'stretch',
  },
  closeBtn: {position: 'absolute', top: 0, right: 0},
  closeBtnText: {
    fontFamily: Fonts.type.base,
    color: 'white',
    marginTop: getStatusBarHeight() + 16,
    marginRight: 16,
    fontSize: Fonts.size.regular,
  },
});

export default VideoPlayer;
