import { object as yupObject, string as yupString } from 'yup'

export default yupObject().shape({
  username: yupString().required('A username is required.'),
  password: yupString().required('A password is required.'),
})
