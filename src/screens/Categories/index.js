/* eslint-disable no-unused-vars */
/* eslint-disable react/prefer-stateless-function */
import React, {Component} from 'react';
import {Image, Platform} from 'react-native';
import {connect} from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import {Container, Content, Icon, Text, Left, Body, Picker, Form, CardItem, Thumbnail} from 'native-base';
import {Dropdown} from 'react-native-material-dropdown';
import {Card} from 'react-native-elements';
import styles from './styles';
import HeaderBack from '../../components/header/HeaderBack';
import {Colors, ApplicationStyles, Fonts} from '../../themes';
import HeaderCustom from '../../components/header/HeaderCustom';
import ProductItem from './components/ProductItem';
import Horizontal from '../../components/wrapper/Horizontal';
import VerticalList from '../../components/list/VerticalList';
import { LocalizationContext } from '../../localization';

const platform = Platform.OS;

class CategoriesScreen extends Component {
  static contextType = LocalizationContext;

  constructor(props) {
    super(props);
    const {navigation} = props;
    const id = navigation.getParam('id', null);
    this.state = {
      offset: 0,
      initialPage: true,
      selectedCategoryId: id,
    };
    this.handleLoadMore = this.handleLoadMore.bind(this);
  }

  componentDidMount() {
    const {getProduct, navigation} = this.props;
    const {offset, selectedCategoryId} = this.state;
    getProduct({limit: 10, offset, search_category: selectedCategoryId});
  }

  handleLoadMore() {
    this.setState(
      prevState => ({
        offset: prevState.offset + 10,
        initialPage: false,
      }),
      () => {
        const {getProduct} = this.props;
        const {offset, selectedCategoryId} = this.state;
        getProduct({limit: 10, offset, search_category: selectedCategoryId});
      },
    );
  }

  handleSelectedCategory = value => {
    this.setState(
      {
        initialPage: true,
        offset: 0,
        selectedCategoryId: value,
      },
      () => {
        const {getProduct} = this.props;
        getProduct({limit: 10, offset: 0, search_category: value});
      },
    );
  };

  render() {
    const {navigation, products, productCategories} = this.props;
    const {selectedCategoryId} = this.state;
    const {translations} = this.context
    const selectedCategoryName = productCategories?.find(item => item.id === selectedCategoryId)?.category_name;
    return (
      <Container style={styles.mainContainer}>
        <HeaderBack title="Categories" />
        <Content style={styles.container}>
          <Card containerStyle={styles.cardContainer}>
            {productCategories.length ? (
              <Dropdown
                containerStyle={ApplicationStyles.flex}
                label={translations.LABEL_CHOOSE_SERVICE}
                data={productCategories?.map(item => ({
                  label: item.category_name,
                  value: item.id,
                }))}
                onChangeText={this.handleSelectedCategory}
                dropdownOffset={styles.pickerOffset}
                itemColor={Colors.gray600}
                fontSize={Fonts.size.medium}
                textColor={Colors.primary}
                baseColor={Colors.gray600}
                value={selectedCategoryId}
                itemTextStyle={{fontFamily: Fonts.type.base}}
                labelTextStyle={{fontFamily: Fonts.type.base}}
                style={{fontFamily: Fonts.type.base}}
              />
            ) : null}
          </Card>
          <Card containerStyle={styles.cardContainer}>
            <Text style={styles.title}>{translations.LABEL_CATEGORY_WITH.replace('$name', selectedCategoryName)}</Text>
            <VerticalList
              data={products}
              windowSize={13}
              maxToRenderPerBatch={6}
              updateCellsBatchingPeriod={30}
              renderItem={({item}) => (
                <ProductItem
                  onPress={() =>
                    navigation.navigate('ProductDetailScreen', {
                      id: item.product_id,
                    })
                  }
                  name={item.product_name}
                  price={item.product_price}
                  uri={item.product_thumbnail_url}
                  userName={item.seller}
                />
              )}
              numColumns={2}
              onEndReached={this.handleLoadMore}
            />
          </Card>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    products: state.product.product_list,
    productCategories: state.product.product_category_list || [],
  };
};

const mapDispatchToProps = ({product}) => {
  return {
    getProduct: data => product.getProduct(data),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CategoriesScreen);
