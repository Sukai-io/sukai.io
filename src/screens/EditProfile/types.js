export type FormValues = {
  member_name: String,
  member_email: String,
  member_address: String,
  member_province: String,
  member_city: String,
  member_district: String,
  member_gender: String,
  member_phone: String,
  member_idcard: String,
  member_bank: String,
  member_bill: String,
  member_bill_name: String,
  member_branch: String,
};
