export default {
  INFO_AD_EMPTY_LIST: `You don't have ads yet`,

  INFO_AN_ERROR_OCCURED: 'An error occured',
  
  INFO_BALANCE_WILL_DEDUCTED: 'Your current balance is $balance. Your balance will be deducted after making a purchase',

  INFO_CONFIRM: 'Confirm',
  INFO_CONFIRM_ADDRESS_DELETE: 'Are you sure to delete this address?',

  INFO_ERROR_CLIENT: 'Please check your form again',
  INFO_ERROR_CONNECTION: '',
  INFO_ERROR_SERVER: 'There is a problem with the server, please try again later.',
  INFO_ERROR_TIMEOUT: 'Timeout, please check your connections.',

  INFO_REGISTRATION_AS_CLIENT: 'Choice client if you or your company want to post ads or become an official distributor Store on Sukai marketplace',
  INFO_REGISTRATION_AS_MEMBER: 'Choice member if you want to become a Sukai member and get all the conveniences on the Sukai application (Get the point, Marketplace, Video / Advertising Fun Click, Etc)',

  INFO_SUCCESS_SET_PRIMARY_ADDRESS: 'Successfully to set primary address',
};
