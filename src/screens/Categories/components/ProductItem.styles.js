import {StyleSheet} from 'react-native';
import {ApplicationStyles, Metrics, Colors} from '../../../themes';

export default StyleSheet.create({
  cardContainer: {
    ...ApplicationStyles.shadow,
    ...ApplicationStyles.flex,
    padding: 0,
    marginRight: 0,
    marginLeft: 0,
    marginTop: 0,
    // marginBottom: Metrics.doubleBaseMargin,
  },
  icon: {
    fontSize: Metrics.icons.tiny,
    color: Colors.gray400,
  },
});
