import {StyleSheet} from 'react-native';
import {ApplicationStyles, Metrics, Colors, Fonts} from '../../themes';

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  list: {
    marginLeft: Metrics.baseMargin,
    marginRight: Metrics.baseMargin,
    paddingVertical: Metrics.doubleBaseMargin,
    borderBottomColor: Colors.gray300,
    borderBottomWidth: 0.5,
  },
  title: {
    fontSize: 16,
    fontWeight: '600',
    color: Colors.gray800,
    textDecorationLine: 'underline',
  },
  desc: {
    fontSize: 14,
    marginTop: 16,
    color: Colors.gray800,
  },
  date: {
    fontSize: Fonts.size.small,
    color: Colors.gray500,
  },
  amount: {
    fontSize: Fonts.size.medium,
    color: Colors.green500,
    marginLeft: 16,
  },
  activity: {
    ...ApplicationStyles.flex,
    fontSize: Fonts.size.medium,
    color: Colors.gray700,
    textAlign: 'right',
  },
  success: {
    ...ApplicationStyles.flex,
    fontSize: Fonts.size.medium,
    color: Colors.green500,
    textAlign: 'right',
  },
});
