import {object as yupObject, string as yupString} from 'yup';

export default yupObject().shape({
  ppob_code: yupString().required(),
  ppob_provider: yupString().required(),
  ppob_number: yupString().required(),
});
