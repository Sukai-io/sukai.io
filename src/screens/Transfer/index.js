/* eslint-disable react/prefer-stateless-function */
import React, {Component} from 'react';
import {connect} from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import {ActivityIndicator, Alert} from 'react-native';
import {Container, Content, Text, Card, Item, Label, Input, Form} from 'native-base';
import {Button} from 'react-native-elements';
import {Formik, FormikProps} from 'formik';
import styles from './styles';
import HeaderBack from '../../components/header/HeaderBack';
import validation from './validation';
import store from '../../config/CreateStore';
import ApiClient from '../../api/ApiClient';
import {transformToFormData} from '../../api/ApiHelper';
import Loader from '../../components/indicator/Loader';
import {numeralFormatter} from '../../utils/TextUtils';
import { Fonts } from '../../themes';

type Props = {
  loading: Boolean,
  loadingPost: Boolean,
  balance: String,
  username: String,
  getBalance: Function,
  doTransfer: Function,
};

type FormValues = {
  username: String,
  amount: Number,
};

class WalletScreen extends Component<Props> {
  state = {
    loadingGet: false,
    username: ''
  };

  componentDidMount() {
    let username = this.props.navigation.getParam('username');
    if (username) {
        this.setState({  username })
    }
    const {getBalance} = this.props;
    getBalance();
  }

  handleLoading = value => {
    this.setState({loadingGet: value});
  };

  renderForm = ({handleSubmit, setFieldValue, setFieldTouched, touched, errors, values}: FormikProps<FormValues>) => {
    const {loading, loadingPost, balance} = this.props;
    const {loadingGet} = this.state;
    return (
      <Container style={styles.mainContainer}>
        <Loader loading={loadingGet} />
        <HeaderBack withoutRight title="Transfer" />
        <Content style={styles.container}>
          <Card style={styles.cardContainer}>
            <Text style={styles.subTitle}>Sukai Wallet Balance</Text>
            {loading ? <ActivityIndicator /> : <Text style={styles.title}>{balance}</Text>}
          </Card>
          <Form>
            <Item stackedLabel>
              <Label style={{fontFamily: Fonts.type.base}}>Username *</Label>
              <Input
                autoCapitalize="none"
                onBlur={() => setFieldTouched('username')}
                onChangeText={value => setFieldValue('username', value)}
              />
            </Item>
            {errors.username && touched.username && <Text style={styles.error}>{errors.username}</Text>}
            <Item stackedLabel>
              <Label style={{fontFamily: Fonts.type.base}}>Transfer Amount</Label>
              <Input
                keyboardType="number-pad"
                onBlur={() => setFieldTouched('amount')}
                onChangeText={value => setFieldValue('amount', value)}
              />
            </Item>
            {errors.amount && touched.amount && <Text style={styles.error}>{errors.amount}</Text>}
            <Button
              loading={loadingPost}
              disabled={loadingPost}
              onPress={handleSubmit}
              raised
              containerStyle={styles.buttonContainer}
              buttonStyle={styles.button}
              icon={{name: 'ios-send', type: 'ionicon', color: 'white'}}
              title="TRANSFER"
            />
          </Form>
        </Content>
      </Container>
    );
  };

  handleSubmit = (values: FormValues) => {
    this.handleLoading(true);
    ApiClient.searchMember(transformToFormData({username: values.username})).then(({data: {data, success}, ok}) => {
      this.handleLoading(false);
      setTimeout(() => {
        if (ok && success) {
          const name = data?.data?.data?.member_name || '-';
          Alert.alert(
            'Konfirmasi',
            `Anda akan melakukan transfer ke ${name} sebesar ${numeralFormatter(
              values.amount,
            )}. Yakin ingin melanjutkan?`,
            [
              {
                text: 'Batal',
                onPress: () => {},
                style: 'cancel',
              },
              {
                text: 'OK',
                onPress: () => {
                  const {doTransfer} = this.props;
                  doTransfer({transfer_username_receiver: values.username, transfer_amount: values.amount});
                },
              },
            ],
          );
        } else {
          Alert.alert('Terjadi Kesalahan', data?.error);
        }
      }, 500);
    });
  };

  render() {
    
    return (
      <Formik
        initialValues={{username: this.state.username, amount: '657567'}}
        enableReinitialize
        onSubmit={(values: FormValues) => this.handleSubmit(values)}
        validationSchema={validation}
        render={(actions: FormikProps<FormValues>) => this.renderForm(actions)}
      />
    );
  }
}

const mapStateToProps = ({loading, balance}) => {
  return {
    loading: loading.effects.balance.getBalance,
    loadingPost: loading.effects.balance.doTransfer,
    balance: store.select.balance.getFormattedBalance(balance),
  };
};

const mapDispatchToProps = ({balance}) => {
  return {
    getBalance: () => balance.getBalance(),
    doTransfer: data => balance.doTransfer(data),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(WalletScreen);
