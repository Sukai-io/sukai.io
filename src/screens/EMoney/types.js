export type FormValues = {
  price_index: Number,
  ppob_provider: String,
  ppob_number: String,
}
