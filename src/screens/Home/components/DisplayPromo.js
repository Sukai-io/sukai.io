import React from 'react';
import { View, Text } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { LocalizationContext } from '../../../localization';
import styles from './DisplayPromo.style';

const DisplayPromo = () => {
  const { translations } = React.useContext(LocalizationContext);

  return (
    <>
      <View style={styles.headerContainer}>
        <Text style={styles.textFeature}>{translations.TITLE_DISPLAY_PROMO}</Text>
        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          <TouchableOpacity>
            <Text style={styles.textPromo}>{translations.TITLE_ACCESSORIES}</Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text style={styles.textPromo}>{translations.TITLE_ELECTRONICS}</Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text style={styles.textPromo}>{translations.TITLE_MOBILE_PHONE}</Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text style={styles.textPromo}>{translations.TITLE_HOME_INTERIOR}</Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text style={styles.textPromo}>{translations.TITLE_SHOES}</Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    </>
  );
};

export default DisplayPromo;
