import { object as yupObject, string as yupString, boolean as yupBoolean, number as yupNumber } from 'yup';

export default yupObject().shape({
  sponsored: yupString(),
  reg_member_sponsor_id: yupString().nullable(),
  reg_member_sponsor: yupString().min(4),
  reg_member_username: yupString()
    .min(6)
    .required(),
  is_member_username_valid: yupBoolean().oneOf([true], 'Username tidak dapat digunakan karena sudah terdaftar.'),
  reg_member_password: yupString().required(),
  reg_member_name: yupString().required(),
  reg_member_email: yupString()
    .email()
    .required(),
  reg_member_address: yupString().required(),
  reg_member_country: yupString().nullable().required(),
  reg_member_province: yupString().when('countryName', {
    is: 'Indonesia',
    then: yupString().nullable().required(),
    otherwise: yupString().nullable(),
  }),
  reg_member_city: yupString().when('reg_member_province', {
    is: null || undefined,
    then: yupString().nullable(),
    otherwise: yupString()
      .nullable()
      .required(),
  }),
  reg_member_district: yupString().when('reg_member_city', {
    is: null || undefined,
    then: yupString().nullable(),
    otherwise: yupString().nullable().required(),
  }),
  reg_member_gender: yupString().when('asClient', {
    is: false,
    then: yupString()
      .nullable()
      .required(),
  }),
  reg_member_phone: yupNumber().when('asClient', {
    is: false,
    then: yupNumber()
      .typeError('Hanya boleh berisi angka')
      .required(),
  }),
  reg_member_idcard: yupNumber().when('asClient', {
    is: false,
    then: yupNumber()
      .typeError('Hanya boleh berisi angka')
      .required(),
  }),
  reg_member_bank: yupString().when('asClient', {
    is: false,
    then: yupString().required(),
  }),
  reg_member_bill: yupNumber().when('asClient', {
    is: false,
    then: yupNumber()
      .typeError('Hanya boleh berisi angka')
      .required(),
  }),
  reg_member_bill_name: yupString().when('asClient', {
    is: false,
    then: yupString().required(),
  }),
  reg_member_branch: yupString().when('asClient', {
    is: false,
    then: yupString().required(),
  }),
});
