/* eslint-disable react/state-in-constructor */
import React, {useCallback, useContext} from 'react';
import {Container} from 'native-base';
import {Card, Button} from 'react-native-elements';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import styles from './styles';
import HeaderProfile from '../EditProfile/components/HeaderProfile';
import {Props} from './Props';
import {useSelector, shallowEqual, useDispatch} from 'react-redux';
import InputText from '../NewMember/components/InputText';
import SelectPicker from '../NewMember/components/SelectPicker';
import Loader from '../../components/indicator/Loader';
import {Colors} from '../../themes';
import { LocalizationContext } from '../../localization';

const Content = ({
  touched,
  errors,
  values,
  setFieldValue,
  handleBlur,
  handleChange,
  handleSubmit,
  onPrimaryAddress,
  onDeleteAddress,
}: Props) => {
  const {translations} = useContext(LocalizationContext)
  const {
    support: {getCitiesRajaOngkir},
  } = useDispatch();
  const {
    provinces_rajaongkir: provinces,
    cities_rajaongkir: cities,
  } = useSelector(({support}) => support, shallowEqual);
  const loading = useSelector(
    ({loading}) => loading.effects.support.addShippingAddress,
    shallowEqual,
  );
  const selectedAddress = useSelector(
    ({auth}) => auth.selectedAddress,
    shallowEqual,
  );
  const handleProvinceChanged = useCallback(
    value => {
      getCitiesRajaOngkir({province_id: value[0]});
      setFieldValue('address_province', value[0]);
      setFieldValue(
        'address_province_name',
        provinces?.find(item => item.id == value[0])?.name,
      );
      if (values.address_city || values.address_district) {
        setFieldValue('address_city', null);
        setFieldValue('address_city_name', null);
        setFieldValue('address_district', '');
      }
    },
    [provinces],
  );
  const handleCityChanged = useCallback(
    value => {
      setFieldValue('address_city', value[0]);
      setFieldValue(
        'address_city_name',
        cities?.find(item => item.id == value[0])?.name,
      );
      values.reg_member_district && setFieldValue('address_district', '');
    },
    [cities],
  );
  return (
    <Container style={styles.mainContainer}>
      <HeaderProfile title={translations.TITLE_EDIT_ADDRESS} onSave={handleSubmit} />
      <Loader loading={loading} />
      <KeyboardAwareScrollView
        style={styles.container}
        keyboardShouldPersistTaps="always">
        <Card containerStyle={styles.cardContainer}>
          <InputText
            label={`${translations.LABEL_ADDRESS_SAVE_AS} *`}
            error={touched.address_type && errors.address_type}
            onBlur={handleBlur('address_type')}
            onChangeText={handleChange('address_type')}
            value={values.address_type}
          />
          <InputText
            label={`${translations.LABEL_ADDRESS} *`}
            error={touched.address_location && errors.address_location}
            onBlur={handleBlur('address_location')}
            onChangeText={handleChange('address_location')}
            value={values.address_location}
          />
          <InputText
            label={`${translations.LABEL_POSTAL_CODE} *`}
            keyboardType="number-pad"
            error={touched.address_zipcode && errors.address_zipcode}
            onBlur={handleBlur('address_zipcode')}
            onChangeText={handleChange('address_zipcode')}
            value={values.address_zipcode}
          />
          <SelectPicker
            selectText={`${translations.LABEL_PROVINCE} *`}
            items={provinces}
            selectedItems={
              values.address_province ? [values.address_province] : []
            }
            onSelectedItemsChange={handleProvinceChanged}
            error={touched.address_province && errors.address_province}
          />
          {cities.length > 0 && (
            <SelectPicker
              selectText={`${translations.LABEL_CITY} *`}
              items={cities}
              selectedItems={values.address_city ? [values.address_city] : []}
              onSelectedItemsChange={handleCityChanged}
              error={touched.address_city && errors.address_city}
            />
          )}
          {values.address_city && (
            <InputText
              label={translations.LABEL_SUBDISTRICT}
              error={touched.address_district && errors.address_district}
              onBlur={handleBlur('address_district')}
              onChangeText={handleChange('address_district')}
              value={values.address_district}
            />
          )}
        </Card>
        <Card containerStyle={styles.cardContainer}>
          {selectedAddress?.id !== values.address_id && (
            <Button
              onPress={onPrimaryAddress}
              raised
              containerStyle={styles.buttonContainer}
              buttonStyle={styles.buttonConfirm}
              titleStyle={styles.buttonContainerTextStyle}
              title={translations.ACTION_SET_ADDRESS_PRIMARY.toUpperCase()}
            />
          )}
          <Button
            onPress={onDeleteAddress}
            raised
            containerStyle={[styles.buttonContainer, {marginTop: 8}]}
            buttonStyle={[
              styles.buttonConfirm,
              {backgroundColor: Colors.error},
            ]}
            titleStyle={styles.buttonContainerTextStyle}
            title={translations.ACTION_DELETE_ADDRESS.toUpperCase()}
          />
        </Card>
      </KeyboardAwareScrollView>
    </Container>
  );
};
export default Content;
