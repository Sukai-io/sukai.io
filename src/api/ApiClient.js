import apisauce from 'apisauce';
import store from '../config/CreateStore';
import Axios from 'axios';

export const baseURL = 'https://sukai.io/api';
const customAxiosInstance = Axios.create({
  baseURL,
  headers: {
    'Cache-Control': 'no-cache',
  },
  timeout: 20000,
});
const api = apisauce.create({
  baseURL,
  headers: {
    'Cache-Control': 'no-cache',
    'Content-Type': 'application/json',
  },
  timeout: 20000,
  axiosInstance: customAxiosInstance,
});
const apiFormData = apisauce.create({
  baseURL,
  headers: {
    'Cache-Control': 'no-cache',
    'Content-Type': 'multipart/form-data',
  },
  axiosInstance: customAxiosInstance,
  timeout: 60000,
});
const naviMonitor = response => {
  const url = `${response.config.url}`;
  console.log(`[API] ${url}`, response);
};
api.addMonitor(naviMonitor);
api.addRequestTransform(request => {
  const {getState} = store;
  if (getState().auth.isLoggedIn) {
    request.headers['X-Auth-Token'] = getState().auth.authToken;
  }
});
apiFormData.addRequestTransform(request => {
  const {getState} = store;
  if (getState().auth.isLoggedIn) {
    request.headers['X-Auth-Token'] = getState().auth.authToken;
  }
});
// api.axiosInstance.interceptors.response.use(
//   response => response,
//   async error => {
//     if (error.response && error.response.status == '403') {
//       const {dispatch} = store;
//       await dispatch.auth.doLogout();
//       goTo('Auth');
//     }
//     return Promise.reject(error);
//   },
// );

const login = data => api.post('/authenticate', data);
const forgetPassword = data => api.post('/forgotpassword', data);
const memberData = () => api.get('/member/get');
const resetPassword = () => api.get('/resetpassword', null);
const updateProfile = data => api.post('/updateprofile', data);
const setFcmToken = data => api.post('/setfcmtoken', data);
const messageList = data => api.post('/messageslist', data);
const readMessage = data => api.post('/readmessage', data);
const logout = () => api.post('/logout');

// Balance
const walletBalance = data => api.get('/wallet/balance/get', data);
const walletHistory = data => api.post('/wallet/ewallethistorydata', data);
const topupPending = data => api.get('/wallet/topuppending', data);
const topupHistory = data => api.get('/wallet/topuphistory', data);
const withdrawal = data => api.get('/wallet/withdrawal', data);
const withdrawalHistory = params => api.post('/wallet/withdrawalhistory', params);
const bonusHistory = params => api.get('/bonus/history/get', params);
const topup = data => api.post('/wallet/topup', data);
const topupConfirm = data => api.post('/wallet/topupconfirmpayment', data);
const transfer = data => api.post('/wallet/transfer', data);

// PPOB
const provider = data => api.get('/ppob/getprovider', data);
const providerPrice = data => api.get('/ppob/getproviderprice', data);
const ppobCheckInquiry = data => api.post('/ppob/checkinquiry', data);
const ppobCheckout = data => api.post('/ppob/orderprocess', data);

// MARKETPLACE
const listProductCategory = () => api.get('/product_category');
const listProduct = data => api.post('/productlist', data);
const listProductMine = () => api.post('/productlistmine');
const detailProduct = params => api.get('/productdetail', params);
const addProduct = data => api.post('/addproduct', data);
const editProduct = data => api.post('/editproduct', data);
const editStockProduct = data => api.post('/updatestockproduct', data);
const deleteProduct = data => api.post('/deleteproduct', data);

// MEMBER
const registerMember = data => api.post('/register', data);
const registerClient = data => api.post('/registerclient', data);
const checkSaldoForRegister = () => api.post('/checksaldo_for_register');

// ORDER
const checkShippingFee = data => api.post('/checkshippingfee', data);
const orderCheckout = data => api.post('/addtocheckout', data);
const orderResiInput = data => api.post('/shippingorder', data);
const orderConfirm = data => api.post('/orderconfirmation', data);
const listOrder = data => api.post('/productcheckoutlist', data);

// Support
const listCountry = () => api.get('/countrylist');
const listProvince = () => api.get('/provincelist');
const listProvinceRajaOngkir = () => api.get('/provincelist_rajaongkir');
const listCity = params => api.get('/citylist/get', params);
const listCityRajaOngkir = params => api.get('/citylist_rajaongkir', params);
const listPartnership = () => api.get('/partnership');
const listBank = () => api.get('/banklist');
const listBudget = () => api.get('/budgetlist');
const listCurrencyPoin = () => api.get('/currency_poin');
const listPaymentOptions = (params) => api.get('/payment_options', params);
const listBankAccount = () => api.get('/bank_account');
const listDigitalCompetition = data => api.post('/digcomlist', data);
const listShippingAddress = data => api.post('/listshippingaddr', data);
const addShippingAddress = data => api.post('/addshippingaddress', data);
const editShippingAddress = data => api.post('/editshippingaddress', data);
const deleteShippingAddress = data => api.post('/deleteshippingaddr', data);
const addDigitalCompetition = data => api.post('/submitdigcom', data);
const searchSponsor = data => api.post('/searchsponsordata', data);
const searchMember = data => api.post('/searchmemberdata', data);
const searchUsername = data => api.post('/checkusername', data);

// SMART ADVERTISING
const listAds = params => api.get('/advertlist/get', params);
const detailAds = params => api.get('/viewadvert/get', params);
const createAd = data => apiFormData.post('/addadvert', data);
const editAd = data => apiFormData.post('/editadvert', data);
const updateAdStatus = data => api.post('/changestatus_advert', data);
const claimAdBonus = params => api.get('/viewadvert/get', params);
const adTopup = data => api.post('/advert_topupbudget', data);
const adInsight = data => api.post('/advert_report', data);

// SURVEY
const checkSurveyStatus = () => api.post('/getsurveydata');
const surveyQuestions = () => api.get('/getsurveyquestion');
const saveSurvey = (payload) => api.post('/savesurvey', payload);

export const getAuthToken = () => {
  const {getState} = store;
  return getState().auth.authToken;
};

export default {
  getApi: () => api,
  login,
  forgetPassword,
  resetPassword,
  updateProfile,
  setFcmToken,
  messageList,
  readMessage,
  logout,
  memberData,
  walletBalance,
  walletHistory,
  topupPending,
  topupHistory,
  bonusHistory,
  topup,
  topupConfirm,
  withdrawal,
  withdrawalHistory,
  transfer,
  checkSaldoForRegister,
  checkShippingFee,
  orderCheckout,
  listOrder,
  orderResiInput,
  orderConfirm,
  registerMember,
  registerClient,
  provider,
  providerPrice,
  ppobCheckInquiry,
  ppobCheckout,
  listProductCategory,
  listProduct,
  listProductMine,
  detailProduct,
  addProduct,
  editProduct,
  editStockProduct,
  deleteProduct,
  listCountry,
  listProvince,
  listProvinceRajaOngkir,
  listCity,
  listCityRajaOngkir,
  listBank,
  listBudget,
  listCurrencyPoin,
  listPaymentOptions,
  listBankAccount,
  listPartnership,
  listDigitalCompetition,
  listShippingAddress,
  addShippingAddress,
  addDigitalCompetition,
  editShippingAddress,
  deleteShippingAddress,
  searchSponsor,
  searchMember,
  searchUsername,
  listAds,
  detailAds,
  createAd,
  editAd,
  claimAdBonus,
  updateAdStatus,
  adTopup,
  adInsight,
  checkSurveyStatus,
  surveyQuestions,
  saveSurvey
};
