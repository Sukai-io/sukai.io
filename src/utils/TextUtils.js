import numeral from 'numeral';
import moment from 'moment';

export const numeralFormatter = value => numeral(value).format('0,0.00');

export const formattedDate = date => {
  const formatted = moment(date);
  return formatted.isValid ? formatted.format('MMM, DD YYYY') : 'Date not valid';
};

export const formattedDateYYYYMMDD = date => {
  const formatted = moment(date);
  return formatted.isValid ? formatted.format('YYYY-MM-DD') : 'Date not valid';
};


export const formattedDateNum = date => {
  const formatted = moment(date);
  return formatted.isValid ? formatted.format('DD/MM/YYYY') : 'Date not valid';
};

export const formattedDateTime = date => {
  const formatted = moment(date);
  return formatted.isValid ? formatted.format('MMM, DD YYYY, HH:mm') : 'Date not valid';
};

export const formattedTime = date => {
  const formatted = moment(date);
  return formatted.isValid ? formatted.format('HH:mm') : 'Date not valid';
};

export const formattedTimeAgo = date => {
  const formatted = moment(date);
  return formatted.isValid ? formatted.fromNow() : 'Date not valid';
};
