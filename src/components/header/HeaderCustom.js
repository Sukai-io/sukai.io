import React from 'react'
import {
  Header, Left, Icon, Button, Body, Right, Text,
} from 'native-base'
import styles from './styles'

type Props = {
  onBack: Function,
  leftIcon: String,
  title: String,
}

const HeaderCustom = ({ leftIcon, onBack, title }: Props) => (
  <Header>
    <Left>
      <Button transparent onPress={onBack}>
        <Icon name={leftIcon} />
      </Button>
    </Left>
    <Body>
      <Text style={styles.text}>{title}</Text>
    </Body>
    <Right />
  </Header>
)

export default HeaderCustom
