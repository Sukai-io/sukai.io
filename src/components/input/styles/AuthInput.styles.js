import { StyleSheet } from 'react-native'
import { Fonts, Metrics, Colors } from '../../../themes'

export default StyleSheet.create({
  item: {
    borderTopWidth: 2,
    borderRightWidth: 2,
    borderLeftWidth: 2,
    borderBottomWidth: 2,
    borderRadius: Metrics.smallMargin,
    borderColor: Colors.primary,
    marginTop: Metrics.doubleBaseMargin,
    marginBottom: Metrics.smallMargin,
    paddingTop: 0,
  },
  leftIcon: { marginLeft: 0, marginRight: 14 },
  rightIcon: { marginLeft: 14, marginRight: 0 },
  input: {
    ...Fonts.style.description,
    color: Colors.primary,
    paddingTop: Metrics.marginHorizontal,
    paddingBottom: Metrics.marginHorizontal,
  },
  margin: { marginTop: 30 },
  error: { ...Fonts.style.error },
  icon: {
    color: Colors.primary,
    fontSize: Metrics.icons.small,
  },
})
