import React, {Component} from 'react';
import {View, Image, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import AlertPro from 'react-native-alert-pro';

// Styles
import {Container, Content, Text} from 'native-base';
import styles from './styles';
import HeaderDrawer from '../../components/header/HeaderDrawerNew';
import Wallet from './components/Wallet';
import AdVideo from './components/AdVideo';
import Feature from './components/Feature';
import Marketplace from './components/Marketplace';
import DisplayPromo from './components/DisplayPromo';
import store from '../../config/CreateStore';
import {Fonts, Colors, Videos, Metrics} from '../../themes';
import LockscreenUtils from '../../utils/LockscreenUtils';
import {baseURL, getAuthToken} from '../../api/ApiClient';
import {LocalizationContext} from '../../localization';
import VideoPlayer from '../../components/video/VideoPlayer';

const data = [
  {
    brand: 'Pantene',
    company: 'P&G',
    duration: '02:55',
    point: '100',
    view: '2000',
    type: 'Partnership',
  },
  {
    brand: 'Pantene',
    company: 'P&G',
    duration: '02:55',
    point: '100',
    view: '2000',
    type: 'Partnership',
  },
];

class HomeScreen extends Component {
  static contextType = LocalizationContext;

  constructor(props) {
    super(props);
    this.state = {
      snapIndex: 0,
    };
    this.renderCategorySource = this.renderCategorySource.bind(this);
    this.renderAd = this.renderAd.bind(this);
  }

  componentDidMount() {
    const {
      getBalance,
      getProductCategory,
      getAllAds,
      getMemberData,
      getShippingAddresses,
      getHomeAds,
      getMessageList,
      navigation: {navigate},
    } = this.props;
    getBalance();
    getProductCategory();
    getShippingAddresses();
    getMemberData();
    getHomeAds();
    getAllAds(0);
    getMessageList({message_type: 'private'});
    getMessageList({message_type: 'broadcast'});
    LockscreenUtils.setEndpoint(baseURL);
    LockscreenUtils.setAuthToken(getAuthToken());
    LockscreenUtils.getLockscrenStatus(() => {});
    LockscreenUtils.getAdId(id => {
      id && setTimeout(() => navigate('AdDetail', {id}), 2000);
    });
    // setTimeout(() => LockscreenUtils.activate(true), 1000);
  }

  renderCategorySource(name) {
    switch (name) {
      case 'Makanan':
        return require('../../images/Icons/ic_food.png');
      case 'Minuman':
        return require('../../images/Icons/ic_drink.png');
      case 'Kesehatan':
        return require('../../images/Icons/ic_health.png');
      case 'Otomotif':
        return require('../../images/Icons/ic_gear.png');
      case 'Kecantikan':
        return require('../../images/Icons/ic_beauty.png');
      default:
        return null;
    }
  }

  renderAd({item: {title, id, image_thumbnail, bonus_status, member_id}}) {
    let {
      navigation: {navigate},
      userData,
    } = this.props;
    return (
      <TouchableOpacity
        onPress={() => {
          navigate('AdDetail', {id});
        }}>
        <View style={{padding: 10}}>
          <Image style={{width: '100%', height: 220, borderRadius: 15}} source={{uri: image_thumbnail}} />
          <Text style={{fontSize: Fonts.size.small, marginTop: 5}}>{title}</Text>
          {bonus_status == 0 && member_id != userData.id && (
            <Image
              resizeMode={'contain'}
              style={{position: 'absolute', width: 60, height: 20, top: 20, right: 20}}
              source={require('../../images/Icons/bonus500.png')}
            />
          )}
          {bonus_status == 1 && member_id != userData.id && (
            <Image
              resizeMode={'contain'}
              style={{position: 'absolute', width: 60, height: 20, top: 20, right: 20}}
              source={require('../../images/Icons/bonus500claimed.png')}
            />
          )}
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    const {translations} = this.context;

    const {username, userData, ads, navigation, balance} = this.props;
    const features =
      userData?.package === 'basic'
        ? [
            {
              name: 'Market Place',
              onPress: () => navigation.navigate('CategoriesScreen'),
              icon: require('../../images/Icons/ic_bag.png'),
            },
            {
              name: 'Official Store',
              onPress: () => navigation.navigate('OfficialStoreScreen'),
              icon: require('../../images/Icons/ic_official.png'),
            },
            {
              name: 'MyCommission',
              onPress: () => navigation.navigate('MyCommissionScreen'),
              icon: require('../../images/Icons/ic_history.png'),
            },
          ]
        : [
            {
              name: 'My Advertising',
              onPress: () => navigation.navigate('MyAdsScreen', {key: Math.random()}),
              icon: require('../../images/Icons/ic_ads.png'),
            },
            {
              name: 'Add New',
              onPress: () => navigation.navigate('AdForm', {id: null}),
              icon: require('../../images/Icons/ic_ads.png'),
            },
            {
              name: 'Reseller List',
              onPress: () => navigation.navigate('ResellerList', {key: Math.random()}),
              icon: require('../../images/Icons/ic_profile.png'),
            },
          ];
    return (
      <Container style={styles.mainContainer}>
        <AlertPro
          ref={ref => {
            this.AlertPro = ref;
          }}
          onConfirm={() => this.AlertPro.close()}
          onCancel={() => this.AlertPro.close()}
          showCancel={false}
          title="Sukai Wallet"
          message={`Masukkan username Anda saat ditransfer\nUsername Anda : ${username}`}
          textConfirm="OK"
          customStyles={{
            container: {
              width: '100%',
              shadowColor: '#000000',
              shadowOpacity: 0.1,
              shadowRadius: 10,
            },
            title: {
              fontSize: Fonts.size.h6,
            },
            buttonCancel: {
              backgroundColor: Colors.gray400,
            },
            buttonConfirm: {
              backgroundColor: Colors.bloodOrange,
            },
          }}
        />
        <HeaderDrawer
          title={translations.TITLE_HI_USER.replace('$name', `${userData?.name},`)}
          subtitle={translations.TITLE_WELCOME}
        />
        <Content>
          {userData?.package === 'basic' && <Wallet balance={balance || 0} />}
          <VideoPlayer video={Videos.homeBanner} style={{marginTop: Metrics.baseMargin}} />
          <View style={styles.viewTextAd}>
            <Text style={styles.textSmartAdv}>{translations.TITLE_SMART_ADV}</Text>
            <Text style={styles.textClickAd}>{translations.TITLE_CLICK_AD}</Text>
          </View>
          <AdVideo data={ads} />
          <Feature data={features} />
          <Marketplace />
          <DisplayPromo />
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: store.select.auth,
    userData: state.auth.userData,
    ads: state.smartadv.homeAds,
    focusedAdId: state.smartadv.focusedAdId,
    username: store.select.auth.getUsername(state),
    loading: state.loading.effects.balance.getBalance,
    loadingCategory: state.loading.effects.product.getProductCategory,
    balance: store.select.balance.getFormattedBalance(state.balance),
    productCategories: state.product.product_category_list,
  };
};

const mapDispatchToProps = ({balance, product, support, auth, smartadv}) => {
  return {
    getBalance: () => balance.getBalance(),
    getMemberData: () => auth.getMemberData(),
    getProductCategory: () => product.getProductCategory(),
    getShippingAddresses: () => support.getShippingAddresses(),
    getMessageList: data => auth.getMessageList(data),
    emptyAd: smartadv.emptyAd,
    getHomeAds: smartadv.getHomeAds,
    getAllAds: offset => smartadv.getAllAds(offset),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
