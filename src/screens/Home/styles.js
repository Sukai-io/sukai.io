import { StyleSheet } from 'react-native';
import {
  ApplicationStyles, Metrics, Colors, Fonts,
} from '../../themes';

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  avatarContainer: {
    ...ApplicationStyles.flexCenter,
    padding: Metrics.doubleBaseMargin,
  },
  avatar: {
    backgroundColor: Colors.primary,
  },
  marginLeft: {
    marginLeft: Metrics.doubleBaseMargin,
  },
  avatarText: {
    ...Fonts.style.description,
  },
  avatarTextBold: {
    ...Fonts.style.description,
    fontFamily: Fonts.type.bold,
    color: Colors.gray800,
  },
  avatarTextBoldPrimary: {
    ...Fonts.style.description,
    fontFamily: Fonts.type.bold,
    color: Colors.primary,
  },
  colContainer: {
    marginHorizontal: Metrics.smallMargin,
  },
  categoryContainer: {
    ...ApplicationStyles.shadow,
    borderRadius: Metrics.doubleBaseMargin,
    paddingHorizontal: Metrics.smallMargin,
    paddingBottom: Metrics.doubleBaseMargin,
    marginBottom: Metrics.doubleBaseMargin,
  },
  categoryText: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.bold,
    color: Colors.gray700,
    padding: Metrics.doubleBaseMargin,
  },
  marginTop: {
    marginTop: Metrics.doubleBaseMargin,
  },
  banner: {
    width: '100%',
    height: 150,
    marginVertical: Metrics.baseMargin,
  },
  viewTextAd: {
    ...ApplicationStyles.rowCenterBetween,
    marginHorizontal: Metrics.smallMargin,
    marginVertical: Metrics.baseMargin,
  },
  textSmartAdv: {
    fontSize: Fonts.size.h5,
    color: Colors.orange,
    marginLeft: Metrics.baseMargin,
    fontFamily: Fonts.type.bold,
  },
  textClickAd: {
    fontFamily: Fonts.type.bold,
    fontSize: Fonts.size.medium,
    color: Colors.bloodOrange,
    marginHorizontal: Metrics.baseMargin,
  }
});
