import { StyleSheet } from 'react-native';

import {Metrics as metrics, Colors as colors} from '../../themes';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: colors.gray200,
  },
  preview: {
    flex: 1,
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
  overlayBarcode: {
    position: 'absolute',
    width: 200,
    height: 200,
    backgroundColor: colors.grayTransparent,
  },
  panelInfoCode: {
    position: 'absolute',
    width: metrics.screenWidth,
    height: metrics.screenHeiht * (1 / 3),
    backgroundColor: colors.white,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 30
  },
});

export default styles;
