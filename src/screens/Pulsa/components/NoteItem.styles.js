import { StyleSheet } from 'react-native'
import {
  ApplicationStyles, Metrics, Colors, Fonts,
} from '../../../themes'

export default StyleSheet.create({
  container: {
    ...ApplicationStyles.flex,
    padding: Metrics.baseMargin,
    borderWidth: 0.5,
    borderColor: Colors.bloodOrange,
    borderRadius: Metrics.buttonRadius,
    marginHorizontal: Metrics.section,
    marginBottom: Metrics.section,
    backgroundColor: Colors.orange50,
  },
  title: {
    ...ApplicationStyles.flex,
    fontSize: Fonts.size.medium,
    color: Colors.bloodOrange,
    marginBottom: Metrics.baseMargin,
  },
  desc: {
    ...ApplicationStyles.flex,
    fontSize: Fonts.size.medium,
    color: Colors.gray800,
  },
  descBold: {
    ...ApplicationStyles.flex,
    fontFamily: Fonts.type.bold,
    fontSize: Fonts.size.medium,
    color: Colors.gray800,
  },
})
