import {Button, Container, Content, Text} from 'native-base';
import React, {useContext, useEffect, useState} from 'react';
import {Alert, BackHandler} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import HeaderBack from '../../components/header/HeaderBack';
import Loading from '../../components/indicator/Loading';
import {LocalizationContext} from '../../localization';
import {formattedDate, formattedDateYYYYMMDD} from '../../utils/TextUtils';
import DateTimePicker from '../NewMember/components/DateTimePicker';
import SelectPicker from '../NewMember/components/SelectPicker';

import styles from './styles';

const IntroSurveyScreen = () => {
  const {translations} = useContext(LocalizationContext);
  const dispatch = useDispatch();
  const questions = useSelector(state => state.survey.questions);
  const loading = useSelector(state => state.loading.effects.survey.getQuestions);
  const [visibleModal, setVisibleModal] = useState('');

  const handleSubmit = () => {
    const notAnswered = questions.find(item => item.answered === '');
    if (notAnswered) {
      Alert.alert(
        translations.INFO_AN_ERROR_OCCURED,
        'Please fill the ' + notAnswered.question.toLowerCase() + ' field',
      );
    } else {
      dispatch.survey.doSaveSurvey({
        id: questions.map(item => item.id),
        question: questions.map(item => item.question),
        answer: questions.map(item => (item.type === 'date' ? formattedDateYYYYMMDD(item.answered) : item.answered)),
        type: questions.map(item => item.type),
      });
    }
  };

  const renderForm = () => {
    return questions.map((item, index) => {
      switch (item.type) {
        case 'options':
          return (
            <SelectPicker
              key={index}
              label={item.question}
              items={item.answer.map(item => ({id: item, name: item}))}
              selectedItems={item.answered ? [item.answered] : []}
              onSelectedItemsChange={value => dispatch.survey.setSelectedQuestions({...item, answered: value[0]})}
              // error={touched.reg_member_gender && errors.reg_member_gender}
            />
          );
        case 'date':
          return (
            <DateTimePicker
              key={index}
              label={item.question}
              value={item.answered ? formattedDate(item.answered) : item.answered}
              date={item.answered}
              showModal={visibleModal === 'DATE_MODAL'}
              onShow={() => setVisibleModal('DATE_MODAL')}
              onCancel={() => setVisibleModal('')}
              onConfirm={date => {
                dispatch.survey.setSelectedQuestions({...item, answered: date});
                setVisibleModal('');
              }}
            />
          );
        default:
          return null;
      }
    });
  };
  useEffect(() => {
    dispatch.survey.getQuestions();
  }, []);
  useEffect(() => {
    const backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      Alert.alert(null, 'Please complete the survey questions first');
      return true;
    });
    return () => backHandler.remove();
  }, []);
  return (
    <Container>
      <HeaderBack
        onBackPress={() => Alert.alert(null, 'Please complete the survey questions first')}
        title="Fill the survey"
        withoutRight
      />
      {loading ? (
        <Loading />
      ) : (
        <Content>
          {renderForm()}
          <Button full style={styles.buttonRegister} onPress={handleSubmit}>
            <Text style={styles.buttonRegisterText}>{translations.ACTION_SAVE}</Text>
          </Button>
        </Content>
      )}
    </Container>
  );
};

export default IntroSurveyScreen;
