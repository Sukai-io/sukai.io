/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
import React, {PureComponent} from 'react';
import {ActivityIndicator} from 'react-native';
import {connect} from 'react-redux';
import {Container, Content, Text, Grid, Row, Col} from 'native-base';
import styles from './styles';
import VerticalList from '../../components/list/VerticalList';
import Loading from '../../components/indicator/Loading';
import {formattedDate, numeralFormatter, formattedTime} from '../../utils/TextUtils';
import {Colors} from '../../themes';

type Props = {
  getTopupHistory: () => Promise,
  loading: Boolean,
  topupHistories: Array<Object>,
};

class TopUp extends PureComponent<Props> {
  componentDidMount() {
    const {getTopupHistory} = this.props;
    getTopupHistory();
  }

  render() {
    const {loading, topupHistories} = this.props;
    return (
      <Container>
        <Content>
          {loading && <Loading />}
          <VerticalList
            data={topupHistories}
            renderItem={({item}) => {
              return (
                <Row style={styles.list}>
                  <Col size={1}>
                    <Text style={styles.date}>{`${formattedDate(item.datecreated)}\n${formattedTime(
                      item.datecreated,
                    )}`}</Text>
                  </Col>
                  <Col size={1}>
                    <Text style={styles.amount}>{numeralFormatter(parseInt(item.total, 10))}</Text>
                  </Col>
                  <Col size={1}>
                    <Text style={[styles.activity, item.status === '1' && {color: Colors.green500}]}>
                      {item.status === '1' ? 'SUCCESS' : 'PENDING'}
                    </Text>
                  </Col>
                </Row>
              );
            }}
          />
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = ({loading, balance}) => {
  return {
    topupHistories: balance.topup_histories,
    loading: loading.effects.balance.getTopupHistory,
  };
};

const mapDispatchToProps = ({balance}) => {
  return {
    getTopupHistory: data => balance.getTopupHistory(data),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TopUp);
