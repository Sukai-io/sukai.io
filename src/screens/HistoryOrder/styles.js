import { StyleSheet } from 'react-native'
import color from "color"
import {
  ApplicationStyles, Metrics, Fonts, Colors,
} from '../../themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    ...ApplicationStyles.flex,
    padding: Metrics.baseMargin,
  },
  activeTabText: {
    fontSize: Fonts.size.regular,
    color: Colors.snow,
    fontFamily: Fonts.type.bold,
  },
  tabText: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.bold,
    color: color(Colors.primary).lighten(0.3).hex(),
  },
})
