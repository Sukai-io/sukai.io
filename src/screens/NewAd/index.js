/* eslint-disable no-unused-vars */
import React, { Component } from 'react'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

import { Formik } from 'formik'
import Content from './Content'
import { FormValues } from './types'
import validation from './validation'

type Props = {
  // eslint-disable-next-line react/no-unused-prop-types
  navigation: any,
}

class NewAdScreen extends Component<Props> {
  render() {
    return (
      <Formik
        initialValues={{
          ppob_action: '',
          ppob_provider: '',
          ppob_code: '',
          ppob_number: '',
        }}
        onSubmit={(values: FormValues) => this.handleSubmit(values)}
        validationSchema={validation}
        render={(props: Props) => <Content {...props} />}
      />
    )
  }
}

const mapStateToProps = (state) => {
  return {
    // fetching: PPOBSelectors.fetching(state.ppob),
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    // doCheckout: data => dispatch(PPOBActions.ppobCheckoutRequest(data)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NewAdScreen)
