import React from 'react';
import {View, Text} from 'react-native';
import {Icon} from 'native-base';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {LocalizationContext} from '../../../localization';
import {ApplicationStyles} from '../../../themes';
import styles from './Feature.style';
import {ScrollView} from 'react-native';
import { Image } from 'react-native';

const Divider = () => <View style={styles.divider} />;

const Feature = ({data}) => {
  const {translations} = React.useContext(LocalizationContext);

  return (
    <>
      <View style={styles.headerContainer}>
        <Text style={styles.textFeature}>{translations.TITLE_FEATURE}</Text>
        <TouchableOpacity>
          <Text style={styles.textShowAll}>{translations.TITLE_SHOW_ALL}</Text>
        </TouchableOpacity>
      </View>
      <View style={ApplicationStyles.rowFlex}>
        <View style={styles.bodyLeftView}>
          <TouchableOpacity>
            <Icon style={styles.iconStar} type="FontAwesome" name="star" />
          </TouchableOpacity>
        </View>
        <View style={styles.bodyRightView}>
          <Icon type="AntDesign" name="caretleft" style={styles.iconArrow} />
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            {data.map((item, index )=> (
              <TouchableOpacity onPress={item.onPress} key={String(index)} style={styles.viewFeatureText}>
                <Image source={item.icon} style={styles.iconFeature} />
                <Text style={styles.featureText}>{item.name}</Text>
              </TouchableOpacity>
            ))}
          </ScrollView>
          <Icon type="AntDesign" name="caretright" style={styles.iconArrow} />
        </View>
      </View>
    </>
  );
};

export default Feature;
