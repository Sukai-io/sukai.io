import React, { useState, useEffect, useRef } from 'React';
import {View, TouchableOpacity, Image, Dimensions, ScrollView} from 'react-native';
import {Text, Container, Content, Button} from 'native-base';
import {Fonts, Colors} from '../../themes';
import {connect} from 'react-redux';
import HeaderBack from '../../components/header/HeaderBack';
import { FlatGrid } from 'react-native-super-grid';
import InputText from '../NewMember/components/InputText';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import ActionSheet from 'react-native-actionsheet';
import ImagePicker from 'react-native-image-crop-picker';
import Loading from '../../components/Loading';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import store from '../../config/CreateStore';

const AdForm = ({ myAd, test, focusedAdId, getFocusedAdId, updateFocusedAdId, claimAdBonus, userData, getAd, emptyAd, createAd, editAd, navigation }) => {

	const { navigate, state: { params: {id}, key } } = navigation
	const focusedIdRef = useRef(id);
	focusedIdRef.current = id;
	let [ad, setAd] = useState(null)
	let [loading, setLoading] = useState(false)
	let [timer, setTimer] = useState(0)

	useEffect(() => {
		setTimer(0);
		id && setLoading(true);
		id && getAd({
			id,
			type: 'multi',
			limit: 1,
			offset: 0
		});
		updateFocusedAdId(id);
	}, [id, userData]);

	let countdown;

	useEffect(() => {
		focusedIdRef.current = id;
		return () => {
			focusedIdRef.current = "0"
	    };
  	}, [navigation]);

	const runTimer = time => {
		if(myAd || ad){
			if(parseInt(myAd ? myAd.member_id : ad.member_id) != parseInt(userData.id)){
				setTimer(time)
				if(time > 0){
					if(parseInt(focusedIdRef.current) === parseInt(myAd ? myAd.id : ad.id)) setTimeout(() => runTimer(time - 1), 1650)
					else setTimer(0)
				} else {
					if(parseInt(focusedIdRef.current) === parseInt(myAd ? myAd.id : ad.id)) claimAdBonus(focusedIdRef.current)
				}
			}
		}
	}

	useEffect(() => {
		id && myAd && parseInt(myAd.bonus_status) == 0 && timer == 0 && runTimer(15);
		id && myAd && setLoading(false);
		id && myAd && setAd(myAd);
		setTimeout(() => emptyAd(), 2000);
	}, [myAd, userData]);

	const { width, height } = Dimensions.get('window')


	return (
		<Container>
			<HeaderBack 
				title={'Smart Adv'}
				onRightPress={() => navigation.navigate('TopUpScreen')}
				withoutRight />
			{ad && (<Content>
				<Image resizeMode={'cover'} style={{ width, height: width }} source={{ uri: ad.image_ori }} />
				<View style={{ padding: 20 }}>
					<Text style={{ fontSize: Fonts.size.h6, marginBottom: 10 }}>{ad.title}</Text>
					<Text style={{ fontSize: Fonts.size.small }}>{ad.description}</Text>
				</View>
			</Content>)}
			{timer > 0 && ad && (<View style={{ paddingHorizontal: 20, paddingTop: 12, paddingBottom: 14, flexDirection: 'row', alignItems: 'center', backgroundColor: Colors.gray200, elevation: 2 }}>
				<Icon style={{ marginBottom: -2, marginRight: 10 }} name={'alarm'} />
				<Text style={{ fontSize: Fonts.size.small }}>Dapatkan bonus +500 setelah {timer} detik</Text>
			</View>)}
	        <Loading visible={loading} />
		</Container>
	)

}

const mapStateToProps = ({smartadv, auth}) => {
  	return {
	  	userData: auth.userData,
	    myAd: smartadv.myAd,
	    focusedAdId: smartadv.focusedAdId,
  	}
};

const mapDispatchToProps = ({smartadv}) => {
  	return {
	    emptyAd: smartadv.emptyAd,
	    getAd: params => smartadv.getAd(params),
	    updateFocusedAdId: id => smartadv.updateFocusedAdId(id),
	    editAd: data => smartadv.editAd(data),
	    createAd: data => smartadv.createAd(data),
	    claimAdBonus: advert_id => smartadv.claimAdBonus(advert_id)
  	}
}

export default connect(mapStateToProps, mapDispatchToProps)(AdForm);