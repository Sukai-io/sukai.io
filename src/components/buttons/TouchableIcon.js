import React from 'react'
import { BorderlessButton, BorderlessButtonProperties } from 'react-native-gesture-handler'
import { ApplicationStyles } from '../../themes'

type Props = BorderlessButtonProperties & {
  children: React.Node,
}

const TouchableIcon = (props: Props) => {
  const { children } = props
  return (
    <BorderlessButton style={ApplicationStyles.iconPad} {...props}>
      {children}
    </BorderlessButton>
  )
}

export default TouchableIcon
