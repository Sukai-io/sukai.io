import React from 'react'
import Carousel from 'react-native-carousel-view'
import { View, StyleSheet } from 'react-native'
import FastImage from 'react-native-fast-image'
import { Metrics, Colors, ApplicationStyles } from '../../../themes'

type Props = {
  images: Array<String>,
}

const styles = StyleSheet.create({
  flex: {
    ...ApplicationStyles.flex,
  },
})

const Slider = ({ images }: Props) => (
  <Carousel
    width={null}
    height={300}
    indicatorSize={Metrics.baseMargin}
    inactiveIndicatorColor={Colors.gray300}
    indicatorColor="white"
    indicatorOffset={Metrics.baseMargin}
    indicatorSpace={Metrics.baseMargin}
    delay={2000}
    contentContainerStyle={{ paddingLeft: 0, marginLeft: 0 }}
  >
    {images.map((item, index) => (
      <View style={{ flex: 1 }} key={index.toString()}>
        <FastImage resizeMode="contain" style={styles.flex} source={{ uri: item }} />
      </View>
    ))}
  </Carousel>
)

export default Slider
