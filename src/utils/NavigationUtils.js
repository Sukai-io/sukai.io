import { NavigationActions, StackActions } from 'react-navigation'
import RouteServices from '../navigation/RouteServices'

export const goBack = () => {
  RouteServices.back()
}

export const goToSplash = () => {
  RouteServices.navigate('Splash')
}

export const goTo = (routeName, params) => {
  RouteServices.navigate(routeName, params)
}

export const resetAndGoTo = (routeName) => {
  const resetAction = StackActions.reset({
    index: 0,
    key: null,
    actions: [NavigationActions.navigate({ routeName })],
  })
  RouteServices.dispatch(resetAction)
}
