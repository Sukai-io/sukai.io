import R from 'ramda';
import api from '../ApiClient';
import errorGenerator from '../ErrorGenerator';
import { showToast } from '../../utils/ToastUtils';

const getSuccess = data => R.pathOr(false, ['success'], data);

export default {
  state: {},
  reducers: {},
  effects: () => ({
    async postNewMember(data) {
      const response = data.asClient ? await api.registerClient(data) : await api.registerMember(data);
      const success = getSuccess(response.data);
      if (response.ok && success) {
        showToast(`Penambahan ${data.asClient ? 'client' : 'member'} berhasil`);
        return Promise.resolve(response.data);
      }
      setTimeout(() => errorGenerator(response, (!success && response?.data?.data?.error) || null), 500);
      return Promise.reject(response);
    },
  }),
};
