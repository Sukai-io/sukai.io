import {FormikProps} from 'formik';
import {FormValues} from './types';
export type Props = FormikProps<FormValues>;
