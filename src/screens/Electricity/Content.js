import React, {Component} from 'react';
import {View, Switch, ActivityIndicator} from 'react-native';
import {connect} from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import {Container, Icon, Item, Input, Text} from 'native-base';
import {Button} from 'react-native-elements';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {FormikProps} from 'formik';
import styles from './styles';

import HeaderBack from '../../components/header/HeaderBack';
import Horizontal from '../../components/wrapper/Horizontal';
import {ApplicationStyles} from '../../themes';
import VerticalList from '../../components/list/VerticalList';
import PaymentModal from '../Checkout/components/PaymentModal';
import ElecItem from './components/ElecItem';
import TotalItem from './components/TotalItem';
import {FormValues} from './types';
import {numeralFormatter} from '../../utils/TextUtils';
import Loader from '../../components/indicator/Loader';

type Props = FormikProps<FormValues> & {
  loading: Boolean,
  loadingCheckout: Boolean,
  remember: Boolean,
  providerPriceList: Array<Object>,
  getPriceList: Function,
  doClear: Function,
  doSaveRemember: Function,
};

type State = {
  showPayment: Boolean,
};

class Content extends Component<Props, State> {
  state = {
    showPayment: false,
  };

  componentDidMount() {
    const {doClear, getPriceList, values} = this.props;
    doClear();
    getPriceList({provider: values.ppob_provider});
  }

  handlePaymentVisibility = value => {
    this.setState({showPayment: value});
  };

  render() {
    const {showPayment} = this.state;
    const {
      handleSubmit,
      setFieldTouched,
      setFieldValue,
      errors,
      touched,
      values,
      remember,
      loading,
      loadingCheckout,
      doSaveRemember,
      providerPriceList,
    } = this.props;
    const totalPrice =
      providerPriceList.length > 0
        ? providerPriceList[values.price_index].price
        : null;
    return (
      <Container style={styles.mainContainer}>
        <Loader loading={loadingCheckout} />
        <PaymentModal
          isVisible={showPayment}
          onBackButtonPress={() => this.handlePaymentVisibility(false)}
          onBackdropPress={() => this.handlePaymentVisibility(false)}
        />
        <HeaderBack withoutRight title="Voucher Listrik" />
        <KeyboardAwareScrollView style={styles.container}>
          <View style={styles.firstSubContainer}>
            <Text style={styles.titleText}>No. Meter/ID Pel</Text>
            <Horizontal style={ApplicationStyles.flexCenter}>
              <Icon
                name="flash"
                type="MaterialCommunityIcons"
                style={styles.icon}
              />
              <Item style={styles.inputContainer}>
                <Input
                  keyboardType="number-pad"
                  onBlur={() => setFieldTouched('ppob_number')}
                  onChangeText={value => setFieldValue('ppob_number', value)}
                />
              </Item>
            </Horizontal>
            {errors.ppob_number && touched.ppob_number && (
              <Text style={styles.error}>{errors.ppob_number}</Text>
            )}
          </View>
          <View style={styles.secondSubContainer}>
            <Horizontal style={styles.thirdSubContainer}>
              <Text style={styles.description}>
                Simpan nomor ini untuk transaksi berikutnya
              </Text>
              <Switch
                value={remember}
                onValueChange={() => doSaveRemember('electricity')}
              />
            </Horizontal>
            {loading ? (
              <ActivityIndicator />
            ) : (
              <VerticalList
                data={providerPriceList.filter(
                  item => item.type !== 'postpaid',
                )}
                scrollEnabled={false}
                renderItem={({item, index}) => {
                  return (
                    <ElecItem
                      onPress={() => setFieldValue('price_index', index)}
                      price={item.name}
                      salePrice={item.price}
                      selected={values.price_index === index}
                    />
                  );
                }}
              />
            )}
          </View>
          {/* <NoteItem /> */}
          {totalPrice && <TotalItem total={numeralFormatter(totalPrice)} />}
          <Button
            loading={loadingCheckout}
            disabled={loadingCheckout}
            onPress={handleSubmit}
            raised
            containerStyle={styles.buttonContainer}
            buttonStyle={styles.button}
            title="BELI"
          />
        </KeyboardAwareScrollView>
      </Container>
    );
  }
}

const mapStateToProps = ({loading, ppob}) => {
  return {
    loading: loading.effects.ppob.getProviderPrice,
    loadingCheckout: loading.effects.ppob.doCheckout,
    remember: ppob.remember_electricity,
    providerPriceList: ppob.provider_price_list,
  };
};

const mapDispatchToProps = ({ppob}) => {
  return {
    getPriceList: data => ppob.getProviderPrice(data),
    doClear: () => ppob.clear(),
    doSaveRemember: type => ppob.saveRemember(type),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Content);
