import {object as yupObject, string as yupString} from 'yup';

export default yupObject().shape({
  product_stock: yupString().required(),
  product_name: yupString().required(),
  product_detail: yupString().required(),
  product_weight: yupString().required(),
  product_price: yupString().required(),
  product_commission: yupString().required(),
  product_image: yupObject()
    .nullable()
    .required(),
});
