export type FormValues = {
  ppob_action: String,
  ppob_provider: String,
  ppob_code: String,
  ppob_number: String,
}
