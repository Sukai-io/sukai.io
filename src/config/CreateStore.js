import {init} from '@rematch/core';
import createRematchPersist from '@rematch/persist';
import {withDefaultReducers} from 'rematch-default-reducers';
import AsyncStorage from '@react-native-community/async-storage';
import createLoadingPlugin from '@rematch/loading';
import createSelectPlugin from '@rematch/select';
import createUpdatedPlugin from '@rematch/updated';

import * as models from '../api';

const persistPlugin = createRematchPersist({
  whitelist: ['auth', 'cart', 'balance'],
  version: 1,
  storage: AsyncStorage,
});

const loadingPlugin = createLoadingPlugin();
const selectPlugin = createSelectPlugin();
const updatedPlugin = createUpdatedPlugin();

const store = init({
  models: withDefaultReducers(models),
  plugins: [persistPlugin, loadingPlugin, selectPlugin, updatedPlugin],
});

export default store;
