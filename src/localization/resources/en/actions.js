export default {
  ACTION_AD_CREATE: 'Create Ads',
  ACTION_AD_SAVE: 'Save Ads',
  ACTION_AD_INSIGHT: 'Ad Insight',

  ACTION_CANCEL: 'Cancel',
  ACTION_CHANGE_PASSWORD: 'Change Password',
  ACTION_CHECKOUT: 'Checkout',
  ACTION_CHOOSE_FROM_GALLERY: 'Choose from Gallery',
  ACTION_CHOOSE_IMAGE: 'Choose Image',

  ACTION_DELETE_ADDRESS: 'Delete Address',
  
  ACTION_EDIT: 'Edit',

  ACTION_FORGET_PASSWORD: 'Forget Password',

  ACTION_PICK_AN_ITEM: 'Pick an item',

  ACTION_SAVE: 'Save',
  ACTION_SET_ADDRESS: 'Set Address',
  ACTION_SET_ADDRESS_PRIMARY: 'Set To Primary Address',

  ACTION_SIGNUP: 'Or Create An Account',

  ACTION_USE_CAMERA: 'Use Camera',

};
