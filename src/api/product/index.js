import R from 'ramda';
import api from '../ApiClient';
import {transformToFormData} from '../ApiHelper';
import errorGenerator from '../ErrorGenerator';
import SimpleToast from 'react-native-simple-toast';
import {goBack} from '../../utils/NavigationUtils';

const getSuccess = data => R.pathOr(false, ['success'], data);

export default {
  state: {
    product_category_list: [],
    product_list: [],
    product_official_list: [],
    product_mine_list: [],
    product_detail: {},
  },
  reducers: {
    setProductCategory(state, data) {
      return {...state, product_category_list: data?.data?.product_category};
    },
    setDefaultProduct(state) {
      return {
        ...state,
        product_list: [],
      };
    },
    setInitialProduct(state, data) {
      return {...state, product_list: data?.data?.productlist};
    },
    setProduct(state, data) {
      return {
        ...state,
        product_list: state.product_list.concat(data?.data?.productlist),
      };
    },
    setDefaultProductOfficial(state) {
      return {
        ...state,
        product_official_list: [],
      };
    },
    setInitialProductOfficial(state, data) {
      return {...state, product_official_list: data?.data?.productlist};
    },
    setProductOfficial(state, data) {
      return {
        ...state,
        product_official_list: state.product_official_list.concat(data?.data?.productlist),
      };
    },
    setProductMine(state, data) {
      return {
        ...state,
        product_mine_list: data?.data?.productlist,
      };
    },
    setProductDetail(state, {data}) {
      return {
        ...state,
        product_detail: {
          ...state.product_detail,
          [data.id]: {
            ...(state.product_detail[data.id] || {}),
            ...data,
          },
        },
      };
    },
  },
  effects: dispatch => ({
    async getProductCategory() {
      const response = await api.listProductCategory();
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.product.setProductCategory(response.data);
        return Promise.resolve(response.data);
      }
    },
    async getProduct(data) {
      const response = await api.listProduct(transformToFormData(data));
      const success = getSuccess(response.data);
      if (response.ok && success) {
        if (data.offset == 0) {
          await dispatch.product.setInitialProduct(response.data);
        } else {
          await dispatch.product.setProduct(response.data);
        }
        return Promise.resolve(response.data);
      }
      if (!success && data.offset == 0) {
        await dispatch.product.setDefaultProduct();
      }
    },
    async getProductOfficial(data) {
      const response = await api.listProduct(transformToFormData(data));
      const success = getSuccess(response.data);
      if (response.ok && success) {
        if (data.offset == 0) {
          await dispatch.product.setInitialProductOfficial(response.data);
        } else {
          await dispatch.product.setProductOfficial(response.data);
        }
        return Promise.resolve(response.data);
      }
      if (!success && data.offset == 0) {
        await dispatch.product.setDefaultProductOfficial();
      }
    },
    async getProductMine() {
      const response = await api.listProductMine();
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.product.setProductMine(response.data);
        return Promise.resolve(response.data);
      }
    },
    async getProductDetail(params) {
      const response = await api.detailProduct(params);
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.product.setProductDetail(response.data);
        return Promise.resolve(response.data);
      }
      errorGenerator(response);
    },
    async postNewProduct(data) {
      const response = await api.addProduct(data);
      const success = getSuccess(response.data);
      if (response.ok && success) {
        goBack();
        SimpleToast.show(response.data?.data);
        return Promise.resolve(response.data);
      }
      setTimeout(() => errorGenerator(response, (!success && response?.data?.data?.error) || null), 400);
    },
    async postEditProduct(data) {
      const responses = await Promise.all([api.editProduct(data.data), api.editStockProduct(data.stock)]);
      const success = getSuccess(responses[0].data);
      if (responses[0].ok && success) {
        await dispatch.product.getProductMine();
        goBack();
        SimpleToast.show(responses[0].data?.data);
        return Promise.resolve(responses[0].data);
      }
      errorGenerator(responses[0], (!success && responses[0]?.data?.data?.error) || null);
    },
    async postDeleteProduct(data) {
      const response = await api.deleteProduct(transformToFormData(data));
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.product.getProductMine();
        SimpleToast.show(response.data?.data);
        return Promise.resolve(response.data);
      }
      errorGenerator(response, (!success && response?.data?.data?.error) || null);
    },
  }),
};
