import React, {useCallback, useRef} from 'react';
import {Text, View} from 'react-native';
import {Container, CardItem, Body} from 'native-base';
import HeaderBack from '../../components/header/HeaderBack';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Horizontal from '../../components/wrapper/Horizontal';
import {ApplicationStyles, Metrics, Colors, Fonts} from '../../themes';
import {numeralFormatter} from '../../utils/TextUtils';
import styles from '../Checkout/styles';
import {Card, Button} from 'react-native-elements';
import AlertPro from 'react-native-alert-pro';
import {useDispatch, useSelector, shallowEqual} from 'react-redux';

const LeasingCheckout = ({navigation}) => {
  const {
    ppob: {doCheckout},
  } = useDispatch();
  const loading = useSelector(
    ({loading}) => loading.effects.ppob.doCheckout,
    shallowEqual,
  );
  const alertProRef = useRef(null);
  const data = navigation.getParam('data', null);
  const handleClose = useCallback(() => {
    alertProRef.current.close();
  }, [alertProRef]);
  const handleConfirm = useCallback(() => {
    doCheckout({
      ppob_action: 'payment',
      ppob_provider: data?.ppob_provider,
      ppob_code: data?.code,
      ppob_number: data?.number,
      inquiry_id: data?.inquiry_id,
      total_tagihan: data?.total_tagihan,
    });
    handleClose();
  }, [data, doCheckout, handleClose]);
  return (
    <Container>
      <AlertPro
        ref={alertProRef}
        onConfirm={handleConfirm}
        onCancel={handleClose}
        title="Konfirmasi Pembelian"
        message="Saldo Anda akan berkurang setelah melakukan billing Leasing"
        textCancel="Batal"
        textConfirm="Beli"
        customStyles={{
          container: {
            width: 300,
            shadowColor: '#000000',
            shadowOpacity: 0.1,
            shadowRadius: 10,
          },
          title: {
            fontSize: Fonts.size.h6,
          },
          buttonCancel: {
            backgroundColor: Colors.gray400,
          },
          buttonConfirm: {
            backgroundColor: Colors.bloodOrange,
          },
        }}
      />
      <HeaderBack withoutRight />
      <KeyboardAwareScrollView
        contentContainerStyle={{
          flexGrow: 1,
          backgroundColor: Colors.gray100,
        }}>
        <View style={{flex: 1}}>
          <Card containerStyle={[styles.cardContainer, {flex: 0}]}>
            <CardItem>
              <Body>
                <Text style={styles.title}>Informasi Billing</Text>
              </Body>
            </CardItem>
            <CardItem>
              <Body>
                <Horizontal style={ApplicationStyles.flexCenter}>
                  <Text style={styles.totalLeft}>Kode Billing</Text>
                  <Text style={styles.totalRight}>{data?.number}</Text>
                </Horizontal>
                <Horizontal
                  style={[
                    ApplicationStyles.flexCenter,
                    {marginTop: Metrics.baseMargin},
                  ]}>
                  <Text style={styles.totalLeft}>Customer</Text>
                  <Text style={styles.totalRight}>{data?.customer}</Text>
                </Horizontal>
                <Horizontal
                  style={[
                    ApplicationStyles.flexCenter,
                    {marginTop: Metrics.baseMargin},
                  ]}>
                  <Text style={styles.totalLeft}>Provider</Text>
                  <Text style={styles.totalRight}>{data?.provider}</Text>
                </Horizontal>
                <Horizontal
                  style={[
                    ApplicationStyles.flexCenter,
                    {marginTop: Metrics.baseMargin},
                  ]}>
                  <Text style={styles.totalLeft}>Jatuh Tempo</Text>
                  <Text style={styles.totalRight}>{data?.jatuh_tempo}</Text>
                </Horizontal>
                <Horizontal
                  style={[
                    ApplicationStyles.flexCenter,
                    {marginTop: Metrics.baseMargin},
                  ]}>
                  <Text style={styles.totalLeft}>Tenor Ke</Text>
                  <Text style={styles.totalRight}>{data?.tenor_ke}</Text>
                </Horizontal>
              </Body>
            </CardItem>
          </Card>
          <Card containerStyle={[styles.cardContainer, {flex: 0}]}>
            <CardItem>
              <Body>
                <Horizontal style={ApplicationStyles.flexCenter}>
                  <Text style={styles.totalLeft}>Tagihan</Text>
                  <Text style={styles.totalRight}>
                    {numeralFormatter(data?.tagihan)}
                  </Text>
                </Horizontal>
                <Horizontal
                  style={[
                    ApplicationStyles.flexCenter,
                    {marginTop: Metrics.baseMargin},
                  ]}>
                  <Text style={styles.totalLeft}>Denda</Text>
                  <Text style={styles.totalRight}>
                    {numeralFormatter(data?.denda)}
                  </Text>
                </Horizontal>
                <Horizontal
                  style={[
                    ApplicationStyles.flexCenter,
                    {marginTop: Metrics.baseMargin},
                  ]}>
                  <Text style={styles.totalLeft}>Total Tagihan</Text>
                  <Text style={styles.grantTotal}>
                    {numeralFormatter(data?.total_tagihan)}
                  </Text>
                </Horizontal>
              </Body>
            </CardItem>
          </Card>
        </View>
        <Button
          loading={loading}
          disabled={loading}
          onPress={() => alertProRef.current.open()}
          raised
          containerStyle={styles.buttonContainer}
          buttonStyle={styles.button}
          icon={{name: 'ios-send', type: 'ionicon', color: 'white'}}
          title="BAYAR SEKARANG"
        />
      </KeyboardAwareScrollView>
    </Container>
  );
};

export default LeasingCheckout;
