import React from 'react';
import {View} from 'react-native';
import {Text, Thumbnail, Item, Input} from 'native-base';
import Horizontal from '../../../components/wrapper/Horizontal';
import styles from './CartItem.styles';
import {numeralFormatter} from '../../../utils/TextUtils';
import {Fonts, Metrics} from '../../../themes';
import colors from '../../../themes/Colors';

const CartItem = ({isLast, dataSource, onNoteChange}) => (
  <Horizontal style={[styles.container, isLast && styles.isLast]}>
    <Thumbnail
      square
      source={{
        uri: dataSource?.product_thumbnail_url,
      }}
      resizeMode="contain"
    />
    <View style={styles.infoContainer}>
      <Text style={styles.title}>{dataSource?.product_name}</Text>
      <Text style={styles.qty}>{`Qty: ${dataSource?.cart_qty}`}</Text>
      <Text style={styles.qty}>{`Berat: ${dataSource?.product_weight} gr`}</Text>
      <Text style={styles.price}>{numeralFormatter(dataSource?.cart_price)}</Text>
      <Item regular style={{height: 30, marginTop: Metrics.smallMargin}}>
        <Input
          placeholder="Note"
          placeholderTextColor={colors.gray500}
          onChangeText={onNoteChange}
          style={{fontSize: 12, fontFamily: Fonts.type.base, color: colors.gray800}}
        />
      </Item>
    </View>
  </Horizontal>
);

export default CartItem;
