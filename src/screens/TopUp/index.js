import React, { Component } from 'react'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import {
  Container, Content, Item, Label, Input, Form, Text,
} from 'native-base'
import { Formik, FormikProps } from 'formik'
import { Button } from 'react-native-elements'
import styles from './styles'
import validation from './validation'
import HeaderTopup from '../../components/header/HeaderTopup'

type Props = {
  loading: Boolean,
  loading: Boolean,
  doTopup: () => Promise,
}

type FormValues = {
  amount: Number,
}

class TopUpScreen extends Component<Props> {
  handleSubmit = (values: FormValues) => {
    const { doTopup } = this.props
    doTopup(values).then().catch(() => {})
  }

  renderForm = ({
    handleSubmit,
    setFieldValue,
    setFieldTouched,
    touched,
    errors,
  }: FormikProps<FormValues>) => {
    const { loading } = this.props
    return (
      <Container style={styles.mainContainer}>
        <HeaderTopup title="Top Up" />
        <Content style={styles.container}>
          <Form>
            <Item stackedLabel>
              <Label>Jumlah Top Up *</Label>
              <Input
                keyboardType="number-pad"
                onBlur={() => setFieldTouched('amount')}
                onChangeText={(value) => setFieldValue('amount', value)}
              />
            </Item>
            {errors.amount && touched.amount && <Text style={styles.error}>{errors.amount}</Text>}
            <Button
              raised
              onPress={handleSubmit}
              loading={loading}
              disabled={loading}
              containerStyle={styles.buttonContainer}
              buttonStyle={styles.button}
              icon={{ name: 'ios-send', type: 'ionicon', color: 'white' }}
              title="TOP UP SEKARANG"
            />
          </Form>
        </Content>
      </Container>
    )
  }

  render() {
    return (
      <Formik
        initialValues={{ amount: '' }}
        onSubmit={(values: FormValues) => this.handleSubmit(values)}
        validationSchema={validation}
        render={(actions: FormikProps<FormValues>) => this.renderForm(actions)}
      />
    )
  }
}

const mapStateToProps = ({ loading }) => {
  return {
    loading: loading.effects.balance.doTopup || loading.effects.balance.getTopupPending,
  }
}

const mapDispatchToProps = ({ balance }) => {
  return {
    doTopup: (data) => balance.doTopup(data),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TopUpScreen)
