import { StyleSheet } from 'react-native'
import { Fonts, Colors, Metrics } from '../../themes'

export default StyleSheet.create({
  drawerCover: {
    alignSelf: 'stretch',
    height: 180,
    width: null,
    position: 'relative',
    marginBottom: 10,
  },
  drawerImage: {
    position: 'absolute',
    width: '100%',
    height: 180,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontFamily: Fonts.type.base,
    fontSize: Fonts.size.medium,
    marginLeft: Metrics.doubleBaseMargin,
  },
  subMenuText: {
    fontFamily: Fonts.type.base,
    fontSize: Fonts.size.medium,
    marginLeft: 54,
  },
  thumbnail: {
    width: Metrics.images.large,
    height: Metrics.images.large,
    borderRadius: Metrics.images.large / 2,
    marginBottom: Metrics.baseMargin,
  },
  titleBold: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.bold,
    color: Colors.gray200,
    marginTop: Metrics.baseMargin,
  },
  title: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.base,
    color: Colors.gray200,
  },
  icon: {
    width: 24,
    height: 24,
    marginRight: Metrics.baseMargin,
  },
})
