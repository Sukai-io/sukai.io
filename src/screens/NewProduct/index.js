/* eslint-disable no-unused-vars */
import React, {Component, useCallback} from 'react';
import {connect, useDispatch} from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

import {Formik, FormikProps} from 'formik';
import Content from './Content';
import {FormValues} from './types';
import validation from './validation';
import SimpleToast from 'react-native-simple-toast';
import {transformToFormData} from '../../api/ApiHelper';

const NewProductScreen = props => {
  const {
    product: {postNewProduct},
  } = useDispatch();
  const handleSubmit = useCallback(
    (
      {
        product_category: productCategory,
        product_commission,
        product_detail,
        product_image,
        product_name,
        product_price,
        product_weight,
      }: FormValues,
      {resetForm}: FormikProps<FormValues>,
    ) => {
      const formData = transformToFormData({
        product_name,
        product_detail,
        product_weight,
        product_price,
        product_commission,
        product_image,
      });
      productCategory.forEach(item => {
        formData.append('product_category[]', item);
      });
      postNewProduct(formData).then(res => {
        resetForm({});
      });
    },
    [],
  );
  return (
    <Formik
      initialValues={{
        product_name: '',
        product_detail: '',
        product_weight: '',
        product_price: '',
        product_commission: '',
        product_image: null,
        product_category: [],
      }}
      onSubmit={(values, actions) => handleSubmit(values, actions)}
      validationSchema={validation}
      render={props => <Content {...props} />}
    />
  );
};

export default NewProductScreen;
