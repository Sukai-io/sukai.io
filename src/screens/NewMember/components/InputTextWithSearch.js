import React from 'react';
import {Text} from 'react-native';
import {CardItem, Item, Label, Input} from 'native-base';
import styles from '../styles';
import {Colors, ApplicationStyles} from '../../../themes';
import {Button} from 'react-native-elements';

const InputTextWithSearch = ({label, success, error, loading, onSearch, ...otherProps}) => (
  <>
    <CardItem style={{flexDirection: 'row', alignItems: 'flex-end', paddingBottom: 0, backgroundColor: 'transparent'}}>
      <Item stackedLabel style={ApplicationStyles.flex}>
        <Label style={styles.label}>{label}</Label>
        <Input style={styles.input} placeholderTextColor={Colors.gray300} {...otherProps} />
      </Item>
      <Button
        onPress={onSearch}
        raised
        loading={loading}
        buttonStyle={styles.buttonEdit}
        titleStyle={styles.buttonTitle}
        icon={{
          name: 'ios-search',
          type: 'ionicon',
          color: 'white',
          size: 20,
        }}
      />
    </CardItem>
    {error && <Text style={styles.error}>{error}</Text>}
    {success && <Text style={[styles.error, {color: Colors.green500}]}>{success}</Text>}
  </>
);

export default InputTextWithSearch;
