import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Container, ListItem, Text, Right, Icon, Body, Content} from 'native-base';
import HeaderDrawer from '../../components/header/HeaderDrawer';
import styles from './styles';
import VerticalList from '../../components/list/VerticalList';
import {formattedDateTime} from '../../utils/TextUtils';

class News extends Component {
  componentDidMount() {
    this.props.getMessageList({message_type: 'broadcast'});
  }

  handleRefresh = () => {
    this.props.getMessageList({message_type: 'broadcast'});
  };

  render() {
    const {navigation, loading, newsList} = this.props;
    return (
      <Container style={styles.mainContainer}>
        <HeaderDrawer title="News" withoutRight />
        <Content contentContainerStyle={{flex: 1}}>
          <VerticalList
            data={newsList}
            withRefreshControl
            refreshing={loading}
            onRefresh={this.handleRefresh}
            renderItem={({item}) => (
              <ListItem onPress={() => navigation.navigate('InboxDetailScreen', {data: item})} style={styles.list}>
                <Body>
                  <Text style={item.read_status == '0' ? styles.selected : styles.unselected}>{item.title}</Text>
                  <Text note style={item.read_status == '0' ? styles.dateSelected : styles.date}>
                    {formattedDateTime(item.datecreated)}
                  </Text>
                </Body>
                <Right>
                  <Icon style={item.read_status == '0' ? styles.iconSelected : styles.icon} name="arrow-forward" />
                </Right>
              </ListItem>
            )}
          />
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = ({loading, auth}) => {
  return {
    loading: loading.effects.auth.getMessageList,
    newsList: auth.news_list,
  };
};

const mapDispatchToProps = ({auth}) => {
  return {
    getMessageList: data => auth.getMessageList(data),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(News);
