/* eslint-disable no-unused-vars */
import React, { Component } from 'react'
import { View } from 'react-native'
import FastImage from 'react-native-fast-image'
import { connect } from 'react-redux'
import ImagePicker from 'react-native-image-picker'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import {
  Container, Text, Body, CardItem, Item, Input, Label,
} from 'native-base'
import { Dropdown } from 'react-native-material-dropdown'
import { Button, Card } from 'react-native-elements'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import styles from './styles'
import HeaderBack from '../../components/header/HeaderBack'
import { ApplicationStyles, Colors } from '../../themes'
import Horizontal from '../../components/wrapper/Horizontal'
import AddAddressModal from '../Checkout/components/AddAddressModal'
import { imageResizer } from '../../Utils/ImageUtils'

type Props = {
  // eslint-disable-next-line react/no-unused-prop-types
  navigation: any,
}

type State = {
  showAddress: Boolean,
  isAddressComplete: Boolean,
  imgUri: String,
}

class Content extends Component<Props, State> {
  state = {
    isAddressComplete: true,
    showAddress: false,
    imgUri: '',
  }

  handleAddressVisibility = (value) => {
    this.setState({ showAddress: value })
  }

  handleImagePicker = () => {
    ImagePicker.showImagePicker(null, async (response) => {
      if (response.didCancel) {
        console.log('User cancelled photo picker')
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error)
      } else {
        console.log(response.fileSize)
        const imgUri = await imageResizer(response.uri)
        this.setState({ imgUri })
      }
    })
  }

  render() {
    const { isAddressComplete, showAddress, imgUri } = this.state
    const data = [
      {
        value: 'Makanan',
      },
      {
        value: 'Minuman',
      },
      {
        value: 'Kesehatan',
      },
      {
        value: 'Otomatif',
      },
      {
        value: 'Kecantikan',
      },
    ]
    return (
      <Container style={styles.mainContainer}>
        <HeaderBack title="Tambah" withoutRight />
        <AddAddressModal
          isVisible={showAddress}
          onBackButtonPress={() => this.handleAddressVisibility(false)}
          onBackdropPress={() => this.handleAddressVisibility(false)}
        />
        <KeyboardAwareScrollView style={styles.container}>
          <Card containerStyle={styles.cardContainer}>
            <CardItem>
              <Body>
                <Text style={styles.title}>Informasi Usaha</Text>
              </Body>
            </CardItem>
            <CardItem>
              <Item stackedLabel style={ApplicationStyles.flex}>
                <Label style={styles.label}>Deskripsi *</Label>
                <Input style={styles.input} placeholderTextColor={Colors.gray300} />
              </Item>
            </CardItem>
            <CardItem>
              <Body>
                <Label style={styles.label}>Image (opsional)</Label>
                <Button
                  raised
                  containerStyle={styles.buttonImageContainer}
                  buttonStyle={styles.buttonImage}
                  titleStyle={styles.buttonTitle}
                  icon={{
                    name: 'ios-image',
                    type: 'ionicon',
                    color: 'white',
                    size: 20,
                  }}
                  title="CHOOSE IMAGE"
                />
              </Body>
            </CardItem>
          </Card>
          <Card containerStyle={styles.cardContainer}>
            <CardItem>
              <Body>
                <Text style={styles.title}>Informasi Alamat Pengirim</Text>
              </Body>
            </CardItem>
            {isAddressComplete ? (
              <CardItem>
                <Body>
                  <Text>Fadli</Text>
                  <Text note>Jalan Prof. Dr. Ida Bagus Mantra, Blok JG 82</Text>
                  <Text note>Badung, Bali</Text>
                  <Text note>223203</Text>
                  <Button
                    onPress={() => this.handleAddressVisibility(true)}
                    raised
                    containerStyle={styles.buttonImageContainer}
                    buttonStyle={styles.buttonEdit}
                    titleStyle={styles.buttonTitle}
                    icon={{
                      name: 'ios-create',
                      type: 'ionicon',
                      color: 'white',
                      size: 20,
                    }}
                    title="EDIT"
                  />
                </Body>
              </CardItem>
            ) : (
              <CardItem>
                <Button
                  onPress={() => this.handleAddressVisibility(true)}
                  raised
                  containerStyle={styles.buttonImageContainer}
                  buttonStyle={styles.buttonImage}
                  titleStyle={styles.buttonTitle}
                  icon={{
                    name: 'ios-pin',
                    type: 'ionicon',
                    color: 'white',
                    size: 20,
                  }}
                  title="SET ALAMAT PENGIRIM"
                />
              </CardItem>
            )}
          </Card>
          <Card containerStyle={styles.cardContainer}>
            <CardItem>
              <Body>
                <Text style={styles.title}>Informasi Produk</Text>
              </Body>
            </CardItem>
            <CardItem>
              <Item stackedLabel style={ApplicationStyles.flex}>
                <Label style={styles.label}>Nama *</Label>
                <Input
                  style={styles.input}
                  placeholderTextColor={Colors.gray300}
                  placeholder="Nama Produk"
                />
              </Item>
            </CardItem>
            <CardItem>
              <Dropdown
                containerStyle={ApplicationStyles.flex}
                label="Kategori *"
                data={data}
                dropdownOffset={styles.pickerOffset}
              />
            </CardItem>
            <CardItem>
              <Item stackedLabel style={ApplicationStyles.flex}>
                <Label style={styles.label}>Deskripsi *</Label>
                <Input style={styles.input} placeholderTextColor={Colors.gray300} />
              </Item>
            </CardItem>
            <CardItem>
              <Item stackedLabel style={ApplicationStyles.flex}>
                <Label style={styles.label}>Berat (gram) *</Label>
                <Input
                  style={styles.input}
                  placeholderTextColor={Colors.gray300}
                  keyboardType="number-pad"
                  placeholder="Berat Produk"
                />
              </Item>
            </CardItem>
            <CardItem>
              <Item stackedLabel style={ApplicationStyles.flex}>
                <Label style={styles.label}>Harga (Rp.)*</Label>
                <Input
                  style={styles.input}
                  placeholderTextColor={Colors.gray300}
                  keyboardType="number-pad"
                  placeholder="Harga Produk"
                />
              </Item>
            </CardItem>
            <CardItem>
              <Item stackedLabel style={ApplicationStyles.flex}>
                <Label style={styles.label}>Stok *</Label>
                <Input
                  style={styles.input}
                  placeholderTextColor={Colors.gray300}
                  keyboardType="number-pad"
                  placeholder="Stok Produk"
                />
              </Item>
            </CardItem>
            <CardItem>
              <Body>
                <Label style={styles.label}>Image *</Label>
                {imgUri !== '' && (
                  <FastImage resizeMode="cover" source={{ uri: imgUri }} style={styles.image} />
                )}
                <Button
                  onPress={this.handleImagePicker}
                  raised
                  containerStyle={styles.buttonImageContainer}
                  buttonStyle={styles.buttonImage}
                  titleStyle={styles.buttonTitle}
                  icon={{
                    name: 'ios-image',
                    type: 'ionicon',
                    color: 'white',
                    size: 20,
                  }}
                  title="CHOOSE IMAGE"
                />
              </Body>
            </CardItem>
          </Card>
          <Card containerStyle={styles.cardContainer}>
            <Horizontal style={styles.buttonWrapper}>
              <Button
                raised
                containerStyle={styles.buttonContainer}
                buttonStyle={styles.buttonReset}
                titleStyle={styles.buttonContainerTextStyle}
                icon={{ name: 'ios-refresh', type: 'ionicon', color: 'white' }}
                title="RESET"
              />
              <View style={styles.separator} />
              <Button
                raised
                containerStyle={styles.buttonContainer}
                buttonStyle={styles.buttonConfirm}
                titleStyle={styles.buttonContainerTextStyle}
                title="TAMBAH PRODUK"
              />
            </Horizontal>
          </Card>
        </KeyboardAwareScrollView>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {}
}

const mapDispatchToProps = (dispatch) => {
  return {}
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Content)
