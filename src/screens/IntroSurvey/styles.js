import { StyleSheet } from 'react-native';
import { ApplicationStyles, Metrics, Colors, Fonts } from '../../themes';

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
    paddingTop: Metrics.baseMargin,
    backgroundColor: Colors.gray100,
  },
  cardContainer: {
    ...ApplicationStyles.shadow,
    ...ApplicationStyles.flex,
    padding: Metrics.smallMargin,
    marginRight: 0,
    marginLeft: 0,
    marginTop: 0,
    marginBottom: Metrics.doubleBaseMargin,
    paddingBottom: Metrics.doubleBaseMargin,
  },
  label: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    color: Colors.gray700,
  },
  pickerLabel: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    color: Colors.gray700,
    marginLeft: Metrics.doubleBaseMargin,
    marginTop: Metrics.doubleBaseMargin,
  },
  disableInput: {
    backgroundColor: Colors.gray200,
    fontSize: Fonts.size.regular,
    color: Colors.gray500,
  },
  title: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.bold,
    color: Colors.gray700,
    marginBottom: 0,
  },
  buttonImageContainer: {
    marginTop: Metrics.baseMargin,
  },
  buttonImage: {
    backgroundColor: Colors.primary,
    height: 40,
  },
  buttonEdit: {
    backgroundColor: Colors.green500,
    height: 40,
  },
  buttonTitle: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    marginLeft: Metrics.smallMargin,
  },
  pickerOffset: {
    top: Metrics.doubleBaseMargin,
  },
  input: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    paddingLeft: 0,
  },
  dateContainer: {
    ...ApplicationStyles.flex,
    borderBottomWidth: 1,
    borderBottomColor: Colors.gray300,
  },
  datePicker: {
    fontSize: Fonts.size.regular,
    paddingVertical: 14,
  },
  buttonWrapper: {
    ...ApplicationStyles.flex,
    padding: Metrics.smallMargin,
  },
  buttonContainer: {
    ...ApplicationStyles.flex,
  },
  buttonReset: {
    backgroundColor: Colors.gray400,
    height: 40,
    flex: 1,
  },
  buttonConfirm: {
    backgroundColor: Colors.bloodOrange,
    height: 40,
    flex: 1,
  },
  buttonContainerTextStyle: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.base,
  },
  separator: {
    width: Metrics.baseMargin,
  },
  error: {
    ...Fonts.style.error,
    marginLeft: 16,
    marginBottom: 8,
  },
  buttonRegister: {
    backgroundColor: Colors.primary,
    height: 40,
    borderRadius: Metrics.smallMargin,
    marginHorizontal: Metrics.marginHorizontal,
    marginBottom: Metrics.section,
    marginTop: Metrics.section
  },
  buttonRegisterText: {
    color: Colors.white,
    fontSize: Fonts.size.regular
  }
});
