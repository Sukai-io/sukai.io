import {StyleSheet} from 'react-native';
import {ApplicationStyles, Metrics, Fonts, Colors} from '../../themes';

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
    padding: Metrics.section,
  },
  date: {
    ...ApplicationStyles.flex,
    fontSize: Fonts.size.small,
    color: Colors.gray500,
  },
  title: {
    ...ApplicationStyles.flex,
    fontSize: Fonts.size.input,
    color: Colors.gray900,
    marginTop: Metrics.smallMargin,
  },
  desc: {
    ...ApplicationStyles.flex,
    fontSize: Fonts.size.medium,
    color: Colors.gray600,
    marginTop: Metrics.doubleBaseMargin,
  },
  image: {
    width: '100%',
    height: Metrics.images.logo,
    marginTop: Metrics.smallMargin,
  },
  subtitleContainer: {
    ...ApplicationStyles.flex,
    alignItems: 'flex-start',
    marginTop: Metrics.smallMargin,
  },
  cardContainer: {
    ...ApplicationStyles.shadow,
    ...ApplicationStyles.flex,
    padding: Metrics.smallMargin,
    marginRight: 0,
    marginLeft: 0,
    marginTop: Metrics.doubleBaseMargin,
    marginBottom: Metrics.doubleBaseMargin,
  },
  cardTitle: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.bold,
    color: Colors.gray700,
    marginBottom: Metrics.baseMargin,
  },
  cardSubtitle: {
    ...ApplicationStyles.flex,
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    color: Colors.gray600,
  },
  cardSubtitleUrl: {
    ...ApplicationStyles.flex,
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    color: Colors.primary,
    textDecorationLine: 'underline',
  },
});
