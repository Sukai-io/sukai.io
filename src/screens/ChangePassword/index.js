import React, { Component } from 'react'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import {
  Container, Content, Item, Label, Input, Form, Text,
} from 'native-base'
import { Button } from 'react-native-elements'
import { Formik, FormikProps } from 'formik'
import styles from './styles'
import HeaderBack from '../../components/header/HeaderBack'
import validation from './validation'
import { LocalizationContext } from '../../localization'

type Props = {
  loading: Boolean,
  doResetPassword: Function,
}

type FormValues = {
  old_password: string,
  password: string,
  confirm_password: string,
}

class ChangePasswordScreen extends Component<Props> {
  static contextType = LocalizationContext;
  handleSubmit = (values: FormValues) => {
    const { doResetPassword } = this.props
    doResetPassword(values)
  }

  renderForm = ({
    handleSubmit,
    setFieldValue,
    setFieldTouched,
    touched,
    errors,
  }: FormikProps<FormValues>) => {
    const { loading } = this.props
    const {translations} = this.context
    return (
      <Container style={styles.mainContainer}>
        <HeaderBack withoutRight title={translations.ACTION_CHANGE_PASSWORD} />
        <Content style={styles.container}>
          <Form>
            <Item stackedLabel>
              <Label>{translations.LABEL_PASSWORD_OLD} *</Label>
              <Input
                secureTextEntry
                onBlur={() => setFieldTouched('old_password')}
                onChangeText={(value) => setFieldValue('old_password', value)}
              />
            </Item>
            {touched.old_password && errors.old_password && (
              <Text style={styles.error}>{errors.old_password}</Text>
            )}
            <Item stackedLabel>
              <Label>{translations.LABEL_PASSWORD_NEW} *</Label>
              <Input
                secureTextEntry
                onBlur={() => setFieldTouched('password')}
                onChangeText={(value) => setFieldValue('password', value)}
              />
            </Item>
            {touched.password && errors.password && (
              <Text style={styles.error}>{errors.password}</Text>
            )}
            <Item stackedLabel>
              <Label>{translations.LABEL_PASSWORD_NEW_REPEAT} *</Label>
              <Input
                secureTextEntry
                onBlur={() => setFieldTouched('confirm_password')}
                onChangeText={(value) => setFieldValue('confirm_password', value)}
              />
            </Item>
            {touched.confirm_password && errors.confirm_password && (
              <Text style={styles.error}>{errors.confirm_password}</Text>
            )}
            <Button
              onPress={handleSubmit}
              raised
              loading={loading}
              disabled={loading}
              containerStyle={styles.buttonContainer}
              buttonStyle={styles.button}
              icon={{ name: 'ios-send', type: 'ionicon', color: 'white' }}
              title={translations.ACTION_CHANGE_PASSWORD}
            />
          </Form>
        </Content>
      </Container>
    )
  }

  render() {
    return (
      <Formik
        initialValues={{ old_password: '', password: '', confirm_password: '' }}
        enableReinitialize
        onSubmit={(values: FormValues) => this.handleSubmit(values)}
        validationSchema={validation}
        render={(actions: FormikProps<FormValues>) => this.renderForm(actions)}
      />
    )
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.loading.effects.auth.doResetPassword,
  }
}

const mapDispatchToProps = ({ auth }) => {
  return {
    doResetPassword: (data) => auth.doResetPassword(data),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ChangePasswordScreen)
