import React, {Component} from 'react';
import {View, Image} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import {Text} from 'native-base';
import styles from './styles';
import {Colors, Fonts, Images} from '../../themes';

const slides = [
  {
    key: 'somethun',
    title: 'Title 1',
    text: 'Description.\nSay something cool',
    image: Images.intro1,
    backgroundColor: '#59b2ab',
  },
  {
    key: 'somethun-dos',
    title: 'Title 2',
    text: 'Other cool stuff',
    image: Images.intro2,
    backgroundColor: '#febe29',
  },
  {
    key: 'somethun1',
    title: 'Title 3',
    text: "I'm already out of descriptions",
    image: Images.intro3,
    backgroundColor: '#22bcb5',
  },
];

type Props = {
  navigation: any,
};

class IntroScreen extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  _renderItem = ({item}) => {
    return <Image source={item.image} style={{width: '100%', height: '100%'}} resizeMode="contain" />;
    // return (
    //   <View style={styles.mainContent}>
    //     <Text style={styles.title}>{item.title}</Text>
    //     <Image source={item.image} style={{width:200, height: 200}} resizeMode="contain" />
    //     <Text style={styles.text}>{item.text}</Text>
    //   </View>
    // )
  };

  _onDone = () => {
    const {navigation} = this.props;
    navigation.navigate('Auth');
  };

  render() {
    // eslint-disable-next-line no-underscore-dangle
    return (
      <AppIntroSlider
        renderItem={this._renderItem}
        slides={slides}
        onDone={this._onDone}
        buttonTextStyle={{fontFamily: Fonts.type.base, color: Colors.primary}}
        activeDotStyle={{backgroundColor: Colors.primary}}
      />
    );
  }
}

export default IntroScreen;
