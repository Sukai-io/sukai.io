export default {
  LABEL_AD_BANNER: 'Ad Banner',
  LABEL_AD_BUDGET: 'Ad Budget',
  LABEL_AD_DESC: 'Ad Desc',
  LABEL_AD_TITLE: 'Ad Title',
  LABEL_ADDRESS: 'Address',
  LABEL_ADDRESS_SAVE_AS: 'Save Address As',
  LABEL_ADDRESS_SHIPPING_INFO: 'Shipping Address Info',
  
  LABEL_BANK_BRANCH_NAME: 'Bank Branch Name',
  LABEL_BANK_NAME: 'Bank Name',
  LABEL_BANK_ACCOUNT_NUMBER: 'Account Number',
  LABEL_BANK_ACCOUNT_HOLDER: 'Account Holder',

  LABEL_CATEGORY_INFO: 'Category Info',
  LABEL_CATEGORY_WITH: 'Category $name',
  LABEL_CART_EMPTY: 'Your cart is empty',
  LABEL_CHOOSE_SERVICE: 'Choose The Service',
  LABEL_CHOOSE_DELIVERY_SERVICE: 'Choose Delivery Service',
  
  LABEL_CITY: 'City',
  LABEL_COUNTRY: 'Country',
  LABEL_COURIER_INFO: 'Courier Info',

  LABEL_DELIVERY_COST: 'Delivery Cost',

  LABEL_EMAIL: 'Email',

  LABEL_GENDER: 'Gender',
  LABEL_GENDER_FEMALE: 'Female',
  LABEL_GENDER_MALE: 'Male',

  LABEL_IDENTITY_NUMBER: 'Identity Number',
  LABEL_IMAGE: 'Image',

  LABEL_LOGIN_DESC: 'Login',
  LABEL_LOGIN_DESC_2: 'to know what’s new today',

  LABEL_NAME: 'Name',

  LABEL_PASSWORD: 'Password',
  LABEL_PASSWORD_NEW: 'New Password',
  LABEL_PASSWORD_NEW_REPEAT: 'Repeat New Password',
  LABEL_PASSWORD_OLD: 'Old Password',
  LABEL_PASSWORD_PLACEHOLDER: 'Enter Password',
  LABEL_PHONE: 'Phone',
  LABEL_POSTAL_CODE: 'Postal Code',
  LABEL_PRODUCT_CATEGORY: 'Product Category',
  LABEL_PRODUCT_COMMISION: 'Product Commision',
  LABEL_PRODUCT_DETAIL: 'Product Detail',
  LABEL_PRODUCT_INFO: 'Product Info',
  LABEL_PRODUCT_NAME: 'Product Name',
  LABEL_PRODUCT_PRICE: 'Product Price',
  LABEL_PROVINCE: 'Province',

  LABEL_REGISTER_AS_MEMBER: 'Register as Member',
  LABEL_REGISTER_AS_CLIENT: 'Register as Client',

  LABEL_STOCK_INFO: 'Stock Info',
  LABEL_STOCK_ITEM: 'Item Stock',
  LABEL_SUBDISTRICT: 'Subdistrict',

  LABEL_TOTAL_PAYMENT: 'Total Payment',
  LABEL_TOTAL_PRICE_ITEM_WITH_BALANCE: 'Total Price of $qty Items',

  LABEL_USERNAME: 'Username',
  LABEL_USERNAME_REGISTERED: 'Username Registered',
  LABEL_USERNAME_SPONSOR: 'Username Referral',
  LABEL_USERNAME_PLACEHOLDER: 'Enter Username',
  LABEL_USERNAME_VALID: 'Username valid',

  LABEL_WEIGHT: 'Berat (gram)',
  LABEL_WELCOME_TO: 'Welcome To',
};
