/* eslint-disable react/state-in-constructor */
import React, {useCallback} from 'react';
import {FormikProps} from 'formik';

import {Container, Text, Body, CardItem} from 'native-base';
import {Card, Button} from 'react-native-elements';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import styles from './styles';
import {FormValues} from './types';
import HeaderProfile from '../EditProfile/components/HeaderProfile';

import {useSelector, shallowEqual, useDispatch} from 'react-redux';
import {goBack, goTo} from '../../utils/NavigationUtils';
import InputText from '../NewMember/components/InputText';
import SelectPicker from '../NewMember/components/SelectPicker';

const gender = [
  {
    id: 'male',
    name: 'LAKI-LAKI',
  },
  {
    id: 'female',
    name: 'PEREMPUAN',
  },
];

const Content = ({
  touched,
  errors,
  values,
  setFieldValue,
  handleBlur,
  handleChange,
  handleSubmit,
  userData,
}: FormikProps<FormValues>) => {
  const {
    support: {getCities},
  } = useDispatch();
  const {provinces, cities, banks} = useSelector(({support}) => support, shallowEqual);
  const selectedAddress = useSelector(({auth}) => auth.selectedAddress, shallowEqual);
  const handleProvinceChanged = useCallback(value => {
    getCities({province_id: value[0]});
    setFieldValue('member_province', value[0]);
    if (values.member_city || values.member_district) {
      setFieldValue('member_city', null);
      setFieldValue('member_district', '');
    }
  }, []);
  const handleCityChanged = useCallback(value => {
    setFieldValue('member_city', value[0]);
    values.member_district && setFieldValue('member_district', '');
  }, []);
  const handleGenderChanged = useCallback(value => {
    setFieldValue('member_gender', value[0]);
  }, []);
  const handleBankChanged = useCallback(value => {
    setFieldValue('member_bank', value[0]);
  }, []);
  return (
    <Container style={styles.mainContainer}>
      <HeaderProfile title="Edit Profil" onSave={handleSubmit} onBack={() => goBack()} />
      <KeyboardAwareScrollView style={styles.container} keyboardShouldPersistTaps="always">
        <Card containerStyle={styles.cardContainer}>
          <CardItem>
            <Body>
              <Text style={styles.title}>Informasi Pribadi</Text>
            </Body>
          </CardItem>
          <InputText label="Username *" disabled value={userData?.username} />
          <InputText
            label="Nama *"
            error={touched.member_name && errors.member_name}
            onBlur={handleBlur('member_name')}
            onChangeText={handleChange('member_name')}
            value={values.member_name}
          />
          <InputText
            label="Email *"
            keyboardType="email-address"
            error={touched.member_email && errors.member_email}
            onBlur={handleBlur('member_email')}
            onChangeText={handleChange('member_email')}
            value={values.member_email}
          />
          <InputText
            label="Alamat *"
            error={touched.member_address && errors.member_address}
            onBlur={handleBlur('member_address')}
            onChangeText={handleChange('member_address')}
            value={values.member_address}
          />
          <SelectPicker
            selectText="Provinsi *"
            items={provinces}
            selectedItems={values.member_province ? [values.member_province] : []}
            onSelectedItemsChange={handleProvinceChanged}
            error={touched.member_province && errors.member_province}
          />
          {cities.length > 0 && (
            <SelectPicker
              selectText="Kota/Kabupaten *"
              items={cities}
              selectedItems={values.member_city ? [values.member_city] : []}
              onSelectedItemsChange={handleCityChanged}
              error={touched.member_city && errors.member_city}
            />
          )}
          {values.member_city && (
            <InputText
              label="Kecamatan/Kelurahan"
              error={touched.member_district && errors.member_district}
              onBlur={handleBlur('member_district')}
              onChangeText={handleChange('member_district')}
              value={values.member_district}
            />
          )}
          <SelectPicker
            selectText="Jenis Kelamin"
            items={gender}
            selectedItems={values.member_gender ? [values.member_gender] : []}
            onSelectedItemsChange={handleGenderChanged}
            error={touched.member_gender && errors.member_gender}
          />
          <InputText
            label="No. Telp/HP *"
            keyboardType="number-pad"
            error={touched.member_phone && errors.member_phone}
            onBlur={handleBlur('member_phone')}
            onChangeText={handleChange('member_phone')}
            value={values.member_phone}
          />
          <InputText
            label="No. KTP *"
            keyboardType="number-pad"
            error={touched.member_idcard && errors.member_idcard}
            onBlur={handleBlur('member_idcard')}
            onChangeText={handleChange('member_idcard')}
            value={values.member_idcard}
          />
        </Card>
        <Card containerStyle={styles.cardContainer}>
          <CardItem>
            <Body>
              <Text style={styles.title}>Informasi Alamat Pengiriman</Text>
            </Body>
          </CardItem>
          {selectedAddress ? (
            <CardItem>
              <Body>
                <Text>{selectedAddress?.address_type}</Text>
                <Text note>{selectedAddress?.address_location}</Text>
                <Text
                  note>{`${selectedAddress?.address_district}, ${selectedAddress?.address_city_name}, ${selectedAddress?.address_province_name}`}</Text>
                <Text note>{selectedAddress?.address_zipcode}</Text>
                <Button
                  onPress={() => goTo('AddressListScreen', {isEdit: true})}
                  raised
                  containerStyle={styles.buttonImageContainer}
                  buttonStyle={styles.buttonEdit}
                  titleStyle={styles.buttonTitle}
                  icon={{
                    name: 'ios-create',
                    type: 'ionicon',
                    color: 'white',
                    size: 20,
                  }}
                  title="EDIT"
                />
              </Body>
            </CardItem>
          ) : (
            <CardItem>
              <Button
                onPress={() => goTo('AddressListScreen', {isEdit: true})}
                raised
                containerStyle={styles.buttonImageContainer}
                buttonStyle={styles.buttonImage}
                titleStyle={styles.buttonTitle}
                icon={{
                  name: 'ios-pin',
                  type: 'ionicon',
                  color: 'white',
                  size: 20,
                }}
                title="SET ALAMAT"
              />
            </CardItem>
          )}
        </Card>
        <Card containerStyle={styles.cardContainer}>
          <CardItem>
            <Body>
              <Text style={styles.title}>Informasi Akun Bank</Text>
            </Body>
          </CardItem>
          <SelectPicker
            selectText="Nama Bank"
            items={banks}
            selectedItems={values.member_bank ? [values.member_bank] : []}
            onSelectedItemsChange={handleBankChanged}
            error={touched.member_bank && errors.member_bank}
          />
          <InputText
            label="No. Rekening Bank *"
            keyboardType="number-pad"
            error={touched.member_bill && errors.member_bill}
            onBlur={handleBlur('member_bill')}
            onChangeText={handleChange('member_bill')}
            value={values.member_bill}
          />
          <InputText
            label="Nama Pemilik Bank *"
            error={touched.member_bill_name && errors.member_bill_name}
            onBlur={handleBlur('member_bill_name')}
            onChangeText={handleChange('member_bill_name')}
            value={values.member_bill_name}
          />
          <InputText
            label="Cabang Bank *"
            error={touched.member_branch && errors.member_branch}
            onBlur={handleBlur('member_branch')}
            onChangeText={handleChange('member_branch')}
            value={values.member_branch}
          />
        </Card>
      </KeyboardAwareScrollView>
    </Container>
  );
};
export default Content;
