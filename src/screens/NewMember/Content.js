/* eslint-disable react/state-in-constructor */
import React, {useCallback, useState, useEffect, useRef, useContext} from 'react';
import {debounce, filter} from 'lodash';
import {Container, Text, Body, CardItem, Button} from 'native-base';
import {Card} from 'react-native-elements';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import styles from './styles';
import HeaderProfile from '../EditProfile/components/HeaderProfile';
import {Props} from './Props';
import InputText from './components/InputText';
import InputTextWithSearch from './components/InputTextWithSearch';
import SelectPicker from './components/SelectPicker';
import {useSelector, shallowEqual, useDispatch} from 'react-redux';
import {goBack} from '../../utils/NavigationUtils';
import ApiClient from '../../api/ApiClient';
import {transformToFormData} from '../../api/ApiHelper';
import UserTypeItem from './components/UserTypeItem';
import {View} from 'react-native';
import AlertPro from 'react-native-alert-pro';
import {Colors, Fonts} from '../../themes';
import {LocalizationContext} from '../../localization';
import {COUNTRY_INDONESIA} from '../../static/Constant';

const Content = ({
  touched,
  errors,
  values,
  setFieldValue,
  setFieldTouched,
  handleBlur,
  handleChange,
  handleSubmit,
  setFieldError,
}: Props) => {
  const alertRef = useRef(null);
  const {translations} = useContext(LocalizationContext);
  const GENDER = [
    {
      id: 'male',
      name: translations.LABEL_GENDER_MALE,
    },
    {
      id: 'female',
      name: translations.LABEL_GENDER_FEMALE,
    },
  ];
  const ALERT = {
    CLIENT_INFO: {
      title: translations.LABEL_REGISTER_AS_CLIENT,
      desc: translations.INFO_REGISTRATION_AS_CLIENT,
    },
    MEMBER_INFO: {
      title: translations.LABEL_REGISTER_AS_MEMBER,
      desc: translations.INFO_REGISTRATION_AS_MEMBER,
    },
  };
  console.log('values', values);
  console.log('errors', errors);
  const [sponsorLoading, setSponsorLoading] = useState(false);
  const [usernameLoading, setUsernameLoading] = useState(false);
  const [signupAlertInfo, setSignupAlertInfo] = useState('CLIENT_INFO');
  const {
    support: {getCities},
  } = useDispatch();
  const {countries, provinces, cities, banks} = useSelector(({support}) => support, shallowEqual);
  const userData = useSelector(({auth}) => auth.userData, shallowEqual);
  const allowDoRegister = useSelector(({balance: {allowDoRegister}}) => allowDoRegister, shallowEqual);
  const handleCountryChanged = useCallback(
    value => {
      const filteredCountry = countries.filter(row => row.id === value[0]);
      setFieldValue('countryName', filteredCountry[0]?.name);
      setFieldValue('reg_member_country', value[0]);
      if (filteredCountry[0]?.name === COUNTRY_INDONESIA) {
        setFieldValue('reg_member_province', null);
        setFieldValue('reg_member_city', null);
        setFieldValue('reg_member_district', null);
      } else {
        setFieldValue('reg_member_province', undefined);
        setFieldValue('reg_member_city', undefined);
        setFieldValue('reg_member_district', undefined);
      }
    },
    [countries],
  );
  const handleProvinceChanged = useCallback(value => {
    getCities({province_id: value[0]});
    setFieldValue('reg_member_province', value[0]);
    if (values.reg_member_city || values.reg_member_district) {
      setFieldValue('reg_member_city', null);
      setFieldValue('reg_member_district', '');
    }
  }, []);
  const handleCityChanged = useCallback(value => {
    setFieldValue('reg_member_city', value[0]);
    values.reg_member_district && setFieldValue('reg_member_district', '');
  }, []);
  const handleGenderChanged = useCallback(value => {
    setFieldValue('reg_member_gender', value[0]);
  }, []);
  const handleBankChanged = useCallback(value => {
    setFieldValue('reg_member_bank', value[0]);
  }, []);
  const handleSponsorFetched = useCallback(value => {
    setSponsorLoading(true);
    ApiClient.searchSponsor(transformToFormData({sponsor_username: value})).then(({data: {data, success}, ok}) => {
      if (ok && success) {
        setFieldValue('reg_member_sponsor_id', data?.data?.data?.sponsor_id);
      } else {
        setFieldValue('reg_member_sponsor_id', '', false);
        setFieldError('reg_member_sponsor_id', 'Username sponsor not registered');
      }
      setSponsorLoading(false);
    });
  }, []);
  const handleSponsorChanged = useCallback(
    debounce(value => {
      setFieldTouched('reg_member_sponsor');
      setFieldTouched('reg_member_sponsor_id');
      setFieldValue('reg_member_sponsor', value || '');
      if (value === '') {
        setFieldValue('reg_member_sponsor_id', '');
        setFieldError('reg_member_sponsor_id', null);
      }
      if (value.length >= 4) {
        handleSponsorFetched(value);
      }
    }, 100),
    [],
  );
  const handleSponsorSearch = useCallback(() => {
    if (values.reg_member_sponsor?.length >= 4) {
      handleSponsorFetched(values.reg_member_sponsor);
    }
  }, [values]);
  const handleUsernameFetched = useCallback(value => {
    setUsernameLoading(true);
    ApiClient.searchUsername(transformToFormData({username: value})).then(({data: {success}, ok}) => {
      if (ok && success) {
        setFieldValue('is_member_username_valid', true);
      } else {
        setFieldValue('is_member_username_valid', false);
      }
      setUsernameLoading(false);
    });
  }, []);
  const handleUsernameChanged = useCallback(
    debounce(value => {
      setFieldTouched('reg_member_username');
      setFieldTouched('is_member_username_valid');
      setFieldValue('reg_member_username', value);
      handleUsernameFetched(value);
    }, 100),
    [],
  );
  const handleUsernameSearch = useCallback(() => {
    handleUsernameFetched(values.reg_member_username);
  }, [values]);

  useEffect(() => {
    setFieldValue('reg_member_sponsor_id', userData?.id);
    setFieldValue('reg_member_sponsor', userData?.username);
  }, [userData]);

  return (
    <>
      <Container style={styles.mainContainer}>
        <HeaderProfile
          title={translations.TITLE_REGISTRATION}
          onSave={handleSubmit}
          onBack={() => goBack()}
          hideRight
        />
        <KeyboardAwareScrollView style={styles.container} keyboardShouldPersistTaps="always">
          <Card containerStyle={styles.cardContainer}>
            <CardItem>
              <Body>
                <Text style={styles.title}>{translations.TITLE_INFORMATION_USER_TYPE}</Text>
              </Body>
            </CardItem>
            <View style={{marginHorizontal: 8}}>
              <UserTypeItem
                onInfoPress={() => {
                  alertRef.current.open();
                  setSignupAlertInfo('MEMBER_INFO');
                }}
                onPress={() => setFieldValue('asClient', false)}
                desc={translations.LABEL_REGISTER_AS_MEMBER}
                selected={!values.asClient}
              />
              <UserTypeItem
                onInfoPress={() => {
                  alertRef.current.open();
                  setSignupAlertInfo('CLIENT_INFO');
                }}
                onPress={() => setFieldValue('asClient', true)}
                desc={translations.LABEL_REGISTER_AS_CLIENT}
                selected={values.asClient}
              />
            </View>
          </Card>
          <Card containerStyle={styles.cardContainer}>
            <CardItem>
              <Body>
                <Text style={styles.title}>{translations.TITLE_INFORMATION_PERSONAL}</Text>
              </Body>
            </CardItem>
            <InputTextWithSearch
              label={`${translations.LABEL_USERNAME_SPONSOR}`}
              loading={sponsorLoading}
              autoCapitalize="none"
              error={
                touched.reg_member_sponsor && errors.reg_member_sponsor
                  ? errors.reg_member_sponsor
                  : touched.reg_member_sponsor_id && errors.reg_member_sponsor_id
                  ? errors.reg_member_sponsor_id
                  : null
              }
              success={values.reg_member_sponsor_id ? translations.LABEL_USERNAME_REGISTERED : null}
              onChangeText={handleSponsorChanged}
              onSearch={handleSponsorSearch}
              defaultValue={userData?.username}
            />
            <InputTextWithSearch
              label={`${translations.LABEL_USERNAME} *`}
              loading={usernameLoading}
              autoCapitalize="none"
              error={
                touched.reg_member_username && errors.reg_member_username
                  ? errors.reg_member_username
                  : touched.is_member_username_valid && errors.is_member_username_valid
                  ? errors.is_member_username_valid
                  : null
              }
              success={values.is_member_username_valid ? translations.LABEL_USERNAME_VALID : null}
              onChangeText={handleUsernameChanged}
              onSearch={handleUsernameSearch}
            />
            <InputText
              label={`${translations.LABEL_PASSWORD} *`}
              error={touched.reg_member_password && errors.reg_member_password}
              secureTextEntry
              onBlur={handleBlur('reg_member_password')}
              onChangeText={handleChange('reg_member_password')}
            />
            <InputText
              label={`${translations.LABEL_NAME} *`}
              error={touched.reg_member_name && errors.reg_member_name}
              onBlur={handleBlur('reg_member_name')}
              onChangeText={handleChange('reg_member_name')}
            />
            <InputText
              label={`${translations.LABEL_EMAIL} *`}
              keyboardType="email-address"
              error={touched.reg_member_email && errors.reg_member_email}
              onBlur={handleBlur('reg_member_email')}
              onChangeText={handleChange('reg_member_email')}
            />
            <InputText
              label={`${translations.LABEL_ADDRESS} *`}
              error={touched.reg_member_address && errors.reg_member_address}
              onBlur={handleBlur('reg_member_address')}
              onChangeText={handleChange('reg_member_address')}
            />
            <SelectPicker
              label={`${translations.LABEL_COUNTRY} *`}
              items={countries}
              selectedItems={values.reg_member_country ? [values.reg_member_country] : []}
              onSelectedItemsChange={handleCountryChanged}
              error={touched.reg_member_country && errors.reg_member_country}
            />
            {values.countryName?.toLowerCase() === COUNTRY_INDONESIA.toLowerCase() && (
              <SelectPicker
                label={`${translations.LABEL_PROVINCE} *`}
                items={provinces}
                selectedItems={values.reg_member_province ? [values.reg_member_province] : []}
                onSelectedItemsChange={handleProvinceChanged}
                error={touched.reg_member_province && errors.reg_member_province}
              />
            )}
            {cities.length > 0 && (
              <SelectPicker
                label={`${translations.LABEL_CITY} *`}
                items={cities}
                selectedItems={values.reg_member_city ? [values.reg_member_city] : []}
                onSelectedItemsChange={handleCityChanged}
                error={touched.reg_member_city && errors.reg_member_city}
              />
            )}
            {values.reg_member_city && (
              <InputText
                label={`${translations.LABEL_SUBDISTRICT} *`}
                error={touched.reg_member_district && errors.reg_member_district}
                onBlur={handleBlur('reg_member_district')}
                onChangeText={handleChange('reg_member_district')}
              />
            )}
            <SelectPicker
              label={translations.LABEL_GENDER + ' *'}
              items={GENDER}
              selectedItems={values.reg_member_gender ? [values.reg_member_gender] : []}
              onSelectedItemsChange={handleGenderChanged}
              error={touched.reg_member_gender && errors.reg_member_gender}
            />
            <InputText
              label={`${translations.LABEL_PHONE} *`}
              keyboardType="number-pad"
              error={touched.reg_member_phone && errors.reg_member_phone}
              onBlur={handleBlur('reg_member_phone')}
              onChangeText={handleChange('reg_member_phone')}
            />
            <InputText
              label={`${translations.LABEL_IDENTITY_NUMBER} *`}
              keyboardType="number-pad"
              error={touched.reg_member_idcard && errors.reg_member_idcard}
              onBlur={handleBlur('reg_member_idcard')}
              onChangeText={handleChange('reg_member_idcard')}
            />
          </Card>
          {/* <Card containerStyle={styles.cardContainer}>
          <CardItem>
            <Body>
              <Text style={styles.title}>Informasi Alamat</Text>
            </Body>
          </CardItem>
          {isAddressComplete ? (
            <CardItem>
              <Body>
                <Text>Fadli</Text>
                <Text note>Jalan Prof. Dr. Ida Bagus Mantra, Blok JG 82</Text>
                <Text note>Badung, Bali</Text>
                <Text note>223203</Text>
                <Button
                  onPress={() => handleAddressVisibility(true)}
                  raised
                  containerStyle={styles.buttonImageContainer}
                  buttonStyle={styles.buttonEdit}
                  titleStyle={styles.buttonTitle}
                  icon={{
                    name: 'ios-create',
                    type: 'ionicon',
                    color: 'white',
                    size: 20,
                  }}
                  title="EDIT"
                />
              </Body>
            </CardItem>
          ) : (
            <CardItem>
              <Button
                onPress={() => handleAddressVisibility(true)}
                raised
                containerStyle={styles.buttonImageContainer}
                buttonStyle={styles.buttonImage}
                titleStyle={styles.buttonTitle}
                icon={{
                  name: 'ios-pin',
                  type: 'ionicon',
                  color: 'white',
                  size: 20,
                }}
                title="SET ALAMAT"
              />
            </CardItem>
          )}
        </Card> */}
          {!values.asClient && (
            <Card containerStyle={styles.cardContainer}>
              <CardItem>
                <Body>
                  <Text style={styles.title}>{translations.TITLE_INFORMATION_BANK}</Text>
                </Body>
              </CardItem>
              <SelectPicker
                label={translations.LABEL_BANK_NAME + ' *'}
                items={banks}
                selectedItems={values.reg_member_bank ? [values.reg_member_bank] : []}
                onSelectedItemsChange={handleBankChanged}
                error={touched.reg_member_bank && errors.reg_member_bank}
              />
              <InputText
                label={translations.LABEL_BANK_ACCOUNT_NUMBER + ' *'}
                keyboardType="number-pad"
                error={touched.reg_member_bill && errors.reg_member_bill}
                onBlur={handleBlur('reg_member_bill')}
                onChangeText={handleChange('reg_member_bill')}
              />
              <InputText
                label={translations.LABEL_BANK_ACCOUNT_HOLDER + ' *'}
                error={touched.reg_member_bill_name && errors.reg_member_bill_name}
                onBlur={handleBlur('reg_member_bill_name')}
                onChangeText={handleChange('reg_member_bill_name')}
              />
              <InputText
                label={translations.LABEL_BANK_BRANCH_NAME + ' *'}
                error={touched.reg_member_branch && errors.reg_member_branch}
                onBlur={handleBlur('reg_member_branch')}
                onChangeText={handleChange('reg_member_branch')}
              />
            </Card>
          )}
          <Button full style={styles.buttonRegister} onPress={handleSubmit}>
            <Text style={styles.buttonRegisterText}>{translations.ACTION_SAVE}</Text>
          </Button>
        </KeyboardAwareScrollView>
      </Container>
      <AlertPro
        ref={alertRef}
        onConfirm={() => alertRef.current.close()}
        title={ALERT[signupAlertInfo].title}
        message={ALERT[signupAlertInfo].desc}
        textConfirm="OK"
        showCancel={false}
        customStyles={{
          container: {
            width: 300,
            shadowColor: '#000000',
            shadowOpacity: 0.1,
            shadowRadius: 10,
          },
          title: {
            fontSize: Fonts.size.h6,
          },
          buttonCancel: {
            backgroundColor: Colors.gray400,
          },
          buttonConfirm: {
            backgroundColor: Colors.bloodOrange,
          },
        }}
      />
    </>
  );
};
export default Content;
