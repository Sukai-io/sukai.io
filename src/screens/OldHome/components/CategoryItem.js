import React from 'react';
import {View} from 'react-native';
import {Text} from 'native-base';
import styles from './CategoryItem.styles';
import FlatTouchables from '../../../components/buttons/FlatTouchables';
import {SvgUri} from 'react-native-svg';
import {Metrics} from '../../../themes';

type Props = {
  name: String,
  icon: String,
  onPress: Function,
};

const CategoryItem = ({name, icon, onPress}: Props) => (
  <FlatTouchables onPress={onPress}>
    <View style={styles.container}>
      <SvgUri width={Metrics.icons.medium} height={Metrics.icons.medium} uri={icon} />
      <Text style={styles.title}>{name}</Text>
    </View>
  </FlatTouchables>
);

export default CategoryItem;
