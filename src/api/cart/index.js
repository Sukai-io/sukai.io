export default {
  state: {
    carts: [],
  },
  reducers: {
    addToCart(state, data) {
      return {
        ...state,
        carts: state.carts.filter(item => item.id !== data.id).concat(data),
      };
    },
    deleteFromCart(state, data) {
      return {
        ...state,
        carts: state.carts.filter(item => item.id !== data.id),
      };
    },
    clearCart(state) {
      return {
        ...state,
        carts: [],
      };
    },
    increaseQtyCart(state, id) {
      return {
        ...state,
        carts: state.carts.map(item => {
          if (item.id === id) {
            return {
              ...item,
              cart_qty: item.cart_qty + 1,
              cart_price: parseInt(item.cart_price, 10) + parseInt(item.product_price, 10),
            };
          }
          return item;
        }),
      };
    },
    decreaseQtyCart(state, id) {
      return {
        ...state,
        carts: state.carts.map(item => {
          if (item.id === id) {
            return {
              ...item,
              cart_qty: item.cart_qty === 0 ? item.cart_qty : item.cart_qty - 1,
              cart_price:
                item.cart_price === item.product_price
                  ? parseInt(item.cart_price, 10)
                  : parseInt(item.cart_price, 10) - parseInt(item.product_price, 10),
            };
          }
          return item;
        }),
      };
    },
  },
  selectors: {
    getCartQty: () => {
      return rootState => rootState.cart.carts.reduce((acc, item) => acc + item.cart_qty, 0);
    },
    getCartCheckout: () => {
      return rootState =>
        rootState.cart.carts.reduce((acc, item) => {
          const {id_member, username, seller, seller_city, seller_city_name, ...restItem} = item;
          const selectedItem = acc.find(item => item.id_member === id_member);
          if (selectedItem) {
            return acc.map(item =>
              selectedItem.id_member === item.id_member
                ? {
                    ...item,
                    products: item.products.concat(restItem),
                  }
                : item,
            );
          } else {
            return acc.concat({
              id_member,
              username,
              seller,
              seller_city,
              seller_city_name,
              courier: '',
              service: '',
              service_options: [],
              products: [restItem],
            });
          }
        }, []);
    },
  },
  effects: () => ({}),
};
