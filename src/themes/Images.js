const Images = {
  logo: require('../images/sukai-logo.png'),
  logo_barcode: require('../images/sukai-logo-barcode.jpg'),
  intro1: require('../images/intro-1.jpeg'),
  intro2: require('../images/intro-2.jpeg'),
  intro3: require('../images/intro-3.jpeg'),
  drawerCover: require('../images/drawer_cover.png'),
  dummyAdVideo: require('../images/pantene_ad.png')
}

export default Images
