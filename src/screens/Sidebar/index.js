import React, {Component} from 'react';
import {View, Image} from 'react-native';
import Collapsible from 'react-native-collapsible';
import {connect} from 'react-redux';
import R from 'ramda';
import {Container, Content, List, ListItem, Left, Icon, Right, Text} from 'native-base';
import FastImage from 'react-native-fast-image';
import styles from './styles';
import {baseUrl} from '../../static/Constant';
import {Images} from '../../themes';
import DropdownAlert from 'react-native-dropdownalert';
import {LocalizationContext} from '../../localization';

class SidebarScreen extends Component {
  static contextType = LocalizationContext;

  state = {
    openMarketplaceSub: false,
    openHistorySub: false,
  };

  handleHistoryCollapsible = () => {
    this.setState(prevState => ({openHistorySub: !prevState.openHistorySub}));
  };

  handleMarketplaceCollapsible = () => {
    this.setState(prevState => ({
      openMarketplaceSub: !prevState.openMarketplaceSub,
    }));
  };

  handleCheckRegister = () => {
    const {navigation} = this.props;
    navigation.navigate('MemberNavigator');
  };

  renderHistorySubMenu = () => {
    const {navigation, activeItemKey} = this.props;
    const {openHistorySub} = this.state;
    const {translations} = this.context;
    return (
      <Collapsible collapsed={!openHistorySub}>
        <ListItem
          selected={activeItemKey === 'HistoryWalletScreen'}
          button
          noBorder
          onPress={() => navigation.navigate('HistoryWalletScreen')}>
          <Left>
            <Text style={styles.subMenuText}>{translations.TITLE_HISTORY_WALLET}</Text>
          </Left>
        </ListItem>
        <ListItem
          selected={activeItemKey === 'HistoryTopupScreen'}
          button
          noBorder
          onPress={() => navigation.navigate('HistoryTopupScreen')}>
          <Left>
            <Text style={styles.subMenuText}>{translations.TITLE_HISTORY_TOPUP}</Text>
          </Left>
        </ListItem>
        <ListItem
          selected={activeItemKey === 'HistoryOrderScreen'}
          button
          noBorder
          onPress={() => navigation.navigate('HistoryOrderScreen')}>
          <Left>
            <Text style={styles.subMenuText}>{translations.TITLE_HISTORY_ORDER}</Text>
          </Left>
        </ListItem>
        <ListItem
          selected={activeItemKey === 'HistoryBonusScreen'}
          button
          noBorder
          onPress={() => navigation.navigate('HistoryBonusScreen')}>
          <Left>
            <Text style={styles.subMenuText}>{translations.TITLE_HISTORY_BONUS}</Text>
          </Left>
        </ListItem>
      </Collapsible>
    );
  };

  renderMarketplaceSubMenu = () => {
    const {openMarketplaceSub} = this.state;
    const {navigation, activeItemKey} = this.props;
    const {translations} = this.context;
    return (
      <Collapsible collapsed={!openMarketplaceSub}>
        <ListItem
          selected={activeItemKey === 'NewProductScreen'}
          button
          noBorder
          onPress={() => navigation.navigate('NewProductScreen')}>
          <Left>
            <Text style={styles.subMenuText}>{translations.TITLE_ADD_NEW_PRODUCT}</Text>
          </Left>
        </ListItem>
        <ListItem
          selected={activeItemKey === 'MyProductScreen'}
          button
          noBorder
          onPress={() => navigation.navigate('MyProductScreen')}>
          <Left>
            <Text style={styles.subMenuText}>{translations.TITLE_PRODUCT_LIST}</Text>
          </Left>
        </ListItem>
      </Collapsible>
    );
  };

  render() {
    const {activeItemKey, navigation, userData} = this.props;
    const {openMarketplaceSub, openHistorySub} = this.state;
    const {translations} = this.context;
    return (
      <Container>
        <DropdownAlert ref={ref => (this.dropdown = ref)} />
        <Content bounces={false} style={{flex: 1, backgroundColor: '#fff', top: -1}}>
          <Image source={Images.drawerCover} style={styles.drawerCover} />
          <View style={styles.drawerImage}>
            <FastImage
              source={{
                uri: baseUrl + R.pathOr('', ['avatar_url'], userData),
              }}
              style={styles.thumbnail}
            />
            <Text style={styles.titleBold}>{R.pathOr('-', ['name'], userData)}</Text>
            <Text style={styles.title}>
              <Text style={styles.title}>{translations.LABEL_WELCOME_TO}</Text>{' '}
              <Text style={styles.titleBold}>Sukai</Text>
            </Text>
          </View>
          <List>
            <ListItem
              selected={activeItemKey === 'HomeTab'}
              button
              noBorder
              onPress={() => navigation.navigate('HomeTab')}>
              <Left>
                <FastImage source={require('../../images/Icons/ic_home.png')} style={styles.icon} />
                <Text style={styles.text}>{translations.TITLE_HOME}</Text>
              </Left>
            </ListItem>
            <ListItem button noBorder onPress={this.handleMarketplaceCollapsible}>
              <Left>
                <FastImage source={require('../../images/Icons/ic_bag.png')} style={styles.icon} />
                <Text style={styles.text}>{translations.TITLE_MARKETPLACE}</Text>
              </Left>
              <Right>
                <Icon name={openMarketplaceSub ? 'arrow-dropdown' : 'arrow-dropright'} />
              </Right>
            </ListItem>
            {this.renderMarketplaceSubMenu()}
            <ListItem button noBorder onPress={this.handleHistoryCollapsible}>
              <Left>
                <FastImage source={require('../../images/Icons/ic_history.png')} style={styles.icon} />
                <Text style={styles.text}>{translations.TITLE_HISTORY}</Text>
              </Left>
              <Right>
                <Icon name={openHistorySub ? 'arrow-dropdown' : 'arrow-dropright'} />
              </Right>
            </ListItem>
            {this.renderHistorySubMenu()}
            <ListItem
              selected={activeItemKey === 'WalletNavigator'}
              button
              noBorder
              onPress={() => navigation.navigate('WalletNavigator')}>
              <Left>
                <FastImage source={require('../../images/Icons/ic_wallet.png')} style={styles.icon} />
                <Text style={styles.text}>{translations.TITLE_SUKAI_WALLET}</Text>
              </Left>
            </ListItem>
            {/* <ListItem selected={activeItemKey === 'MemberNavigator'} button noBorder onPress={this.handleCheckRegister}>
              <Left>
                <FastImage source={require('../../images/Icons/ic_add_member.png')} style={styles.icon} />
                <Text style={styles.text}>Add New Member</Text>
              </Left>
            </ListItem> */}
            <ListItem
              selected={activeItemKey === 'ProfileNavigator'}
              button
              noBorder
              onPress={() => navigation.navigate('ProfileNavigator')}>
              <Left>
                <FastImage source={require('../../images/Icons/ic_profile.png')} style={styles.icon} />
                <Text style={styles.text}>{translations.TITLE_PROFILE}</Text>
              </Left>
            </ListItem>
            <ListItem button noBorder onPress={() => {}}>
              <Left>
                <FastImage source={require('../../images/Icons/ic_faq.png')} style={styles.icon} />
                <Text style={styles.text}>{translations.TITLE_FAQ}</Text>
              </Left>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = ({auth}) => {
  return {
    userData: auth.userData,
  };
};

export default connect(mapStateToProps)(SidebarScreen);
