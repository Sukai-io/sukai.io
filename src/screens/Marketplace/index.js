/* eslint-disable no-unused-vars */
/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import {
  Container, Content, Card as CardNB, Text, Grid, Col, Row,
} from 'native-base'
import { Card } from 'react-native-elements'
import styles from './styles'
import CategoryItem from '../OldHome/components/CategoryItem'
import HeaderBack from '../../components/header/HeaderBack'
import Horizontal from '../../components/wrapper/Horizontal'
import { ApplicationStyles } from '../../themes'
import ProductItem from '../Categories/components/ProductItem'

type Props = {
  navigation: any,
}

class MarketplaceScreen extends Component<Props> {
  render() {
    const { navigation } = this.props
    return (
      <Container style={styles.mainContainer}>
        <HeaderBack title="Marketplace" />
        <Content style={styles.container}>
          <CardNB style={styles.categoryContainer}>
            <Text style={styles.categoryText}>Categories</Text>
            <Grid>
              <Row>
                <Col>
                  <CategoryItem
                    onPress={() => navigation.navigate('CategoriesScreen')}
                    name="Makanan"
                    icon={require('../../images/Icons/ic_food.png')}
                  />
                </Col>
                <Col>
                  <CategoryItem
                    onPress={() => navigation.navigate('CategoriesScreen')}
                    name="Minuman"
                    icon={require('../../images/Icons/ic_drink.png')}
                  />
                </Col>
                <Col>
                  <CategoryItem
                    name="Kesehatan"
                    icon={require('../../images/Icons/ic_health.png')}
                  />
                </Col>
                <Col>
                  <CategoryItem
                    onPress={() => navigation.navigate('CategoriesScreen')}
                    name="Otomotif"
                    icon={require('../../images/Icons/ic_gear.png')}
                  />
                </Col>
              </Row>
              <Row style={styles.marginTop}>
                <Col>
                  <CategoryItem
                    onPress={() => navigation.navigate('CategoriesScreen')}
                    name="Kecantikan"
                    icon={require('../../images/Icons/ic_beauty.png')}
                  />
                </Col>
                <Col />
                <Col />
                <Col />
              </Row>
            </Grid>
          </CardNB>
          <Card containerStyle={styles.cardContainer}>
            <Text style={styles.title}>Latest Product</Text>
            <Horizontal style={ApplicationStyles.flex}>
              <ProductItem onPress={() => navigation.navigate('ProductDetailScreen')} />
              <ProductItem onPress={() => navigation.navigate('ProductDetailScreen')} isNotFirst />
            </Horizontal>
            <Horizontal style={ApplicationStyles.flex}>
              <ProductItem onPress={() => navigation.navigate('ProductDetailScreen')} />
              <ProductItem onPress={() => navigation.navigate('ProductDetailScreen')} isNotFirst />
            </Horizontal>
          </Card>
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {}
}

const mapDispatchToProps = (dispatch) => {
  return {}
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MarketplaceScreen)
