import { object as yupObject, number as yupNumber } from 'yup'

export default yupObject().shape({
  amount: yupNumber()
    .required()
    .min(100000),
})
