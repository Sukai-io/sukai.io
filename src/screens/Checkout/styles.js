import {StyleSheet} from 'react-native';
import {ApplicationStyles, Metrics, Colors, Fonts} from '../../themes';

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
    paddingTop: Metrics.baseMargin,
    backgroundColor: Colors.gray100,
  },
  cardContainer: {
    ...ApplicationStyles.shadow,
    ...ApplicationStyles.flex,
    padding: Metrics.smallMargin,
    marginRight: 0,
    marginLeft: 0,
    marginTop: 0,
    marginBottom: Metrics.doubleBaseMargin,
  },
  title: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.bold,
    color: Colors.gray700,
    marginBottom: 0,
  },
  subtitle: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    color: Colors.gray600,
    marginTop: Metrics.smallMargin,
  },
  dropdownItem: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    color: Colors.gray800,
  },
  icon: {
    fontSize: Metrics.icons.tiny,
    color: Colors.gray400,
  },
  description: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    color: Colors.gray600,
    marginLeft: Metrics.baseMargin,
  },
  danger: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.bold,
    color: Colors.error,
  },
  buttonAddress: {
    backgroundColor: Colors.primary,
    marginTop: Metrics.baseMargin,
    height: 40,
  },
  buttonAddressText: {
    fontSize: Fonts.size.medium,
  },
  buttonImageContainer: {
    marginTop: Metrics.baseMargin,
  },
  buttonImage: {
    backgroundColor: Colors.primary,
    height: 40,
  },
  buttonEdit: {
    backgroundColor: Colors.green500,
    height: 40,
  },
  buttonTitle: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    marginLeft: Metrics.smallMargin,
  },
  pickerOffset: {
    top: Metrics.baseMargin,
  },
  input: {
    fontSize: Fonts.size.regular,
    paddingLeft: 0,
  },
  infoCourier: {
    ...ApplicationStyles.flex,
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    color: Colors.gray600,
  },
  shipping: {
    ...ApplicationStyles.flex,
    textAlign: 'right',
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    color: Colors.primary,
  },
  subTotalShipping: {
    ...ApplicationStyles.flex,
    textAlign: 'right',
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    color: Colors.gray900,
  },
  totalLeft: {
    ...ApplicationStyles.flex,
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    color: Colors.gray600,
  },
  totalRight: {
    ...ApplicationStyles.flex,
    textAlign: 'right',
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    color: Colors.gray900,
  },
  grandContainer: {
    marginTop: Metrics.smallMargin,
    ...ApplicationStyles.flexCenter,
  },
  grantTotal: {
    ...ApplicationStyles.flex,
    textAlign: 'right',
    fontSize: 16,
    fontFamily: Fonts.type.bold,
    color: Colors.primary,
  },
  amount: {
    ...ApplicationStyles.flex,
    textAlign: 'right',
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.bold,
    color: Colors.bloodOrange,
  },
  buttonContainer: {
    padding: Metrics.baseMargin,
  },
  button: {
    backgroundColor: Colors.bloodOrange,
  },
  error: {
    ...Fonts.style.error,
  },
});
