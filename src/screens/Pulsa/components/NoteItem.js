import React from 'react'
import { Text, View } from 'react-native'
import styles from './NoteItem.styles'

const NoteItem = () => (
  <View style={styles.container}>
    <Text style={styles.title}>Keterangan</Text>
    <Text>
      <Text style={styles.desc}>Masa berlaku 30 hari</Text>
    </Text>
  </View>
)

export default NoteItem
