import React, {Component} from 'react';
import DeviceInfo from 'react-native-device-info';
import {Container, Content, List, ListItem, Left, Right, Thumbnail, Body, H3, Icon, Text} from 'native-base';
import {connect} from 'react-redux';
import {Switch} from 'react-native';
import R from 'ramda';
// Add Actions - replace 'Your' with whatever your reducer is called :)
import moment from 'moment';

// Styles
import HeaderBack from '../../components/header/HeaderBack';
import styles from './styles';
import {baseUrl} from '../../static/Constant';
import LockscreenUtils from '../../utils/LockscreenUtils';
import Loader from '../../components/indicator/Loader';
import { LocalizationContext } from '../../localization';

type Props = {
  loading: Boolean,
  userData: Object,
  navigation: any,
  doLogout: Function,
};

class ProfileScreen extends Component<Props> {
  static contextType = LocalizationContext;

  state = {
    lockscreenStatus: false,
  };

  handleLogout = () => {
    const {doLogout} = this.props;
    doLogout();
  };

  componentDidMount() {
    LockscreenUtils.getLockscrenStatus(lockscreenStatus => this.setState({lockscreenStatus}));
  }

  onLockscreenUpdate = lockscreenStatus => {
    LockscreenUtils.activate(lockscreenStatus);
    this.setState({lockscreenStatus});
  };

  render() {
    const {navigation, userData, loading} = this.props;
    const {appLanguage} = this.context;
    const {lockscreenStatus} = this.state;
    return (
      <Container style={styles.mainContainer}>
        <HeaderBack
          customRight="create"
          onBackPress={() => navigation.navigate('HomeTab')}
          onRightPress={() => navigation.navigate('EditProfileScreen')}
        />
        <Loader loading={loading} />
        <Content padder>
          <List>
            <ListItem thumbnail>
              <Left>
                <Thumbnail
                  source={{
                    uri: baseUrl + R.pathOr('', ['avatar_url'], userData),
                  }}
                />
              </Left>
              <Body>
                <H3>{R.pathOr('-', ['name'], userData)}</H3>
                <Text style={{alignSelf: 'flex-start'}}>{R.pathOr('-', ['phone'], userData)}</Text>
              </Body>
            </ListItem>
            <ListItem itemDivider style={{marginTop: 10}}>
              <Text>Account Info</Text>
            </ListItem>
            <ListItem icon>
              <Left>
                <Icon name="calendar" />
              </Left>
              <Body>
                <Text>Registered At</Text>
              </Body>
              <Right>
                <Text>{moment(R.path(['datecreated'], userData)).format('DD MMM YYYY')}</Text>
              </Right>
            </ListItem>
            <ListItem icon>
              <Left>
                <Icon name="ribbon" />
              </Left>
              <Body>
                <Text>Membership</Text>
              </Body>
              <Right>
                <Text>{R.pathOr('-', ['package'], userData)}</Text>
              </Right>
            </ListItem>
            <ListItem icon button onPress={() => navigation.navigate('AddressListScreen', {isEdit: true})}>
              <Left>
                <Icon name="map" />
              </Left>
              <Body>
                <Text>Shipping Address</Text>
              </Body>
            </ListItem>
            <ListItem icon button onPress={() => navigation.navigate('ChangeLanguageScreen')}>
              <Left>
                <Icon name="translate" type="MaterialCommunityIcons" />
              </Left>
              <Body>
                <Text>Language</Text>
              </Body>
              <Right>
                <Text>{appLanguage === 'en' ? 'english' : ''}</Text>
              </Right>
            </ListItem>
            <ListItem icon button onPress={() => navigation.navigate('ChangePasswordScreen')}>
              <Left>
                <Icon name="key" />
              </Left>
              <Body>
                <Text>Change Password</Text>
              </Body>
            </ListItem>
            <ListItem icon>
              <Left>
                <Icon name="lock" />
              </Left>
              <Body>
                <Text>Lockscreen Status</Text>
              </Body>
              <Right>
                <Switch onValueChange={this.onLockscreenUpdate.bind(this)} value={lockscreenStatus} />
              </Right>
            </ListItem>
            <ListItem icon button onPress={this.handleLogout}>
              <Left>
                <Icon name="log-out" />
              </Left>
              <Body>
                <Text>Logout</Text>
              </Body>
            </ListItem>
            <ListItem itemDivider style={{marginTop: 10}}>
              <Text>Application Info</Text>
            </ListItem>
            <ListItem icon>
              <Left>
                <Icon name="phone-portrait" />
              </Left>
              <Body>
                <Text>Current Version</Text>
              </Body>
              <Right>
                <Text>{DeviceInfo.getVersion()}</Text>
              </Right>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.loading.effects.auth.doLogout,
    userData: state.auth.userData,
  };
};

const mapDispatchToProps = ({auth}) => {
  return {
    doLogout: () => auth.doLogout(),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);
