import {object as yupObject, string as yupString} from 'yup';

export default yupObject().shape({
  address_type: yupString().required(),
  address_location: yupString().required(),
  address_zipcode: yupString().required(),
  address_province: yupString()
    .nullable()
    .required(),
  address_city: yupString().when('address_province', {
    is: null,
    then: yupString().nullable(),
    otherwise: yupString()
      .nullable()
      .required(),
  }),
  address_district: yupString().when('address_city', {
    is: null,
    then: yupString(),
    otherwise: yupString().required(),
  }),
});
