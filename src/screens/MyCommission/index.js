import React from 'react';
import {View} from 'react-native';
import HeaderBack from '../../components/header/HeaderBack';

const MyCommissionScreen = () => (
  <View>
    <HeaderBack title="My Commision" withoutRight />
  </View>
);

export default MyCommissionScreen;
