import { StyleSheet } from 'react-native';
import {
  ApplicationStyles,
  Colors,
  Fonts,
  Metrics,
} from '../../themes';

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  list: {
    marginLeft: Metrics.baseMargin,
    marginRight: Metrics.baseMargin,
  },
  date: {
    fontSize: Fonts.size.small,
    color: Colors.gray400,
  },
  dateSelected: {
    fontSize: Fonts.size.small,
    color: Colors.gray800,
  },
  unselected: {
    fontSize: Fonts.size.regular,
    color: Colors.gray400,
  },
  selected: {
    fontSize: Fonts.size.regular,
    color: Colors.gray900,
  },
  icon: {
    fontSize: Metrics.icons.small,
    color: Colors.gray400,
  },
  iconSelected: {
    fontSize: Metrics.icons.small,
    color: Colors.gray900,
  },
});
