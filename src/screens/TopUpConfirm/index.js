/* eslint-disable no-nested-ternary */
/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react'
import R from 'ramda'
import AlertPro from 'react-native-alert-pro'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import {
  Container, Content, Card, Text,
} from 'native-base'
import { Button } from 'react-native-elements'
import { View, ActivityIndicator } from 'react-native'
import styles from './styles'
import HeaderBack from '../../components/header/HeaderBack'
import { Fonts, Colors } from '../../themes'
import { numeralFormatter } from '../../utils/TextUtils'

type Props = {
  loading: Boolean,
  loadingPost: Boolean,
  topupPending: Object,
  getTopupPending: () => Promise,
  doTopupConfirm: () => Promise
}

class TopUpConfirmScreen extends Component<Props> {
  componentDidMount() {
    const { getTopupPending } = this.props
    getTopupPending().then().catch(() => {})
  }

  handleConfirm = () => {
    const { doTopupConfirm } = this.props
    this.AlertPro.close()
    doTopupConfirm()
  }

  render() {
    const {
      topupPending, loading, loadingPost,
    } = this.props
    const statusConfirm = R.path(['confirm_payment'], topupPending)
    const total = parseInt(R.pathOr(0, ['total'], topupPending), 0)
    return (
      <Container style={styles.mainContainer}>
        <AlertPro
          ref={(ref) => {
            this.AlertPro = ref
          }}
          onConfirm={this.handleConfirm}
          onCancel={() => this.AlertPro.close()}
          title="Konfirmasi Top Up"
          message="Anda yakin telah melengkapi pembayaran?"
          textCancel="Batal"
          textConfirm="Iya"
          customStyles={{
            container: {
              width: 300,
              shadowColor: '#000000',
              shadowOpacity: 0.1,
              shadowRadius: 10,
            },
            title: {
              fontSize: Fonts.size.h6,
            },
            buttonCancel: {
              backgroundColor: Colors.gray400,
            },
            buttonConfirm: {
              backgroundColor: Colors.bloodOrange,
            },
          }}
        />
        <HeaderBack withoutRight title="Top Up Sukai Wallet" />
        {loading ? (
          <View style={styles.flexCenterJustity}>
            <ActivityIndicator />
          </View>
        )
          : (
            <Content style={styles.container}>
              {statusConfirm === '0'
                ? (
                  <Card style={styles.cardContainer}>
                    <Text style={styles.title}>Silahkan lakukan pembayaran ke :</Text>
                    <Text>
                      <Text style={styles.title}>Nama Rekening : </Text>
                      <Text style={styles.titleBold}>PT. Sukai Indonesia Berjaya</Text>
                    </Text>
                    <Text>
                      <Text style={styles.title}>Bank : </Text>
                      <Text style={styles.titleBold}>BCA</Text>
                    </Text>
                    <Text>
                      <Text style={styles.title}>No Rekening : </Text>
                      <Text style={styles.titleBold}>701-292-2485</Text>
                    </Text>
                    <Text>
                      <Text style={styles.title}>Kode Unik : </Text>
                      <Text style={styles.titleBold}>{R.pathOr('', ['uniquecode'], topupPending)}</Text>
                    </Text>
                    <Text>
                      <Text style={styles.title}>Jumlah Pembayaran : </Text>
                      <Text style={styles.titleBold}>{numeralFormatter(total)}</Text>
                    </Text>
                  </Card>
                ) : statusConfirm === '1' ? (
                  <Card style={styles.cardContainer}>
                    <Text style={[styles.title, { textAlign: 'center' }]}>Permintaan Anda sudah kami terima, sedang diproses oleh Admin</Text>
                  </Card>
                ) : (
                  <Card style={styles.cardContainer}>
                    <Text style={[styles.title, { textAlign: 'center' }]}>Tidak ada permintaan topup</Text>
                  </Card>
                )}
              {statusConfirm === '0'
              && (
              <Button
                loading={loadingPost}
                disabled={loadingPost}
                onPress={() => this.AlertPro.open()}
                raised
                containerStyle={styles.buttonContainer}
                buttonStyle={styles.button}
                icon={{ name: 'ios-send', type: 'ionicon', color: 'white' }}
                title="KONFIRMASI PEMBAYARAN TOP UP"
              />
              )}
            </Content>
          )}
      </Container>
    )
  }
}

const mapStateToProps = ({ balance, loading }) => {
  return {
    loading: loading.effects.balance.getTopupPending,
    loadingPost: loading.effects.balance.doTopupConfirm,
    topupPending: balance.topup_pending,
  }
}

const mapDispatchToProps = ({ balance }) => {
  return {
    getTopupPending: () => balance.getTopupPending(),
    doTopupConfirm: () => balance.doTopupConfirm(),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TopUpConfirmScreen)
