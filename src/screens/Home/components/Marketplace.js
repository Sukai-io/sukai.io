import React, {useState} from 'react';
import {Dimensions} from 'react-native';
import {View, Text, Image} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import { withNavigation } from 'react-navigation';
import {LocalizationContext} from '../../../localization';
import {Colors, Images} from '../../../themes';
import styles from './Marketplace.style';

const renderItem = ({item}) => {
  return (
    <TouchableOpacity style={{paddingHorizontal: 16}}>
      <Image style={styles.image} source={item} />
    </TouchableOpacity>
  );
};

const Marketplace = ({navigation}) => {
  const [snapIndex, setSnapIndex] = useState(0);
  const {translations} = React.useContext(LocalizationContext);
  const items = [Images.dummyAdVideo, Images.dummyAdVideo, Images.dummyAdVideo];

  return (
    <>
      <View style={styles.headerContainer}>
        <Text style={styles.textFeature}>{translations.TITLE_MARKETPLACE}</Text>
        <TouchableOpacity onPress={() => navigation.navigate('CategoriesScreen')}>
          <Text style={styles.textShowAll}>{translations.TITLE_SHOW_ALL}</Text>
        </TouchableOpacity>
      </View>
      <Carousel
        data={items}
        onBeforeSnapToItem={setSnapIndex}
        onSnapToItem={setSnapIndex}
        activeSlideAlignment="start"
        inactiveSlideScale={1}
        firstItem={snapIndex}
        renderItem={renderItem}
        sliderWidth={Dimensions.get('window').width}
        itemWidth={widthPercentageToDP('80%')}
      />
      <View style={{paddingVertical: 0}}>
        {/* <Pagination
          dotsLength={items.length}
          activeDotIndex={snapIndex}
          containerStyle={styles.paginationContainer}
          dotStyle={styles.dot}
          inactiveDotOpacity={0.2}
          inactiveDotScale={1}
        /> */}
        <Pagination
          dotsLength={items.length}
          activeDotIndex={snapIndex}
          containerStyle={{justifyContent: 'center', paddingVertical: 10}}
          dotContainerStyle={{marginHorizontal: 3}}
          dotStyle={{
            width: 8,
            height: 8,
            borderRadius: 5,
            backgroundColor: Colors.gray900,
            elevation: 2,
          }}
          inactiveDotStyle={{
            backgroundColor: Colors.gray800,
          }}
          inactiveDotOpacity={0.2}
          inactiveDotScale={1}
        />
      </View>
      {/* <ScrollView horizontal>
        <TouchableOpacity>
          <Image style={styles.image} source={Images.dummyAdVideo} />
        </TouchableOpacity>
        <TouchableOpacity>
          <Image style={styles.image} source={Images.dummyAdVideo} />
        </TouchableOpacity>
        <TouchableOpacity>
          <Image style={styles.image} source={Images.dummyAdVideo} />
        </TouchableOpacity>
      </ScrollView> */}
    </>
  );
};

export default withNavigation(Marketplace);
