import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Image, View} from 'react-native';
import {Images, Colors} from '../../themes';
import {LocalizationContext} from '../../localization';

type Props = {
  isLoggedIn: Boolean,
  authToken: String,
  navigation: any,
  getSurveyStatus: Function,
};

class SplashScreen extends Component<Props> {
  static contextType = LocalizationContext;

  componentDidMount() {
    const {initializeAppLanguage} = this.context;
    const {getSurveyStatus, isLoggedIn, navigation} = this.props;
    initializeAppLanguage();
    if (isLoggedIn) {
      getSurveyStatus();
    } else {
      setTimeout(() => {
        navigation.navigate('IntroScreen');
      }, 1000);
    }
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: Colors.snow, justifyContent: 'center', alignItems: 'center'}}>
        <Image source={Images.logo} resizeMode="contain" style={{width: 200, height: 200}} />
      </View>
    );
  }
}

const mapStateToProps = ({auth}) => {
  return {
    isLoggedIn: auth.isLoggedIn,
  };
};

const mapDispatchToProps = ({survey: {getSurveyStatus}}) => {
  return {
    getSurveyStatus,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen);
