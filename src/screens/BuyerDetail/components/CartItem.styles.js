import {StyleSheet} from 'react-native';
import {ApplicationStyles, Metrics, Fonts, Colors} from '../../../themes';

export default StyleSheet.create({
  container: {
    ...ApplicationStyles.flex,
    flexDirection: 'row',
    backgroundColor: 'white',
    paddingBottom: Metrics.baseMargin,
    marginBottom: Metrics.doubleBaseMargin,
    borderBottomColor: Colors.gray300,
    borderBottomWidth: 1,
  },
  isLast: {
    marginBottom: 0,
    borderBottomWidth: 0,
  },
  subContainer: {
    ...ApplicationStyles.flex,
  },
  infoContainer: {
    ...ApplicationStyles.flex,
    marginLeft: Metrics.doubleBaseMargin,
  },
  title: {
    fontSize: Fonts.size.regular,
    color: Colors.gray800,
  },
  price: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.base,
    color: Colors.primary,
    marginTop: Metrics.baseMargin,
  },
  qty: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    color: Colors.gray600,
  },
  deleteContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.cloud,
    padding: Metrics.doubleBaseMargin,
  },
  operatorIcon: {
    fontSize: Metrics.icons.small,
    color: Colors.primary,
  },
  deleteIcon: {
    fontSize: Metrics.icons.medium,
    color: Colors.gray600,
  },
  amount: {
    paddingVertical: Metrics.smallMargin,
    paddingHorizontal: Metrics.doubleBaseMargin,
    borderWidth: 1,
    borderColor: Colors.gray200,
    borderRadius: Metrics.buttonRadius,
  },
  buttonOperator: {
    paddingTop: 0,
    paddingBottom: 0,
    paddingLeft: 0,
    paddingRight: 0,
    width: 50,
    height: 30,
    alignSelf: 'center',
  },
});
