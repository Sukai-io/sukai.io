import React, {Component} from 'react';
import {connect} from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
import {Formik} from 'formik';
import {FormValues} from './types';
import Content from './Content';
import validation from './validation';
import {Alert} from 'react-native';
import {goBack} from '../../utils/NavigationUtils';
import SimpleToast from 'react-native-simple-toast';
import {LocalizationContext} from '../../localization';

class EditAddressScreen extends Component {
  static contextType = LocalizationContext;

  async componentDidMount() {
    const {getProvinces, getCities, navigation} = this.props;
    const detail = navigation.getParam('detail', null);
    await getProvinces();
    await getCities({province_id: detail?.address_province});
  }

  handlePrimaryAddress = () => {
    const {setSelectedAddress, navigation} = this.props;
    const {translations} = this.context;
    const detail = navigation.getParam('detail', null);
    setSelectedAddress(detail);
    SimpleToast.show(translations.INFO_SUCCESS_SET_PRIMARY_ADDRESS);
    goBack();
  };

  handleDeleteAddress = () => {
    const {translations} = this.context;
    Alert.alert(translations.INFO_CONFIRM, translations.INFO_CONFIRM_ADDRESS_DELETE, [
      {
        text: translations.ACTION_CANCEL,
        onPress: () => {},
        style: 'cancel',
      },
      {
        text: 'OK',
        onPress: () => {
          const {deleteShippingAddress, navigation} = this.props;
          const detail = navigation.getParam('detail', null);
          deleteShippingAddress({address_id: detail?.id});
        },
      },
    ]);
  };

  handleSubmit = (values: FormValues, {resetForm}) => {
    const {editShippingAddress} = this.props;
    editShippingAddress(values).then(() => {
      resetForm({});
    });
  };

  render() {
    const {navigation} = this.props;
    const detail = navigation.getParam('detail', null);
    return (
      <Formik
        initialValues={{
          address_id: detail?.id || '',
          address_type: detail?.address_type || '',
          address_location: detail?.address_location || '',
          address_district: detail?.address_district || '',
          address_city: detail?.address_city || null,
          address_city_name: detail?.address_city_name || null,
          address_province: detail?.address_province || null,
          address_province_name: detail?.address_province_name || null,
          address_zipcode: detail?.address_zipcode || '',
        }}
        enableReinitialize
        onSubmit={(values, actions) => this.handleSubmit(values, actions)}
        validationSchema={validation}
        render={props => (
          <Content onDeleteAddress={this.handleDeleteAddress} onPrimaryAddress={this.handlePrimaryAddress} {...props} />
        )}
      />
    );
  }
}

const mapDispatchToProps = ({support, auth}) => {
  return {
    setSelectedAddress: data => auth.setSelectedAddress(data),
    getProvinces: () => support.getProvincesRajaOngkir(),
    getCities: data => support.getCitiesRajaOngkir(data),
    editShippingAddress: data => support.editShippingAddress(data),
    deleteShippingAddress: data => support.deleteShippingAddress(data),
  };
};

export default connect(null, mapDispatchToProps)(EditAddressScreen);
