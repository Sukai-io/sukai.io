import React from 'react';
import {View} from 'react-native';
import {Text, Button} from 'native-base';
import Horizontal from '../../../components/wrapper/Horizontal';
import styles from './TotalItem.styles';

const TotalItem = ({onCheckout, totalPrice, totalQty}) => (
  <Horizontal style={styles.container}>
    <View style={styles.subContainer}>
      <Text style={styles.desc}>Total Harga (sebelum ongkir)</Text>
      <Text style={styles.price}>{totalPrice}</Text>
    </View>
    <Button onPress={onCheckout} style={styles.button}>
      <Text style={styles.checkout}>{`BELI (${totalQty})`}</Text>
    </Button>
  </Horizontal>
);

export default TotalItem;
