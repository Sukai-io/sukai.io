export type FormValues = {
  product_stock: String,
  product_id: String,
  product_name: String,
  product_detail: String,
  product_weight: String,
  product_price: String,
  product_commission: String,
  product_image: Object,
  product_category: Array<Object>,
};
