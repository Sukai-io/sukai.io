/* eslint-disable react/state-in-constructor */
import React, {useCallback} from 'react';
import {Container} from 'native-base';
import {Card} from 'react-native-elements';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import styles from './styles';
import HeaderProfile from '../EditProfile/components/HeaderProfile';
import {Props} from './Props';
import {useSelector, shallowEqual, useDispatch} from 'react-redux';
import InputText from '../NewMember/components/InputText';
import SelectPicker from '../NewMember/components/SelectPicker';
import Loader from '../../components/indicator/Loader';

const Content = ({
  touched,
  errors,
  values,
  setFieldValue,
  handleBlur,
  handleChange,
  handleSubmit,
}: Props) => {
  const {
    support: {getCitiesRajaOngkir},
  } = useDispatch();
  const {
    provinces_rajaongkir: provinces,
    cities_rajaongkir: cities,
  } = useSelector(({support}) => support, shallowEqual);
  const loading = useSelector(
    ({loading}) => loading.effects.support.addShippingAddress,
    shallowEqual,
  );
  const handleProvinceChanged = useCallback(
    value => {
      getCitiesRajaOngkir({province_id: value[0]});
      setFieldValue('address_province', value[0]);
      if (values.address_city || values.address_district) {
        setFieldValue('address_city', null);
        setFieldValue('address_district', '');
      }
    },
    [provinces],
  );
  const handleCityChanged = useCallback(value => {
    setFieldValue('address_city', value[0]);
    values.reg_member_district && setFieldValue('address_district', '');
  }, []);
  return (
    <Container style={styles.mainContainer}>
      <HeaderProfile title="Tambah Alamat" onSave={handleSubmit} />
      <Loader loading={loading} />
      <KeyboardAwareScrollView
        style={styles.container}
        keyboardShouldPersistTaps="always">
        <Card containerStyle={styles.cardContainer}>
          <InputText
            label="Simpan Alamat Sebagai *"
            error={touched.address_type && errors.address_type}
            onBlur={handleBlur('address_type')}
            onChangeText={handleChange('address_type')}
          />
          <InputText
            label="Alamat *"
            error={touched.address_location && errors.address_location}
            onBlur={handleBlur('address_location')}
            onChangeText={handleChange('address_location')}
          />
          <InputText
            label="Kode pos *"
            keyboardType="number-pad"
            error={touched.address_zipcode && errors.address_zipcode}
            onBlur={handleBlur('address_zipcode')}
            onChangeText={handleChange('address_zipcode')}
          />
          <SelectPicker
            selectText="Provinsi *"
            items={provinces}
            selectedItems={
              values.address_province ? [values.address_province] : []
            }
            onSelectedItemsChange={handleProvinceChanged}
            error={touched.address_province && errors.address_province}
          />
          {cities.length > 0 && (
            <SelectPicker
              selectText="Kota/Kabupaten *"
              items={cities}
              selectedItems={values.address_city ? [values.address_city] : []}
              onSelectedItemsChange={handleCityChanged}
              error={touched.address_city && errors.address_city}
            />
          )}
          {values.address_city && (
            <InputText
              label="Kecamatan/Kelurahan"
              error={touched.address_district && errors.address_district}
              onBlur={handleBlur('address_district')}
              onChangeText={handleChange('address_district')}
            />
          )}
        </Card>
      </KeyboardAwareScrollView>
    </Container>
  );
};
export default Content;
