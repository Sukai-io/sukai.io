import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView } from 'react-native'
import { connect } from 'react-redux'

// Styles
import { Formik, FormikProps } from 'formik'
import styles from './styles'
import AuthInput from '../../components/input/AuthInput'
import AuthButton from '../../components/buttons/AuthButton'
import validation from './validation'

type Props = {
  loading: Boolean,
  doForgetPassword: Function,
}

type FormValues = {
  username: string,
  email: string,
}

class ForgetPasswordScreen extends Component<Props> {
  handleSubmit = (values: FormValues) => {
    const { doForgetPassword } = this.props
    const { email, username } = values
    doForgetPassword({ username, email })
  }

  renderForm = ({
    handleSubmit,
    setFieldValue,
    setFieldTouched,
    touched,
    errors,
    values,
  }: FormikProps<FormValues>) => {
    const { loading } = this.props
    return (
      <ScrollView keyboardShouldPersistTaps="always" style={styles.authContainer}>
        <KeyboardAvoidingView behavior="position">
          <Text style={styles.sectionContainer}>
            <Text style={styles.sectionText}>Enter your </Text>
            <Text style={styles.sectionText}>registered username and email</Text>
          </Text>
          <Text style={styles.marginTop}>
            <Text style={styles.sectionText}>We will send you a </Text>
            <Text style={styles.sectionText}>new Password</Text>
          </Text>
          <AuthInput
            name="account"
            placeholder="Enter Username"
            autoCapitalize="none"
            onBlur={() => setFieldTouched('username')}
            onChangeText={(value) => setFieldValue('username', value)}
            error={touched.username && errors.username ? errors.username : null}
            value={values.username}
          />
          <AuthInput
            name="email"
            placeholder="Enter Email"
            keyboardType="email-address"
            autoCapitalize="none"
            onBlur={() => setFieldTouched('email')}
            onChangeText={(value) => setFieldValue('email', value)}
            error={touched.email && errors.email ? errors.email : null}
            value={values.email}
          />
          <AuthButton loading={loading} onPress={handleSubmit} title="Request new password" />
        </KeyboardAvoidingView>
      </ScrollView>
    )
  }

  render() {
    return (
      <Formik
        initialValues={{ username: '', email: '' }}
        enableReinitialize
        onSubmit={(values: FormValues) => this.handleSubmit(values)}
        validationSchema={validation}
        render={(actions: FormikProps<FormValues>) => this.renderForm(actions)}
      />
    )
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.loading.effects.auth.doForgetPassword,
  }
}

const mapDispatchToProps = ({ auth }) => {
  return {
    doForgetPassword: (data) => auth.doForgetPassword(data),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ForgetPasswordScreen)
