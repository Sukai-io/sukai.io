// /* eslint-disable max-len */
// import React from 'react';
// import { withNavigation } from 'react-navigation';
// import { connect } from 'react-redux';

// import {
//   Icon,
//   Header,
//   Left,
//   Button,
//   Body,
//   Right,
//   Text,
//   View,
// } from 'native-base';
// import styles from './styles';
// import store from '../../config/CreateStore';

// type Props = {
//   title: string,
//   subtitle: string,
//   withoutRight: Boolean,
//   navigation: any,
//   carts: Number,
// };

// const HeaderDraw = (props: Props) => {
//   const { title, subtitle, navigation, withoutRight, carts } = props;

//   return (
//     <Header style={styles.container}>
//       <Left>
//         <Button transparent onPress={() => navigation.openDrawer()}>
//           <Icon name="menu" />
//         </Button>
//       </Left>
//       <Body>
//         <Text style={styles.text}>{title}</Text>
//         <Text style={styles.subtitle}>{subtitle}</Text>
//       </Body>
//       {withoutRight ? (
//         <Right />
//       ) : (
//         <Right>
//           <View style={styles.imageContainer}>
//             <Icon type="FontAwesome" name="user" />
//           </View>
//         </Right>
//       )}
//     </Header>
//   );
// };

// const mapStateToProps = state => ({
//   carts: store.select.cart.getCartQty(state),
// });

// export default connect(mapStateToProps)(withNavigation(HeaderDraw));

/* eslint-disable max-len */
import React from 'react';
import {withNavigation} from 'react-navigation';
import {connect} from 'react-redux';

import {Icon, Header, Left, Button, Body, Right, Text} from 'native-base';
import {Badge} from 'react-native-elements';
import styles from './styles';
import store from '../../config/CreateStore';
import {View} from 'react-native';
import {TouchableOpacity} from 'react-native';

type Props = {
  title: string,
  withoutRight: Boolean,
  navigation: any,
  carts: Number,
};

const HeaderDraw = (props: Props) => {
  const {title, navigation, withoutRight, carts} = props;
  return (
    <Header>
      <Left>
        <Button transparent onPress={() => navigation.openDrawer()}>
          <Icon name="menu" />
        </Button>
      </Left>
      <Body >
        <Text style={styles.text}>{title}</Text>
      </Body>
      {withoutRight ? (
        <Right />
      ) : (
        <Right>
          <TouchableOpacity onPress={() => navigation.navigate('ProfileNavigator')} style={styles.imageContainer}>
            <Icon type="FontAwesome" name="user" />
          </TouchableOpacity>
        </Right>
      )}
    </Header>
  );
};

const mapStateToProps = state => ({
  carts: store.select.cart.getCartQty(state),
});

export default connect(mapStateToProps)(withNavigation(HeaderDraw));
