import actions from './actions';
import infos from './infos';
import labels from './labels';
import titles from './titles';

export default {
  ...actions,
  ...infos,
  ...labels,
  ...titles
};
