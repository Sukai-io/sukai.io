/* eslint-disable max-len */
import React, { useContext } from 'react';
import {
  NavigationScreenProps,
  NavigationState,
  withNavigation,
} from 'react-navigation';
import {Icon, Header, Left, Button, Body, Right, Text} from 'native-base';
import styles from './HeaderProfile.styles';
import { LocalizationContext } from '../../../localization';

type Props = {
  title: string,
  hideRight: Boolean,
  navigation: NavigationScreenProps<NavigationState>,
  onSave: Function,
  onBack: Function,
};

const HeaderProfile = (props: Props) => {
  const {title, navigation, onSave, onBack, hideRight} = props;
  const {translations } = useContext(LocalizationContext)
  return (
    <Header>
      <Left>
        <Button
          transparent
          onPress={() => (onBack ? onBack() : navigation.goBack())}>
          <Icon name="arrow-back" />
        </Button>
      </Left>
      <Body>
        <Text style={styles.text}>{title}</Text>
      </Body>
      <Right>
        {!hideRight && (
          <Button transparent onPress={onSave}>
            <Text style={styles.right}>{translations.ACTION_SAVE}</Text>
          </Button>
        )}
      </Right>
    </Header>
  );
};

export default withNavigation(HeaderProfile);
