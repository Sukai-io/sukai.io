import { StyleSheet } from 'react-native'
import { Fonts, Colors } from '../../../themes'

export default StyleSheet.create({
  text: {
    fontSize: Fonts.size.h6,
    fontFamily: Fonts.type.bold,
    color: Colors.snow,
  },
  right: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.bold,
    color: Colors.snow,
  },
})
