import { StyleSheet } from 'react-native'
import {
  ApplicationStyles, Fonts, Metrics, Colors,
} from '../../../themes'

export default StyleSheet.create({
  container: {
    ...ApplicationStyles.flexCenter,
    paddingVertical: Metrics.smallMargin,
    paddingHorizontal: Metrics.baseMargin,
    marginTop: Metrics.smallMargin,
    backgroundColor: Colors.snow,
  },
  subContainer: {
    flex: 6,
    marginLeft: Metrics.baseMargin,
  },
  description: {
    ...ApplicationStyles.flex,
    ...Fonts.style.description,
    fontSize: Fonts.size.medium,
  },
  amountContainer: {
    flex: 2,
    paddingVertical: Metrics.smallMargin,
    paddingHorizontal: Metrics.marginHorizontal,
    backgroundColor: Colors.bloodOrange,
  },
  amount: {
    ...Fonts.style.description,
    fontSize: Fonts.size.medium,
    textAlign: 'center',
    color: Colors.snow,
  },
})
