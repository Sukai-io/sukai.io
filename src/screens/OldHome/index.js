import React, {Component} from 'react';
import {Dimensions, View, Image, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import AlertPro from 'react-native-alert-pro';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import {Container, Content, Card, Text, Grid, Col, Row} from 'native-base';
import styles from './styles';
import HeaderDrawer from '../../components/header/HeaderDrawer';
import WalletItem from './components/WalletItem';
import FeatureItem from './components/FeatureItem';
import CategoryItem from './components/CategoryItem';
import store from '../../config/CreateStore';
import {Fonts, Colors} from '../../themes';
import VerticalList from '../../components/list/VerticalList';
import LockscreenUtils from '../../utils/LockscreenUtils';
import {baseURL, getAuthToken} from '../../api/ApiClient';
import Carousel, {Pagination} from 'react-native-snap-carousel';

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snapIndex: 0,
    };
    this.renderCategorySource = this.renderCategorySource.bind(this);
    this.renderAd = this.renderAd.bind(this);
  }

  componentDidMount() {
    const {
      getBalance,
      getProductCategory,
      getAllAds,
      getMemberData,
      getShippingAddresses,
      getHomeAds,
      getMessageList,
      navigation: {navigate},
    } = this.props;
    getBalance();
    getProductCategory();
    getShippingAddresses();
    getMemberData();
    getHomeAds();
    getAllAds(0);
    getMessageList({message_type: 'private'});
    getMessageList({message_type: 'broadcast'});
    LockscreenUtils.setEndpoint(baseURL);
    LockscreenUtils.setAuthToken(getAuthToken());
    LockscreenUtils.getLockscrenStatus(lockscreenStatus => {});
    LockscreenUtils.getAdId(id => {
      id && setTimeout(() => navigate('AdDetail', {id}), 2000);
    });
    // setTimeout(() => LockscreenUtils.activate(true), 1000);
  }

  renderCategorySource(name) {
    switch (name) {
      case 'Makanan':
        return require('../../images/Icons/ic_food.png');
      case 'Minuman':
        return require('../../images/Icons/ic_drink.png');
      case 'Kesehatan':
        return require('../../images/Icons/ic_health.png');
      case 'Otomotif':
        return require('../../images/Icons/ic_gear.png');
      case 'Kecantikan':
        return require('../../images/Icons/ic_beauty.png');
      default:
        return null;
    }
  }

  renderAd({item: {title, id, image_thumbnail, bonus_status, member_id}}) {
    let {
      navigation: {navigate},
      emptyAd,
      userData,
    } = this.props;
    return (
      <TouchableOpacity
        onPress={() => {
          navigate('AdDetail', {id});
        }}>
        <View style={{padding: 10}}>
          <Image style={{width: '100%', height: 220, borderRadius: 15}} source={{uri: image_thumbnail}} />
          <Text style={{fontSize: Fonts.size.small, marginTop: 5}}>{title}</Text>
          {bonus_status == 0 && member_id != userData.id && (
            <Image
              resizeMode={'contain'}
              style={{position: 'absolute', width: 60, height: 20, top: 20, right: 20}}
              source={require('../../images/Icons/bonus500.png')}
            />
          )}
          {bonus_status == 1 && member_id != userData.id && (
            <Image
              resizeMode={'contain'}
              style={{position: 'absolute', width: 60, height: 20, top: 20, right: 20}}
              source={require('../../images/Icons/bonus500claimed.png')}
            />
          )}
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    const {
      navigation,
      loading,
      ads,
      loadingCategory,
      balance,
      username,
      productCategories,
      focusedAdId,
      userData,
    } = this.props;
    return (
      <Container style={styles.mainContainer}>
        <AlertPro
          ref={ref => {
            this.AlertPro = ref;
          }}
          onConfirm={() => this.AlertPro.close()}
          onCancel={() => this.AlertPro.close()}
          showCancel={false}
          title="Sukai Wallet"
          message={`Masukkan username Anda saat ditransfer\nUsername Anda : ${username}`}
          textConfirm="OK"
          customStyles={{
            container: {
              width: '100%',
              shadowColor: '#000000',
              shadowOpacity: 0.1,
              shadowRadius: 10,
            },
            title: {
              fontSize: Fonts.size.h6,
            },
            buttonCancel: {
              backgroundColor: Colors.gray400,
            },
            buttonConfirm: {
              backgroundColor: Colors.bloodOrange,
            },
          }}
        />
        <HeaderDrawer title="Sukai" />
        <Content style={styles.subContainer}>
          {/* <Horizontal style={styles.avatarContainer}>
            <Thumbnail
              source={require('../../images/Icons/ic_user.png')}
              style={styles.avatar}
            />
            <Text style={styles.marginLeft}>
              <Text style={styles.avatarText}>
                {'Status anda saat ini adalah\n'}
              </Text>
              <Text style={styles.avatarTextBold}>Member with grade </Text>
              <Text style={styles.avatarTextBoldPrimary}>NOT QUALIFIED</Text>
            </Text>
          </Horizontal> */}
          {userData?.package === 'basic' && (
            <WalletItem onReceive={() => this.AlertPro.open()} fetching={loading} balance={balance || '0'} />
          )}
          {/*<Text>{JSON.stringify(this.props.ads)}</Text>*/}
          {userData?.package === 'client' && (
            <Grid>
              <Row>
                <Col>
                  <FeatureItem
                    onPress={() => navigation.navigate('MyAdsScreen', {key: Math.random()})}
                    name="My Advertising"
                    icon={require('../../images/Icons/ic_ads.png')}
                  />
                </Col>
                <Col>
                  <FeatureItem
                    onPress={() => navigation.navigate('AdForm', {id: null})}
                    name="Add New"
                    icon={require('../../images/Icons/ic_ads.png')}
                  />
                </Col>
                <Col>
                  <FeatureItem
                    onPress={() => navigation.navigate('ResellerList', {key: Math.random()})}
                    name="Reseller List"
                    icon={require('../../images/Icons/ic_profile.png')}
                  />
                </Col>
              </Row>
            </Grid>
          )}
          {userData?.package === 'basic' && (
            <Grid>
              <Row>
                <Col style={styles.colContainer}>
                  <FeatureItem
                    onPress={() => navigation.navigate('CategoriesScreen')}
                    name="Market Place"
                    icon={require('../../images/Icons/ic_bag.png')}
                  />
                </Col>
                <Col style={styles.colContainer}>
                  <FeatureItem
                    onPress={() => navigation.navigate('OfficialStoreScreen')}
                    name="Official Store"
                    icon={require('../../images/Icons/ic_official.png')}
                  />
                </Col>
                <Col style={styles.colContainer}>
                  <FeatureItem
                    onPress={() => navigation.navigate('MyCommissionScreen')}
                    name="MyCommission"
                    icon={require('../../images/Icons/ic_history.png')}
                  />
                </Col>
              </Row>
            </Grid>
          )}
          {/* <Grid>
            <Row>
              <Col>
                <FeatureItem
                  onPress={() => navigation.navigate('PpobListScreen')}
                  name="Order PPOB"
                  icon={require('../../images/Icons/ic_sim.png')}
                />
              </Col>
              <Col style={styles.colContainer}>
                <FeatureItem
                  onPress={() => navigation.navigate('CategoriesScreen')}
                  name="Market Place"
                  icon={require('../../images/Icons/ic_bag.png')}
                />
              </Col>
              <Col style={styles.colContainer}>
                <FeatureItem
                  onPress={() => navigation.navigate('OfficialStoreScreen')}
                  name="Official Store"
                  icon={require('../../images/Icons/ic_official.png')}
                />
              </Col>
              <Col style={styles.colContainer}>
                <FeatureItem
                  onPress={() => navigation.navigate('CommisionList')}
                  name="MyCommission"
                  icon={require('../../images/Icons/ic_history.png')}
                />
              </Col>
              <Col>
                <FeatureItem onPress={() => {}} name="Wifi Provider" icon={require('../../images/Icons/ic_wifi.png')} />
              </Col>
            </Row>
            <Row>
              {userData?.package === 'client' && (
                <Col>
                  <FeatureItem
                    onPress={() => navigation.navigate('MyAdsScreen', {key: Math.random()})}
                    name="Smart Adv"
                    icon={require('../../images/Icons/ic_ads.png')}
                  />
                </Col>
              )}
              <Col style={styles.colContainer}>
                <FeatureItem
                  onPress={() => navigation.navigate('DigitalCompetitionScreen')}
                  name="Digital Competition"
                  icon={require('../../images/Icons/ic_letter.png')}
                />
              </Col>
              <Col>
                <FeatureItem
                  onPress={() => navigation.navigate('PartnershipScreen')}
                  name="Partnership"
                  icon={require('../../images/Icons/ic_partnership.png')}
                />
              </Col>
            </Row>
          </Grid> */}
          <View style={{marginTop: 20, marginBottom: 5, flexDirection: 'row', alignItems: 'center'}}>
            <Text
              style={[
                styles.categoryText,
                {padding: 0, marginVertical: 0, flex: 1, fontSize: 16, marginHorizontal: 3},
              ]}>
              Smart Advertising
            </Text>
            <TouchableOpacity onPress={() => navigation.navigate('AllAdsScreen')}>
              <Text style={{fontSize: Fonts.size.small, color: Colors.bloodOrange}}>Lihat semua</Text>
            </TouchableOpacity>
          </View>
          <View style={{marginHorizontal: -7, paddingBottom: 10}}>
            <Carousel
              ref={c => {
                this._carousel = c;
              }}
              data={ads}
              onBeforeSnapToItem={snapIndex => this.setState({snapIndex})}
              onSnapToItem={snapIndex => this.setState({snapIndex})}
              activeSlideAlignment="start"
              inactiveSlideScale={1}
              firstItem={this.state.snapIndex}
              renderItem={this.renderAd}
              sliderWidth={Dimensions.get('window').width}
              itemWidth={240}
            />
            <View style={{paddingVertical: 0}}>
              <Pagination
                dotsLength={ads.length}
                activeDotIndex={this.state.snapIndex}
                containerStyle={{justifyContent: 'center', paddingVertical: 15}}
                dotStyle={{
                  width: 8,
                  height: 8,
                  borderRadius: 5,
                  marginHorizontal: 0,
                  marginVertical: 0,
                  backgroundColor: Colors.bloodOrange,
                  elevation: 2,
                }}
                inactiveDotStyle={
                  {
                    // Define styles for inactive dots here
                  }
                }
                inactiveDotOpacity={0.2}
                inactiveDotScale={1}
              />
            </View>
          </View>
          {!loadingCategory && productCategories.length > 0 && (
            <Card style={styles.categoryContainer}>
              <Text style={styles.categoryText}>Marketplace</Text>
              <VerticalList
                data={productCategories}
                renderItem={({item}) => (
                  <CategoryItem
                    onPress={() => navigation.navigate('CategoriesScreen', {id: item.id})}
                    name={item.category_name}
                    icon={item.icon_url}
                  />
                )}
                numColumns={5}
              />
              {/* <Row style={{flexWrap: 'wrap', flex: 1}}>
                  <CategoryItem
                    onPress={() => navigation.navigate('CategoriesScreen')}
                    name="Makanan"
                    icon={require('../../images/Icons/ic_food.png')}
                  /> */}

              {/* <Col>
                    <CategoryItem
                      onPress={() => navigation.navigate('CategoriesScreen')}
                      name="Minuman"
                      icon={require('../../images/Icons/ic_drink.png')}
                    />
                  </Col>
                  <Col>
                    <CategoryItem
                      onPress={() => navigation.navigate('CategoriesScreen')}
                      name="Kesehatan"
                      icon={require('../../images/Icons/ic_health.png')}
                    />
                  </Col>
                  <Col>
                    <CategoryItem
                      onPress={() => navigation.navigate('CategoriesScreen')}
                      name="Otomotif"
                      icon={require('../../images/Icons/ic_gear.png')}
                    />
                  </Col>
                  <Col>
                    <CategoryItem
                      onPress={() => navigation.navigate('CategoriesScreen')}
                      name="Makanan"
                      icon={require('../../images/Icons/ic_food.png')}
                    />
                  </Col> */}
              {/* </Row> */}
              {/* <Row style={styles.marginTop}>
                  <Col>
                    <CategoryItem
                      onPress={() => navigation.navigate('CategoriesScreen')}
                      name="Kecantikan"
                      icon={require('../../images/Icons/ic_beauty.png')}
                    />
                  </Col>
                  <Col />
                  <Col />
                  <Col />
                </Row> */}
            </Card>
          )}
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: store.select.auth,
    userData: state.auth.userData,
    ads: state.smartadv.homeAds,
    focusedAdId: state.smartadv.focusedAdId,
    username: store.select.auth.getUsername(state),
    loading: state.loading.effects.balance.getBalance,
    loadingCategory: state.loading.effects.product.getProductCategory,
    balance: store.select.balance.getFormattedBalance(state.balance),
    productCategories: state.product.product_category_list,
  };
};

const mapDispatchToProps = ({balance, product, support, auth, smartadv}) => {
  return {
    getBalance: () => balance.getBalance(),
    getMemberData: () => auth.getMemberData(),
    getProductCategory: () => product.getProductCategory(),
    getShippingAddresses: () => support.getShippingAddresses(),
    getMessageList: data => auth.getMessageList(data),
    emptyAd: smartadv.emptyAd,
    getHomeAds: smartadv.getHomeAds,
    getAllAds: offset => smartadv.getAllAds(offset),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
