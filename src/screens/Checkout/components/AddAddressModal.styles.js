import { StyleSheet } from 'react-native'
import {
  Metrics, Colors, Fonts, ApplicationStyles,
} from '../../../themes'

export default StyleSheet.create({
  modal: { margin: 0, justifyContent: 'center' },
  container: {
    backgroundColor: 'white',
    paddingBottom: Metrics.doubleBaseMargin,
    paddingTop: Metrics.baseMargin,
  },
  subContainer: {
    paddingHorizontal: Metrics.baseMargin,
  },
  dropdown: {
    ...ApplicationStyles.flex,
    marginHorizontal: 6,
    marginTop: 0,
  },
  icon: {
    fontSize: Metrics.icons.small,
    color: Colors.gray600,
  },
  pickerPadder: {
    marginHorizontal: Metrics.baseMargin,
  },
  input: {
    fontSize: Fonts.size.medium,
    color: Colors.gray800,
  },
  buttonContainer: {
    marginTop: Metrics.doubleBaseMargin,
  },
  button: {
    backgroundColor: Colors.bloodOrange,
  },
})
