import React, {useEffect, useCallback} from 'react';
import {connect} from 'react-redux';
import {Container, ListItem, Text, Right, Icon, Body} from 'native-base';
import styles from './styles';
import HeaderBack from '../../components/header/HeaderBack';
import VerticalList from '../../components/list/VerticalList';
import {formattedDate} from '../../utils/TextUtils';
import Loading from '../../components/indicator/Loading';
import {goTo, goBack} from '../../utils/NavigationUtils';

const AddressList = ({
  getShippingAddresses,
  setSelectedAddress,
  shippingAddresses,
  selectedAddress,
  loading,
  navigation,
}) => {
  const isEdit = navigation.getParam('isEdit', false);
  const handleAddressPressed = useCallback(
    (item = null) => {
      if (isEdit) {
        goTo('EditAddressScreen', {detail: item});
      } else {
        setSelectedAddress(item);
        goBack();
      }
    },
    [isEdit],
  );
  useEffect(() => {
    getShippingAddresses();
  }, []);
  return (
    <Container style={styles.mainContainer}>
      <HeaderBack
        title="Daftar Alamat"
        customRight="add"
        onRightPress={() => goTo('NewAddressScreen')}
      />
      {loading ? (
        <Loading isTopIndicator />
      ) : (
        <VerticalList
          contentContainerStyle={{padding: 0}}
          data={shippingAddresses}
          renderItem={({item}) => (
            <ListItem
              onPress={() => handleAddressPressed(item)}
              style={styles.list}>
              <Body>
                <Text style={styles.selected}>{item.address_type}</Text>
                <Text note style={styles.dateSelected}>
                  {formattedDate(item.datecreated)}
                </Text>
              </Body>
              <Right>
                {selectedAddress?.id === item.id && (
                  <Icon style={styles.iconSelected} name="checkmark" />
                )}
              </Right>
            </ListItem>
          )}
        />
      )}
    </Container>
  );
};

const mapStateToProps = ({
  loading,
  support: {shipping_addresses},
  auth: {selectedAddress},
}) => {
  return {
    loading: loading.effects.support.getShippingAddresses,
    shippingAddresses: shipping_addresses,
    selectedAddress,
  };
};

const mapDispatchToProps = ({
  support: {getShippingAddresses},
  auth: {setSelectedAddress},
}) => {
  return {
    getShippingAddresses,
    setSelectedAddress,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddressList);
