/* eslint-disable react/prefer-stateless-function */
import React, {useEffect} from 'react';
import {Linking} from 'react-native';
import {connect} from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import {Container, Text, ListItem, Left, Body, Right} from 'native-base';
import styles from './styles';
import HeaderBack from '../../components/header/HeaderBack';
import Loading from '../../components/indicator/Loading';
import VerticalList from '../../components/list/VerticalList';
import FastImage from 'react-native-fast-image';
import {normalizedSource} from '../../utils/ImageUtils';

const PartnershipScreen = ({loading, partnerships, getPartnerships}) => {
  useEffect(() => {
    getPartnerships();
  }, []);
  return (
    <Container style={styles.mainContainer}>
      <HeaderBack withoutRight title="Partnership" />
      {loading ? (
        <Loading isTopIndicator />
      ) : (
        <VerticalList
          style={styles.container}
          data={partnerships}
          renderItem={({item}) => {
            const res = item.url?.split('id=');
            return (
              <ListItem
                onPress={() => Linking.openURL(`market://details?id=${res[1]}`)}
                icon>
                <Left>
                  <FastImage
                    source={
                      item.partner_logo_thumbnail_url &&
                      normalizedSource({uri: item.partner_logo_thumbnail_url})
                    }
                    style={{width: 40, height: 40}}
                  />
                </Left>
                <Body>
                  <Text>{item.partner_name}</Text>
                </Body>
                <Right />
              </ListItem>
            );
          }}
        />
      )}
    </Container>
  );
};

// eslint-disable-next-line no-unused-vars
const mapStateToProps = ({loading, support: {partnerships}}) => {
  return {
    loading: loading.effects.support.getPartnerships,
    partnerships,
  };
};

// eslint-disable-next-line no-unused-vars
const mapDispatchToProps = ({support: {getPartnerships}}) => {
  return {
    getPartnerships,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PartnershipScreen);
