import {
  TouchableWithoutFeedback,
  TouchableOpacity,
  Platform,
  View,
  StyleSheetProperties,
} from 'react-native';
import * as React from 'react';

type Props = {
  children: React.Node,
  onPress: Function,
  style: StyleSheetProperties,
};

const FlatTouchables = (props: Props) => {
  const {children, onPress, style} = props;
  if (Platform.OS === 'android') {
    return (
      <TouchableWithoutFeedback onPress={onPress}>
        <View style={style} {...props}>
          {children}
        </View>
      </TouchableWithoutFeedback>
    );
  }
  return (
    <TouchableOpacity style={style} onPress={onPress} {...props}>
      {children}
    </TouchableOpacity>
  );
};

export default FlatTouchables;
