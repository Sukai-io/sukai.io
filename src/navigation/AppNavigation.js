/* eslint-disable react/display-name */
/* eslint-disable react/prop-types */
import React, {useEffect} from 'react';
import {messaging, notifications} from 'react-native-firebase';
import type {Notification, NotificationOpen} from 'react-native-firebase';
import {Dimensions, Platform} from 'react-native';
import {
  createStackNavigator,
  createSwitchNavigator,
  createAppContainer,
  createDrawerNavigator,
  createBottomTabNavigator,
} from 'react-navigation';
import AddressListScreen from '../screens/AddressList';
import CartScreen from '../screens/Cart';
import ChangeLanguageScreen from '../screens/ChangeLanguage';
import ChangePasswordScreen from '../screens/ChangePassword';
import CheckoutScreen from '../screens/Checkout';
import CategoriesScreen from '../screens/Categories';
import DigitalCompetitionScreen from '../screens/DigitalCompetition';
import DigitalCompetitionDetailScreen from '../screens/DigitalCompetitionDetail';
import EditAddressScreen from '../screens/EditAddress';
import EditProductScreen from '../screens/EditProduct';
import EditProfileScreen from '../screens/EditProfile';
import ElectricityScreen from '../screens/Electricity';
import EMoneyScreen from '../screens/EMoney';
import HomeScreen from '../screens/Home';
import HistoryBonusScreen from '../screens/HistoryBonus';
import HistoryOrderScreen from '../screens/HistoryOrder';
import HistoryTopupScreen from '../screens/HistoryTopup';
import HistoryWalletScreen from '../screens/HistoryWallet';
import InboxScreen from '../screens/Inbox';
import InboxDetailScreen from '../screens/InboxDetail';
import IntroSurveyScreen from "../screens/IntroSurvey"
import LeasingScreen from '../screens/Leasing';
import LeasingCheckoutScreen from '../screens/LeasingCheckout';
import MarketplaceScreen from '../screens/Marketplace';
import MyCommissionScreen from "../screens/MyCommission"
import MyProductScreen from '../screens/MyProduct';
import NewAddressScreen from '../screens/NewAddress';
import NewDigitalCompetitionScreen from '../screens/NewDigitalCompetition';
import NewProductScreen from '../screens/NewProduct';
import NewsScreen from '../screens/News';
import OfficialStoreScreen from '../screens/OfficialStore';
import BuyerDetailScreen from '../screens/BuyerDetail';
import SellerDetailScreen from '../screens/SellerDetail';
import PartnershipScreen from '../screens/Partnership';
import ProductDetailScreen from '../screens/ProductDetail';
import ProfileScreen from '../screens/Profile';
import PulsaScreen from '../screens/Pulsa';
import PpobListScreen from '../screens/PpobList';
import SidebarScreen from '../screens/Sidebar';
import TransferScreen from '../screens/Transfer';
import TopUpScreen from '../screens/TopUp';
import TopUpConfirmScreen from '../screens/TopUpConfirm';
import WalletScreen from '../screens/Wallet';
import WithdrawScreen from '../screens/Withdraw';
import MyAdsScreen from '../screens/SmartAdv/MyAdsScreen';
import AllAdsScreen from '../screens/SmartAdv/AllAdsScreen';
import AdForm from '../screens/SmartAdv/AdForm';
import AdDetail from '../screens/SmartAdv/AdDetail';
import AdBalanceForm from '../screens/SmartAdv/AdBalanceForm';
import AdInsight from '../screens/SmartAdv/AdInsight';

import ForgetPasswordScreen from '../screens/ForgetPassword';
import LoginScreen from '../screens/Login';
import IntroScreen from '../screens/Intro';
import NewMemberScreen from '../screens/NewMember';
import SplashScreen from '../screens/Splash';
import {Colors, Fonts} from '../themes';
import RouteServices from './RouteServices';
import {useDispatch} from 'react-redux';
import BadgeIcon from '../components/icon/BadgeIcon';
import Scan from '../screens/Scan';
const {width} = Dimensions.get('window');
const platform = Platform.OS;

const MemberNavigator = createStackNavigator(
  {
    NewMemberScreen,
  },
  {
    initialRouteName: 'NewMemberScreen',
    headerMode: 'none',
  },
);

const ProfileNavigator = createStackNavigator(
  {
    ChangePasswordScreen,
    EditProfileScreen,
    ProfileScreen,
  },
  {
    initialRouteName: 'ProfileScreen',
    headerMode: 'none',
  },
);

const WalletNavigator = createStackNavigator(
  {
    WalletScreen,
  },
  {
    initialRouteName: 'WalletScreen',
    headerMode: 'none',
  },
);

const iconForTab = routeName => {
  switch (routeName) {
    case 'Home':
      return 'ios-home';
    case 'Inbox':
      return 'ios-search';
    case 'News':
      return 'ios-menu';
    default:
      return null;
  }
};

const HomeTab = createBottomTabNavigator(
  {
    Home: {screen: HomeScreen},
    Inbox: {screen: InboxScreen},
    News: {screen: NewsScreen},
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: props => {
        const {tintColor} = props;
        const {routeName} = navigation.state;
        return <BadgeIcon iconName={iconForTab(routeName)} size={24} color={tintColor} type="ionicon" />;
      },
    }),
    tabBarOptions: {
      showLabel: false,
      activeTintColor: Colors.primary,
      inactiveTintColor: Colors.gray400,
      allowFontScaling: true,
      labelStyle: {
        marginTop: 0,
        fontSize: Fonts.size.small,
      },
      style: {
        height: platform === 'ios' ? 56 : 48,
      },
    },
  },
);

const HomeDrawer = createDrawerNavigator(
  {
    HomeTab,
    ProfileNavigator,
    WalletNavigator,
    HistoryBonusScreen,
    HistoryOrderScreen,
    HistoryTopupScreen,
    HistoryWalletScreen,
    MemberNavigator,
    MyProductScreen,
    NewProductScreen,
  },
  {
    drawerWidth: width - width / 4,
    initialRouteName: 'HomeTab',
    contentOptions: {
      activeTintColor: Colors.primary,
    },
    overlayColor: '#000000B3',
    drawerBackgroundColor: 'transparent',
    contentComponent: props => <SidebarScreen {...props} />,
  },
);

const Main = createStackNavigator(
  {
    HomeDrawer,
    AddressListScreen,
    CartScreen,
    CheckoutScreen,
    CategoriesScreen,
    DigitalCompetitionScreen,
    DigitalCompetitionDetailScreen,
    EditAddressScreen,
    EditProductScreen,
    ElectricityScreen,
    EMoneyScreen,
    HistoryTopupScreen,
    InboxDetailScreen,
    IntroSurveyScreen,
    LeasingScreen,
    LeasingCheckoutScreen,
    MarketplaceScreen,
    MyCommissionScreen,
    NewAddressScreen,
    NewDigitalCompetitionScreen,
    PartnershipScreen,
    ChangeLanguageScreen,
    BuyerDetailScreen,
    SellerDetailScreen,
    OfficialStoreScreen,
    ProductDetailScreen,
    PpobListScreen,
    PulsaScreen,
    TransferScreen,
    TopUpScreen,
    TopUpConfirmScreen,
    WithdrawScreen,
    MyAdsScreen,
    AdForm,
    AdDetail,
    AdBalanceForm,
    AdInsight,
    AllAdsScreen,
    Scan
  },
  {
    initialRouteName: 'HomeDrawer',
    headerMode: 'none',
  },
);

const Auth = createStackNavigator(
  {
    LoginScreen,
    ForgetPasswordScreen,
    NewMemberScreen,
  },
  {
    // Default config for all screens
    headerMode: 'none',
    initialRouteName: 'LoginScreen',
  },
);

const PrimaryNav = createSwitchNavigator(
  {
    Auth,
    Main,
    IntroScreen,
    SplashScreen,
  },
  {
    // Default config for all screens
    headerMode: 'none',
    initialRouteName: 'SplashScreen',
  },
);

const AppContainer = createAppContainer(PrimaryNav);

const AppNavigation = () => {
  const {auth} = useDispatch();
  const {setFcmToken} = auth;
  useEffect(() => {
    messaging()
      .getToken()
      .then(token => {
        if (token) {
          setFcmToken(token);
        }
      });
    notifications()
      .getInitialNotification()
      .then((notificationOpen: NotificationOpen) => {
        if (notificationOpen) {
          const {notification} = notificationOpen;
          console.log(notification);
          // setNotifPayload(notification?._data);
        }
      });
    return messaging().onTokenRefresh(token => {
      if (token) {
        setFcmToken(token);
      }
    });
  }, []);
  useEffect(() => {
    const unsubscribe = notifications().onNotificationOpened((notificationOpen: NotificationOpen) => {
      const {notification} = notificationOpen;
      console.log(notification);
    });
    return unsubscribe;
  }, []);
  useEffect(() => {
    const unsubscribe = notifications().onNotificationDisplayed((notification: Notification) => {
      console.log(notification);
    });
    return unsubscribe;
  }, []);
  useEffect(() => {
    const unsubscribe = notifications().onNotification((notification: Notification) => {
      const notificationBuilder = new notifications.Notification()
        .setNotificationId(notification._notificationId)
        .setTitle(notification._title)
        .setBody(notification._body)
        .setSound('default')
        .android.setPriority(notifications.Android.Priority.High)
        .setData(notification._data);

      notifications().displayNotification(notificationBuilder);
    });
    return unsubscribe;
  }, []);

  return (
    <AppContainer
      ref={routeRef => {
        RouteServices.setTopLevelNavigator(routeRef);
      }}
    />
  );
};

export default AppNavigation;
