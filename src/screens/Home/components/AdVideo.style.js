import { StyleSheet } from "react-native";
import { Metrics, Colors, Fonts } from '../../../themes';

export default StyleSheet.create({
  container: {
    borderTopColor: Colors.blue200,
    borderTopWidth: 1,
    height: 250,
    elevation: 5,
  },
  blackTransparentView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: Metrics.doubleBaseMargin,
    paddingVertical: Metrics.baseMargin,
    backgroundColor: Colors.blackTransparent,
  },
  blueTransparentView: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Colors.blueTransparent,
    paddingVertical: Metrics.smallMargin,
  },
  divider: {
    height: 20,
    borderWidth: 0.5,
  },
  textBold: {
    fontFamily: Fonts.type.bold,
    fontSize: Fonts.size.small,
  },
  textBrand: {
    color: Colors.snow,
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.bold,
  },
  textCompany: {
    fontFamily: Fonts.type.base,
    color: Colors.snow,
    fontSize: Fonts.size.small,
  },
  textDuration: {
    color: Colors.snow,
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.bold,
  },
  imageVideo: {
    height: '100%',
    width: '100%',
  },
  iconPlay: {
    fontSize: 50,
    color: 'rgba(52, 52, 52, 0.7)'
  }
});