package io.sukai.lockscreen;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;
import io.sukai.MainActivity;
import io.sukai.MainApplication;
import io.sukai.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class LockscreenActivity extends AppCompatActivity {


    private TextView tvTitle;
    private ImageView posterView;
    private TextView tvClock;
    private TextView tvDate;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private int adId = 0;
    private boolean loading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.activity_lockscreen);
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        editor = pref.edit();
        UnlockBar unlock = (UnlockBar) findViewById(R.id.unlock);
        unlock.setOnUnlockListenerRight(new UnlockBar.OnUnlockListener() {
            @Override
            public void onUnlock()
            {
                editor.putInt(getString(R.string.ad_id), adId);
                editor.apply();
                Intent mainAactivity = new Intent(getApplicationContext(), MainActivity.class);
                mainAactivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(mainAactivity);
                finish();
            }
        });
        unlock.setOnUnlockListenerLeft(new UnlockBar.OnUnlockListener() {
            @Override
            public void onUnlock()
            {
                finish();
            }
        });
        Window window = this.getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                window.setStatusBarColor(getColor(R.color.light_blue));
            }
        }
        tvTitle = findViewById(R.id.tvTitle);
        posterView = findViewById(R.id.posterView);
        tvDate = findViewById(R.id.tvDate);
        tvClock = findViewById(R.id.tvClock);
        runDatetime();
    }

    private void runDatetime() {
        Timer t = new Timer();
        t.scheduleAtFixedRate(new TimerTask()
        {
            @Override
            public void run() {
                Calendar c = Calendar.getInstance();
                final String finalTime = String.format(Locale.US, "0%d:0%d", c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE));
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String pattern = "\\d(\\d\\d)";
                        tvClock.setText(finalTime.replaceAll(pattern, "$1"));
                        DateFormat format = DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.SHORT, Locale.getDefault());
                        Calendar cal = Calendar.getInstance();
                        SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMMM d, yyyy");
                        tvDate.setText(sdf.format(cal.getTime()));
                    }
                });
            }
        }, 1000, 1000);
    }

    @Override
    public void onAttachedToWindow() {

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON|
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD|
//                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON|
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        super.onAttachedToWindow();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!loading){
            ((MainApplication) getApplication()).lockScreenShow = true;
            try {
                getAd();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void getAd() throws IOException {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String endpoint = pref.getString(getString(R.string.endpoint), "");
        String authToken = pref.getString(getString(R.string.authToken), "");
        String url = endpoint + "/advertlist/get?type=single";
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url(url)
                .method("GET", null)
                .addHeader("X-Auth-Token", authToken.toString())
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                loading = false;
                try {
                    getAd();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    JSONObject jsonResult = new JSONObject(response.body().string());
                    JSONObject jsonData = jsonResult.getJSONObject("data").getJSONArray("advert_list").getJSONObject(0);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                LockscreenActivity.this.adId = jsonData.getInt("id");
                                tvTitle.setText(jsonData.getString("title"));
                                Picasso.with(getApplicationContext()).load(jsonData.getString("image_thumbnail")).resize(400, 400).centerCrop()
                                        .placeholder(R.drawable.iklan)
                                        .error(R.drawable.iklan)
                                        .into(posterView);
                                loading = false;
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        ((MainApplication) getApplication()).lockScreenShow = false;
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return false;
    }

}