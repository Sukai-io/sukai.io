/* eslint-disable react/prefer-stateless-function */
import '../config/TextDefaultConfig';
import React, {Component} from 'react';
import {Root, StyleProvider, Container} from 'native-base';
import {getPersistor} from '@rematch/persist';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {StatusBar, View} from 'react-native';
import getTheme from '../../native-base-theme/components';
import material from '../../native-base-theme/variables/material';
import {ApplicationStyles, Colors} from '../themes';
import store from '../config/CreateStore';
import AppNavigation from '../navigation/AppNavigation';
import { LocalizationProvider } from '../localization';

const persistor = getPersistor();

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Root>
          <PersistGate loading={<Container />} persistor={persistor}>
          <LocalizationProvider>
            <StyleProvider style={getTheme(material)}>
              <View style={ApplicationStyles.flex}>
                <StatusBar backgroundColor={Colors.darkPrimary} barStyle="light-content" />
                <AppNavigation />
              </View>
            </StyleProvider>
            </LocalizationProvider>
          </PersistGate>
        </Root>
      </Provider>
    );
  }
}

// allow reactotron overlay for fast design in dev mode
export default App;
