/* eslint-disable no-unused-vars */
import React, {Component} from 'react';
import {connect} from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import {Container} from 'native-base';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import styles from './styles';
import HeaderBack from '../../components/header/HeaderBack';
import CartItem from './components/CartItem';
import Loading from '../../components/indicator/Loading';
import VerticalList from '../../components/list/VerticalList';
import {Alert} from 'react-native';

class MyProductScreen extends Component {
  state = {
    isDataExist: true,
    // tableHead: [
    //   'No',
    //   'Foto Produk',
    //   'Nama Produk',
    //   'Kategori',
    //   'Deskripsi',
    //   'Harga',
    //   'Berat',
    //   'Stok',
    //   'Status',
    //   'Tanggal Dibuat',
    // ],
    // widthArr: [40, 120, 120, 100, 150, 120, 80, 80, 80, 120],
  };

  componentDidMount() {
    const {getProductMine} = this.props;
    getProductMine();
  }

  handleDelete = productId => {
    Alert.alert(
      'Konfirmasi',
      'Apakah anda yakin ingin menghapus produk ini ?',
      [
        {
          text: 'Batal',
          onPress: () => {},
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => {
            const {postDeleteProduct} = this.props;
            postDeleteProduct({product_id: productId});
          },
        },
      ],
    );
  };

  render() {
    const {navigation, loading, productMine} = this.props;
    const {
      isDataExist,
      // tableHead, widthArr
    } = this.state;
    // const tableData = [];
    // for (let i = 0; i < 30; i += 1) {
    //   const rowData = [];
    //   for (let j = 0; j < 10; j += 1) {
    //     rowData.push(`${i}${j}`);
    //   }
    //   tableData.push(rowData);
    // }
    return (
      <Container style={styles.mainContainer}>
        <HeaderBack title="Produk Saya" withoutRight />
        {/* {isDataExist ? (
          <View style={styles.tableContainer}>
            <ScrollView horizontal>
              <View style={ApplicationStyles.flex}>
                <Table borderStyle={{ borderColor: '#C1C0B9' }}>
                  <Row
                    data={tableHead}
                    widthArr={widthArr}
                    style={styles.header}
                    textStyle={styles.headerText}
                  />
                </Table>
                <ScrollView style={styles.dataWrapper}>
                  <Table borderStyle={{ borderColor: '#C1C0B9' }}>
                    {tableData.map((rowData, index) => (
                      <Row
                        key={index.toString()}
                        data={rowData}
                        widthArr={widthArr}
                        style={[styles.row, index % 2 && { backgroundColor: '#F7F6E7' }]}
                        textStyle={styles.text}
                      />
                    ))}
                  </Table>
                </ScrollView>
              </View>
            </ScrollView>
          </View>
        ) : (
          <CardItem>
            <Body>
              <Text style={styles.empty}>Tidak ada data yang ditemukan</Text>
            </Body>
          </CardItem>
        )} */}
        {loading ? (
          <Loading />
        ) : (
          <VerticalList
            data={productMine}
            windowSize={13}
            maxToRenderPerBatch={6}
            updateCellsBatchingPeriod={30}
            style={styles.container}
            renderItem={({item}) => (
              <CartItem
                onPress={() =>
                  navigation.navigate('EditProductScreen', {id: item?.id})
                }
                onDelete={() => this.handleDelete(item.id)}
                dataSource={item}
              />
            )}
            onEndReached={this.handleLoadMore}
          />
        )}
      </Container>
    );
  }
}

const mapStateToProps = ({loading, product}) => {
  return {
    loading:
      loading.effects.product.getProductMine ||
      loading.effects.product.postDeleteProduct,
    productMine: product.product_mine_list,
  };
};

const mapDispatchToProps = ({product}) => {
  return {
    getProductMine: () => product.getProductMine(),
    postDeleteProduct: data => product.postDeleteProduct(data),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MyProductScreen);
