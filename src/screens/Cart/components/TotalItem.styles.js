import { StyleSheet } from 'react-native'
import {
  ApplicationStyles, Metrics, Fonts, Colors,
} from '../../../themes'

export default StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: Metrics.doubleBaseMargin,
    borderTopColor: Colors.gray300,
    borderTopWidth: 1,
  },
  subContainer: {
    ...ApplicationStyles.flex,
  },
  desc: {
    fontSize: Fonts.size.medium,
    color: Colors.gray600,
  },
  price: {
    fontSize: Fonts.size.h6,
    fontFamily: Fonts.type.bold,
    color: Colors.bloodOrange,
    marginTop: 2,
  },
  button: {
    backgroundColor: Colors.bloodOrange,
    marginLeft: Metrics.baseMargin,
  },
  checkout: {
    fontSize: Fonts.size.regular,
  },
})
