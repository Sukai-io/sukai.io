import React, {Component} from 'react';
import {connect} from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

import {Formik} from 'formik';
import Content from './Content';
import {FormValues} from './types';
import validation from './validation';
import Loader from '../../components/indicator/Loader';

class EditProfileScreen extends Component {
  async componentDidMount() {
    const {getProvinces, getCities, getBanks, userData} = this.props;
    getBanks();
    await getProvinces();
    await getCities({province_id: userData.province});
  }

  handleSubmit = (values: FormValues) => {
    const {doUpdateProfile} = this.props;
    doUpdateProfile(values);
  };

  render() {
    const {userData, loading} = this.props;
    return (
      <>
        <Loader loading={loading} />
        <Formik
          initialValues={{
            member_name: userData.name,
            member_email: userData.email,
            member_address: userData.address,
            member_province: userData.province,
            member_city: userData.city,
            member_district: userData.district,
            member_gender: userData.gender,
            member_phone: userData.phone,
            member_idcard: userData.idcard,
            member_bank: userData.bank_id,
            member_bill: userData.bill,
            member_bill_name: userData.bill_name,
            member_branch: userData.branch,
          }}
          onSubmit={(values: FormValues) => this.handleSubmit(values)}
          validationSchema={validation}
          render={props => <Content {...props} {...this.props} />}
        />
      </>
    );
  }
}

const mapStateToProps = ({auth, loading}) => {
  return {
    userData: auth.userData,
    loading: loading.effects.auth.doUpdateProfile,
  };
};

const mapDispatchToProps = ({support, auth}) => {
  return {
    getProvinces: data => support.getProvinces(data),
    getCities: data => support.getCities(data),
    getBanks: () => support.getBanks(),
    doUpdateProfile: data => auth.doUpdateProfile(data),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditProfileScreen);
