import React from 'react';
import { ScrollView, Text, KeyboardAvoidingView, Image } from 'react-native';
import { connect } from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
import { Button, Container } from 'native-base';
import { Formik, FormikProps } from 'formik';

// Styles
import styles from './styles';
import AuthInput from '../../components/input/AuthInput';
import AuthButton from '../../components/buttons/AuthButton';
import validation from './validation';
import { Images } from '../../themes';
import { LocalizationContext } from '../../localization';

type Props = {
  loading: Boolean,
  navigation: any,
  doLogin: () => Promise,
  doLogout: () => Promise,
};

type State = {
  hidePassword: Boolean,
};

type FormValues = {
  username: string,
  password: string,
};

class LoginScreen extends React.Component<Props, State> {
  static contextType = LocalizationContext;

  state = {
    hidePassword: true,
  };

  handleEyePress = () => {
    this.setState(prevState => ({ hidePassword: !prevState.hidePassword }));
  };

  handleSubmit = (values: FormValues) => {
    const { doLogin } = this.props;
    const { password, username } = values;
    doLogin({ username, password })
      .then()
      .catch(() => { });
  };

  renderForm = ({ handleSubmit, setFieldValue, setFieldTouched, touched, errors }: FormikProps<FormValues>) => {
    const { hidePassword } = this.state;
    const { navigation, loading } = this.props;
    const { translations } = this.context;
    return (
      <Container>
        <ScrollView keyboardShouldPersistTaps="always" style={styles.authContainer}>
          <KeyboardAvoidingView behavior="position">
            <Image style={styles.image} resizeMode="cover" source={Images.logo} />
            <Text style={styles.sectionContainer}>
              <Text style={styles.sectionText}>{translations.LABEL_LOGIN_DESC} </Text>
              <Text style={styles.sectionText}>{translations.LABEL_LOGIN_DESC_2}</Text>
            </Text>
            <AuthInput
              name="account"
              placeholder={translations.LABEL_USERNAME_PLACEHOLDER}
              autoCapitalize="none"
              onBlur={() => setFieldTouched('username')}
              onChangeText={value => setFieldValue('username', value)}
              error={touched.username && errors.username ? errors.username : null}
            />
            <AuthInput
              hasEye
              name="lock"
              placeholder={translations.LABEL_PASSWORD_PLACEHOLDER}
              secureTextEntry={hidePassword}
              onBlur={() => setFieldTouched('password')}
              onChangeText={value => setFieldValue('password', value)}
              onEyePress={this.handleEyePress}
              error={touched.password && errors.password ? errors.password : null}
            />
            <AuthButton loading={loading} disabled={loading} onPress={handleSubmit} title="Login" />
            <Button onPress={() => navigation.navigate('ForgetPasswordScreen')} transparent full style={styles.forget}>
              <Text style={styles.forgetText}>{translations.ACTION_FORGET_PASSWORD}?</Text>
            </Button>
          </KeyboardAvoidingView>
        </ScrollView>
        <Button onPress={() => navigation.navigate('NewMemberScreen')} full style={styles.signup}>
          <Text style={styles.text}>{translations.ACTION_SIGNUP}</Text>
        </Button>
      </Container>
    );
  };

  render() {
    return (
      <Formik
        initialValues={{ username: '', password: '' }}
        enableReinitialize
        onSubmit={(values: FormValues) => this.handleSubmit(values)}
        validationSchema={validation}
        render={(actions: FormikProps<FormValues>) => this.renderForm(actions)}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.loading.effects.auth.doLogin,
  };
};

const mapDispatchToProps = ({ auth }) => {
  return {
    doLogin: data => auth.doLogin(data),
    doLogout: () => auth.doLogout(),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
