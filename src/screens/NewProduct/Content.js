/* eslint-disable no-unused-vars */
import React, {Component, useState, useCallback} from 'react';
import {Formik, FormikProps} from 'formik';
import {View} from 'react-native';
import FastImage from 'react-native-fast-image';
import {connect, useSelector, shallowEqual} from 'react-redux';
import ImagePicker from 'react-native-image-picker';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import {Container, Text, Body, CardItem, Item, Input, Label} from 'native-base';
import {Dropdown} from 'react-native-material-dropdown';
import {Button, Card} from 'react-native-elements';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Props} from './Props';
import styles from './styles';
import HeaderBack from '../../components/header/HeaderBack';
import {ApplicationStyles, Colors} from '../../themes';
import Horizontal from '../../components/wrapper/Horizontal';
import AddAddressModal from '../Checkout/components/AddAddressModal';
import {imageResizer, normalizedSource} from '../../utils/ImageUtils';
import InputText from '../NewMember/components/InputText';
import SelectPicker from './components/SelectPicker';

const Content = ({
  values,
  touched,
  errors,
  setFieldValue,
  handleChange,
  handleBlur,
  handleSubmit,
  handleReset,
}: Props) => {
  const productCategories = useSelector(({product: {product_category_list}}) => product_category_list, shallowEqual);
  const loading = useSelector(({loading}) => loading.effects.product.postNewProduct, shallowEqual);
  const [isAddressComplete, setAddressCompletion] = useState(true);
  const handleImagePicker = useCallback(() => {
    ImagePicker.showImagePicker(null, async response => {
      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        console.log(response.fileSize);
        const uri = await imageResizer(response.uri);
        const imgSource = {
          data: response.data,
          name: response.fileName,
          type: 'image/jpeg',
          uri,
          path: response.path,
          width: response.width,
          height: response.height,
          fileSize: response.fileSize,
          isVertical: response.isVertical,
          originalRotation: response.originalRotation,
        };
        setFieldValue('product_image', imgSource);
      }
    });
  }, []);
  const handleCategorySelected = useCallback(values => {
    setFieldValue('product_category', values);
  }, []);
  return (
    <Container style={styles.mainContainer}>
      <HeaderBack title="Tambah" withoutRight />
      <KeyboardAwareScrollView style={styles.container} keyboardShouldPersistTaps="always">
        <Card containerStyle={styles.cardContainer}>
          <CardItem>
            <Body>
              <Text style={styles.title}>Informasi Produk</Text>
            </Body>
          </CardItem>
          <InputText
            label="Nama Produk *"
            value={values.product_name}
            error={touched.product_name && errors.product_name}
            onBlur={handleBlur('product_name')}
            onChangeText={handleChange('product_name')}
          />
          <InputText
            label="Detail Produk *"
            value={values.product_detail}
            error={touched.product_detail && errors.product_detail}
            onBlur={handleBlur('product_detail')}
            onChangeText={handleChange('product_detail')}
          />
          <InputText
            label="Berat (gram) *"
            value={values.product_weight}
            error={touched.product_weight && errors.product_weight}
            keyboardType="number-pad"
            onBlur={handleBlur('product_weight')}
            onChangeText={handleChange('product_weight')}
          />
          <InputText
            label="Harga Produk *"
            value={values.product_price}
            error={touched.product_price && errors.product_price}
            keyboardType="number-pad"
            onBlur={handleBlur('product_price')}
            onChangeText={handleChange('product_price')}
          />
          <InputText
            label="Komisi Produk *"
            value={values.product_commission}
            error={touched.product_commission && errors.product_commission}
            keyboardType="number-pad"
            onBlur={handleBlur('product_commission')}
            onChangeText={handleChange('product_commission')}
          />
          <CardItem>
            <Body>
              <Label style={styles.label}>Image *</Label>
              {values.product_image?.uri && (
                <FastImage resizeMode="cover" source={{uri: values.product_image?.uri}} style={styles.image} />
              )}
              <Button
                onPress={handleImagePicker}
                raised
                containerStyle={styles.buttonImageContainer}
                buttonStyle={styles.buttonImage}
                titleStyle={styles.buttonTitle}
                icon={{
                  name: 'ios-image',
                  type: 'ionicon',
                  color: 'white',
                  size: 20,
                }}
                title="CHOOSE IMAGE"
              />
            </Body>
          </CardItem>
          {errors.product_image && <Text style={styles.error}>{errors.product_image}</Text>}
        </Card>
        <Card containerStyle={styles.cardContainer}>
          <CardItem>
            <Body>
              <Text style={styles.title}>Informasi Kategori</Text>
            </Body>
          </CardItem>
          <SelectPicker
            selectText="Kategori Produk"
            items={productCategories.map(item => ({
              id: item.id,
              name: item.category_name,
            }))}
            selectedItems={values.product_category}
            onSelectedItemsChange={handleCategorySelected}
            error={touched.product_category && errors.product_category}
          />
        </Card>
        <Card containerStyle={styles.cardContainer}>
          <Horizontal style={styles.buttonWrapper}>
            <Button
              onPress={handleReset}
              raised
              containerStyle={styles.buttonContainer}
              buttonStyle={styles.buttonReset}
              titleStyle={styles.buttonContainerTextStyle}
              icon={{name: 'ios-refresh', type: 'ionicon', color: 'white'}}
              title="RESET"
            />
            <View style={styles.separator} />
            <Button
              loading={loading}
              onPress={handleSubmit}
              raised
              containerStyle={styles.buttonContainer}
              buttonStyle={styles.buttonConfirm}
              titleStyle={styles.buttonContainerTextStyle}
              title="TAMBAH PRODUK"
            />
          </Horizontal>
        </Card>
      </KeyboardAwareScrollView>
    </Container>
  );
};

export default Content;
