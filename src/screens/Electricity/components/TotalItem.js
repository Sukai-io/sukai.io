import React from 'react'
import { Text } from 'native-base'
import Horizontal from '../../../components/wrapper/Horizontal'
import styles from './TotalItem.styles'

type Props = {
  total: String,
}

const TotalItem = ({ total }: Props) => (
  <Horizontal style={styles.grandContainer}>
    <Text style={styles.totalLeft}>Total Pembayaran</Text>
    <Text style={styles.grantTotal}>{total}</Text>
  </Horizontal>
)

export default TotalItem
