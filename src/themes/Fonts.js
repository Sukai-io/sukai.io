import {Platform} from 'react-native';
import Colors from './Colors';

const type = {
  base: Platform.OS === 'ios' ? 'Quicksand-Regular' : 'Quicksand-Regular',
  bold: Platform.OS === 'ios' ? 'Quicksand-Bold' : 'Quicksand-Bold',
  light: Platform.OS === 'ios' ? 'Quicksand-Light' : 'Quicksand-Light',
};

const size = {
  h1: 38,
  h2: 34,
  h3: 30,
  h4: 26,
  h5: 20,
  h6: 19,
  input: 18,
  regular: 17,
  medium: 14,
  small: 12,
  mini: 10,
  tiny: 8.5,
};

const style = {
  h1: {
    fontFamily: type.base,
    fontSize: size.h1,
  },
  h2: {
    fontWeight: 'bold',
    fontSize: size.h2,
  },
  h3: {
    fontFamily: type.emphasis,
    fontSize: size.h3,
  },
  h4: {
    fontFamily: type.base,
    fontSize: size.h4,
  },
  h5: {
    fontFamily: type.base,
    fontSize: size.h5,
  },
  h6: {
    fontFamily: type.emphasis,
    fontSize: size.h6,
  },
  normal: {
    fontFamily: type.base,
    fontSize: size.regular,
  },
  description: {
    fontFamily: type.base,
    fontSize: size.medium,
  },
  error: {
    fontFamily: type.base,
    fontSize: size.small,
    color: Colors.error,
  },
};

export default {
  type,
  size,
  style,
};
