export type State = {
  showDate: Boolean,
  showAddress: Boolean,
  date: String,
  isAddressComplete: Boolean,
};
