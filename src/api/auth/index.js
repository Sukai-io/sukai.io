import R from 'ramda';
import api from '../ApiClient';
import errorGenerator from '../ErrorGenerator';
import {showToast} from '../../utils/ToastUtils';
import {goBack, goTo} from '../../utils/NavigationUtils';
import {transformToFormData} from '../ApiHelper';
import AsyncStorage from '@react-native-community/async-storage';

const getSuccess = data => R.pathOr(false, ['success'], data);

export default {
  state: {
    isLoggedIn: false,
    authToken: {},
    fcmToken: {},
    userData: {},
    selectedAddress: false,
    news_list: [],
    inbox_list: [],
  },
  reducers: {
    setToken(state, data) {
      return {...state, authToken: data.data};
    },
    setFcmToken(state, data) {
      try {
        AsyncStorage.setItem('@fcmToken', data);
      } catch (e) {
        console.log(e);
      }
      return {...state, fcmToken: data};
    },
    setUserData(state, data) {
      return {
        ...state,
        isLoggedIn: true,
        userData: data.data,
      };
    },
    setInboxList(state, data) {
      return {
        ...state,
        inbox_list: data?.data?.messageslist || [],
      };
    },
    setNewsList(state, data) {
      return {
        ...state,
        news_list: data?.data?.messageslist || [],
      };
    },
    setSelectedAddress(state, data) {
      return {
        ...state,
        selectedAddress: data,
      };
    },
  },
  selectors: {
    getUsername() {
      return rootState => R.path(['userData', 'username'], rootState.auth);
    },
    isNewsUnread() {
      return rootState => {
        const items = R.pathOr([], ['news_list'], rootState.auth);
        let unread = false;
        let i;
        for (i = 0; i < items.length; i++) {
          if (items[i]?.read_status === '0') {
            unread = true;
            break;
          }
        }
        return unread;
      };
    },
    isInboxUnread() {
      return rootState => {
        const items = R.pathOr([], ['inbox_list'], rootState.auth);
        let unread = false;
        let i;
        for (i = 0; i < items.length; i++) {
          if (items[i]?.read_status === '0') {
            unread = true;
            break;
          }
        }
        return unread;
      };
    },
  },
  effects: dispatch => ({
    async doLogin(data) {
      const response = await api.login(data);
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.auth.setToken(response.data);
        await dispatch.auth.getMemberData(response.data.data);
        await dispatch.auth.postFcmToken();
        dispatch.survey.getSurveyStatus();
        return Promise.resolve(response.data);
      }
      errorGenerator(response, response.data?.success === false ? 'Username atau Password Salah' : null);
      return Promise.reject(response);
    },
    async postFcmToken() {
      const fcmToken = await AsyncStorage.getItem('@fcmToken');
      const response = await api.setFcmToken(transformToFormData({fcm_token: fcmToken}));
      const success = getSuccess(response.data);
      if (response.ok && success) {
        return Promise.resolve(response.data);
      }
      errorGenerator(response);
      return Promise.reject(response);
    },
    async getMemberData(data) {
      if (data) {
        api.getApi().setHeader('X-Auth-Token', data);
      }
      const response = await api.memberData();
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.auth.setUserData(response.data);
        return Promise.resolve(response.data);
      }
      errorGenerator(response);
      return Promise.reject(response);
    },
    async doForgetPassword(data) {
      const response = await api.forgetPassword(data);
      const success = getSuccess(response.data);
      if (response.ok && success) {
        goBack();
        showToast('Periksa email Anda untuk proses berikutnya');
        return Promise.resolve(response.data);
      }
      errorGenerator(response, !success ? 'Username atau Email Salah' : null);
      return Promise.reject(response);
    },
    async doResetPassword() {
      const response = await api.resetPassword();
      const success = getSuccess(response.data);
      if (response.ok && success) {
        goBack();
        showToast('Password Berhasil Diubah');
        return Promise.resolve(response.data);
      }
      setTimeout(() => {
        errorGenerator(response);
      }, 300);
      return Promise.reject(response);
    },
    async doUpdateProfile(data) {
      const response = await api.updateProfile(transformToFormData(data));
      const success = getSuccess(response.data);
      if (response.ok && success) {
        await dispatch.auth.getMemberData();
        showToast('Update profil berhasil');
        goBack();
        return Promise.resolve(response.data);
      }
      errorGenerator(response, !success ? response.data?.data?.error : null);
      return Promise.reject(response);
    },
    async getMessageList(data) {
      const response = await api.messageList(transformToFormData(data));
      const success = getSuccess(response.data);
      if (response.ok && success) {
        if (data.message_type === 'private') {
          await dispatch.auth.setInboxList(response.data);
        } else {
          await dispatch.auth.setNewsList(response.data);
        }
        return Promise.resolve(response.data);
      }
      return Promise.reject(response);
    },
    async postReadMessage(data) {
      const response = await api.readMessage(transformToFormData(data));
      const success = getSuccess(response.data);
      if (response.ok && success) {
        return Promise.resolve(response.data);
      }
      errorGenerator(response);
      return Promise.reject(response);
    },
    async doLogout() {
      const response = await api.logout();
      const success = getSuccess(response.data);
      if (response.ok && success) {
        try {
          return new Promise(resolve => {
            dispatch.rootState.reset();
            goTo('SplashScreen');
            resolve();
          });
        } catch (e) {
          return Promise.reject(e.response.data);
        }
      }
      return new Promise(resolve => {
        dispatch.rootState.reset();
        goTo('SplashScreen');
        resolve();
      });
      // setTimeout(() => errorGenerator(response), 400);
      // return Promise.reject(response);
    },
  }),
};
