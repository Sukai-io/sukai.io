import { StyleSheet } from 'react-native'
import { Colors, Fonts, Metrics } from '../../themes'

export default StyleSheet.create({
  mainContent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: Colors.primary,
  },
  image: {
    width: 200,
    height: 200,
  },
  text: {
    color: Colors.snow,
    fontSize: Fonts.size.medium,
    textAlign: 'center',
    paddingHorizontal: Metrics.baseMargin,
  },
  title: {
    fontSize: Fonts.size.h4,
    color: Colors.snow,
    backgroundColor: 'transparent',
    textAlign: 'center',
    marginBottom: Metrics.baseMargin,
  },
})
