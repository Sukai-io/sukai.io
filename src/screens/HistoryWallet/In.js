import React, {useEffect, useCallback, useState} from 'react';
import {Text, View} from 'react-native';
import VerticalList from '../../components/list/VerticalList';
import {Col, Row, Container} from 'native-base';
import styles from './styles';
import {formattedDateTime} from '../../utils/TextUtils';
import {useDispatch, useSelector} from 'react-redux';
import Loading from '../../components/indicator/Loading';
import {ApplicationStyles} from '../../themes';
import colors from '../../themes/Colors';

const InHistory = () => {
  const [offset, setOffset] = useState(0);
  const [initialPage, setInitialPage] = useState(true);
  const histories = useSelector(({balance}) => balance.wallet_in_histories);
  const loading = useSelector(({loading}) => loading.effects.balance.getWalletHistory);
  const dispatch = useDispatch();
  const {getWalletHistory} = dispatch.balance;
  const handleLoadMore = useCallback(() => {
    setOffset(value => value + 10);
    setInitialPage(false);
  }, []);
  const handleRefresh = useCallback(() => {
    setOffset(0);
  }, [])
  useEffect(() => {
    getWalletHistory({limit: 10, offset, search_type: 'IN'});
  }, [offset]);
  return (
    <Container>
      {initialPage && loading ? (
        <Loading />
      ) : (
        <VerticalList
          data={histories}
          windowSize={13}
          maxToRenderPerBatch={6}
          updateCellsBatchingPeriod={30}
          onEndReached={handleLoadMore}
          withRefreshControl
          refreshing={loading && offset === 0}
          onRefresh={handleRefresh}
          renderItem={({item}) => (
            <Col style={styles.list}>
              <Row style={{justifyContent: 'space-between'}}>
                <View style={ApplicationStyles.flex}>
                  <Text style={styles.desc}>{item.detail}</Text>
                </View>
                <Text style={styles.amount}>{`+${item.amount}`}</Text>
              </Row>
              <Row style={{alignItems: 'center', flexDirection: 'row'}}>
                <View style={ApplicationStyles.flex}>
                  <Text style={styles.date}>{formattedDateTime(item.datecreated)}</Text>
                </View>
                <Text style={[styles.success, item.status !== 'CONFIRMED' && {color: colors.gray500}]}>
                  {item.status}
                </Text>
              </Row>
            </Col>
          )}
        />
      )}
    </Container>
  );
};

export default InHistory;
