import { StyleSheet } from 'react-native'
import {
  ApplicationStyles, Metrics, Colors, Fonts,
} from '../../themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
    paddingTop: Metrics.baseMargin,
    backgroundColor: Colors.gray100,
  },
  title: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.bold,
    color: Colors.gray700,
    marginBottom: Metrics.doubleBaseMargin,
  },
  categoryContainer: {
    ...ApplicationStyles.shadow,
    borderRadius: Metrics.doubleBaseMargin,
    paddingHorizontal: Metrics.smallMargin,
    paddingBottom: Metrics.doubleBaseMargin,
    marginBottom: Metrics.doubleBaseMargin,
    marginLeft: Metrics.baseMargin,
    marginRight: Metrics.baseMargin,
  },
  cardContainer: {
    ...ApplicationStyles.shadow,
    ...ApplicationStyles.flex,
    padding: Metrics.doubleBaseMargin,
    marginRight: 0,
    marginLeft: 0,
    marginTop: 0,
    marginBottom: Metrics.doubleBaseMargin,
  },
  categoryText: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.bold,
    color: Colors.gray700,
    padding: Metrics.doubleBaseMargin,
  },
  marginTop: {
    marginTop: Metrics.doubleBaseMargin,
  },
})
