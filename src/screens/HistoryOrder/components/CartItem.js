import React from 'react';
import {View} from 'react-native';
import {Text} from 'native-base';
import Horizontal from '../../../components/wrapper/Horizontal';
import styles from './CartItem.styles';
import FlatTouchables from '../../../components/buttons/FlatTouchables';
import {formattedDateTime} from '../../../utils/TextUtils';

const CartItem = ({onPress, isLast, dataSource}) => (
  <FlatTouchables onPress={onPress}>
    <View style={[styles.container, isLast && styles.isLast]}>
      <View style={styles.status}>
        <Text style={styles.statusText}>{dataSource?.checkout_status}</Text>
      </View>
      <Text style={styles.date}>{formattedDateTime(dataSource?.datecreated)}</Text>
      <Horizontal style={styles.secContainer}>
        <View style={styles.infoContainer}>
          <Text style={styles.title}>{dataSource?.checkout_code}</Text>
          <Text style={styles.qty}>{`${dataSource?.courier?.toUpperCase()} - ${dataSource?.courier_service}`}</Text>
          <Text style={styles.qty}>{`${dataSource?.products?.length} Produk`}</Text>
        </View>
      </Horizontal>
    </View>
  </FlatTouchables>
);

export default CartItem;
