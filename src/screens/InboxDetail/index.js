import React, {useEffect} from 'react';
import {Container, Text} from 'native-base';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import HeaderBack from '../../components/header/HeaderBack';
import styles from './styles';
import {formattedDateTime} from '../../utils/TextUtils';
import {useDispatch} from 'react-redux';

const InboxDetail = ({navigation}) => {
  const {
    auth: {postReadMessage, getMessageList},
  } = useDispatch();
  const data = navigation.getParam('data', null);
  useEffect(() => {
    if (data) {
      postReadMessage({message_id: data?.id}).then(() => {
        getMessageList({message_type: data?.message_type});
      });
    }
  }, []);
  return (
    <Container>
      <HeaderBack withoutRight />
      <KeyboardAwareScrollView style={styles.container}>
        <Text style={styles.date}>{formattedDateTime(data?.datecreated)}</Text>
        <Text style={styles.title}>{data?.title}</Text>
        <Text style={styles.desc}>{data?.message}</Text>
      </KeyboardAwareScrollView>
    </Container>
  );
};

export default InboxDetail;
