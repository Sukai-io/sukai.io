import React, {Component} from 'react';
import {connect} from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import {Container, Content, Text} from 'native-base';
import styles from './styles';
import HeaderBack from '../../components/header/HeaderBack';
import ProductItem from '../Categories/components/ProductItem';
import VerticalList from '../../components/list/VerticalList';

class OfficialStoreScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      offset: 0,
      initialPage: true,
    };
    this.handleLoadMore = this.handleLoadMore.bind(this);
  }

  componentDidMount() {
    const {getProduct} = this.props;
    const {offset} = this.state;
    getProduct({limit: 10, offset, type: 'os'});
  }

  handleLoadMore() {
    this.setState(
      prevState => ({
        offset: prevState.offset + 10,
        initialPage: false,
      }),
      () => {
        const {getProduct} = this.props;
        const {offset} = this.state;
        getProduct({limit: 10, offset, type: 'os'});
      },
    );
  }

  handleRefresh = () => {
    this.setState({offset: 0}, () => {
      const {getProduct} = this.props;
      getProduct({limit: 10, offset: 0, type: 'os'});
    });
  };

  render() {
    const {navigation, products, loading} = this.props;
    const {offset} = this.state;
    return (
      <Container style={styles.mainContainer}>
        <HeaderBack title="Official Store" />
        <Content style={styles.container} contentContainerStyle={{flex: 1}}>
          <VerticalList
            data={products}
            windowSize={13}
            maxToRenderPerBatch={6}
            updateCellsBatchingPeriod={30}
            withRefreshControl
            refreshing={loading && offset === 0}
            onRefresh={this.handleRefresh}
            customStyle={styles.cardContainer}
            renderItem={({item}) => (
              <ProductItem
                onPress={() =>
                  navigation.navigate('ProductDetailScreen', {
                    id: item.product_id,
                  })
                }
                name={item.product_name}
                price={item.product_price}
                uri={item.product_thumbnail_url}
                userName={item.seller}
              />
            )}
            numColumns={2}
            onEndReached={this.handleLoadMore}
            ListHeaderComponent={<Text style={styles.title}>Latest Product</Text>}
          />
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.loading.effects.product.getProductOfficial,
    products: state.product.product_official_list,
  };
};

const mapDispatchToProps = ({product}) => {
  return {
    getProduct: data => product.getProductOfficial(data),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(OfficialStoreScreen);
