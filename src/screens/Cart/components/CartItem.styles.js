import { StyleSheet } from 'react-native'
import {
  ApplicationStyles, Metrics, Fonts, Colors,
} from '../../../themes'

export default StyleSheet.create({
  touch: {
    ...ApplicationStyles.flex,
  },
  avatar: {
    width: 80,
    height: 80,
  },
  container: {
    ...ApplicationStyles.flex,
    backgroundColor: 'white',
    borderBottomColor: Colors.gray300,
    borderBottomWidth: 1,
  },
  subContainer: {
    ...ApplicationStyles.flex,
    padding: Metrics.doubleBaseMargin,
  },
  infoContainer: {
    ...ApplicationStyles.flex,
    marginLeft: Metrics.doubleBaseMargin,
  },
  title: {
    fontSize: Fonts.size.regular,
    color: Colors.gray800,
  },
  price: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.base,
    color: Colors.primary,
    marginTop: Metrics.baseMargin,
  },
  qty: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    color: Colors.primary,
  },
  deleteContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.cloud,
    padding: Metrics.doubleBaseMargin,
  },
  operatorIcon: {
    fontSize: Metrics.icons.small,
    color: Colors.primary,
  },
  disabledIcon: {
    fontSize: Metrics.icons.small,
    color: Colors.primary,
    opacity: 0.5,
  },
  deleteIcon: {
    fontSize: Metrics.icons.medium,
    color: Colors.gray600,
  },
  amount: {
    paddingVertical: Metrics.smallMargin,
    paddingHorizontal: Metrics.doubleBaseMargin,
    borderWidth: 1,
    borderColor: Colors.gray200,
    borderRadius: Metrics.buttonRadius,
  },
  buttonOperator: {
    paddingTop: 0,
    paddingBottom: 0,
    paddingLeft: 0,
    paddingRight: 0,
    width: 50,
    height: 30,
    alignSelf: 'center',
  },
  marginLeft: {
    marginLeft: Metrics.smallMargin,
  },
  personContainer: {
    ...ApplicationStyles.flexCenter,
    marginBottom: Metrics.smallMargin,
  },
  personIcon: {
    fontSize: Metrics.icons.tiny,
    color: Colors.gray400,
  },
  by: {
    fontSize: Fonts.size.small,
    color: Colors.gray800,
  },
})
