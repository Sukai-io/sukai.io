import { object as yupObject, string as yupString, number as yupNumber } from 'yup'

export default yupObject().shape({
  username: yupString().required(),
  amount: yupNumber()
    .required()
    .min(100000),
})
