import React, {Component} from 'react';
import {View, Switch, ActivityIndicator} from 'react-native';
import {connect} from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import {Container, Icon, Item, Input, Text} from 'native-base';
import {Button} from 'react-native-elements';
import {Dropdown} from 'react-native-material-dropdown';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {FormikProps} from 'formik';
import styles from './styles';
import HeaderBack from '../../components/header/HeaderBack';
import Horizontal from '../../components/wrapper/Horizontal';
import {ApplicationStyles, Colors, Fonts, Metrics} from '../../themes';
import VerticalList from '../../components/list/VerticalList';
import PaymentModal from '../Checkout/components/PaymentModal';
import ElecItem from '../Electricity/components/ElecItem';
import TotalItem from '../Electricity/components/TotalItem';
import {FormValues} from './types';
import {numeralFormatter} from '../../utils/TextUtils';
import Loader from '../../components/indicator/Loader';

type Props = FormikProps<FormValues> & {
  loading: Boolean,
  loadingCheckout: Boolean,
  remember: Boolean,
  providerList: Array<Object>,
  providerPriceList: Array<Object>,
  getProvider: Function,
  getPriceList: Function,
  doClear: Function,
  doSaveRemember: Function,
};

type State = {
  showPayment: Boolean,
};

class Content extends Component<Props, State> {
  state = {
    showPayment: false,
  };

  componentDidMount() {
    const {doClear, getProvider} = this.props;
    doClear();
    getProvider({type: 'etoll'});
  }

  handlePaymentVisibility = value => {
    this.setState({showPayment: value});
  };

  handleProviderValue = value => {
    const {setFieldValue, getPriceList} = this.props;
    setFieldValue('ppob_provider', value);
    getPriceList({provider: value});
  };

  render() {
    const {showPayment} = this.state;
    const {
      handleSubmit,
      setFieldTouched,
      setFieldValue,
      errors,
      touched,
      providerList,
      providerPriceList,
      values,
      remember,
      doSaveRemember,
      loading,
      loadingCheckout,
    } = this.props;
    const totalPrice =
      providerPriceList.length > 0
        ? providerPriceList[values.price_index].price
        : null;
    return (
      <Container style={styles.mainContainer}>
        <Loader loading={loadingCheckout} />
        <PaymentModal
          isVisible={showPayment}
          onBackButtonPress={() => this.handlePaymentVisibility(false)}
          onBackdropPress={() => this.handlePaymentVisibility(false)}
        />
        <HeaderBack withoutRight title="E-Money" />
        <KeyboardAwareScrollView style={styles.container}>
          <View style={styles.firstSubContainer}>
            <Dropdown
              containerStyle={styles.dropdownContainer}
              label="Tipe E-Money"
              data={providerList}
              dropdownOffset={{top: Metrics.baseMargin, left: 0}}
              itemTextStyle={styles.dropdownItem}
              itemColor={Colors.gray600}
              fontSize={Fonts.size.medium}
              textColor={Colors.primary}
              baseColor={Colors.gray600}
              onChangeText={this.handleProviderValue}
            />
            <Text style={styles.titleText}>Nomor Kartu</Text>
            <Horizontal style={ApplicationStyles.flexCenter}>
              <Icon
                name="wallet"
                type="MaterialCommunityIcons"
                style={styles.icon}
              />
              <Item style={styles.inputContainer}>
                <Input
                  keyboardType="number-pad"
                  onBlur={() => setFieldTouched('ppob_number')}
                  onChangeText={value => setFieldValue('ppob_number', value)}
                />
              </Item>
            </Horizontal>
            {errors.ppob_number && touched.ppob_number && (
              <Text style={styles.error}>{errors.ppob_number}</Text>
            )}
          </View>
          <View style={styles.secondSubContainer}>
            <Horizontal style={styles.thirdSubContainer}>
              <Text style={styles.description}>
                Simpan nomor ini untuk transaksi berikutnya
              </Text>
              <Switch
                value={remember}
                onValueChange={() => doSaveRemember('emoney')}
              />
            </Horizontal>
            {loading ? (
              <ActivityIndicator />
            ) : (
              <VerticalList
                data={providerPriceList}
                scrollEnabled={false}
                renderItem={({item, index}) => {
                  return (
                    <ElecItem
                      onPress={() => setFieldValue('price_index', index)}
                      price={item.name}
                      salePrice={item.price}
                      selected={values.price_index === index}
                    />
                  );
                }}
              />
            )}
          </View>
          {/* <NoteItem /> */}
          {totalPrice && <TotalItem total={numeralFormatter(totalPrice)} />}
          <Button
            loading={loadingCheckout}
            disabled={loadingCheckout}
            onPress={handleSubmit}
            raised
            containerStyle={styles.buttonContainer}
            buttonStyle={styles.button}
            title="BELI"
          />
        </KeyboardAwareScrollView>
      </Container>
    );
  }
}

const mapStateToProps = ({loading, ppob}) => {
  return {
    loading: loading.effects.ppob.getProviderPrice,
    loadingCheckout: loading.effects.ppob.doCheckout,
    remember: ppob.remember_emoney,
    providerList: ppob.provider_list,
    providerPriceList: ppob.provider_price_list,
  };
};

const mapDispatchToProps = ({ppob}) => {
  return {
    getProvider: data => ppob.getProvider(data),
    getPriceList: data => ppob.getProviderPrice(data),
    doClear: () => ppob.clear(),
    doSaveRemember: type => ppob.saveRemember(type),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Content);
