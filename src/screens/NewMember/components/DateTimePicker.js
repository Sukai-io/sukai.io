import React, {memo} from 'react';
import DateTimePickerModal, {DateTimePickerProps} from 'react-native-modal-datetime-picker';
import {Text} from 'react-native';
import {CardItem, Item, Label, Input} from 'native-base';
import styles from '../styles';
import {Colors, ApplicationStyles} from '../../../themes';

type Props = DateTimePickerProps & {
  label: string,
  error: string,
  date: string,
  showModal: Boolean,
  onShow: Function,
};

const DateTimePicker = ({label, error, date, value, showModal, onShow, ...otherProps}: Props) => (
  <>
    <CardItem style={{paddingBottom: 0, marginTop: 8}}>
      <Item onPress={onShow} stackedLabel style={ApplicationStyles.flex}>
        <Label style={styles.label}>{label}</Label>
        <Input disabled style={styles.input} placeholderTextColor={Colors.gray300} value={value || 'Pilih Tanggal'} />
      </Item>
    </CardItem>
    {error && <Text style={styles.error}>{error}</Text>}
    <DateTimePickerModal isVisible={showModal} date={date ? date : new Date()} {...otherProps} />
  </>
);

export default memo(DateTimePicker);
