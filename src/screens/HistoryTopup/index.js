/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
import React, {PureComponent} from 'react';
import {ActivityIndicator} from 'react-native';
import {connect} from 'react-redux';
import {Container, Content, Text, Grid, Row, Col} from 'native-base';
import styles from '../HistoryWallet/styles';
import VerticalList from '../../components/list/VerticalList';
import Loading from '../../components/indicator/Loading';
import {formattedDate, numeralFormatter, formattedTime} from '../../utils/TextUtils';
import {Colors} from '../../themes';
import HeaderBack from '../../components/header/HeaderBack';

type Props = {
  getTopupHistory: () => Promise,
  loading: Boolean,
  topupHistories: Array<Object>,
};

class HistoryTopup extends PureComponent<Props> {
  componentDidMount() {
    const {getTopupHistory} = this.props;
    getTopupHistory();
  }

  handleRefresh = () => {
    const {getTopupHistory} = this.props;
    getTopupHistory();
  }

  render() {
    const {loading, topupHistories} = this.props;
    return (
      <Container>
        <HeaderBack title="History Topup" withoutRight />
        <Content  
          style={{flex: 1}}
          contentContainerStyle={{flex: 1}} >
          <VerticalList
            data={topupHistories}
            withRefreshControl
            refreshing={loading}
            onRefresh={this.handleRefresh}
            renderItem={({item}) => {
              return (
                <Row style={styles.list}>
                  <Col size={1}>
                    <Text style={styles.date}>{`${formattedDate(item.datecreated)}\n${formattedTime(
                      item.datecreated,
                    )}`}</Text>
                  </Col>
                  <Col size={1}>
                    <Text style={styles.amount}>{numeralFormatter(parseInt(item.total, 10))}</Text>
                  </Col>
                  <Col size={1}>
                    <Text
                      style={[
                        styles.activity,
                        item.status === '1' && {color: Colors.green500},
                        item.status === '2' && {color: Colors.error},
                      ]}>
                      {item.status === '2' ? 'EXPIRED' : item.status === '1' ? 'SUCCESS' : 'PENDING'}
                    </Text>
                  </Col>
                </Row>
              );
            }}
          />
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = ({loading, balance}) => {
  return {
    topupHistories: balance.topup_histories,
    loading: loading.effects.balance.getTopupHistory,
  };
};

const mapDispatchToProps = ({balance}) => {
  return {
    getTopupHistory: data => balance.getTopupHistory(data),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HistoryTopup);
