import React, {useMemo, useRef, useCallback} from 'react';
import {connect} from 'react-redux';

import {View, Clipboard, TouchableOpacity} from 'react-native';
import {Container, CardItem, Body, Text} from 'native-base';
import {Card, Button} from 'react-native-elements';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import HeaderBack from '../../components/header/HeaderBack';
import {ApplicationStyles, Fonts, Colors} from '../../themes';
import Horizontal from '../../components/wrapper/Horizontal';
import styles from './styles';
import {formattedDateTime, numeralFormatter} from '../../utils/TextUtils';
import {pathOr} from 'ramda';
import CartItem from './components/CartItem';
import AlertPro from 'react-native-alert-pro';
import Loader from '../../components/indicator/Loader';
import {showToast} from '../../utils/ToastUtils';

const BuyerDetail = ({navigation, loading, doConfirm}) => {
  const alertRef = useRef(null);
  const detail = useMemo(() => navigation.getParam('detail', null));
  const totalProductPrice = useMemo(
    () =>
      pathOr([], ['products'], detail).reduce(
        (acc, item) => acc + parseInt(item.product_price, 10) * parseInt(item.product_qty, 10),
        0,
      ),
    [detail],
  );
  const totalProductAndShippingPrice = useMemo(() => parseInt(detail?.shipping_fee, 10) + totalProductPrice, [
    totalProductPrice,
    detail,
  ]);
  const handleCopyResi = useCallback(() => {
    if (detail?.no_resi) {
      Clipboard.setString(detail?.no_resi);
      showToast('Copy to Clipboard');
    }
  }, [detail]);
  const handleConfirm = useCallback(() => {
    alertRef.current.close();
    doConfirm({checkout_group_id: detail?.id, type: 'buyer'});
  }, [detail]);
  return (
    <Container>
      <HeaderBack title="Detail Order" withoutRight />
      <Loader loading={loading} />
      <AlertPro
        ref={alertRef}
        onConfirm={handleConfirm}
        onCancel={() => alertRef.current.close()}
        title="Konfirmasi Penerimaan Barang"
        message={`Konfirmasi ini ditujukan apabila barang sudah sampai ditempat Anda, yakin akan melakukan konfirmasi?`}
        textCancel="Batal"
        textConfirm="Ya"
        customStyles={{
          container: {
            width: 300,
            shadowColor: '#000000',
            shadowOpacity: 0.1,
            shadowRadius: 10,
          },
          title: {
            fontSize: Fonts.size.h6,
          },
          buttonCancel: {
            backgroundColor: Colors.gray400,
          },
          buttonConfirm: {
            backgroundColor: Colors.bloodOrange,
          },
        }}
      />
      <KeyboardAwareScrollView style={styles.container}>
        <View style={styles.status}>
          <Text style={styles.statusText}>{detail?.checkout_status}</Text>
        </View>
        <Card containerStyle={styles.cardContainer}>
          <CardItem>
            <Body>
              <Text style={styles.title}>Informasi Order</Text>
            </Body>
          </CardItem>
          <CardItem>
            <Body>
              <Text selectable>{detail?.checkout_code}</Text>
              <Text selectable note>
                {`Penjual: ${detail?.seller_name}`}
              </Text>
              <Text selectable note>
                {`Waktu Order: ${formattedDateTime(detail?.datecreated)}`}
              </Text>
              <Text selectable note>
                {`Kurir: ${detail?.courier?.toUpperCase()} - ${detail?.courier_service}`}
              </Text>
            </Body>
          </CardItem>
        </Card>
        <Card containerStyle={styles.cardContainer}>
          <CardItem>
            <Body>
              <Text style={styles.title}>Informasi Produk</Text>
            </Body>
          </CardItem>
          <CardItem>
            <Body>
              {pathOr([], ['products'], detail).map((item, index) => (
                <CartItem
                  key={index.toString()}
                  onPress={() =>
                    navigation.navigate('ProductDetailScreen', {
                      id: item.product_id,
                    })
                  }
                  dataSource={item}
                />
              ))}
            </Body>
          </CardItem>
        </Card>
        <Card containerStyle={styles.cardContainer}>
          <CardItem>
            <Body>
              <Text style={styles.title}>Lokasi Penjual</Text>
            </Body>
          </CardItem>
          <CardItem>
            <Body>
              <Text selectable>{detail?.seller_name}</Text>
              <Text selectable note>
                {detail?.seller_shipping_info?.address_location}
              </Text>
              <Text selectable note>
                {`${detail?.seller_shipping_info?.address_district}, ${detail?.seller_shipping_info?.address_city_name}, ${detail?.seller_shipping_info?.address_province_name}`}
              </Text>
              <Text selectable note>
                {detail?.seller_shipping_info?.address_zipcode}
              </Text>
            </Body>
          </CardItem>
        </Card>
        <Card containerStyle={styles.cardContainer}>
          <CardItem>
            <Body>
              <Horizontal style={ApplicationStyles.flexCenter}>
                <Text style={styles.totalLeft}>Biaya Pengiriman</Text>
                <Text style={styles.totalRight}>{numeralFormatter(detail?.shipping_fee)}</Text>
              </Horizontal>
              <Horizontal style={styles.grandContainer}>
                <Text style={styles.totalLeft}>Total Pembayaran</Text>
                <Text style={styles.grantTotal}>{numeralFormatter(totalProductAndShippingPrice)}</Text>
              </Horizontal>
            </Body>
          </CardItem>
        </Card>
        <Card containerStyle={styles.cardContainer}>
          <CardItem>
            <Body>
              <Horizontal style={ApplicationStyles.flexCenter}>
                <Text style={styles.totalLeft}>Nomor Resi</Text>
                <TouchableOpacity onPress={handleCopyResi} style={{flex: 1}}>
                  <Text selectable style={[styles.resi, !detail?.no_resi && {textDecorationLine: 'none'}]}>
                    {detail?.no_resi || '-'}
                  </Text>
                </TouchableOpacity>
              </Horizontal>
            </Body>
          </CardItem>
        </Card>
        {detail?.checkout_status?.toLowerCase() === 'dalam pengiriman' && (
          <Button
            onPress={() => alertRef.current.open()}
            raised
            containerStyle={styles.buttonContainer}
            buttonStyle={styles.button}
            title="KONFIRMASI"
          />
        )}
        {/* <Button
          raised
          containerStyle={[styles.buttonContainer, {marginBottom: Metrics.doubleBaseMargin}]}
          buttonStyle={styles.buttonDisable}
          title="BATALKAN"
        /> */}
      </KeyboardAwareScrollView>
    </Container>
  );
};

const mapStateToProps = ({loading}) => {
  return {
    loading: loading.effects.order.doConfirm,
  };
};

const mapDispatchToProps = ({order}) => {
  return {
    doConfirm: data => order.doConfirm(data),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BuyerDetail);