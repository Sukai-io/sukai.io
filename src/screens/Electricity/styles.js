import { StyleSheet } from 'react-native'
import {
  ApplicationStyles, Metrics, Colors, Fonts,
} from '../../themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    ...ApplicationStyles.flex,
    backgroundColor: Colors.gray100,
  },
  firstSubContainer: {
    ...ApplicationStyles.flex,
    paddingVertical: Metrics.doubleBaseMargin,
    paddingHorizontal: Metrics.section,
    backgroundColor: Colors.snow,
  },
  icon: {
    fontSize: Metrics.icons.medium,
    color: Colors.facebook,
  },
  inputContainer: {
    ...ApplicationStyles.flex,
    marginLeft: Metrics.baseMargin,
  },
  secondSubContainer: {
    ...ApplicationStyles.flex,
    paddingVertical: Metrics.doubleBaseMargin,
    paddingHorizontal: Metrics.section,
  },
  description: {
    ...ApplicationStyles.flex,
    ...Fonts.style.description,
    fontSize: Fonts.size.small,
  },
  thirdSubContainer: {
    ...ApplicationStyles.flexCenter,
    marginBottom: Metrics.section,
  },
  buttonContainer: {
    marginTop: Metrics.doubleBaseMargin,
    marginHorizontal: Metrics.section,
    marginBottom: Metrics.doubleBaseMargin,
  },
  button: {
    backgroundColor: Colors.bloodOrange,
  },
})
