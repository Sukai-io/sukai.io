import React from 'react';
import {Grid, Col, Item, Label, Input, NativeBase} from 'native-base';
import {Button} from 'react-native-elements';
import {Colors, Metrics} from '../../../themes';
import styles from './InputWithButton.styles';

type Props = NativeBase.Input & {
  onPress: Function,
  titleButton: String,
  disabledButton: Boolean,
  loadingButton: Boolean,
};

const InputWithButton = (props: Props) => {
  const {onPress, titleButton, loadingButton, disabledButton} = props;
  return (
    <Grid style={styles.withdrawContainer}>
      <Col size={60}>
        <Item stackedLabel>
          <Label style={styles.title}>Amount *</Label>
          <Input
            placeholder="Jumlah Transfer"
            placeholderTextColor={Colors.gray400}
            keyboardType="number-pad"
            style={styles.input}
            {...props}
          />
        </Item>
      </Col>
      <Col size={40}>
        <Button
          onPress={onPress}
          raised
          disabled={disabledButton}
          loading={loadingButton}
          containerStyle={styles.buttonContainer}
          buttonStyle={styles.button}
          titleStyle={styles.buttonTitle}
          icon={{
            name: 'ios-send',
            type: 'ionicon',
            color: 'white',
            size: Metrics.icons.tiny,
          }}
          title={titleButton}
        />
      </Col>
    </Grid>
  );
};

export default InputWithButton;
