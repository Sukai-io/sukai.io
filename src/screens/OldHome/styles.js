import { StyleSheet } from 'react-native'
import {
  ApplicationStyles, Metrics, Colors, Fonts,
} from '../../themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  avatarContainer: {
    ...ApplicationStyles.flexCenter,
    padding: Metrics.doubleBaseMargin,
  },
  avatar: {
    backgroundColor: Colors.primary,
  },
  marginLeft: {
    marginLeft: Metrics.doubleBaseMargin,
  },
  avatarText: {
    ...Fonts.style.description,
  },
  avatarTextBold: {
    ...Fonts.style.description,
    fontFamily: Fonts.type.bold,
    color: Colors.gray800,
  },
  avatarTextBoldPrimary: {
    ...Fonts.style.description,
    fontFamily: Fonts.type.bold,
    color: Colors.primary,
  },
  subContainer: {
    padding: Metrics.baseMargin,
  },
  colContainer: {
    marginHorizontal: Metrics.smallMargin,
  },
  categoryContainer: {
    ...ApplicationStyles.shadow,
    borderRadius: Metrics.doubleBaseMargin,
    paddingHorizontal: Metrics.smallMargin,
    paddingBottom: Metrics.doubleBaseMargin,
    marginBottom: Metrics.doubleBaseMargin,
  },
  categoryText: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.bold,
    color: Colors.gray700,
    padding: Metrics.doubleBaseMargin,
  },
  marginTop: {
    marginTop: Metrics.doubleBaseMargin,
  },
})
