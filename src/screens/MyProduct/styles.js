import {StyleSheet} from 'react-native';
import {ApplicationStyles, Metrics, Colors, Fonts} from '../../themes';

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
  },
  cardContainer: {
    ...ApplicationStyles.shadow,
    ...ApplicationStyles.flex,
    backgroundColor: 'white',
    padding: Metrics.smallMargin,
    marginRight: 0,
    marginLeft: 0,
    marginTop: 0,
    marginBottom: Metrics.doubleBaseMargin,
  },
  empty: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    color: Colors.gray700,
    alignSelf: 'center',
    padding: Metrics.doubleSection,
  },
  title: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.bold,
    color: Colors.gray700,
    marginBottom: 0,
  },
  tableContainer: {
    flex: 1,
    padding: 16,
    paddingTop: 30,
    backgroundColor: '#fff',
  },
  header: {height: 50, backgroundColor: Colors.bloodOrange},
  headerText: {
    textAlign: 'center',
    fontFamily: Fonts.type.bold,
    fontSize: Fonts.size.medium,
    color: 'white',
  },
  text: {
    textAlign: 'center',
    fontFamily: Fonts.type.base,
    fontSize: Fonts.size.medium,
  },
  dataWrapper: {marginTop: -1, flex: 1},
  row: {height: 40, backgroundColor: '#E7E6E1'},
});
