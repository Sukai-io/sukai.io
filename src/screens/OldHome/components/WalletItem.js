import React, { useContext } from 'react'
import { ActivityIndicator, Image } from 'react-native'
import { withNavigation } from 'react-navigation'

import { Card, Text } from 'native-base'
import Horizontal from '../../../components/wrapper/Horizontal'
import styles from './WalletItem.styles'
import FlatTouchables from '../../../components/buttons/FlatTouchables'
import { LocalizationContext } from '../../../localization'

type Props = {
  navigation: any,
  fetching: Boolean,
  balance: String,
  onReceive: Function
}

const WalletItem = ({
 navigation, fetching, balance, onReceive 
}: Props) => {
  const {translations} = useContext(LocalizationContext)
  return (
    <Card style={styles.container}>
      <Horizontal style={styles.subContainer}>
        <Text style={styles.amountText}>{translations.TITLE_WALLET}</Text>
        {fetching ? <ActivityIndicator /> : <Text style={styles.amountText}>{balance}</Text>}
      </Horizontal>
      <Horizontal style={styles.sub2Container}>
        <FlatTouchables
          style={styles.sub3Container}
          onPress={() => navigation.navigate('TransferScreen')}
        >
          <Image style={styles.icon} source={require('../../../images/Icons/ic_upload.png')} />
          <Text style={styles.title}>{translations.TITLE_TRANSFER}</Text>
        </FlatTouchables>
        <FlatTouchables style={styles.sub3Container} onPress={onReceive}>
          <Image style={styles.icon} source={require('../../../images/Icons/ic_download.png')} />
          <Text style={styles.title}>{translations.TITLE_RECEIVED}</Text>
        </FlatTouchables>
        <FlatTouchables
          style={styles.sub3Container}
          onPress={() => navigation.navigate('WithdrawScreen')}
        >
          <Image style={styles.icon} source={require('../../../images/Icons/ic_hand.png')} />
          <Text style={styles.title}>{translations.TITLE_WITHDRAW}</Text>
        </FlatTouchables>
      </Horizontal>
    </Card>
  )
}

export default withNavigation(WalletItem)
