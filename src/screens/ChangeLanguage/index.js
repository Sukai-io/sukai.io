/* eslint-disable react/prefer-stateless-function */
import React, {useContext} from 'react';

// Styles
import {Container, Text, ListItem, Left, Right, Icon} from 'native-base';
import styles from './styles';
import HeaderBack from '../../components/header/HeaderBack';
import VerticalList from '../../components/list/VerticalList';
import {LocalizationContext} from '../../localization';

const languages = [{title: 'English', value: 'en'}];

const ChangeLanguageScreen = () => {
  const {appLanguage, setAppLanguage} = useContext(LocalizationContext);
  return (
    <Container style={styles.mainContainer}>
      <HeaderBack withoutRight title="Partnership" />
      <VerticalList
        style={styles.container}
        data={languages}
        renderItem={({item}) => {
          return (
            <ListItem onPress={() => setAppLanguage(item.value)} selected={appLanguage === item.value}>
              <Left>
                <Text>{item.title}</Text>
              </Left>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
          );
        }}
      />
    </Container>
  );
};

export default ChangeLanguageScreen;
