import React from 'react';
import {CardItem, Body, Text, Left, Icon} from 'native-base';
import FastImage from 'react-native-fast-image';
import {Card} from 'react-native-elements';
import {ApplicationStyles} from '../../../themes';
import FlatTouchables from '../../../components/buttons/FlatTouchables';
import styles from './ProductItem.styles';
import {normalizedSource} from '../../../utils/ImageUtils';

type Props = {
  onPress: Function,
  uri: String,
  name: String,
  price: String,
  userName: String,
  userImg: String,
};

const ProductItem = ({onPress, name, price, uri, userName}: Props) => (
  <FlatTouchables style={ApplicationStyles.flex} onPress={onPress}>
    <Card containerStyle={[styles.cardContainer]}>
      <FastImage
        source={uri && normalizedSource({uri})}
        resizeMode="contain"
        style={{height: 200, ...ApplicationStyles.flex}}
      />
      <CardItem bordered>
        <Body>
          <Text numberOfLines={1} note>
            {name}
          </Text>
          <Text>{price}</Text>
        </Body>
      </CardItem>
      <CardItem>
        <Left>
          {/* <FastImage
            square
            style={{
              width: Metrics.images.small,
              height: Metrics.images.small,
              borderRadius: Metrics.images.small / 2,
            }}
            source={userImg && normalizedSource({uri: userImg})}
          /> */}
          <Icon name="person" style={styles.icon} />
          <Body>
            <Text numberOfLines={1} note>
              {userName}
            </Text>
          </Body>
        </Left>
      </CardItem>
    </Card>
  </FlatTouchables>
);

export default ProductItem;
