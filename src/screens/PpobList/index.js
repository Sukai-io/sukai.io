/* eslint-disable no-unused-vars */
import React, { Component } from 'react'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import {
  Container,
  Content,
  Icon,
  Text,
  ListItem,
  Left,
  Button,
  Body,
  Card,
  Grid,
  Row,
  Col,
} from 'native-base'
import styles from './styles'
import HeaderBack from '../../components/header/HeaderBack'
import { Colors } from '../../themes'
import CategoryItem from './components/CategoryItem'

type Props = {
  navigation: any,
}

class PpobListScreen extends Component<Props> {
  constructor(props) {
    super(props)
    const { navigation } = props
    this.categories = [
      {
        name: 'wifi',
        isFirst: true,
        title: 'Pulsa',
        onPress: () => navigation.navigate('PulsaScreen'),
      },
      {
        name: 'wallet',
        isFirst: false,
        title: 'E-Money',
        onPress: () => navigation.navigate('EMoneyScreen'),
      },
      {
        name: 'waves',
        isFirst: true,
        title: 'Leasing',
        onPress: () => navigation.navigate('LeasingScreen'),
      },
      {
        name: 'flash',
        isFirst: false,
        title: 'Voucher Listrik',
        onPress: () => navigation.navigate('ElectricityScreen'),
      },
    ]
  }

  render() {
    const { navigation } = this.props
    return (
      <Container style={styles.mainContainer}>
        <HeaderBack withoutRight title="PPOB" />
        <Content style={styles.container}>
          <Card style={styles.categoryContainer}>
            <Text style={styles.categoryText}>PPOB List</Text>
            <Grid>
              {this.categories.map((item, index) => {
                return (
                  <Col key={index.toString()}>
                    <CategoryItem
                      onPress={item.onPress}
                      isFirst={item.isFirst}
                      name={item.title}
                      icon={item.name}
                    />
                  </Col>
                )
              })}
            </Grid>
          </Card>
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {}
}

const mapDispatchToProps = (dispatch) => {
  return {}
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PpobListScreen)
