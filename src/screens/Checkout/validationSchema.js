import {object as yupObject, string as yupString, array as yupArray} from 'yup';

export default yupObject().shape({
  product: yupArray().of(
    yupObject().shape({
      courier: yupString().required(),
      service: yupString().required(),
    }),
  ),
});
