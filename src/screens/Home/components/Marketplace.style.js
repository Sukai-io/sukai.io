import {StyleSheet} from 'react-native';
import {ApplicationStyles, Metrics, Colors, Fonts} from '../../../themes';

export default StyleSheet.create({
  headerContainer: {
    ...ApplicationStyles.rowCenterBetween,
    paddingHorizontal: Metrics.baseMargin,
    marginVertical: Metrics.doubleBaseMargin,
    alignItems: 'center',
  },
  textFeature: {
    fontFamily: Fonts.type.bold,
    color: Colors.facebook,
  },
  textShowAll: {
    fontFamily: Fonts.type.base,
    fontSize: Fonts.size.small,
    color: Colors.gray700,
  },
  image: {width: '100%', height: 150},
  dot: {
    width: 8,
    height: 8,
    borderRadius: 5,
    marginHorizontal: 0,
    marginVertical: 0,
    backgroundColor: Colors.primary,
    elevation: 2,
  },
  slideContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: Metrics.doubleBaseMargin,
  },
  paginationContainer: {justifyContent: 'center', paddingVertical: 16},
});
